(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

unit umlcsxmlpaneltreeviews;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlctreecntrs,
  umlcmsgtypes,
  umlcmsgtreecntrs,
  umlcguidstrs,
  umlcpaneltreeviews,
  umlcmsgpaneltreeviews,
  umlcxmlfileansisymbols,
  umlcxmlfiletreenodetokens,
  umlcsxmltreecntrs,
  dummy;

type

(* TCustomUMLCSXMLPanelTreeView *)

  TCustomUMLCSXMLPanelTreeView = class(TCustomUMLCMsgPanelTreeView)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    procedure InsertNode
      (const AContainerParentNode: TUMLCContainerTreeNode); override;

    procedure UpdateStateUponToken
      (var AContainerNode: TUMLCSXMLTreeNode);

    procedure TreeNodeAfterInsertHandler
      (const AMsgRec: TUMLCMessageParamsRecord); override;

    procedure TreeNodeAfterChangeSymbol
      (const AMsgRec: TUMLCMessageParamsRecord);
    procedure TreeNodeAfterChangeTextValue
      (const AMsgRec: TUMLCMessageParamsRecord);
    procedure TreeNodeAfterChangeTreeToken
      (const AMsgRec: TUMLCMessageParamsRecord);

    procedure AssignHandlers(); override;
  public
    (* Public declarations *)
  end;


implementation

(* TCustomUMLCSXMLPanelTreeView *)

procedure TCustomUMLCSXMLPanelTreeView.InsertNode
  (const AContainerParentNode: TUMLCContainerTreeNode);
var ATreeviewParentNode: TUMLCMsgPanelTreeViewNode;
begin
  // find the node from treeview that references the given parent node,
  // from the container
  ATreeviewParentNode := NodeOf(AContainerParentNode);
  if (ATreeviewParentNode <> nil) then
  begin
    // remove all subitems
    ATreeviewParentNode.Empty();

    // refresh subitems
    //ATreeviewParentNode.InternalExplore();

    //ATreeviewParentNode.Hide();

    ATreeviewParentNode.UpdateExpand();
  end;
end;

procedure TCustomUMLCSXMLPanelTreeView.UpdateStateUponToken
  (var AContainerNode: TUMLCSXMLTreeNode);
var ATreeviewNode: TUMLCMsgPanelTreeViewNode;
begin
  ATreeviewNode := Self.NodeOf(AContainerNode);
  if (ATreeviewNode <> nil) then
  begin
    case (AContainerNode.TreeToken) of
      UMLCxmlfiletreenodetokens.xmlfiletrntkDocument:
        begin
          Self.DoNothing();
        end;

      UMLCxmlfiletreenodetokens.xmlfiletrntkEoF:
        begin
          Self.DoNothing();
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkEoPg:
        begin
          Self.DoNothing();
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkEoLn:
        begin
          Self.DoNothing();
        end;

      UMLCxmlfiletreenodetokens.xmlfiletrntkTab:
        begin
          Self.DoNothing();
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkSpace:
        begin
          Self.DoNothing();
        end;

      UMLCxmlfiletreenodetokens.xmlfiletrntkBlock:
        begin
          Self.DoNothing();
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkSingle:
        begin
          // force change to "empty" status
          ATreeviewNode.UpdateExpand();
          ATreeviewNode.Explore();
        end;

      UMLCxmlfiletreenodetokens.xmlfiletrntkComment:
        begin
          Self.DoNothing();
        end;
      UMLCxmlfiletreenodetokens.xmlfiletrntkEncoding:
        begin
          Self.DoNothing();
        end;

      UMLCxmlfiletreenodetokens.xmlfiletrntkText:
        begin
          Self.DoNothing();
        end;

      else
        begin
          Self.DoNothing();
        end;
    end;
  end;
end;

procedure TCustomUMLCSXMLPanelTreeView.TreeNodeAfterInsertHandler
  (const AMsgRec: TUMLCMessageParamsRecord);
var AContainerParentNode, AContainerNewNode: TUMLCSXMLTreeNode;
begin
  AContainerParentNode := TUMLCSXMLTreeNode(AMsgRec.Sender);
  AContainerNewNode    := TUMLCSXMLTreeNode(AMsgRec.Param);
  if (AContainerNewNode <> nil) then
  begin
    if (AContainerNewNode.IsRoot()) then
    begin
      InsertRootNode(AContainerNewNode);
    end else
    begin
      InsertNode(AContainerParentNode);
    end;
  end;
end;

procedure TCustomUMLCSXMLPanelTreeView.TreeNodeAfterChangeSymbol
  (const AMsgRec: TUMLCMessageParamsRecord);
var AContainerNode: TUMLCSXMLTreeNode;
begin
  AContainerNode := TUMLCSXMLTreeNode(AMsgRec.Sender);
  UpdateStateUponToken(AContainerNode);
end;

procedure TCustomUMLCSXMLPanelTreeView.TreeNodeAfterChangeTextValue
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  //
end;

procedure TCustomUMLCSXMLPanelTreeView.TreeNodeAfterChangeTreeToken
  (const AMsgRec: TUMLCMessageParamsRecord);
var AContainerNode: TUMLCSXMLTreeNode;
begin
  AContainerNode := TUMLCSXMLTreeNode(AMsgRec.Sender);
  UpdateStateUponToken(AContainerNode);
end;

procedure TCustomUMLCSXMLPanelTreeView.AssignHandlers();
var AMsgID: TGUID;
begin
  // --> keep previous message-handlers
  inherited AssignHandlers();

  // --> declare additional message-handlers

  UMLCguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeSymbol, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeAfterChangeSymbol);

  UMLCguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeTextValue, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeAfterChangeTextValue);

  UMLCguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeTreeToken, AMsgID);
  FMsgHandlerList.Insert
    (AMsgID, {$IFDEF FPC}@{$ENDIF}TreeNodeAfterChangeTreeToken);
end;

end.

