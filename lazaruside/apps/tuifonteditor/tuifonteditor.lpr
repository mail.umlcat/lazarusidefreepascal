program tuifonteditor;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, ufrmmain, ufrmfontfolder, ufrmfontglyph, doscpifont, fonts, matrices,
  tuifonts, ufrmfontcodepage, udmbuttons, ufrmmain_res, ufrmfontfolder_res,
  ufrmfontresolution;

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(TdmButtons, dmButtons);
  Application.CreateForm(Tfrmmain, frmmain);
  Application.Run;
end.

