{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit dsgumlcstreams60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcstreamsregister, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('dsgumlcstreams60', @Register);
end.
