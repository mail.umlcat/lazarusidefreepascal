(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcrecrefstreams;

interface

uses
  SysUtils, Classes,
  umlcStreams, umlcRecStreams, umlcRefStreams,
  dummy;

type

(* TCustomUMLCRecordReferenceStream *)

  TCustomUMLCRecordReferenceStream = class(TCustomUMLCReferenceStream)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function RecordStream: TCustomUMLCRecordStream;

    function GetRecord(const Buffer: pointer): Boolean; virtual;
    function PutRecord(const Buffer: pointer): Boolean; virtual;

    function ReadRecord(const Buffer: pointer): Boolean; virtual;
    function WriteRecord(const Buffer: pointer): Boolean; virtual;

    procedure PrevRecord;
    procedure NextRecord;
  public
    (* public declarations *)

    function IsEoF: Boolean; virtual;
  end;

implementation

(* TCustomUMLCRecordReferenceStream *)

function TCustomUMLCRecordReferenceStream.RecordStream: TCustomUMLCRecordStream;
begin
  Result := (FReference as TCustomUMLCRecordStream);
end;

function TCustomUMLCRecordReferenceStream.GetRecord(const Buffer: pointer): Boolean;
begin
  Result := (FReference <> nil);
  if (Result)
    then Result := RecordStream.GetRecord(Buffer);
end;

function TCustomUMLCRecordReferenceStream.PutRecord(const Buffer: pointer): Boolean;
begin
  Result := (FReference <> nil);
  if (Result)
    then Result := RecordStream.PutRecord(Buffer);
end;

function TCustomUMLCRecordReferenceStream.ReadRecord(const Buffer: pointer): Boolean;
begin
  Result := (FReference <> nil);
  if (Result)
    then Result := RecordStream.ReadRecord(Buffer);
end;

function TCustomUMLCRecordReferenceStream.WriteRecord(const Buffer: pointer): Boolean;
begin
  Result := Assigned(FReference);
  if (Result)
    then Result := RecordStream.WriteRecord(Buffer);
end;

procedure TCustomUMLCRecordReferenceStream.PrevRecord;
begin
  if (FReference <> nil)
    then RecordStream.PrevRecord;
end;

procedure TCustomUMLCRecordReferenceStream.NextRecord;
begin
  if (FReference <> nil)
    then RecordStream.NextRecord;
end;

function TCustomUMLCRecordReferenceStream.IsEoF: Boolean;
begin
  Result := (FReference <> nil);
  if (Result)
    then Result := RecordStream.IsEoF;
end;

end.
