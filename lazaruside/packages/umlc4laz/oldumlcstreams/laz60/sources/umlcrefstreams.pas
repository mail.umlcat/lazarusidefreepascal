(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcrefstreams;

interface

uses
  //Windows,
  SysUtils, Classes,
  umlcStreams,
  dummy;

type

(* TCustomUMLCReferenceStream *)

  TCustomUMLCReferenceStream = class(TCustomUMLCStream)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FReference: TCustomUMLCStream;

    function getReference: TCustomUMLCStream; virtual;

    procedure setReference(const Value: TCustomUMLCStream); virtual;

    procedure Notification
      (AComponent: TComponent; Operation: TOperation); override;
  public
    (* public declarations *)

    function IsInput: Boolean; override;
    function IsOutput: Boolean; override;

    function IsConnected: Boolean; override;

    function Connect: Boolean; override;
    function Disconnect: Boolean; override;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property Reference: TCustomUMLCStream
      read getReference write setReference;
  end;

implementation

(* TCustomUMLCReferenceStream *)

function TCustomUMLCReferenceStream.getReference: TCustomUMLCStream;
begin
  Result := FReference;
end;

procedure TCustomUMLCReferenceStream.setReference(const Value: TCustomUMLCStream);
begin
  if (Value <> FReference) then
  begin
     FReference := Value;
  end;
end;

function TCustomUMLCReferenceStream.IsInput(): Boolean;
begin
  Result := (FReference <> nil);
  if (Result) then
  begin
    Result := FReference.IsInput();
  end;
end;

function TCustomUMLCReferenceStream.IsOutput(): Boolean;
begin
  Result := (FReference <> nil);
  if (Result) then
  begin
    Result := FReference.IsOutput();
  end;
end;

function TCustomUMLCReferenceStream.IsConnected(): Boolean;
begin
  Result := (FReference <> nil);
  if (Result) then
  begin
    Result := FReference.IsConnected();
  end;
end;

function TCustomUMLCReferenceStream.Connect(): Boolean;
begin
  Result := (FReference <> nil);
  if (Result) then
  begin
    Result := FReference.Connect();
  end;
end;

function TCustomUMLCReferenceStream.Disconnect(): Boolean;
begin
  Result := (FReference <> nil);
  if (Result) then
  begin
    Result := FReference.Disconnect();
  end;
end;

procedure TCustomUMLCReferenceStream.Notification
  (AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then
  begin
    if (FReference = AComponent)
      then FReference := nil;
  end;
end;

constructor TCustomUMLCReferenceStream.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FReference := nil;
end;

destructor TCustomUMLCReferenceStream.Destroy;
begin
  FReference := nil;
  inherited Destroy;
end;

end.
 
