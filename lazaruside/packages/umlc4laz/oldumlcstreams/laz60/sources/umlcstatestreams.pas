(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstatestreams;

interface
uses
  SysUtils, Classes,
  umlcStreams, umlcRecStreams,
  dummy;

type

(* TStreamStates *)

  TStreamStates = (ssClosed, ssReset, ssRewrite, ssAppend);

(* TCustomUMLCStateStream *)

  TCustomUMLCStateStream = class(TCustomUMLCRecordStream)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FState: TStreamStates;
  public
    (* public declarations *)

    function IsInput: Boolean; override;
    function IsOutput: Boolean; override;
    function IsConnected: Boolean; override;

    function Connect: Boolean; override;
    function Disconnect: Boolean; override;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property State: TStreamStates
      read FState write FState;
  end;

implementation

(* TCustomUMLCStateStream *)

function TCustomUMLCStateStream.IsInput: Boolean;
begin
  Result := (FState <> ssClosed);
end;

function TCustomUMLCStateStream.IsOutput: Boolean;
begin
  Result := (FState <> ssClosed);
end;

function TCustomUMLCStateStream.IsConnected: Boolean;
begin
  Result := (FState <> ssClosed);
end;

function TCustomUMLCStateStream.Connect: Boolean;
begin
  FState := ssReset;
  Result := TRUE;
end;

function TCustomUMLCStateStream.Disconnect: Boolean;
begin
  FState := ssClosed;
  Result := TRUE;  
end;

constructor TCustomUMLCStateStream.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FState := ssClosed;
end;

destructor TCustomUMLCStateStream.Destroy;
begin
  FState := ssClosed;
  inherited Destroy;
end;

end.
 
