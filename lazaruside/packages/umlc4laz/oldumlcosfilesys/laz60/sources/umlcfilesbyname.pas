(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfilesbyname;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  windows;

  function FileNameFound(const FullPath: string): Boolean;
  function FileNameFoundSens(const FullPath: string): Boolean;

  function FileNameSize(const FullPath: string): Int64;

  procedure FileNameRename(const SourceFullPath, DestPlainFilename: string);
  procedure FileNameCopy(const SourceFullPath, DestFullPath: string);
  procedure FileNameMove(const SourceFullPath, DestFullPath: string);
  procedure FileNameRemove(const FullPath: string);

  function FileNameRandomByPath( const APath, AExt: string ): string;

implementation


//function FileNameFound(const FullPath: string): Boolean;
//const
//  INVALID_FILE_ATTRIBUTES = DWORD(-1);
//  CNotExistsErrors = [ERROR_FILE_NOT_FOUND, ERROR_PATH_NOT_FOUND, ERROR_INVALID_NAME];
//var
//  Attr: DWORD;
//  FileName: UnicodeString;
//begin
//  FileName := UnicodeString(FullPath);
//  Attr := GetFileAttributesW(PWideChar(FileName));
//  if Attr = INVALID_FILE_ATTRIBUTES then
//    Result := not (GetLastError in CNotExistsErrors)
//  else
//    Result := (Attr and FILE_ATTRIBUTE_DIRECTORY) <> 0;
//end;

//function oldFileNameFound(const FullPath: string): Boolean;
//// ## hack {
////var Handle: THandle;
//// ## hack }
//begin
//  (* out *) Result := false;
//
//  // bug:
//  Result := SysUtils.FileExists(FullPath);
//
//  // ## hack {
//  //Handle := FileOpen(FullPath, fmOpenRead);
//  //if (Handle <> 0) then
//  //begin
//  //  FileClose(Handle);
//  //end;
//  // ## hack }
//end;

function FileNameFound(const FullPath: string): Boolean;
var FileName: UnicodeString;

  function FoundByEnum: Boolean;
  var
    R: TWIN32FINDDATAW;
    H: THandle;
  begin
    H := FindFirstFileExW(PWideChar(FileName), FindExInfoBasic, @R,
      FindExSearchNameMatch, nil, 0); // It's faster, than FindFirstFile
    Result := H <> INVALID_HANDLE_VALUE;
    if Result then
    begin
      FindClose(H);
      Result := (R.dwFileAttributes and FILE_ATTRIBUTE_DIRECTORY) = 0;
    end;
  end;

const
  INVALID_FILE_ATTRIBUTES = DWORD(-1);
  CNotExistsErrors = [
    ERROR_FILE_NOT_FOUND,
    ERROR_PATH_NOT_FOUND,
    ERROR_INVALID_NAME,    // Protects from names in the form of masks like '*'
    ERROR_INVALID_DRIVE,
    ERROR_NOT_READY,
    ERROR_INVALID_PARAMETER,
    ERROR_BAD_PATHNAME,
    ERROR_BAD_NETPATH];
var
  Attr: DWORD;
begin
  FileName := UnicodeString(FullPath);
  Attr := GetFileAttributesW(PWideChar(FileName));
  if Attr = INVALID_FILE_ATTRIBUTES then
    Result := not (GetLastError in CNotExistsErrors) and FoundByEnum()
  else
    Result := (Attr and FILE_ATTRIBUTE_DIRECTORY) <> 0;
end;

function FileNameFoundSens(const FullPath: string): Boolean;
begin
  (* out *) Result := false;

  Result := SysUtils.FileExists(FullPath);
end;

function FileNameSize(const FullPath: string): Int64;
{$IFDEF DELPHI}
var F: File;
{$ENDIF}
{$IFDEF FPC}
var F: File of char; AChar: char;
{$ENDIF}
begin
  (* out *) Result := 0;

  System.Assign(F, FullPath);
  System.Reset(F);

  if (InOutRes = 0) then
  begin
    {$IFDEF DELPHI}
    Result := System.FileSize(F);
    {$ENDIF}
    {$IFDEF FPC}
    while (not EoF(F)) do
    begin
      System.Read(F, AChar);
      Inc(Result);
    end;
    {$ENDIF}
  end;

  System.Close(F);
end;

procedure FileNameRename(const SourceFullPath, DestPlainFilename: string);
//var F: File;
begin
  SysUtils.RenameFile(SourceFullPath, DestPlainFilename);

  (*
  System.Assign(F, SourceFullPath);
  System.Reset(F);

  // to-do: fix path in "DestPlainFilename"

  if (InOutRes = 0) then
  begin
    System.Rename( ** var ** F, DestPlainFilename);
  end;

  System.Close(F);
  *)
end;

procedure FileNameCopy(const SourceFullPath, DestFullPath: string);
var CanCopy: Boolean;
begin
  CanCopy :=
    (FileNameFound(SourceFullPath) and (not FileNameFound(DestFullPath)));
  if (CanCopy) then
  begin
    // ...

    //SysUtils.DeleteFile(Filename);
    //setIOResult(ioOK);
  end;
end;

procedure FileNameMove(const SourceFullPath, DestFullPath: string);
begin
  if (FileNameFound(SourceFullPath)) then
  begin
    //SysUtils.DeleteFile(SourceFullPath);
    //setIOResult(ioOK);
  end;
end;

procedure FileNameRemove(const FullPath: string);
begin
  if (FileNameFound(FullPath)) then
  begin
    SysUtils.DeleteFile(FullPath);
    //setIOResult(ioOK);
  end; // else setIOResult(ioFileNotFound);

  // Goal: The "FileNameRemove" function erases the file named by "Filename"
  // from the disk.

  // Objetivo: La funcion "DeleteFile" elimina el archivo llamado "Filename"
  // en el disco.
end;

function InternalRandomRange( AMin, AMax: Byte ): Byte;
begin
  Result := Random(AMax - AMin) + AMin;
  // Objetivo: Obtener un numero aleatorio en el rango indicado.
end;

function InternalRandomString( ACount: Byte ): string;
var i: byte;
begin
  Result := StringOfChar( #32, ACount );
  for i := 1 to ACount do
    Result[i] :=  Chr( InternalRandomRange( 65, 90 ) );
  // Objetivo: Obtener una cadena de caracteres aleatorios,
  // con la longuitud indicada.
end;

function FileNameRandomByPath( const APath, AExt: string ): string;
var AFileName, AFullPath, ASepExt: string; WasFound: Boolean;
begin
  if (AExt[1] <> '.') then
  begin
    ASepExt := '.' + AExt;
  end else
  begin
    ASepExt := AExt;
  end;

  repeat
    AFileName := '_' + InternalRandomString(7) + ASepExt;
    AFullPath := APath + DirectorySeparator + AFileName;
    WasFound :=
      FileNameFound(APath + AFileName);
  until (not WasFound);
  Result := AFileName;
  // Objetivo: Obtener una cadena de caracteres aleatorios,
  // con la longuitud indicada.
end;

end.
