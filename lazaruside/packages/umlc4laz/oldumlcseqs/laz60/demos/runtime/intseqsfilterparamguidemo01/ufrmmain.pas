unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcbooleans,
  umlcsequences,
  umlcsimplesequences,
  umlcstdseqs,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnClear: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnlPanel: TPanel;
    sbStatusBar: TStatusBar;
    procedure btnClearClick(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function PrepareSequence
      (): TUMLCIntegerSequence;

    procedure DisplaySequence
      (const SeqName: string;
       const ASequence: TUMLCIntegerSequence);

    procedure FilterDemo();
  end;

  //TFilterPredicateFunctor =
  function GreaterThan
    (const AItem: Pointer;
     const AParam: Pointer): Boolean;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

//TFilterPredicateFunctor =
function GreaterThan
  (const AItem: Pointer;
   const AParam: Pointer): Boolean;
var AIntItem, AIntParam: PInteger;
begin
  AIntItem :=
    PInteger(AItem);
  AIntParam :=
    PInteger(AParam);
  Result :=
    (AIntItem^ > AIntParam^);
end;

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close;
end;

function Tfrmmain.PrepareSequence
  (): TUMLCIntegerSequence;
begin
  Result :=
    TUMLCIntegerSequence.Create();

  Result.Insert(0);
  Result.Insert(1);
  Result.Insert(3);
  Result.Insert(3);
  Result.Insert(5);
  Result.Insert(7);
  Result.Insert(7);
  Result.Insert(9);
  Result.Insert(10);
  Result.Insert(0);
end;

procedure Tfrmmain.DisplaySequence
  (const SeqName: string;
   const ASequence: TUMLCIntegerSequence);
var AIterator: TUMLCSequenceIterator;
    AItem: Integer; S: string;
begin
  AIterator :=
    ASequence.GenerateIterator();

  S := SeqName;
  S := S + ': ';
  S := S + '[';

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCIntegerSequenceForwardIterator).Read();

    S := S + IntToStr(AItem);
    S := S + ', ';

    AIterator.MoveNext();
  end;

  S := S + ']';
  mmConsole.Lines.Add(S);

  ASequence.ReleaseIterator(AIterator);
end;

procedure Tfrmmain.FilterDemo();
var A, C: TUMLCIntegerSequence; AParam: Integer;
begin
  mmConsole.Lines.Add('Sequential Algebra');
  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Filter By Param Sequence Demo');
  mmConsole.Lines.Add('');

  A :=
    Self.PrepareSequence();

  mmConsole.Lines.Add('Before:');
  mmConsole.Lines.Add('');

  Self.DisplaySequence('A', A);

  mmConsole.Lines.Add('');

  mmConsole.Lines.Add('After:');
  mmConsole.Lines.Add('');

  AParam := 5;
  C :=
   (FilterByParamByCopy(A, @GreaterThan, @AParam) as TUMLCIntegerSequence);

  Self.DisplaySequence('A', A);
  Self.DisplaySequence('C', C);

  C.Free;
  A.Free;
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
begin
  Self.FilterDemo();
end;

procedure Tfrmmain.btnClearClick(Sender: TObject);
begin
  mmConsole.Lines.Clear;
end;

end.

