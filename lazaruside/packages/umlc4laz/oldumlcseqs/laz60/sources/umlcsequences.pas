(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsequences;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  dummy;

const

  // ---

  ID_TUMLCSequence : TUMLCType =
    ($4A,$D2,$AA,$8B,$BB,$67,$33,$4C,$AE,$EF,$6F,$45,$70,$F4,$29,$B0);

  ID_TUMLCSequenceIterator : TUMLCType =
    ($FD,$3F,$A6,$F0,$60,$C5,$CD,$4C,$A2,$93,$77,$0B,$9A,$DF,$02,$78);

  ID_TUMLCPointerListSequence : TUMLCType =
    ($CE,$DF,$5B,$8E,$73,$6E,$45,$41,$87,$30,$F1,$98,$15,$AF,$F0,$74);

  ID_TUMLCPointerListSequenceIterator : TUMLCType =
    ($CC,$DF,$D4,$9D,$E1,$7A,$E7,$40,$94,$9D,$F5,$F0,$4D,$09,$B6,$98);

  ID_TUMLCPointerListSequenceForwardIterator : TUMLCType =
    ($94,$B3,$63,$DD,$D5,$07,$BB,$44,$88,$6F,$98,$E4,$CF,$09,$D8,$50);

  ID_TUMLCPointerListSequenceBackwardIterator : TUMLCType =
    ($FC,$80,$3F,$D2,$7A,$D2,$F4,$44,$86,$FD,$6D,$2E,$C4,$CD,$5F,$59);

  // ---

type

  TFilterPredicateObjectFunctor =
    function
      (const AItem: Pointer): Boolean of Object;

  TFilterPredicateFunctor =
    function
      (const AItem: Pointer): Boolean;

  TFilterByParamPredicateObjectFunctor =
    function
      (const AItem: Pointer;
       const AParam: Pointer): Boolean of Object;

  TFilterByParamPredicateFunctor =
    function
      (const AItem: Pointer;
       const AParam: Pointer): Boolean;

type

(* TUMLCSequenceIterator *)

  TUMLCSequenceIterator = (* forward *) class;

(* TUMLCSequence *)

  TUMLCSequence =
    class(TUMLCInternalCloneableObject)
  private
    (* private declarations *)

  protected
    (* protected declarations *)

    FIteratorCount: Integer;

    FBaseType: umlctypes.TUMLCType;
  protected
    (* protected declarations *)

    function InternalIsEmpty(): Boolean; virtual;
    function InternalCount(): Integer; virtual;

    procedure InternalPrepareIterator(); virtual;

    procedure InternalUnprepareIterator(); virtual;

    function InternalGenerateForwardIterator
      (): TUMLCSequenceIterator; virtual;

    function InternalGenerateBackwardIterator
      (): TUMLCSequenceIterator; virtual;

    procedure InternalReleaseIterator
      (var AIterator: TUMLCSequenceIterator); virtual;
  protected
    (* protected declarations *)

    function InternalScalarAverage(): Extended; virtual;
  protected
    (* protected declarations *)

    function InternalNullOf(): TUMLCSequence; virtual;

    function InternalNotNullOf(): TUMLCSequence; virtual;

    function InternalFirstOf(): TUMLCSequence; virtual;

    function InternalLastOf(): TUMLCSequence; virtual;

    function InternalFirstOrNullOf(): TUMLCSequence; virtual;

    function InternalLastOrNullOf(): TUMLCSequence; virtual;

    function InternalNoFirstOf(): TUMLCSequence; virtual;

    function InternalNoLastOf(): TUMLCSequence; virtual;
  protected
    (* protected declarations *)

    //procedure InternalInsert
    //  (var AItem: Pointer); virtual;

    function InternalNewSequence
      (): TUMLCSequence; virtual;

    function InternalNewItem
      (): Pointer; virtual;

    procedure InternalDropItem
      (var AItem: Pointer); virtual;

    function InternalItemSize(): Integer; virtual;

    function InternalCompareItems
      (const A: Pointer;
       const B: Pointer): TComparison; virtual;

    function AreEqualMembers
      (const A: Pointer;
       const B: Pointer): Boolean; virtual;

    function IsMember
      (const AItem: Pointer): boolean; virtual;

    procedure AddFrom
      (const ASource: TUMLCSequence); virtual;

    procedure IntersectFrom
      (const A: TUMLCSequence;
       const B: TUMLCSequence); virtual;

    procedure RightSubsFrom
      (const A: TUMLCSequence;
       const B: TUMLCSequence); virtual;

    procedure LeftSubsFrom
      (const A: TUMLCSequence;
       const B: TUMLCSequence); virtual;

    procedure SymmetryFrom
      (const A: TUMLCSequence;
       const B: TUMLCSequence); virtual;

    procedure InvertFrom
      (const ASource: TUMLCSequence); virtual;

    procedure UnifyFrom
      (const ASource: TUMLCSequence); virtual;

    procedure SortAscFrom
      (const ASource: TUMLCSequence); virtual;

    procedure SortDescFrom
      (const ASource: TUMLCSequence); virtual;

    procedure SortFrom
      (const ASource: TUMLCSequence;
       const ASortMode: SortModesEnum); virtual;

    procedure FilterFrom
      (const ASource: TUMLCSequence;
       const APredicate: TFilterPredicateFunctor); virtual;

    procedure FilterByParamFrom
      (const ASource: TUMLCSequence;
       const APredicate: TFilterByParamPredicateFunctor;
       const AParam: Pointer); virtual;
  public
    (* public declarations *)

    function ScalarCount(): LongInt; virtual;

    function ScalarAverage(): Extended; virtual;
  public
    (* public declarations *)

    procedure InternalInsert
      (var AItem: Pointer); virtual;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    procedure ReadBaseType
      (var ADest: umlctypes.TUMLCType);

    function IsEmpty(): Boolean;
    function Count(): Integer;

    procedure Clear(); virtual;
  public
    (* public declarations *)
  end;

(* TUMLCSequenceIterator *)

  TUMLCSequenceIterator =
    class(TUMLCListIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FSequence: TUMLCSequence;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function InternalRead(): Pointer; virtual;
    function InternalReadCopy(): Pointer; virtual;
  public
    (* public declarations *)

    property Sequence: TUMLCSequence
      read FSequence write FSequence;
  end;

(* TUMLCPointerListSequence *)

  TUMLCPointerListSequence =
    class(TUMLCSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure QuickSortAsc
     (Left, Right: Integer);

    procedure QuickSortDesc
     (Left, Right: Integer);
  protected
    (* protected declarations *)

    FItems: TUMLCPointerList;
    FDefaultItemValue: Pointer;
  protected
    (* protected declarations *)

    function InternalAggregateLowestValue(): Pointer; virtual;

    function InternalAggregateHighestValue(): Pointer; virtual;
  protected
    (* protected declarations *)

    function InternalIsEmpty(): Boolean; override;
    function InternalCount(): Integer; override;

    procedure InternalInsert
      (var AItem: Pointer); override;

    function InternalNewSequence
      (): TUMLCSequence; override;
  protected
    (* protected declarations *)

    function InternalNullItem(): Pointer; virtual;

    function InternalFirstItem(): Pointer; virtual;

    function InternalLastItem(): Pointer; virtual;
  protected
    (* protected declarations *)

    function InternalNullOf(): TUMLCSequence; override;

    function InternalNotNullOf(): TUMLCSequence; override;

    function InternalFirstOf(): TUMLCSequence; override;

    function InternalLastOf(): TUMLCSequence; override;

    function InternalFirstOrNullOf(): TUMLCSequence; override;

    function InternalLastOrNullOf(): TUMLCSequence; override;

    function InternalNoFirstOf(): TUMLCSequence; override;

    function InternalNoLastOf(): TUMLCSequence; override;
  protected
    (* protected declarations *)

    function IsMember
      (const AItem: Pointer): boolean; override;

    procedure AddFrom
      (const ASource: TUMLCSequence); override;

    procedure IntersectFrom
      (const A: TUMLCSequence;
       const B: TUMLCSequence); override;

    procedure RightSubsFrom
      (const A: TUMLCSequence;
       const B: TUMLCSequence); override;

    procedure LeftSubsFrom
      (const A: TUMLCSequence;
       const B: TUMLCSequence); override;

    procedure SymmetryFrom
      (const A: TUMLCSequence;
       const B: TUMLCSequence); override;

    procedure InvertFrom
      (const ASource: TUMLCSequence); override;

    procedure UnifyFrom
      (const ASource: TUMLCSequence); override;

    procedure SortAscFrom
      (const ASource: TUMLCSequence); override;

    procedure SortDescFrom
      (const ASource: TUMLCSequence); override;

    procedure SortFrom
      (const ASource: TUMLCSequence;
       const ASortMode: SortModesEnum); override;

    procedure FilterFrom
      (const ASource: TUMLCSequence;
       const APredicate: TFilterPredicateFunctor); override;

    procedure FilterByParamFrom
      (const ASource: TUMLCSequence;
       const APredicate: TFilterByParamPredicateFunctor;
       const AParam: Pointer); override;
  public
    (* public declarations *)

    function ScalarCount(): LongInt; override;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    property Items: TUMLCPointerList
      read FItems write FItems;
  end;

(* TUMLCPointerListSequenceIterator *)

  TUMLCPointerListSequenceIterator =
    class(TUMLCSequenceIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FCurrentIndex: Integer;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function InternalRead(): Pointer; override;
    function InternalReadCopy(): Pointer; override;
  end;

(* TUMLCPointerListSequenceForwardIterator *)

  TUMLCPointerListSequenceForwardIterator =
    class(TUMLCPointerListSequenceIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure InternalStart(); override;
    procedure InternalStop(); override;

    function InternalListDirection(): ListDirectionsEnum; override;

    function InternalIsDone: Boolean; override;

    procedure InternalMoveNext(); override;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)
  end;

(* TUMLCPointerListSequenceBackwardIterator *)

  TUMLCPointerListSequenceBackwardIterator =
    class(TUMLCPointerListSequenceIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure InternalStart(); override;
    procedure InternalStop(); override;

    function InternalListDirection(): ListDirectionsEnum; override;

    function InternalIsDone: Boolean; override;

    procedure InternalMoveNext(); override;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)
  end;

(* friend declarations *)

  (* friend *) function SameBaseType
    (const A: TUMLCSequence;
     const B: TUMLCSequence): Boolean;

  (* friend *) function AreEqual
    (const A: TUMLCSequence;
     const B: TUMLCSequence): Boolean;

  (* friend *) function UnionByCopy
    (const A: TUMLCSequence;
     const B: TUMLCSequence): TUMLCSequence;

  (* friend *) function IntersectByCopy
    (const A: TUMLCSequence;
     const B: TUMLCSequence): TUMLCSequence;

  (* friend *) function RightSubsByCopy
    (const A: TUMLCSequence;
     const B: TUMLCSequence): TUMLCSequence;

  (* friend *) function LeftSubsByCopy
    (const A: TUMLCSequence;
     const B: TUMLCSequence): TUMLCSequence;

  (* friend *) function SymmetryByCopy
    (const A: TUMLCSequence;
     const B: TUMLCSequence): TUMLCSequence;

  (* friend *) function InvertByCopy
    (const ASource: TUMLCSequence): TUMLCSequence;

  (* friend *) function UnifyByCopy
    (const ASource: TUMLCSequence): TUMLCSequence;

  (* friend *) function SortAscByCopy
    (const ASource: TUMLCSequence): TUMLCSequence;

  (* friend *) function SortDescByCopy
    (const ASource: TUMLCSequence): TUMLCSequence;

  (* friend *) function SortByCopy
    (const ASource: TUMLCSequence;
     const ASortMode: SortModesEnum): TUMLCSequence;

  (* friend *) function FilterByCopy
    (const ASource: TUMLCSequence;
     const APredicate: TFilterPredicateFunctor): TUMLCSequence;

  (* friend *) function FilterByParamByCopy
    (const ASource: TUMLCSequence;
     const APredicate: TFilterByParamPredicateFunctor;
     const AParam: Pointer): TUMLCSequence;

implementation

(* TUMLCSequence *)

constructor TUMLCSequence.Create();
begin
  inherited Create();

  Self.FIteratorCount := 0;

  Self.FBaseType := umlctypes.UnknownType;
end;

destructor TUMLCSequence.Destroy();
begin
  //Self.DoNothing();
  inherited Destroy();
end;

function TUMLCSequence.InternalIsEmpty(): Boolean;
begin
  Result := true;
end;

function TUMLCSequence.InternalCount(): Integer;
begin
  Result := 0;
end;

procedure TUMLCSequence.InternalPrepareIterator();
begin
  if (Self.FIteratorCount < 0) then
  begin
    Self.FIteratorCount := 0;
  end;

  Self.FIteratorCount :=
    (Self.FIteratorCount + 1);
end;

procedure TUMLCSequence.InternalUnprepareIterator();
begin
  Self.FIteratorCount :=
    (Self.FIteratorCount - 1);

  if (Self.FIteratorCount < 0) then
  begin
    Self.FIteratorCount := 0;
  end;
end;

function TUMLCSequence.InternalGenerateForwardIterator(): TUMLCSequenceIterator;
begin
  Result := nil;
  //Self.InternalPrepareIterator();
end;

function TUMLCSequence.InternalGenerateBackwardIterator(): TUMLCSequenceIterator;
begin
  Result := nil;
  //Self.InternalPrepareIterator();
end;

procedure TUMLCSequence.InternalReleaseIterator
  (var AIterator: TUMLCSequenceIterator);
begin
  AIterator.Stop();

  Self.InternalUnprepareIterator();

  AIterator.Free();
end;

function TUMLCSequence.InternalScalarAverage(): Extended;
begin
  Result := 0.0;
end;

function TUMLCSequence.InternalNullOf(): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCSequence.InternalNotNullOf(): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCSequence.InternalFirstOf(): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCSequence.InternalLastOf(): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCSequence.InternalFirstOrNullOf(): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCSequence.InternalLastOrNullOf(): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCSequence.InternalNoFirstOf(): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCSequence.InternalNoLastOf(): TUMLCSequence;
begin
  Result := nil;
end;

procedure TUMLCSequence.InternalInsert
  (var AItem: Pointer);
begin
  Self.DoNothing();
end;

function TUMLCSequence.InternalNewSequence
  (): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCSequence.InternalNewItem
  (): Pointer;
begin
  Result := nil;
end;

procedure TUMLCSequence.InternalDropItem
  (var AItem: Pointer);
begin
  AItem := nil;
end;

function TUMLCSequence.InternalItemSize(): Integer;
begin
  Result := 1;
end;

function TUMLCSequence.InternalCompareItems
  (const A: Pointer;
   const B: Pointer): TComparison;
begin
  Result := cmpEqual;
end;

function TUMLCSequence.AreEqualMembers
  (const A: Pointer;
   const B: Pointer): Boolean;
begin
  Result := false;
end;

function TUMLCSequence.IsMember
  (const AItem: Pointer): boolean;
begin
  Result := false;
end;

procedure TUMLCSequence.AddFrom
  (const ASource: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.IntersectFrom
  (const A: TUMLCSequence;
   const B: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.RightSubsFrom
  (const A: TUMLCSequence;
   const B: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.LeftSubsFrom
  (const A: TUMLCSequence;
   const B: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.SymmetryFrom
  (const A: TUMLCSequence;
   const B: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.InvertFrom
  (const ASource: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.UnifyFrom
  (const ASource: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.SortAscFrom
  (const ASource: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.SortDescFrom
  (const ASource: TUMLCSequence);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.SortFrom
  (const ASource: TUMLCSequence;
   const ASortMode: SortModesEnum);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.FilterFrom
  (const ASource: TUMLCSequence;
   const APredicate: TFilterPredicateFunctor);
begin
  Self.DoNothing();
end;

procedure TUMLCSequence.FilterByParamFrom
  (const ASource: TUMLCSequence;
   const APredicate: TFilterByParamPredicateFunctor;
   const AParam: Pointer);
begin
  Self.DoNothing();
end;

function TUMLCSequence.ScalarCount(): LongInt;
begin
  Result := 0;
end;

function TUMLCSequence.ScalarAverage(): Extended;
begin
  Result :=
    Self.InternalScalarAverage();
end;

procedure TUMLCSequence.ReadBaseType
  (var ADest: umlctypes.TUMLCType);
begin
  umlctypes.AssignType(ADest, Self.FBaseType);
end;

function TUMLCSequence.IsEmpty(): Boolean;
begin
  Result :=
    Self.InternalIsEmpty();
end;

function TUMLCSequence.Count(): Integer;
begin
  Result :=
    Self.InternalCount();
end;

procedure TUMLCSequence.Clear();
var CanContinue: Boolean;
begin
  CanContinue :=
    (not Self.IsEmpty());
  if (CanContinue) then
  begin
    Self.Clear();
  end;
end;

(* TUMLCSequenceIterator *)

constructor TUMLCSequenceIterator.Create();
begin
  inherited Create();
  Self.FSequence := nil;
end;

destructor TUMLCSequenceIterator.Destroy();
begin
  Self.FSequence := nil;
  inherited Destroy();
end;

function TUMLCSequenceIterator.InternalRead(): Pointer;
begin
  Result := nil;
end;

function TUMLCSequenceIterator.InternalReadCopy(): Pointer;
begin
  Result := nil;
end;

(* friend declarations *)

(* friend *) function SameBaseType
  (const A: TUMLCSequence;
   const B: TUMLCSequence): Boolean;
var CanContinue: Boolean;
    C, D: umlctypes.TUMLCType;
begin
  Result := false;

  CanContinue :=
    (Assigned(A) and Assigned(B));
  if (CanContinue) then
  begin
    A.ReadBaseType(C);
    B.ReadBaseType(D);

    Result :=
      (umlctypes.AreEqualTypes(C, D));
  end;
end;

(* friend *) function AreEqual
  (const A: TUMLCSequence;
   const B: TUMLCSequence): Boolean;
begin
  Result :=
    (Assigned(A) and Assigned(B));
  if (Result) then
  begin
    Result :=
      (SameBaseType(A, B) and (A.Count() = B.Count()));
    if (Result) then
    begin
      (* toxdo: ... *)
    end;
  end;
end;

(* friend *) function UnionByCopy
  (const A: TUMLCSequence;
   const B: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    (Assigned(A) and Assigned(B) and SameBaseType(A, B));
  if (CanContinue) then
  begin
    Result :=
      A.InternalNewSequence();

    Result.AddFrom(A);
    Result.AddFrom(B);
  end;
end;

(* friend *) function IntersectByCopy
  (const A: TUMLCSequence;
   const B: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    (Assigned(A) and Assigned(B) and SameBaseType(A, B));
  if (CanContinue) then
  begin
    Result :=
      A.InternalNewSequence();

    Result.IntersectFrom(A, B);
  end;
end;

(* friend *) function RightSubsByCopy
  (const A: TUMLCSequence;
   const B: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    (Assigned(A) and Assigned(B) and SameBaseType(A, B));
  if (CanContinue) then
  begin
    Result :=
      A.InternalNewSequence();

    Result.RightSubsFrom(A, B);
  end;
end;

(* friend *) function LeftSubsByCopy
  (const A: TUMLCSequence;
   const B: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    (Assigned(A) and Assigned(B) and SameBaseType(A, B));
  if (CanContinue) then
  begin
    Result :=
      A.InternalNewSequence();

    Result.LeftSubsFrom(A, B);
  end;
end;

(* friend *) function SymmetryByCopy
  (const A: TUMLCSequence;
   const B: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    (Assigned(A) and Assigned(B) and SameBaseType(A, B));
  if (CanContinue) then
  begin
    Result :=
      A.InternalNewSequence();

    Result.SymmetryFrom(A, B);
  end;
end;

(* friend *) function InvertByCopy
  (const ASource: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    Assigned(ASource);
  if (CanContinue) then
  begin
    Result :=
      ASource.InternalNewSequence();

    Result.InvertFrom(ASource);
  end;
end;

(* friend *) function UnifyByCopy
  (const ASource: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    Assigned(ASource);
  if (CanContinue) then
  begin
    Result :=
      ASource.InternalNewSequence();

    Result.UnifyFrom(ASource);
  end;
end;

(* friend *) function SortAscByCopy
  (const ASource: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    Assigned(ASource);
  if (CanContinue) then
  begin
    Result :=
      ASource.InternalNewSequence();

    Result.SortAscFrom(ASource);
  end;
end;

(* friend *) function SortDescByCopy
  (const ASource: TUMLCSequence): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    Assigned(ASource);
  if (CanContinue) then
  begin
    Result :=
      ASource.InternalNewSequence();

    Result.SortDescFrom(ASource);
  end;
end;

(* friend *) function SortByCopy
  (const ASource: TUMLCSequence;
   const ASortMode: SortModesEnum): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    Assigned(ASource);
  if (CanContinue) then
  begin
    Result :=
      ASource.InternalNewSequence();

    Result.SortFrom(ASource, ASortMode);
  end;
end;

(* friend *) function FilterByCopy
  (const ASource: TUMLCSequence;
   const APredicate: TFilterPredicateFunctor): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    Assigned(ASource);
  if (CanContinue) then
  begin
    Result :=
      ASource.InternalNewSequence();

    Result.FilterFrom
      (ASource, APredicate);
  end;
end;

(* friend *) function FilterByParamByCopy
  (const ASource: TUMLCSequence;
   const APredicate: TFilterByParamPredicateFunctor;
   const AParam: Pointer): TUMLCSequence;
var CanContinue: Boolean;
begin
  Result := nil;

  CanContinue :=
    Assigned(ASource);
  if (CanContinue) then
  begin
    Result :=
      ASource.InternalNewSequence();

    Result.FilterByParamFrom
      (ASource, APredicate, AParam);
  end;
end;

(* TUMLCPointerListSequence *)

constructor TUMLCPointerListSequence.Create();
begin
  inherited Create();
  Self.FItems            := TUMLCPointerList.Create();
  Self.FDefaultItemValue := nil;
end;

destructor TUMLCPointerListSequence.Destroy();
begin
  Self.FDefaultItemValue := nil;
  Self.FItems.Free;
  inherited Destroy();
end;

function TUMLCPointerListSequence.InternalIsEmpty(): Boolean;
begin
  Result :=
    (not Assigned(Self.FItems) or (Self.FItems.Count < 1));
end;

function TUMLCPointerListSequence.InternalCount(): Integer;
var CanContinue: Boolean;
begin
  Result := 0;

  CanContinue :=
    (not Self.IsEmpty());
  if (CanContinue) then
  begin
    Result :=
      Self.FItems.Count;
  end;
end;

procedure TUMLCPointerListSequence.InternalInsert
  (var AItem: Pointer);
begin
  Self.FItems.Insert(AItem);
end;

function TUMLCPointerListSequence.InternalNewSequence
  (): TUMLCSequence;
begin
  Result := nil;
end;

function TUMLCPointerListSequence.InternalNullItem(): Pointer;
begin
  Result :=
    Self.InternalNewItem();
end;

function TUMLCPointerListSequence.InternalFirstItem(): Pointer;
var AItem: Pointer;
    Found: Boolean;
    AIterator: TUMLCSequenceIterator;
begin
  Result := nil;
  AItem  := nil;

  AIterator :=
    Self.InternalGenerateForwardIterator();

  Found := false;
  while (not AIterator.IsDone() and (not Found)) do
  begin
    AItem :=
      AIterator.InternalRead();

    if (not Found) then
    begin
      Found := true;
    end;

    AIterator.MoveNext();
  end;

  if (not Found) then
  begin
    AItem := nil;
  end;

  Self.InternalReleaseIterator(AIterator);

  Result :=
    AItem;
end;

function TUMLCPointerListSequence.InternalLastItem(): Pointer;
var AItem: Pointer;
    Found: Boolean;
    AIterator: TUMLCSequenceIterator;
begin
  Result := nil;
  AItem  := nil;

  AIterator :=
    Self.InternalGenerateBackwardIterator();

  Found := false;
  while (not AIterator.IsDone() and (not Found)) do
  begin
    AItem :=
      AIterator.InternalRead();

    Found := true;

    AIterator.MoveNext();
  end;

  if (not Found) then
  begin
    AItem := nil;
  end;

  Self.InternalReleaseIterator(AIterator);

  Result :=
    AItem;
end;

function TUMLCPointerListSequence.InternalNullOf(): TUMLCSequence;
var AItem: Pointer;
begin
  Result :=
    Self.InternalNewSequence();
  AItem :=
    Self.InternalNullItem();
  Result.InternalInsert(AItem);
end;

function TUMLCPointerListSequence.InternalNotNullOf(): TUMLCSequence;
var AItem: PShortstring;
    AIterator: TUMLCSequenceIterator;
begin
  Result :=
    Self.InternalNewSequence();

  AItem  := nil;

  AIterator :=
    Self.InternalGenerateForwardIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      AIterator.InternalRead();

    if (not Self.AreEqualMembers(AItem, Self.FDefaultItemValue)) then
    begin
      AItem :=
        AIterator.InternalReadCopy();

      Result.InternalInsert(AItem);
    end;

    AIterator.MoveNext();
  end;

  Self.InternalReleaseIterator(AIterator);
end;

function TUMLCPointerListSequence.InternalFirstOf(): TUMLCSequence;
var AItem: Pointer;
begin
  Result :=
    Self.InternalNewSequence();

  AItem :=
    Self.InternalFirstItem();
  if (AItem <> nil) then
  begin
    Result.InternalInsert(AItem);
  end;
end;

function TUMLCPointerListSequence.InternalLastOf(): TUMLCSequence;
var AItem: Pointer;
begin
  Result :=
    Self.InternalNewSequence();

  AItem :=
    Self.InternalLastItem();
  if (AItem <> nil) then
  begin
    Result.InternalInsert(AItem);
  end;
end;

function TUMLCPointerListSequence.InternalFirstOrNullOf(): TUMLCSequence;
var AItem: Pointer;
begin
  Result :=
    Self.InternalNewSequence();

  AItem :=
    Self.InternalFirstItem();
  if (AItem = nil) then
  begin
    AItem :=
      Self.InternalNullItem();
  end;

  Result.InternalInsert(AItem);
end;

function TUMLCPointerListSequence.InternalLastOrNullOf(): TUMLCSequence;
var AItem: Pointer;
begin
  Result :=
    Self.InternalNewSequence();

  AItem :=
    Self.InternalFirstItem();
  if (AItem = nil) then
  begin
    AItem :=
      Self.InternalNullItem();
  end;

  Result.InternalInsert(AItem);
end;

function TUMLCPointerListSequence.InternalNoFirstOf(): TUMLCSequence;
var AItem: Pointer;
    Skip: Boolean;
    AIterator: TUMLCSequenceIterator;
begin
  Result := nil;
  AItem  := nil;

  AIterator :=
    Self.InternalGenerateForwardIterator();

  Skip := true;
  while (not AIterator.IsDone()) do
  begin
    if (not Skip) then
    begin
      AItem := AIterator.InternalRead();

      Self.InternalInsert(AItem);
    end;

    if (not Skip) then
    begin
      Skip := true;
    end;

    AIterator.MoveNext();
  end;

  Self.InternalReleaseIterator(AIterator);
end;

function TUMLCPointerListSequence.InternalNoLastOf(): TUMLCSequence;
begin
  Result :=
    nil;
end;

function TUMLCPointerListSequence.IsMember
  (const AItem: Pointer): boolean;
var Found: Boolean; EachItem: Pointer;
    AIterator: TUMLCSequenceIterator;
begin
  Result :=
    (Assigned(AItem));
  if (Result) then
  begin
    AIterator :=
      Self.InternalGenerateForwardIterator();

    Found := false;
    while ((not AIterator.IsDone()) and (not Found)) do
    begin
      EachItem := AIterator.InternalRead();

      Found :=
        Self.AreEqualMembers(AItem, EachItem);

      AIterator.MoveNext();
    end;

    Self.InternalReleaseIterator(AIterator);

    Result := Found;
  end;
end;

procedure TUMLCPointerListSequence.AddFrom
  (const ASource: TUMLCSequence);
var CanContinue: Boolean; AItem: Pointer;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    SameBaseType(Self, ASource);
  if (CanContinue) then
  begin
    AIterator :=
      ASource.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem :=
        AIterator.InternalReadCopy();

      Self.InternalInsert(AItem);

      AIterator.MoveNext();
    end;

    ASource.InternalReleaseIterator(AIterator);
  end;
end;

procedure TUMLCPointerListSequence.IntersectFrom
  (const A: TUMLCSequence;
   const B: TUMLCSequence);
var CanContinue: Boolean; AItem: Pointer;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    (SameBaseType(Self, A) and SameBaseType(Self, B));
  if (CanContinue) then
  begin
    AIterator :=
      A.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem := AIterator.InternalRead();

      if (B.IsMember(AItem)) then
      begin
        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    A.InternalReleaseIterator(AIterator);

    // ---

    AIterator :=
      B.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem := AIterator.InternalRead();

      if (A.IsMember(AItem)) then
      begin
        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    B.InternalReleaseIterator(AIterator);
  end;
end;

procedure TUMLCPointerListSequence.RightSubsFrom
  (const A: TUMLCSequence;
   const B: TUMLCSequence);
var CanContinue: Boolean; AItem: Pointer;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    (SameBaseType(Self, A) and SameBaseType(Self, B));
  if (CanContinue) then
  begin
    AIterator :=
      A.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem := AIterator.InternalRead();

      if (B.IsMember(AItem)) then
      begin
        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    A.InternalReleaseIterator(AIterator);
  end;
end;

procedure TUMLCPointerListSequence.LeftSubsFrom
  (const A: TUMLCSequence;
   const B: TUMLCSequence);
var CanContinue: Boolean; AItem: Pointer;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    (SameBaseType(Self, A) and SameBaseType(Self, B));
  if (CanContinue) then
  begin
    AIterator :=
      A.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem := AIterator.InternalRead();

      if (B.IsMember(AItem)) then
      begin
        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    A.InternalReleaseIterator(AIterator);
  end;
end;

procedure TUMLCPointerListSequence.SymmetryFrom
  (const A: TUMLCSequence;
   const B: TUMLCSequence);
var CanContinue: Boolean; AItem: Pointer;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    (SameBaseType(Self, A) and SameBaseType(Self, B));
  if (CanContinue) then
  begin
    AIterator :=
      A.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem := AIterator.InternalRead();

      if (B.IsMember(AItem)) then
      begin
        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    A.InternalReleaseIterator(AIterator);

    // ---

    AIterator :=
      B.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem := AIterator.InternalRead();

      if (A.IsMember(AItem)) then
      begin
        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    B.InternalReleaseIterator(AIterator);
  end;
end;

procedure TUMLCPointerListSequence.InvertFrom
  (const ASource: TUMLCSequence);
var CanContinue: Boolean; AItem: Pointer;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    SameBaseType(Self, ASource);
  if (CanContinue) then
  begin
    AIterator :=
      ASource.InternalGenerateBackwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem :=
        AIterator.InternalRead();

      Self.InternalInsert(AItem);

      AIterator.MoveNext();
    end;

    ASource.InternalReleaseIterator(AIterator);
  end;
end;

procedure TUMLCPointerListSequence.UnifyFrom
  (const ASource: TUMLCSequence);
var CanContinue: Boolean; AItem: Pointer;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    SameBaseType(Self, ASource);
  if (CanContinue) then
  begin
    AIterator :=
      ASource.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem :=
        AIterator.InternalRead();

      if (not Self.IsMember(AItem)) then
      begin
        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    ASource.InternalReleaseIterator(AIterator);
  end;
end;

procedure TUMLCPointerListSequence.QuickSortAsc
 (Left, Right: Integer);
var I, J: Integer; P: pointer;
begin
  repeat
    I := Left;
    J := Right;
    P := Self.FItems[(Left + Right) shr 1];
    repeat
      while (Self.InternalCompareItems(Self.FItems[I], P) = cmpLower) do
      begin
        Inc(I);
      end;

      while (Self.InternalCompareItems(Self.FItems[J], P) = cmpHigher) do
      begin
        Dec(J);
      end;

      if (I <= J) then
      begin
        Self.FItems.Exchange(I, J);
        Inc(I);
        Dec(J);
      end;
    until (I > J);

    if (Left < J) then
    begin
      Self.QuickSortAsc(Left, J);
    end;

    Left := I;
  until (I >= Right);
end;

procedure TUMLCPointerListSequence.SortAscFrom
  (const ASource: TUMLCSequence);
var CanContinue: Boolean;
    Left, Right: Integer;
    ACount: Integer;
begin
  CanContinue :=
    SameBaseType(Self, ASource);
  if (CanContinue) then
  begin
    Self.AddFrom(ASource);

    ACount :=
      Self.FItems.Count;
    CanContinue :=
      (ACount > 0);
    if (CanContinue) then
    begin
      Left  := 0;
      Right := (ACount - 1);
      Self.QuickSortAsc(Left, Right);
    end
  end;
end;

procedure TUMLCPointerListSequence.QuickSortDesc
 (Left, Right: Integer);
var I, J: Integer; P: pointer;
begin
  repeat
    I := Left;
    J := Right;
    P := Self.FItems[(Left + Right) shr 1];
    repeat
      while (Self.InternalCompareItems(Self.FItems[I], P) = cmpHigher) do
      begin
        Inc(I);
      end;

      while (Self.InternalCompareItems(Self.FItems[J], P) = cmpLower) do
      begin
        Dec(J);
      end;

      if (I <= J) then
      begin
        Self.FItems.Exchange(I, J);
        Inc(I);
        Dec(J);
      end;
    until (I > J);

    if (Left < J) then
    begin
      Self.QuickSortDesc(Left, J);
    end;

    Left := I;
  until (I >= Right);
end;

function TUMLCPointerListSequence.InternalAggregateLowestValue(): Pointer;
var AItem: Pointer; ALowestValue: Pointer;
    AtStart: Boolean;
    AIterator: TUMLCSequenceIterator;
begin
  Result := nil;

  AtStart := true;
  ALowestValue :=
    Self.FDefaultItemValue;

  AIterator :=
    Self.InternalGenerateForwardIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCPointerListSequenceForwardIterator).InternalRead();

    if (AtStart) then
    begin
      AtStart := false;
      ALowestValue := AItem;
    end;

    if (Self.InternalCompareItems(AItem, ALowestValue) = cmpLower) then
    begin
      ALowestValue := AItem;
    end;

    AIterator.MoveNext();
  end;

  Self.InternalReleaseIterator(AIterator);

  Result :=
    ALowestValue;
end;

function TUMLCPointerListSequence.InternalAggregateHighestValue(): Pointer;
var AItem: Pointer; AHighestValue: Pointer;
    AtStart: Boolean;
    AIterator: TUMLCSequenceIterator;
begin
  Result := nil;

  AtStart := true;
  AHighestValue :=
    Self.FDefaultItemValue;

  AIterator :=
    Self.InternalGenerateForwardIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCPointerListSequenceForwardIterator).InternalRead();

    if (AtStart) then
    begin
      AtStart := false;
      AHighestValue := AItem;
    end;

    if (Self.InternalCompareItems(AItem, AHighestValue) = cmpHigher) then
    begin
      AHighestValue := AItem;
    end;

    AIterator.MoveNext();
  end;

  Self.InternalReleaseIterator(AIterator);

  Result :=
    AHighestValue;
end;

procedure TUMLCPointerListSequence.SortDescFrom
  (const ASource: TUMLCSequence);
var CanContinue: Boolean;
    Left, Right: Integer;
    ACount: Integer;
begin
  CanContinue :=
    SameBaseType(Self, ASource);
  if (CanContinue) then
  begin
    Self.AddFrom(ASource);

    ACount :=
      Self.FItems.Count;
    CanContinue :=
      (ACount > 0);
    if (CanContinue) then
    begin
      Left  := 0;
      Right := (ACount - 1);
      Self.QuickSortDesc(Left, Right);
    end
  end;
end;

procedure TUMLCPointerListSequence.SortFrom
  (const ASource: TUMLCSequence;
   const ASortMode: SortModesEnum);
begin
  case (ASortMode) of
    SortModes_asc:
      Self.SortAscFrom(ASource);
    SortModes_desc:
      Self.SortDescFrom(ASource);
    else
      Self.DoNothing();
  end;
end;

procedure TUMLCPointerListSequence.FilterFrom
  (const ASource: TUMLCSequence;
   const APredicate: TFilterPredicateFunctor);
var AItem: Pointer;
    CanContinue, Match: Boolean;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    (Assigned(ASource) and Assigned(APredicate));
  if (CanContinue) then
  begin
    AIterator :=
      ASource.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem :=
        (AIterator as TUMLCPointerListSequenceForwardIterator).InternalRead();

      Match :=
        APredicate(AItem);
      if (Match) then
      begin
        AItem :=
          (AIterator as TUMLCPointerListSequenceForwardIterator).InternalReadCopy();

        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    Self.InternalReleaseIterator(AIterator);
  end;
end;

procedure TUMLCPointerListSequence.FilterByParamFrom
  (const ASource: TUMLCSequence;
   const APredicate: TFilterByParamPredicateFunctor;
   const AParam: Pointer);
var AItem: Pointer;
    CanContinue, Match: Boolean;
    AIterator: TUMLCSequenceIterator;
begin
  CanContinue :=
    (Assigned(ASource) and Assigned(APredicate));
  if (CanContinue) then
  begin
    AIterator :=
      ASource.InternalGenerateForwardIterator();

    while (not AIterator.IsDone()) do
    begin
      AItem :=
        (AIterator as TUMLCPointerListSequenceForwardIterator).InternalRead();

      Match :=
        APredicate(AItem, AParam);
      if (Match) then
      begin
        AItem :=
          (AIterator as TUMLCPointerListSequenceForwardIterator).InternalReadCopy();

        Self.InternalInsert(AItem);
      end;

      AIterator.MoveNext();
    end;

    Self.InternalReleaseIterator(AIterator);
  end;
end;

function TUMLCPointerListSequence.ScalarCount(): LongInt;
begin
  Result :=
    Self.FItems.Count;
end;

(* TUMLCPointerListSequenceIterator *)

constructor TUMLCPointerListSequenceIterator.Create();
begin
  inherited Create();
  Self.FCurrentIndex := 0;
end;

destructor TUMLCPointerListSequenceIterator.Destroy();
begin
  Self.DoNothing();
  inherited Destroy();
end;

function TUMLCPointerListSequenceIterator.InternalRead(): Pointer;
begin
  Result :=
    (Self.Sequence as TUMLCPointerListSequence).Items[Self.FCurrentIndex];
end;

function TUMLCPointerListSequenceIterator.InternalReadCopy(): Pointer;
var AItem: Pointer; ASize: Integer;
begin
  Result := nil;

  AItem :=
    (Self.Sequence as TUMLCPointerListSequence).Items[Self.FCurrentIndex];
  ASize :=
    Self.Sequence.InternalItemSize();
  System.GetMem(Result, ASize);
  System.Move(AItem^, Result^, ASize);
end;

(* TUMLCPointerListSequenceForwardIterator *)

constructor TUMLCPointerListSequenceForwardIterator.Create();
begin
  inherited Create();
  Self.FCurrentIndex := 0;
end;

destructor TUMLCPointerListSequenceForwardIterator.Destroy();
begin
  Self.DoNothing();
  inherited Destroy();
end;

procedure TUMLCPointerListSequenceForwardIterator.InternalStart();
begin
  Self.FCurrentIndex := 0;
end;

procedure TUMLCPointerListSequenceForwardIterator.InternalStop();
begin
  Self.FCurrentIndex := 0;
end;

function TUMLCPointerListSequenceForwardIterator.InternalListDirection(): ListDirectionsEnum;
begin
  Result := listdirections_forward;
end;

function TUMLCPointerListSequenceForwardIterator.InternalIsDone: Boolean;
var LastIndex: Integer;
begin
  Result :=
    Self.Sequence.IsEmpty();
  if (not Result) then
  begin
    LastIndex :=
      ((Self.Sequence as TUMLCPointerListSequence).Items.Count - 1);
    Result :=
      (Self.FCurrentIndex > LastIndex);
  end;
end;

procedure TUMLCPointerListSequenceForwardIterator.InternalMoveNext();
begin
  inherited InternalMoveNext();
  Self.FCurrentIndex :=
    Self.FCurrentIndex + 1;
end;

(* TUMLCPointerListSequenceBackwardIterator *)

constructor TUMLCPointerListSequenceBackwardIterator.Create();
begin
  inherited Create();
  Self.FCurrentIndex := 0;
end;

destructor TUMLCPointerListSequenceBackwardIterator.Destroy();
begin
  Self.DoNothing();
  inherited Destroy();
end;

procedure TUMLCPointerListSequenceBackwardIterator.InternalStart();
begin
  Self.FCurrentIndex :=
    ((Self.Sequence as TUMLCPointerListSequence).FItems.Count - 1);
end;

procedure TUMLCPointerListSequenceBackwardIterator.InternalStop();
begin
  Self.FCurrentIndex := 0;
end;

function TUMLCPointerListSequenceBackwardIterator.InternalListDirection(): ListDirectionsEnum;
begin
  Result := listdirections_backward;
end;

function TUMLCPointerListSequenceBackwardIterator.InternalIsDone: Boolean;
begin
  Result :=
    Self.Sequence.IsEmpty();
  if (not Result) then
  begin
    Result :=
      (Self.FCurrentIndex < 0);
  end;
end;

procedure TUMLCPointerListSequenceBackwardIterator.InternalMoveNext();
begin
  Self.FCurrentIndex :=
    Self.FCurrentIndex - 1;
end;


end.

