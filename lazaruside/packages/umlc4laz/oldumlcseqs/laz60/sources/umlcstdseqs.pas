(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdseqs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  umlcsequences,
  umlcsimplesequences,
  dummy;


const

  // ---

  ID_TUMLCIntegerSequence : TUMLCType =
    ($86,$3F,$8D,$02,$E1,$07,$86,$43,$9F,$0E,$70,$75,$5D,$35,$61,$A0);

  ID_TUMLCIntegerSequenceForwardIterator : TUMLCType =
    ($01,$FB,$6A,$D0,$DB,$04,$37,$49,$93,$46,$DE,$FE,$01,$99,$60,$BD);

  ID_TUMLCIntegerSequenceBackwardIterator : TUMLCType =
    ($07,$A7,$0D,$C6,$EE,$1F,$14,$48,$B9,$99,$98,$14,$42,$C8,$3D,$33);

  // ---

  ID_TUMLCDoubleSequence : TUMLCType =
    ($38,$54,$73,$9F,$7C,$47,$B8,$43,$92,$47,$EC,$A7,$EB,$79,$1E,$19);

  ID_TUMLCDoubleSequenceForwardIterator : TUMLCType =
    ($BD,$A9,$63,$4C,$16,$16,$46,$48,$91,$C6,$1A,$18,$7B,$2E,$E9,$EE);

  ID_TUMLCDoubleSequenceBackwardIterator : TUMLCType =
    ($6D,$62,$8B,$2E,$94,$A7,$3D,$49,$B3,$B3,$43,$C9,$50,$26,$AF,$F3);

  // ---

  ID_TUMLCBooleanSequence : TUMLCType =
    ($EB,$E3,$A4,$41,$2D,$28,$00,$45,$A8,$63,$E3,$7E,$A2,$98,$5B,$08);

  ID_TUMLCBooleanSequenceForwardIterator : TUMLCType =
    ($B3,$B1,$43,$0F,$33,$C2,$EE,$4B,$8D,$8E,$D5,$D3,$C4,$6C,$E4,$94);

  ID_TUMLCBooleanSequenceBackwardIterator : TUMLCType =
    ($09,$64,$40,$18,$CB,$AC,$85,$4E,$A0,$9E,$B4,$E2,$7D,$48,$E2,$7C);

  // ---

type

//(* TUMLCIntegerSequenceForwardIterator *)
//
//  TUMLCIntegerSequenceForwardIterator = (* forward *) class;
//
//(* TUMLCIntegerSequenceBackwardIterator *)
//
//  TUMLCIntegerSequenceForwardIterator = (* forward *) class;

(* TUMLCIntegerSequence *)

  TUMLCIntegerSequence =
    class(TUMLCSimpleSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure InternalAssignFrom
      (const ASource: IUMLCInternalCloneableObject); override;

    function InternalNewSequence
      (): TUMLCSequence; override;

    function InternalNewItem
      (): Pointer; override;

    procedure InternalDropItem
      (var AItem: Pointer); override;

    function InternalCompareItems
      (const A: Pointer;
       const B: Pointer): TComparison; override;

    function InternalItemSize(): Integer; override;

    function AreEqualMembers
      (const A: Pointer;
       const B: Pointer): Boolean; override;

    function InternalGenerateForwardIterator
      (): TUMLCSequenceIterator; override;

    function InternalGenerateBackwardIterator
      (): TUMLCSequenceIterator; override;
  protected
    (* protected declarations *)

    function InternalScalarAverage(): Extended; override;
  public
    (* protected declarations *)

    function AggregateLowestValue(): Integer;

    function AggregateHighestValue(): Integer;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function NullOf(): TUMLCIntegerSequence;

    function NotNullOf(): TUMLCIntegerSequence;

    function FirstOf(): TUMLCIntegerSequence;

    function LastOf(): TUMLCIntegerSequence;

    function FirstOrNullOf(): TUMLCIntegerSequence;

    function LastOrNullOf(): TUMLCIntegerSequence;
  public
    (* public declarations *)

    function Extract(): Integer;

    procedure Delete();

    procedure Insert
      (const AItem: Integer);

    procedure AssignFrom
      (const ASource: TUMLCIntegerSequence); // nonvirtual;
    procedure AssignTo
      (var ADest: TUMLCIntegerSequence); // nonvirtual;

    function GenerateIterator
      (): TUMLCSequenceIterator; // nonvirtual;

    procedure ReleaseIterator
      (var AIterator: TUMLCSequenceIterator); // nonvirtual;
  end;

(* TUMLCIntegerSequenceForwardIterator *)

  TUMLCIntegerSequenceForwardIterator =
    class(TUMLCPointerListSequenceForwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Read: Integer;
  end;

(* TUMLCIntegerSequenceBackwardIterator *)

  TUMLCIntegerSequenceBackwardIterator =
    class(TUMLCPointerListSequenceBackwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Read: Integer;
  end;

//(* TUMLCDoubleSequenceForwardIterator *)
//
//  TUMLCDoubleSequenceForwardIterator = (* forward *) class;

(* TUMLCDoubleSequence *)

  TUMLCDoubleSequence =
    class(TUMLCSimpleSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure InternalAssignFrom
      (const ASource: IUMLCInternalCloneableObject); override;

    function InternalNewSequence
      (): TUMLCSequence; override;

    function InternalNewItem
      (): Pointer; override;

    procedure InternalDropItem
      (var AItem: Pointer); override;

    function InternalCompareItems
      (const A: Pointer;
       const B: Pointer): TComparison; override;

    function InternalItemSize(): Integer; override;

    function AreEqualMembers
      (const A: Pointer;
       const B: Pointer): Boolean; override;

    function InternalGenerateForwardIterator
      (): TUMLCSequenceIterator; override;

    function InternalGenerateBackwardIterator
      (): TUMLCSequenceIterator; override;
  protected
    (* protected declarations *)

    function InternalScalarAverage(): Extended; override;
  public
    (* protected declarations *)

    function AggregateLowestValue(): Double;

    function AggregateHighestValue(): Double;
  public
    (* public declarations *)

    function NullOf(): TUMLCDoubleSequence;

    function NotNullOf(): TUMLCDoubleSequence;

    function FirstOf(): TUMLCDoubleSequence;

    function LastOf(): TUMLCDoubleSequence;

    function FirstOrNullOf(): TUMLCDoubleSequence;

    function LastOrNullOf(): TUMLCDoubleSequence;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    //function IsEmpty(): Boolean; virtual;

    function Extract(): Double;

    procedure Delete();

    procedure Insert(const AItem: Double);

    procedure AssignFrom
      (const ASource: TUMLCDoubleSequence); // nonvirtual;
    procedure AssignTo
      (var ADest: TUMLCDoubleSequence); // nonvirtual;

    function GenerateIterator
      (): TUMLCSequenceIterator; // nonvirtual;

    function GenerateBackwardIterator
      (): TUMLCSequenceIterator; // nonvirtual;

    procedure ReleaseIterator
      (var AIterator: TUMLCSequenceIterator); // nonvirtual;
  end;

(* TUMLCDoubleSequenceForwardIterator *)

  TUMLCDoubleSequenceForwardIterator =
    class(TUMLCPointerListSequenceForwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Read: Double;
  end;

(* TUMLCDoubleSequenceBackwardIterator *)

  TUMLCDoubleSequenceBackwardIterator =
    class(TUMLCPointerListSequenceBackwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Read: Double;
  end;

//(* TUMLCBooleanSequenceIterator *)
//
//  TUMLCBooleanSequenceIterator = (* forward *) class;

(* TUMLCBooleanSequence *)

  TUMLCBooleanSequence =
    class(TUMLCSimpleSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure InternalAssignFrom
      (const ASource: IUMLCInternalCloneableObject); override;

    function InternalNewSequence
      (): TUMLCSequence; override;

    function InternalNewItem
      (): Pointer; override;

    procedure InternalDropItem
      (var AItem: Pointer); override;

    function InternalCompareItems
      (const A: Pointer;
       const B: Pointer): TComparison; override;

    function InternalItemSize(): Integer; override;

    function AreEqualMembers
      (const A: Pointer;
       const B: Pointer): Boolean; override;

    function InternalGenerateForwardIterator
      (): TUMLCSequenceIterator; override;

    function InternalGenerateBackwardIterator
      (): TUMLCSequenceIterator; override;
  public
    (* protected declarations *)

    function AggregateLowestValue(): Boolean;

    function AggregateHighestValue(): Boolean;
  public
    (* public declarations *)

    function NullOf(): TUMLCBooleanSequence;

    function NotNullOf(): TUMLCBooleanSequence;

    function FirstOf(): TUMLCBooleanSequence;

    function LastOf(): TUMLCBooleanSequence;

    function FirstOrNullOf(): TUMLCBooleanSequence;

    function LastOrNullOf(): TUMLCBooleanSequence;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    //function IsEmpty(): Boolean; virtual;

    function Extract(): Boolean;

    procedure Delete();

    procedure Insert(const AItem: Boolean);

    procedure AssignFrom
      (const ASource: TUMLCBooleanSequence); // nonvirtual;
    procedure AssignTo
      (var ADest: TUMLCBooleanSequence); // nonvirtual;

    function GenerateIterator
      (): TUMLCSequenceIterator; // nonvirtual;

    function GenerateBackwardIterator
      (): TUMLCSequenceIterator; // nonvirtual;

    procedure ReleaseIterator
      (var AIterator: TUMLCSequenceIterator); // nonvirtual;
  end;

(* TUMLCBooleanSequenceForwardIterator *)

  TUMLCBooleanSequenceForwardIterator =
    class(TUMLCPointerListSequenceForwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Read: Boolean;
  end;

(* TUMLCBooleanSequenceBackwardIterator *)

  TUMLCBooleanSequenceBackwardIterator =
    class(TUMLCPointerListSequenceBackwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function Read: Boolean;
  end;

implementation

(* TUMLCIntegerSequence *)

procedure TUMLCIntegerSequence.InternalAssignFrom
  (const ASource: IUMLCInternalCloneableObject);
var AIterator: TUMLCIntegerSequenceForwardIterator;
    AItem: Integer;

    ASeqIterator: TUMLCSequenceIterator;
begin
  AIterator :=
    (Self.GenerateIterator() as TUMLCIntegerSequenceForwardIterator);

  while (not AIterator.IsDone()) do
  begin
    AItem := AIterator.Read();
    Self.Insert(AItem);

    AIterator.MoveNext();
  end;

  ASeqIterator :=
    (AIterator as TUMLCSequenceIterator);
  (ASource as TUMLCIntegerSequence).ReleaseIterator(ASeqIterator);

  //(ASource as TUMLCIntegerSequence).ReleaseIterator(AIterator);
end;

function TUMLCIntegerSequence.InternalNewSequence
  (): TUMLCSequence;
begin
  Result := TUMLCIntegerSequence.Create();
end;

function TUMLCIntegerSequence.InternalNewItem
  (): Pointer;
var P: PInteger;
begin
  P :=
    New(PInteger);
  P^ := 0;

  Result := P;
end;

procedure TUMLCIntegerSequence.InternalDropItem
  (var AItem: Pointer);
var ADest: PInteger;
begin
  if (Assigned(AItem)) then
  begin
    ADest := AItem;
    AItem := nil;
    Dispose(ADest);
  end;
end;

function TUMLCIntegerSequence.InternalCompareItems
  (const A: Pointer;
   const B: Pointer): TComparison;
var C, D: PInteger;
begin
  C := PInteger(A);
  D := PInteger(B);

  if (C^ < D^)
    then Result := cmpLower
  else if (C^ > D^)
    then Result := cmpHigher
  else Result := cmpEqual;
end;

function TUMLCIntegerSequence.InternalItemSize(): Integer;
begin
  Result :=
    sizeof(Integer);
end;

function TUMLCIntegerSequence.AreEqualMembers
  (const A: Pointer;
   const B: Pointer): Boolean;
var C, D: ^Integer;
begin
  Result :=
    (Assigned(A) and Assigned(B));
  if (Result) then
  begin
    C := PInteger(A);
    D := PInteger(B);
    Result :=
      (C^ = D^)
  end;
end;

function TUMLCIntegerSequence.InternalGenerateForwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCIntegerSequenceForwardIterator;
begin
  AIterator :=
    TUMLCIntegerSequenceForwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

function TUMLCIntegerSequence.InternalGenerateBackwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCIntegerSequenceBackwardIterator;
begin
  AIterator :=
    TUMLCIntegerSequenceBackwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

function TUMLCIntegerSequence.InternalScalarAverage(): Extended;
var AItem: Integer; ASum: Extended;
    AIterator: TUMLCSequenceIterator;
begin
  Result := 0.0;

  ASum := 0.0;

  AIterator :=
    Self.InternalGenerateForwardIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCIntegerSequenceForwardIterator).Read();

    ASum := (ASum + AItem);

    AIterator.MoveNext();
  end;

  Self.InternalReleaseIterator(AIterator);

  Result :=
    (ASum / Self.ScalarCount());
end;

function TUMLCIntegerSequence.AggregateLowestValue(): Integer;
var AItem: PInteger;
begin
  Result := 0;

  AItem :=
    Self.InternalAggregateLowestValue();
  if (Assigned(AItem)) then
  begin
    Result :=
      AItem^;
  end;
end;

function TUMLCIntegerSequence.AggregateHighestValue(): Integer;
var AItem: PInteger;
begin
  Result := 0;

  AItem :=
    Self.InternalAggregateHighestValue();
  if (Assigned(AItem)) then
  begin
    Result :=
      AItem^;
  end;
end;

constructor TUMLCIntegerSequence.Create();
begin
  inherited Create();
  Self.FBaseType := umlcstdtypes.ID_Integer;

  System.GetMem(Self.FDefaultItemValue, Sizeof(Integer));
  System.FillChar(Self.FDefaultItemValue^, Sizeof(Integer), 0);
end;

destructor TUMLCIntegerSequence.Destroy();
begin
  System.FreeMem(Self.FDefaultItemValue, Sizeof(Integer));

  inherited Destroy();
end;

function TUMLCIntegerSequence.NullOf(): TUMLCIntegerSequence;
begin
  Result :=
    (Self.InternalNullOf() as TUMLCIntegerSequence);
end;

function TUMLCIntegerSequence.NotNullOf(): TUMLCIntegerSequence;
begin
  Result :=
    (Self.InternalNotNullOf() as TUMLCIntegerSequence);
end;

function TUMLCIntegerSequence.FirstOf(): TUMLCIntegerSequence;
begin
  Result :=
    (Self.InternalFirstOf() as TUMLCIntegerSequence);
end;

function TUMLCIntegerSequence.LastOf(): TUMLCIntegerSequence;
begin
  Result :=
    (Self.InternalLastOf() as TUMLCIntegerSequence);
end;

function TUMLCIntegerSequence.FirstOrNullOf(): TUMLCIntegerSequence;
begin
  Result :=
    (Self.InternalFirstOrNullOf() as TUMLCIntegerSequence);
end;

function TUMLCIntegerSequence.LastOrNullOf(): TUMLCIntegerSequence;
begin
  Result :=
    (Self.InternalLastOrNullOf() as TUMLCIntegerSequence);
end;

function TUMLCIntegerSequence.GenerateIterator
  (): TUMLCSequenceIterator;
begin
  Result :=
    (Self.InternalGenerateForwardIterator() as TUMLCIntegerSequenceForwardIterator);
end;

procedure TUMLCIntegerSequence.ReleaseIterator
  (var AIterator: TUMLCSequenceIterator);
begin
  // ...  as ...
  Self.InternalReleaseIterator(AIterator);
end;

function TUMLCIntegerSequence.Extract(): Integer;
var P: ^Integer;
begin
  P := FItems.Extract(FItems.Count - 1);
  Result := P^;
end;

procedure TUMLCIntegerSequence.Delete();
var P: ^Integer;
begin
  P := FItems.Extract(FItems.Count - 1);
  FreeMem(P, Sizeof(Integer));
end;

procedure TUMLCIntegerSequence.Insert
(const AItem: Integer);
var P: ^Integer;
begin
  GetMem(P, Sizeof(Integer));
  P^ := AItem;
  Self.InternalInsert(P);
end;

procedure TUMLCIntegerSequence.AssignFrom
  (const ASource: TUMLCIntegerSequence);
begin
  Self.InternalAssignFrom(ASource);
end;

procedure TUMLCIntegerSequence.AssignTo
  (var ADest: TUMLCIntegerSequence);
var D: IUMLCInternalCloneableObject;
begin
  D := (ADest as IUMLCInternalCloneableObject);
  Self.InternalAssignTo(D);
end;

(* TUMLCIntegerSequenceForwardIterator *)

function TUMLCIntegerSequenceForwardIterator.Read: Integer;
var P: ^Integer;
begin
  P := Self.InternalRead();
  Result := P^;
end;

(* TUMLCIntegerSequenceBackwardIterator *)

function TUMLCIntegerSequenceBackwardIterator.Read: Integer;
var P: ^Integer;
begin
  P := Self.InternalRead();
  Result := P^;
end;

(* TUMLCDoubleSequence *)

procedure TUMLCDoubleSequence.InternalAssignFrom
  (const ASource: IUMLCInternalCloneableObject);
var ASequence: TUMLCDoubleSequence;
    AIterator: TUMLCSequenceIterator;
    AItem: Double;
begin
  ASequence :=
    (ASource as TUMLCDoubleSequence);
  AIterator :=
    ASequence.GenerateIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCDoubleSequenceForwardIterator).Read();
    Self.Insert(AItem);

    AIterator.MoveNext();
  end;

  ASequence.ReleaseIterator(AIterator);
end;

function TUMLCDoubleSequence.InternalNewSequence
  (): TUMLCSequence;
begin
  Result := TUMLCDoubleSequence.Create();
end;

function TUMLCDoubleSequence.InternalNewItem
  (): Pointer;
var P: PDouble;
begin
  P :=
    New(PDouble);
  P^ := 0.0;

  Result := P;
end;

procedure TUMLCDoubleSequence.InternalDropItem
  (var AItem: Pointer);
var ADest: PDouble;
begin
  if (Assigned(AItem)) then
  begin
    ADest := AItem;
    AItem := nil;
    Dispose(ADest);
  end;
end;

function TUMLCDoubleSequence.InternalCompareItems
  (const A: Pointer;
   const B: Pointer): TComparison;
var C, D: PDouble;
begin
  C := PDouble(A);
  D := PDouble(B);

  if (C^ < D^)
    then Result := cmpLower
  else if (C^ > D^)
    then Result := cmpHigher
  else Result := cmpEqual;
end;

function TUMLCDoubleSequence.InternalItemSize(): Integer;
begin
  Result :=
    sizeof(Double);
end;

function TUMLCDoubleSequence.AreEqualMembers
  (const A: Pointer;
   const B: Pointer): Boolean;
var C, D: PDouble;
begin
  Result :=
    (Assigned(A) and Assigned(B));
  if (Result) then
  begin
    C := PDouble(A);
    D := PDouble(B);
    Result :=
      (C^ = D^)
  end;
end;

function TUMLCDoubleSequence.InternalGenerateForwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCDoubleSequenceForwardIterator;
begin
  AIterator :=
    TUMLCDoubleSequenceForwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

function TUMLCDoubleSequence.InternalGenerateBackwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCDoubleSequenceBackwardIterator;
begin
  AIterator :=
    TUMLCDoubleSequenceBackwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

function TUMLCDoubleSequence.InternalScalarAverage(): Extended;
var AItem: Double; ASum: Extended;
    AIterator: TUMLCSequenceIterator;
begin
  Result := 0.0;

  ASum := 0.0;

  AIterator :=
    Self.InternalGenerateForwardIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCDoubleSequenceForwardIterator).Read();

    ASum := (ASum + AItem);

    AIterator.MoveNext();
  end;

  Self.InternalReleaseIterator(AIterator);

  Result :=
    (ASum / Self.ScalarCount());
end;

function TUMLCDoubleSequence.AggregateLowestValue(): Double;
var AItem: PDouble;
begin
  Result := 0.0;

  AItem :=
    Self.InternalAggregateLowestValue();
  if (Assigned(AItem)) then
  begin
    Result :=
      AItem^;
  end;
end;

function TUMLCDoubleSequence.AggregateHighestValue(): Double;
var AItem: PDouble;
begin
  Result := 0.0;

  AItem :=
    Self.InternalAggregateHighestValue();
  if (Assigned(AItem)) then
  begin
    Result :=
      AItem^;
  end;
end;

function TUMLCDoubleSequence.NullOf(): TUMLCDoubleSequence;
begin
  Result :=
    (Self.InternalNullOf() as TUMLCDoubleSequence);
end;

function TUMLCDoubleSequence.NotNullOf(): TUMLCDoubleSequence;
begin
  Result :=
    (Self.InternalNotNullOf() as TUMLCDoubleSequence);
end;

function TUMLCDoubleSequence.FirstOf(): TUMLCDoubleSequence;
begin
  Result :=
    (Self.InternalFirstOf() as TUMLCDoubleSequence);
end;

function TUMLCDoubleSequence.LastOf(): TUMLCDoubleSequence;
begin
  Result :=
    (Self.InternalLastOf() as TUMLCDoubleSequence);
end;

function TUMLCDoubleSequence.FirstOrNullOf(): TUMLCDoubleSequence;
begin
  Result :=
    (Self.InternalFirstOrNullOf() as TUMLCDoubleSequence);
end;

function TUMLCDoubleSequence.LastOrNullOf(): TUMLCDoubleSequence;
begin
  Result :=
    (Self.InternalLastOrNullOf() as TUMLCDoubleSequence);
end;

constructor TUMLCDoubleSequence.Create();
begin
  inherited Create();
  Self.FBaseType := umlcstdtypes.ID_Double;

  System.GetMem(Self.FDefaultItemValue, Sizeof(Double));
  System.FillChar(Self.FDefaultItemValue^, Sizeof(Double), 0);
end;

destructor TUMLCDoubleSequence.Destroy();
begin
  System.FreeMem(Self.FDefaultItemValue, Sizeof(Double));

  inherited Destroy();
end;

function TUMLCDoubleSequence.Extract(): Double;
var P: ^Double;
begin
  P := FItems.Extract(FItems.Count - 1);
  Result := P^;
end;

procedure TUMLCDoubleSequence.Delete();
var P: ^Double;
begin
  P := FItems.Extract(FItems.Count - 1);
  FreeMem(P, Sizeof(Double));
end;

procedure TUMLCDoubleSequence.Insert(const AItem: Double);
var P: ^Double;
begin
  GetMem(P, Sizeof(Double));
  P^ := AItem;
  Self.InternalInsert(P);
end;

procedure TUMLCDoubleSequence.AssignFrom
  (const ASource: TUMLCDoubleSequence);
begin
  Self.InternalAssignFrom(ASource);
end;

procedure TUMLCDoubleSequence.AssignTo
  (var ADest: TUMLCDoubleSequence);
var D: IUMLCInternalCloneableObject;
begin
  D := (ADest as IUMLCInternalCloneableObject);
  Self.InternalAssignTo(D);
end;

function TUMLCDoubleSequence.GenerateIterator
  (): TUMLCSequenceIterator;
begin
  Result :=
    (Self.InternalGenerateForwardIterator() as TUMLCSequenceIterator);
end;

function TUMLCDoubleSequence.GenerateBackwardIterator
  (): TUMLCSequenceIterator;
begin
  Result :=
    (Self.InternalGenerateBackwardIterator() as TUMLCSequenceIterator);
end;

procedure TUMLCDoubleSequence.ReleaseIterator
  (var AIterator: TUMLCSequenceIterator);
begin
  // ...  as ...
  Self.InternalReleaseIterator(AIterator);
end;

(* TUMLCDoubleSequenceForwardIterator *)

function TUMLCDoubleSequenceForwardIterator.Read: Double;
var P: ^Double;
begin
  P := Self.InternalRead();
  Result := P^;
end;

(* TUMLCDoubleSequenceBackwardIterator *)

function TUMLCDoubleSequenceBackwardIterator.Read: Double;
var P: ^Double;
begin
  P := Self.InternalRead();
  Result := P^;
end;

(* TUMLCBooleanSequence *)

procedure TUMLCBooleanSequence.InternalAssignFrom
  (const ASource: IUMLCInternalCloneableObject);
var ASequence: TUMLCBooleanSequence;
    AIterator: TUMLCSequenceIterator;
    AItem: Boolean;
begin
  ASequence :=
    (ASource as TUMLCBooleanSequence);
  AIterator :=
    ASequence.GenerateIterator();

  while (not AIterator.IsDone()) do
  begin
    AItem :=
      (AIterator as TUMLCBooleanSequenceForwardIterator).Read();
    Self.Insert(AItem);

    AIterator.MoveNext();
  end;

  ASequence.ReleaseIterator(AIterator);
end;

function TUMLCBooleanSequence.InternalNewSequence
  (): TUMLCSequence;
begin
  Result := TUMLCBooleanSequence.Create();
end;

function TUMLCBooleanSequence.InternalNewItem
  (): Pointer;
var P: PBoolean;
begin
  P :=
    New(PBoolean);
  P^ := false;

  Result := P;
end;

procedure TUMLCBooleanSequence.InternalDropItem
  (var AItem: Pointer);
var ADest: PBoolean;
begin
  if (Assigned(AItem)) then
  begin
    ADest := AItem;
    AItem := nil;
    Dispose(ADest);
  end;
end;

function TUMLCBooleanSequence.InternalItemSize(): Integer;
begin
  Result :=
    sizeof(Boolean);
end;

function TUMLCBooleanSequence.InternalCompareItems
  (const A: Pointer;
   const B: Pointer): TComparison;
var C, D: PBoolean;
begin
  C := PBoolean(A);
  D := PBoolean(B);

  if (C^ < D^)
    then Result := cmpLower
  else if (C^ > D^)
    then Result := cmpHigher
  else Result := cmpEqual;
end;

function TUMLCBooleanSequence.AreEqualMembers
  (const A: Pointer;
   const B: Pointer): Boolean;
var C, D: PBoolean;
begin
  Result :=
    (Assigned(A) and Assigned(B));
  if (Result) then
  begin
    C := PBoolean(A);
    D := PBoolean(B);
    Result :=
      (C^ = D^)
  end;
end;

function TUMLCBooleanSequence.InternalGenerateForwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCBooleanSequenceForwardIterator;
begin
  AIterator :=
    TUMLCBooleanSequenceForwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

function TUMLCBooleanSequence.InternalGenerateBackwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCBooleanSequenceBackwardIterator;
begin
  AIterator :=
    TUMLCBooleanSequenceBackwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

function TUMLCBooleanSequence.AggregateLowestValue(): Boolean;
var AItem: PBoolean;
begin
  Result := false;

  AItem :=
    Self.InternalAggregateLowestValue();
  if (Assigned(AItem)) then
  begin
    Result :=
      AItem^;
  end;
end;

function TUMLCBooleanSequence.AggregateHighestValue(): Boolean;
var AItem: PBoolean;
begin
  Result := false;

  AItem :=
    Self.InternalAggregateHighestValue();
  if (Assigned(AItem)) then
  begin
    Result :=
      AItem^;
  end;
end;

function TUMLCBooleanSequence.NullOf(): TUMLCBooleanSequence;
begin
  Result :=
    (Self.InternalNullOf() as TUMLCBooleanSequence);
end;

function TUMLCBooleanSequence.NotNullOf(): TUMLCBooleanSequence;
begin
  Result :=
    (Self.InternalNotNullOf() as TUMLCBooleanSequence);
end;

function TUMLCBooleanSequence.FirstOf(): TUMLCBooleanSequence;
begin
  Result :=
    (Self.InternalFirstOf() as TUMLCBooleanSequence);
end;

function TUMLCBooleanSequence.LastOf(): TUMLCBooleanSequence;
begin
  Result :=
    (Self.InternalLastOf() as TUMLCBooleanSequence);
end;

function TUMLCBooleanSequence.FirstOrNullOf(): TUMLCBooleanSequence;
begin
  Result :=
    (Self.InternalFirstOrNullOf() as TUMLCBooleanSequence);
end;

function TUMLCBooleanSequence.LastOrNullOf(): TUMLCBooleanSequence;
begin
  Result :=
    (Self.InternalLastOrNullOf() as TUMLCBooleanSequence);
end;

constructor TUMLCBooleanSequence.Create();
begin
  inherited Create();
  Self.FBaseType := umlcstdtypes.ID_Boolean;

  System.GetMem(Self.FDefaultItemValue, Sizeof(Boolean));
  System.FillChar(Self.FDefaultItemValue^, Sizeof(Boolean), 0);
end;

destructor TUMLCBooleanSequence.Destroy();
begin
  System.FreeMem(Self.FDefaultItemValue, Sizeof(Boolean));

  inherited Destroy();
end;

function TUMLCBooleanSequence.Extract(): Boolean;
var P: ^Boolean;
begin
  P := FItems.Extract(FItems.Count - 1);
  Result := P^;
end;

procedure TUMLCBooleanSequence.Delete();
var P: ^Boolean;
begin
  P := FItems.Extract(FItems.Count - 1);
  FreeMem(P, Sizeof(Boolean));
end;

procedure TUMLCBooleanSequence.Insert(const AItem: Boolean);
var P: ^Boolean;
begin
  GetMem(P, Sizeof(Boolean));
  P^ := AItem;
  Self.InternalInsert(P);
end;

procedure TUMLCBooleanSequence.AssignFrom
  (const ASource: TUMLCBooleanSequence);
begin
  Self.InternalAssignFrom(ASource);
end;

procedure TUMLCBooleanSequence.AssignTo
  (var ADest: TUMLCBooleanSequence);
var D: IUMLCInternalCloneableObject;
begin
  D := (ADest as IUMLCInternalCloneableObject);
  Self.InternalAssignTo(D);
end;

function TUMLCBooleanSequence.GenerateIterator
  (): TUMLCSequenceIterator;
begin
  Result :=
    (Self.InternalGenerateForwardIterator() as TUMLCSequenceIterator);
end;

function TUMLCBooleanSequence.GenerateBackwardIterator
  (): TUMLCSequenceIterator;
begin
  Result :=
    (Self.InternalGenerateBackwardIterator() as TUMLCSequenceIterator);
end;

procedure TUMLCBooleanSequence.ReleaseIterator
  (var AIterator: TUMLCSequenceIterator);
begin
  // ...  as ...
  Self.InternalReleaseIterator(AIterator);
end;

(* TUMLCBooleanSequenceForwardIterator *)

function TUMLCBooleanSequenceForwardIterator.Read: Boolean;
var P: ^Boolean;
begin
  P := Self.InternalRead();
  Result := P^;
end;

(* TUMLCBooleanSequenceBackwardIterator *)

function TUMLCBooleanSequenceBackwardIterator.Read: Boolean;
var P: ^Boolean;
begin
  P := Self.InternalRead();
  Result := P^;
end;

end.

