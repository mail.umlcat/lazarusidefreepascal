(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctuplesequences;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlctypes, umlcstdtypes,
  umlcnormobjects,
  umlclists,
  umlccollsenums,
  umlciterators,
  umlcsequences,
  dummy;

const

  // ---

  IDTUMLCTupleSequence : TUMLCType =
    ($DD,$6D,$39,$21,$DD,$DF,$6E,$44,$88,$68,$A5,$3E,$CA,$CB,$FD,$10);

  // ---


type

(* TUMLCTupleSequence *)

  TUMLCTupleSequence = class(TUMLCSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    //function IsEmpty(): Boolean; virtual;

    //procedure Clear(); virtual;
  end;

implementation

end.

