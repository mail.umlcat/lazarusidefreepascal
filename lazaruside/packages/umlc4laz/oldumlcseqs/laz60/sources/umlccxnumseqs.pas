        (**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccxnumseqs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcxnums,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  umlcsequences,
  umlcsimplesequences,
  dummy;

const

  // ---

  ID_TUMLCIntegerComplexNumberSequence : TUMLCType =
    ($54,$69,$0B,$8B,$73,$EA,$CB,$4F,$93,$9F,$2A,$2A,$27,$6F,$C5,$34);

  ID_TUMLCIntegerComplexNumberSequenceForwardIterator : TUMLCType =
    ($6C,$A1,$5A,$0F,$9A,$DA,$04,$4F,$A4,$04,$56,$3A,$2B,$1D,$DC,$1A);

  ID_TUMLCIntegerComplexNumberSequenceBackwardIterator : TUMLCType =
    ($DE,$D2,$07,$22,$DB,$32,$87,$4C,$B8,$7C,$24,$15,$BB,$F4,$40,$FF);

  // ---


  ID_TUMLCDoubleComplexNumberSequenceIterator : TUMLCType =
    ($90,$E1,$AA,$D9,$74,$65,$2D,$45,$93,$07,$E5,$26,$59,$76,$0A,$27);

  ID_TUMLCDoubleComplexNumberSequenceForwardIterator : TUMLCType =
    ($8E,$A2,$D7,$BD,$6D,$C7,$12,$44,$A6,$A7,$66,$46,$05,$36,$D6,$83);

  ID_TUMLCDoubleComplexNumberSequenceBackwardIterator : TUMLCType =
    ($4B,$EF,$B5,$C2,$1C,$DD,$4A,$4F,$B4,$53,$E9,$E9,$67,$77,$B2,$C7);

  // ---

  ID_TUMLCTExtendedComplexNumberSequenceIterator : TUMLCType =
    ($2A,$E8,$21,$AF,$09,$C6,$7C,$44,$BF,$C5,$3D,$97,$79,$E2,$86,$4A);

  ID_TUMLCTExtendedComplexNumberSequenceForwardIterator : TUMLCType =
    ($2B,$4D,$02,$F5,$7A,$D3,$59,$46,$B7,$54,$E8,$54,$31,$EE,$D2,$42);

  ID_TUMLCExtendedComplexNumberSequenceBackwardIterator : TUMLCType =
    ($FF,$CC,$52,$60,$69,$69,$52,$47,$89,$AC,$73,$FE,$B7,$A1,$A4,$F7);

  // ---


type

//(* TUMLCIntegerComplexNumberSequenceForwardIterator *)
//
//  TUMLCIntegerComplexNumberSequenceForwardIterator = (* forward *) class;
//
//(* TUMLCIntegerComplexNumberSequenceBackwardIterator *)
//
//  TUMLCIntegerComplexNumberSequenceForwardIterator = (* forward *) class;

(* TUMLCIntegerComplexNumberSequence *)

  TUMLCIntegerComplexNumberSequence =
    class(TUMLCSimpleSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure InternalAssignFrom
      (const ASource: IUMLCInternalCloneableObject); override;
  protected
    (* protected declarations *)

    function InternalNewSequence
      (): TUMLCSequence; override;

    function InternalNewItem
      (): Pointer; override;

    procedure InternalDropItem
      (var AItem: Pointer); override;

    function InternalCompareItems
      (const A: Pointer;
       const B: Pointer): TComparison; override;

    function InternalItemSize(): Integer; override;

    function AreEqualMembers
      (const A: Pointer;
       const B: Pointer): Boolean; override;

    function InternalGenerateForwardIterator
      (): TUMLCSequenceIterator; override;

    function InternalGenerateBackwardIterator
      (): TUMLCSequenceIterator; override;
  protected
    (* protected declarations *)

    //function InternalScalarAverage(): Extended; override;
  public
    (* public declarations *)

    constructor Create(); override;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function NullOf(): TUMLCIntegerComplexNumberSequence;

    function NotNullOf(): TUMLCIntegerComplexNumberSequence;

    function FirstOf(): TUMLCIntegerComplexNumberSequence;

    function LastOf(): TUMLCIntegerComplexNumberSequence;

    function FirstOrNullOf(): TUMLCIntegerComplexNumberSequence;

    function LastOrNullOf(): TUMLCIntegerComplexNumberSequence;
  public
    (* public declarations *)

    procedure Extract
     (out ADest: PUMLCIntegerComplexNumber);

    procedure Delete();

    procedure Insert
     (const AItem: TUMLCIntegerComplexNumber);

    procedure AssignFrom
      (const ASource: TUMLCIntegerComplexNumberSequence); // nonvirtual;
    procedure AssignTo
      (var ADest: TUMLCIntegerComplexNumberSequence); // nonvirtual;

    function GenerateIterator
      (): TUMLCSequenceIterator; // nonvirtual;

    procedure ReleaseIterator
      (var AIterator: TUMLCSequenceIterator); // nonvirtual;
  end;

(* TUMLCIntegerComplexNumberSequenceForwardIterator *)

  TUMLCIntegerComplexNumberSequenceForwardIterator =
    class(TUMLCPointerListSequenceForwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    procedure Read
     (out ADest: TUMLCIntegerComplexNumber);
  end;

(* TUMLCIntegerComplexNumberSequenceBackwardIterator *)

  TUMLCIntegerComplexNumberSequenceBackwardIterator =
    class(TUMLCPointerListSequenceBackwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    procedure Read
     (out ADest: TUMLCIntegerComplexNumber);
  end;

  //(* TUMLCDoubleComplexNumberSequenceForwardIterator *)
  //
  //  TUMLCDoubleComplexNumberSequenceForwardIterator = (* forward *) class;
  //
  //(* TUMLCDoubleComplexNumberSequenceBackwardIterator *)
  //
  //  TUMLCDoubleComplexNumberSequenceForwardIterator = (* forward *) class;

(* TUMLCDoubleComplexNumberSequence *)

  TUMLCDoubleComplexNumberSequence =
    class(TUMLCSimpleSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    //procedure InternalAssignFrom
    //  (const ASource: IUMLCInternalCloneableObject); override;
    //
    //function InternalNewSequence
    //  (): TUMLCSequence; override;
    //
    //function InternalNewItem
    //  (): Pointer; override;
    //
    //procedure InternalDropItem
    //  (var AItem: Pointer); override;
    //
    //function InternalCompareItems
    //  (const A: Pointer;
    //   const B: Pointer): TComparison; override;
    //
    //function InternalItemSize(): Double; override;
    //
    //function AreEqualMembers
    //  (const A: Pointer;
    //   const B: Pointer): Boolean; override;
    //
    //function InternalGenerateForwardIterator
    //  (): TUMLCSequenceIterator; override;
    //
    //function InternalGenerateBackwardIterator
    //  (): TUMLCSequenceIterator; override;
  protected
    (* protected declarations *)

    //function InternalScalarAverage(): Extended; override;
  public
    (* public declarations *)

    //constructor Create(); override;
  public
    (* public declarations *)

    //procedure Extract
    // (out ADest: PUMLCDoubleComplexNumber);
    //
    //procedure Delete();
    //
    //procedure Insert
    // (const AItem: TUMLCDoubleComplexNumber);
    //
    //procedure AssignFrom
    //  (const ASource: TUMLCDoubleComplexNumberSequence); // nonvirtual;
    //procedure AssignTo
    //  (var ADest: TUMLCDoubleComplexNumberSequence); // nonvirtual;
    //
    //function GenerateIterator
    //  (): TUMLCSequenceIterator; // nonvirtual;
    //
    //procedure ReleaseIterator
    //  (var AIterator: TUMLCSequenceIterator); // nonvirtual;
  end;

(* TUMLCDoubleComplexNumberSequenceForwardIterator *)

  TUMLCDoubleComplexNumberSequenceForwardIterator =
    class(TUMLCPointerListSequenceForwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    //procedure Read
    // (out ADest: PUMLCDoubleComplexNumber);
  end;

(* TUMLCDoubleComplexNumberSequenceBackwardIterator *)

  TUMLCDoubleComplexNumberSequenceBackwardIterator =
    class(TUMLCPointerListSequenceBackwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    //procedure Read
    // (out ADest: PUMLCDoubleComplexNumber);
  end;


  //(* TUMLCExtendedComplexNumberSequenceForwardIterator *)
  //
  //  TUMLCExtendedComplexNumberSequenceForwardIterator = (* forward *) class;
  //
  //(* TUMLCExtendedComplexNumberSequenceBackwardIterator *)
  //
  //  TUMLCExtendedComplexNumberSequenceForwardIterator = (* forward *) class;

(* TUMLCExtendedComplexNumberSequence *)

  TUMLCExtendedComplexNumberSequence =
    class(TUMLCSimpleSequence)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    //procedure InternalAssignFrom
    //  (const ASource: IUMLCInternalCloneableObject); override;
    //
    //function InternalNewSequence
    //  (): TUMLCSequence; override;
    //
    //function InternalNewItem
    //  (): Pointer; override;
    //
    //procedure InternalDropItem
    //  (var AItem: Pointer); override;
    //
    //function InternalCompareItems
    //  (const A: Pointer;
    //   const B: Pointer): TComparison; override;
    //
    //function InternalItemSize(): Extended; override;
    //
    //function AreEqualMembers
    //  (const A: Pointer;
    //   const B: Pointer): Boolean; override;
    //
    //function InternalGenerateForwardIterator
    //  (): TUMLCSequenceIterator; override;
    //
    //function InternalGenerateBackwardIterator
    //  (): TUMLCSequenceIterator; override;
  protected
    (* protected declarations *)

    //function InternalScalarAverage(): Extended; override;
  public
    (* public declarations *)

    //constructor Create(); override;
  public
    (* public declarations *)

    //procedure Extract
    // (out ADest: PUMLCExtendedComplexNumber);
    //
    //procedure Delete();
    //
    //procedure Insert
    // (const AItem: TUMLCExtendedComplexNumber);
    //
    //procedure AssignFrom
    //  (const ASource: TUMLCExtendedComplexNumberSequence); // nonvirtual;
    //procedure AssignTo
    //  (var ADest: TUMLCExtendedComplexNumberSequence); // nonvirtual;
    //
    //function GenerateIterator
    //  (): TUMLCSequenceIterator; // nonvirtual;
    //
    //procedure ReleaseIterator
    //  (var AIterator: TUMLCSequenceIterator); // nonvirtual;
  end;

(* TUMLCExtendedComplexNumberSequenceForwardIterator *)

  TUMLCExtendedComplexNumberSequenceForwardIterator =
    class(TUMLCPointerListSequenceForwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    //procedure Read
    // (out ADest: PUMLCExtendedComplexNumber);
  end;

(* TUMLCExtendedComplexNumberSequenceBackwardIterator *)

  TUMLCExtendedComplexNumberSequenceBackwardIterator =
    class(TUMLCPointerListSequenceBackwardIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    //procedure Read
    // (out ADest: PUMLCExtendedComplexNumber);
  end;

implementation

(* TUMLCIntegerComplexNumberSequence *)

procedure TUMLCIntegerComplexNumberSequence.InternalAssignFrom
  (const ASource: IUMLCInternalCloneableObject);
var AIterator: TUMLCIntegerComplexNumberSequenceForwardIterator;

    AItem:    TUMLCIntegerComplexNumber;

    ASeqIterator: TUMLCSequenceIterator;
begin
  AIterator :=
  (Self.InternalGenerateForwardIterator() as TUMLCIntegerComplexNumberSequenceForwardIterator);

  while (not AIterator.IsDone()) do
  begin
    AIterator.Read(AItem);
    Self.Insert(AItem);

    AIterator.MoveNext();
  end;

  ASeqIterator :=
    (AIterator as TUMLCSequenceIterator);
  (ASource as TUMLCIntegerComplexNumberSequence).ReleaseIterator(ASeqIterator);

  //(ASource as TUMLCIntegerComplexNumberSequence).ReleaseIterator(AIterator);
end;

function TUMLCIntegerComplexNumberSequence.InternalNewSequence
  (): TUMLCSequence;
begin
  Result := TUMLCIntegerComplexNumberSequence.Create();
end;

function TUMLCIntegerComplexNumberSequence.InternalNewItem
  (): Pointer;
var P: PUMLCIntegerComplexNumber;
begin
  P :=
    New(PUMLCIntegerComplexNumber);
  P^.Imaginary := 0;
  P^.Real := 0;

  Result := P;
end;

procedure TUMLCIntegerComplexNumberSequence.InternalDropItem
  (var AItem: Pointer);
var ADest: PUMLCIntegerComplexNumber;
begin
  if (Assigned(AItem)) then
  begin
    ADest := AItem;
    AItem := nil;
    Dispose(ADest);
  end;
end;

function TUMLCIntegerComplexNumberSequence.InternalCompareItems
  (const A: Pointer;
   const B: Pointer): TComparison;
//var C, D: PUMLCIntegerComplexNumber;
begin
  Result := cmpEqual;

  //C := PUMLCIntegerComplexNumber(A);
  //D := PUMLCIntegerComplexNumber(B);
  //
  //if (C^ < D^)
  //  then Result := cmpLower
  //else if (C^ > D^)
  //  then Result := cmpHigher
  //else Result := cmpEqual;
end;

function TUMLCIntegerComplexNumberSequence.InternalItemSize(): Integer;
begin
  Result :=
    sizeof(TUMLCIntegerComplexNumber);
end;

function TUMLCIntegerComplexNumberSequence.AreEqualMembers
  (const A: Pointer;
   const B: Pointer): Boolean;
var C, D: PUMLCIntegerComplexNumber;
begin
  Result :=
    (Assigned(A) and Assigned(B));
  if (Result) then
  begin
    C := PUMLCIntegerComplexNumber(A);
    D := PUMLCIntegerComplexNumber(B);
    Result :=
      IntCplxAreEqual(C^, D^)
  end;
end;

function TUMLCIntegerComplexNumberSequence.InternalGenerateForwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCIntegerComplexNumberSequenceForwardIterator;
begin
  AIterator :=
    TUMLCIntegerComplexNumberSequenceForwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

function TUMLCIntegerComplexNumberSequence.InternalGenerateBackwardIterator(): TUMLCSequenceIterator;
var AIterator: TUMLCIntegerComplexNumberSequenceBackwardIterator;
begin
  AIterator :=
    TUMLCIntegerComplexNumberSequenceBackwardIterator.Create();
  AIterator.Sequence := Self;
  Self.InternalPrepareIterator();

  AIterator.Start();

  Result :=
    AIterator;
end;

constructor TUMLCIntegerComplexNumberSequence.Create();
begin
  inherited Create();
  Self.FBaseType :=
    ID_TUMLCIntegerComplexNumberSequence;

  System.GetMem
    (Self.FDefaultItemValue, Sizeof(TUMLCIntegerComplexNumber));
  System.FillChar
    (Self.FDefaultItemValue^, Sizeof(TUMLCIntegerComplexNumber), 0);
end;

destructor TUMLCIntegerComplexNumberSequence.Destroy();
begin
  System.FreeMem
    (Self.FDefaultItemValue, Sizeof(TUMLCIntegerComplexNumber));

  inherited Destroy();
end;

function TUMLCIntegerComplexNumberSequence.NullOf(): TUMLCIntegerComplexNumberSequence;
begin
  Result :=
    (Self.InternalNullOf() as TUMLCIntegerComplexNumberSequence);
end;

function TUMLCIntegerComplexNumberSequence.NotNullOf(): TUMLCIntegerComplexNumberSequence;
begin
  Result :=
    (Self.InternalNotNullOf() as TUMLCIntegerComplexNumberSequence);
end;

function TUMLCIntegerComplexNumberSequence.FirstOf(): TUMLCIntegerComplexNumberSequence;
begin
  Result :=
    (Self.InternalFirstOf() as TUMLCIntegerComplexNumberSequence);
end;

function TUMLCIntegerComplexNumberSequence.LastOf(): TUMLCIntegerComplexNumberSequence;
begin
  Result :=
    (Self.InternalLastOf() as TUMLCIntegerComplexNumberSequence);
end;

function TUMLCIntegerComplexNumberSequence.FirstOrNullOf(): TUMLCIntegerComplexNumberSequence;
begin
  Result :=
    (Self.InternalFirstOrNullOf() as TUMLCIntegerComplexNumberSequence);
end;

function TUMLCIntegerComplexNumberSequence.LastOrNullOf(): TUMLCIntegerComplexNumberSequence;
begin
  Result :=
    (Self.InternalLastOrNullOf() as TUMLCIntegerComplexNumberSequence);
end;

procedure TUMLCIntegerComplexNumberSequence.Extract
 (out ADest: PUMLCIntegerComplexNumber);
begin
  ADest :=
    FItems.Extract(FItems.Count - 1);
end;

procedure TUMLCIntegerComplexNumberSequence.Delete();
var P: ^Integer;
begin
  P := FItems.Extract(FItems.Count - 1);
  FreeMem(P, Sizeof(TUMLCIntegerComplexNumber));
end;

procedure TUMLCIntegerComplexNumberSequence.Insert
  (const AItem: TUMLCIntegerComplexNumber);
var P: PUMLCIntegerComplexNumber;
begin
  System.GetMem(P, Sizeof(TUMLCIntegerComplexNumber));
  System.Move(AItem, P^, Sizeof(TUMLCIntegerComplexNumber));

  Self.InternalInsert(P);
end;

procedure TUMLCIntegerComplexNumberSequence.AssignFrom
  (const ASource: TUMLCIntegerComplexNumberSequence);
begin
  Self.InternalAssignFrom(ASource);
end;

procedure TUMLCIntegerComplexNumberSequence.AssignTo
  (var ADest: TUMLCIntegerComplexNumberSequence);
var D: IUMLCInternalCloneableObject;
begin
  D := (ADest as IUMLCInternalCloneableObject);
  Self.InternalAssignTo(D);
end;

function TUMLCIntegerComplexNumberSequence.GenerateIterator
  (): TUMLCSequenceIterator;
begin
  Result :=
    (Self.InternalGenerateForwardIterator() as TUMLCIntegerComplexNumberSequenceForwardIterator);
end;

procedure TUMLCIntegerComplexNumberSequence.ReleaseIterator
  (var AIterator: TUMLCSequenceIterator);
begin
  // ...  as ...
  Self.InternalReleaseIterator(AIterator);
end;


(* TUMLCIntegerComplexNumberSequenceForwardIterator *)

procedure TUMLCIntegerComplexNumberSequenceForwardIterator.Read
 (out ADest: TUMLCIntegerComplexNumber);
var P: PUMLCIntegerComplexNumber;
begin
  P := Self.InternalRead();
  IntCplxAssign(ADest, P^);
end;

(* TUMLCIntegerComplexNumberSequenceBackwardIterator *)

procedure TUMLCIntegerComplexNumberSequenceBackwardIterator.Read
 (out ADest: TUMLCIntegerComplexNumber);
var P: PUMLCIntegerComplexNumber;
begin
  P := Self.InternalRead();
  IntCplxAssign(ADest, P^);
end;



end.

