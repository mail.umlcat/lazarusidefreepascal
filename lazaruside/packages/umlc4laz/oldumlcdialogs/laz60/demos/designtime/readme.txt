readme.txt
==========

The "umlcdialogs" folder contains the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This package contains several generic commonly used dialogs,
some of them are encapsulated as components.

