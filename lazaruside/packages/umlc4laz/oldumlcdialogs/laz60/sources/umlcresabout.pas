unit umlcresabout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  dummy;

{$include 'umlcdialogs_language.inc'}

{$IFDEF umlcdialogs_language_english}
resourcestring
  resExitButton_Caption = 'Exit';
  resSummaryTabSheet_Caption = 'Summary';
  resCopyrightTabSheet_Caption = 'Copyright';
{$ENDIF}

{$IFDEF umlcdialogs_language_spanisheurope}
resourcestring
  resExitButton_Caption = 'Salir';
  resSummaryTabSheet_Caption = 'Descripcion';
  resAuthorsTabSheet_Caption = 'Copyright';
{$ENDIF}

{$IFDEF umlcdialogs_language_spanishlatam}
resourcestring
  resExitButton_Caption = 'Salir';
  resSummaryTabSheet_Caption = 'Descripcion';
  resAuthorsTabSheet_Caption = 'Copyright';
{$ENDIF}

implementation

end.

