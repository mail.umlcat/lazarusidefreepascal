(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit ufrminputstrings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ComCtrls,
  StdCtrls,
  {$IFDEF FPC}
  LResources,
  {$ENDIF}
  umlcmsgdlgtypes,
  umlcmsgdlgstrs,
  dummy;

type

  { TfrmInputStrings }

  TfrmInputStrings = class(TForm)
    AnswerMemo: TMemo;
    btnCancel: TButton;
    btnOK: TButton;
    lblLabel: TLabel;
    MainStatusBar: TStatusBar;
    procedure btnCancelClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }

    Option:  TMsgDlgButton;
  end;

var
  frmInputStrings: TfrmInputStrings;

implementation

{$R *.lfm}

{ TfrmInputStrings }

procedure TfrmInputStrings.FormCreate(Sender: TObject);
begin
  btnOK.Caption     := ButtonsCaptionsArray[mbOK];
  btnCancel.Caption := ButtonsCaptionsArray[mbCancel];
end;

procedure TfrmInputStrings.btnOKClick(Sender: TObject);
begin
  Option := mbOK;
end;

procedure TfrmInputStrings.btnCancelClick(Sender: TObject);
begin
  Option := mbCancel;
end;

procedure TfrmInputStrings.FormShow(Sender: TObject);
begin
  //
end;

end.

