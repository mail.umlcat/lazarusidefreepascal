(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

(* "umlcdialogs_language.inc" *)

{$UNDEF umlcdialogs_language_english}
{$UNDEF umlcdialogs_language_spanisheurope}
{$UNDEF umlcdialogs_language_spanishlatam}
{$UNDEF umlcdialogs_language_french}
{$UNDEF umlcdialogs_language_german}
{$UNDEF umlcdialogs_language_portuguese}
{$UNDEF umlcdialogs_language_italian}

{$DEFINE umlcdialogs_language_english}
{.DEFINE umlcdialogs_language_spanisheurope}
{.DEFINE umlcdialogs_language_spanishlatam}
{.DEFINE umlcdialogs_language_french}
{.DEFINE umlcdialogs_language_german}
{.DEFINE umlcdialogs_language_portuguese}
{.DEFINE umlcdialogs_language_italian}
