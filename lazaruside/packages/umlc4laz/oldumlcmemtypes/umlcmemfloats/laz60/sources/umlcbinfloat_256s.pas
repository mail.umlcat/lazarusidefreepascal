(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbinfloat_256s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations for I.E.E.E. floating point numerical types.
 ** Note: 256 Bits.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcbinfloattypes,
  umlcmem256s, umlcmem256byptrs,
  dummy;

// ---

const

  MOD_umlcbinfloat_256s : TUMLCModule =
    ($B0,$EF,$06,$63,$A6,$7D,$DD,$4A,$B0,$00,$8B,$C3,$8C,$98,$9F,$D5);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcbinfloat_256): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcbinfloat_256); overload;


(* global operators *)

  procedure Assign
    (out   ADest:   umlcbinfloat_256;
     const ASource: umlcbinfloat_256); overload; // operator :=

  function Compare
    (const A, B: umlcbinfloat_256): umlctcomparison;

  function Different
    (const A, B: umlcbinfloat_256): Boolean; overload; // operator <>
  function Equal
    (const A, B: umlcbinfloat_256): Boolean; overload; // operator =

  function Greater
    (const A, B: umlcbinfloat_256): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcbinfloat_256): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcbinfloat_256): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcbinfloat_256): Boolean; overload; // operator <=


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcbinfloat_256): Boolean;
begin
  Result :=
    umlcmem256byptrs.IsEmpty(umlcpmemint_256(@ASource));
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcbinfloat_256);
begin
  System.FillByte(ADest, sizeof(umlcbinfloat_256), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;




(* global operators *)

procedure Assign
  (out   ADest:   umlcbinfloat_256;
   const ASource: umlcbinfloat_256);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcbinfloat_256): umlccomparison;
begin
  Result :=
    umlcmem256byptrs.Compare
      (umlcpmemint_256(@A), umlcpmemint_256(@B));
end;

function Different
  (const A, B: umlcbinfloat_256): Boolean;
begin
  Result :=
    umlcmem256byptrs.Different
      (umlcpmemint_256(@A), umlcpmemint_256(@B));
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Equal
  (const A, B: umlcbinfloat_256): Boolean;
begin
  Result :=
    umlcmem256byptrs.Equal
      (umlcpmemint_256(@A), umlcpmemint_256(@B));
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Greater
  (const A, B: umlcbinfloat_256): Boolean;
begin
  Result :=
    umlcmem256byptrs.Greater
      (umlcpmemint_256(@A), umlcpmemint_256(@B));
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcbinfloat_256): Boolean;
begin
  Result :=
    umlcmem256byptrs.Lesser
      (umlcpmemint_256(@A), umlcpmemint_256(@B));
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcbinfloat_256): Boolean;
begin
  Result :=
    umlcmem256byptrs.GreaterEqual
      (umlcpmemint_256(@A), umlcpmemint_256(@B));
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcbinfloat_256): Boolean;
begin
  Result :=
    umlcmem256byptrs.LesserEqual
      (umlcpmemint_256(@A), umlcpmemint_256(@B));
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;



end.

