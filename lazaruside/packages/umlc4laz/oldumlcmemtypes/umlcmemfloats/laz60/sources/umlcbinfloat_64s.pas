(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbinfloat_64s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Operations for I.E.E.E. floating point numerical types.
 ** Note: 64 Bits.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcbinfloattypes,
  umlcmem64s,
  dummy;

// ---

const

  MOD_umlcbinfloat_64s : TUMLCModule =
    ($6A,$76,$59,$BC,$00,$4E,$74,$4B,$B4,$EA,$3A,$BB,$59,$EB,$07,$A1);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcbinfloat_64): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcbinfloat_64); overload;


(* global operators *)

  procedure Assign
    (out   ADest:   umlcbinfloat_64;
     const ASource: umlcbinfloat_64); overload; // operator :=

  function Compare
    (const A, B: umlcbinfloat_64): umlctcomparison;

  function Equal
    (const A, B: umlcbinfloat_64): Boolean; overload; // operator =
  function Different
    (const A, B: umlcbinfloat_64): Boolean; overload; // operator <>

  function Greater
    (const A, B: umlcbinfloat_64): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcbinfloat_64): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcbinfloat_64): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcbinfloat_64): Boolean; overload; // operator <=



implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcbinfloat_64): Boolean;
begin
  Result :=
    umlcmem64s.IsEmpty(ASource);
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcbinfloat_64);
begin
  System.FillByte(ADest, sizeof(umlcbinfloat_64), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;





(* global operators *)

procedure Assign
  (out   ADest:   umlcbinfloat_64;
   const ASource: umlcbinfloat_64);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcbinfloat_64): umlccomparison;
begin
  if (A = B)
    then Result := cmpEqual
  else if (A < B)
    then Result := cmpLower
  else Result := cmpHigher;
end;

function Different
  (const A, B: umlcbinfloat_64): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Equal
  (const A, B: umlcbinfloat_64): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Greater
  (const A, B: umlcbinfloat_64): Boolean;
begin
  Result :=
    (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcbinfloat_64): Boolean;
begin
  Result :=
    (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcbinfloat_64): Boolean;
begin
  Result :=
    (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcbinfloat_64): Boolean;
begin
  Result :=
    (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;


end.

