readme.txt
==========

The "umlcmemtypes" folder is part of the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This folder contains several packages for types based on memory unsigned integer types,
and their related operations.

