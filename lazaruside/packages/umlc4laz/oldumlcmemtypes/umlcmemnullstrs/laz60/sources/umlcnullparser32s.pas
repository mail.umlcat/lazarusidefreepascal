unit umlcnullparser32s;

{$mode objfpc}{$H+}

interface

uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemchartypes, umlcmemnullstrtypes,
  umlcchar32s,
  umlcnullstr32s,
  umlccharset32s,
  umlccharsetconst32s,
  dummy;

// ---

const
  MOD_umlcansinullstrs : TUMLCModule =
    ($6B,$57,$87,$43,$7D,$B8,$15,$46,$BA,$0F,$7F,$EF,$74,$92,$10,$3A);

// ---


(* global functions *)

  function MatchesCharSet
    (const AHaystack: umlcnullstring_32; const ValidChars: umlccharset_32): umlcnullstring_32; overload;

  function IsStringInSet
    (const AHaystack: umlcnullstring_32; const ValidChars: umlccharset_32): Boolean; overload;

  function IsWildcard
     (const ASource: umlcnullstring_32): Boolean;


implementation

(* global functions *)

function MatchesCharSet
  (const AHaystack: umlcnullstring_32; const ValidChars: umlccharset_32): umlcnullstring_32;
var CanContinue: Boolean;
    ASource: umlcnullstring_32;
begin
  Result := nil;

  if (System.Assigned(AHaystack)) then
  begin
    ASource :=
      AHaystack;

    CanContinue := TRUE;
    while (not umlcnullstr32s.IsEmpty(ASource) and CanContinue) do
    begin
      CanContinue :=
        umlccharset32s.IsMember(ValidChars, ASource^);
      Inc(ASource);
    end;

    if (not CanContinue) then
    begin
      Dec(ASource);
    end;

    Result := ASource;
  end;
  // Goal: Moves a null termnated string pointer,
  // as long as each character, matches the valid set.

  // Objetivo: Mueve un apuntador cadena terminada en nulo,
  // mientras cada caracter, coincide con el conjunto valido.
end;

function IsStringInSet
  (const AHaystack: umlcnullstring_32; const ValidChars: umlccharset_32): Boolean;
begin
  Result :=
    (umlcnullparser32s.MatchesCharSet(AHaystack, ValidChars) <> AHaystack);
  // Goal: Returns if all the characters, a string are valid.
  // Objetivo: Regresa si todos los caracteres en una cadena son validos.
end;

function IsWildcard
   (const ASource: umlcnullstring_32): Boolean;
var P: umlcnullstring_32;
begin
  Result :=
    umlcnullparser32s.IsStringInSet(ASource, WildcardSet);
  // Goal: Returns if a string is a wildcard.
  // Objetivo: Regresa si una cadena es un comodin.
end;


end.

