unit umlcnullparser16s;

{$mode objfpc}{$H+}

interface

uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemchartypes, umlcmemnullstrtypes,
  umlcchar16s,
  umlcnullstr16s,
  umlccharset16s,
  umlccharsetconst16s,
  dummy;

// ---

const
  MOD_umlcansinullstrs : TUMLCModule =
    ($4B,$EC,$08,$4F,$43,$BE,$AD,$4E,$82,$02,$3B,$DB,$81,$E7,$A5,$32);

// ---

(* global functions *)

  function MatchesCharSet
    (const AHaystack: umlcnullstring_16; const ValidChars: umlccharset_16): umlcnullstring_16; overload;

  function IsStringInSet
    (const AHaystack: umlcnullstring_16; const ValidChars: umlccharset_16): Boolean; overload;

  function IsWildcard
     (const ASource: umlcnullstring_16): Boolean;

implementation

(* global functions *)

function MatchesCharSet
  (const AHaystack: umlcnullstring_16; const ValidChars: umlccharset_16): umlcnullstring_16;
var CanContinue: Boolean;
    ASource: umlcnullstring_16;
begin
  Result := nil;

  if (System.Assigned(AHaystack)) then
  begin
    ASource :=
      AHaystack;

    CanContinue := TRUE;
    while (not umlcnullstr16s.IsEmpty(ASource) and CanContinue) do
    begin
      CanContinue :=
        umlccharset16s.IsMember(ValidChars, ASource^);
      Inc(ASource);
    end;

    if (not CanContinue) then
    begin
      Dec(ASource);
    end;

    Result := ASource;
  end;
  // Goal: Moves a null termnated string pointer,
  // as long as each character, matches the valid set.

  // Objetivo: Mueve un apuntador cadena terminada en nulo,
  // mientras cada caracter, coincide con el conjunto valido.
end;

function IsStringInSet
  (const AHaystack: umlcnullstring_16; const ValidChars: umlccharset_16): Boolean;
begin
  Result :=
    (umlcnullparser16s.MatchesCharSet(AHaystack, ValidChars) <> AHaystack);
  // Goal: Returns if all the characters, a string are valid.
  // Objetivo: Regresa si todos los caracteres en una cadena son validos.
end;

function IsWildcard
   (const ASource: umlcnullstring_16): Boolean;
var P: umlcnullstring_16;
begin
  Result :=
    umlcnullparser16s.IsStringInSet(ASource, WildcardSet);
  // Goal: Returns if a string is a wildcard.
  // Objetivo: Regresa si una cadena es un comodin.
end;


end.

