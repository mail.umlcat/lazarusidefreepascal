(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnullparser8s;

{$mode objfpc}{$H+}

interface

uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemchartypes, umlcmemnullstrtypes,
  umlcchar8s,
  umlcnullstr8s,
  umlccharset8s,
  umlccharsetconst8s,
  dummy;

// ---

const
  MOD_umlcansinullstrs : TUMLCModule =
    ($E1,$51,$40,$03,$A0,$3D,$33,$40,$A1,$EA,$E1,$40,$47,$90,$70,$9B);

// ---

(* global functions *)

  function MatchesCharSet
    (const AHaystack: umlcnullstring_8; const ValidChars: umlccharset_8): umlcnullstring_8; overload;

  function IsStringInSet
    (const AHaystack: umlcnullstring_8; const ValidChars: umlccharset_8): Boolean; overload;

  function IsWildcard
     (const ASource: umlcnullstring_8): Boolean;


implementation

(* global functions *)

function MatchesCharSet
  (const AHaystack: umlcnullstring_8; const ValidChars: umlccharset_8): umlcnullstring_8;
var CanContinue: Boolean;
    ASource: umlcnullstring_8;
begin
  Result := nil;

  if (System.Assigned(AHaystack)) then
  begin
    ASource :=
      AHaystack;

    CanContinue := TRUE;
    while (not umlcnullstr8s.IsEmpty(ASource) and CanContinue) do
    begin
      CanContinue :=
        umlccharset8s.IsMember(ValidChars, ASource^);
      Inc(ASource);
    end;

    if (not CanContinue) then
    begin
      Dec(ASource);
    end;

    Result := ASource;
  end;
  // Goal: Moves a null termnated string pointer,
  // as long as each character, matches the valid set.

  // Objetivo: Mueve un apuntador cadena terminada en nulo,
  // mientras cada caracter, coincide con el conjunto valido.
end;

function IsStringInSet
  (const AHaystack: umlcnullstring_8; const ValidChars: umlccharset_8): Boolean;
begin
  Result :=
    (umlcnullparser8s.MatchesCharSet(AHaystack, ValidChars) <> AHaystack);
  // Goal: Returns if all the characters, a string are valid.
  // Objetivo: Regresa si todos los caracteres en una cadena son validos.
end;

function IsWildcard
   (const ASource: umlcnullstring_8): Boolean;
var P: umlcnullstring_8;
begin
  Result :=
    umlcnullparser8s.IsStringInSet(ASource, WildcardSet);
  // Goal: Returns if a string is a wildcard.
  // Objetivo: Regresa si una cadena es un comodin.
end;

end.

