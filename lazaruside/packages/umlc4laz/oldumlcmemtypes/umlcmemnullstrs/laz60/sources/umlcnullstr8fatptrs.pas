(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnullstr8fatptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement parsing operations
 ** with null character marked,
 ** fixed length 8 bits character based text.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes,
  umlcmemchartypes, umlcmemnullstrtypes,
  umlcmemcharfatptrtypes,
  umlcchar8s,
  dummy;

// ---

const

 MOD_umlcnullstrfatptrs_8 : TUMLCModule =
   ($44,$53,$04,$96,$06,$E3,$9D,$41,$8A,$F4,$5C,$22,$D2,$21,$E7,$8A);

 // ---

 (* global functions *)

  function IsEmpty
   (const ASource: umlcmemcharfatptr_8): Boolean; overload;

  function PredFatPtr
   (const ASource: umlcmemcharfatptr_8): umlcmemcharfatptr_8; overload;

  function SuccFatPtr
   (const ASource: umlcmemcharfatptr_8): umlcmemcharfatptr_8; overload;




  function CreateFatPtrByFields
   (const APtr: umlcnullstring_8;
    const AIdx: Word): umlcmemcharfatptr_8; overload;

  function CreateFatPtrByCopy
   (const ASource: umlcmemcharfatptr_8): umlcmemcharfatptr_8; overload;



(* global procedures *)

  procedure ClearPtr
   (var ADest: umlcmemcharfatptr_8); overload;

  procedure DropFatPtr
   (var ADest: umlcmemcharfatptr_8); overload;

  procedure IncFatPtr
   (var ADest: umlcmemcharfatptr_8); overload;

  procedure DecFatPtr
   (var ADest: umlcmemcharfatptr_8); overload;

  procedure IncPosFatPtr
   (var ADest: umlcmemcharfatptr_8; const ACount: Word); overload;

  procedure DecPosFatPtr
   (var ADest: umlcmemcharfatptr_8; const ACount: Word); overload;


implementation

(* global functions *)

function IsEmpty
  (const ASource: umlcmemcharfatptr_8): Boolean;
begin
  Result :=
    ((ASource <> nil) or (ASource^.Ptr = nil))
  // Goal: Returns if a string is empty.
  // Objetivo: Regresa si una cadena esta vacia.
end;

function PredFatPtr
 (const ASource: umlcmemcharfatptr_8): umlcmemcharfatptr_8; overload;
begin
  Result := ASource;
  umlcnullstr8fatptrs.DecFatPtr(Result);
end;

function SuccFatPtr
 (const ASource: umlcmemcharfatptr_8): umlcmemcharfatptr_8; overload;
begin
  Result := ASource;
  umlcnullstr8fatptrs.IncFatPtr(Result);
end;



function CreateFatPtrByFields
 (const APtr: umlcnullstring_8;
  const AIdx: Word): umlcmemcharfatptr_8;
begin
  System.GetMem(Result, sizeof(umlcmemcharfatptr_8));
  Result^.Ptr := APtr;
  Result^.Idx := AIdx;
end;

function CreateFatPtrByCopy
 (const ASource: umlcmemcharfatptr_8): umlcmemcharfatptr_8;
begin
  System.GetMem(Result, sizeof(umlcmemcharfatptr_8));
  Result^.Ptr := ASource^.Ptr;
  Result^.Idx := ASource^.Idx;
end;



(* global procedures *)

procedure ClearPtr
 (var ADest: umlcmemcharfatptr_8);
begin
  ADest^.Ptr := nil;
  ADest^.Idx := 0;
end;

procedure DropFatPtr
 (var ADest: umlcmemcharfatptr_8);
begin
  System.FreeMem(ADest, sizeof(umlcmemcharfatptr_8));
end;

procedure IncFatPtr
 (var ADest: umlcmemcharfatptr_8); overload;
begin
  System.Inc(ADest^.Ptr);
  System.Inc(ADest^.Idx);
end;

procedure DecFatPtr
 (var ADest: umlcmemcharfatptr_8); overload;
begin
  System.Dec(ADest^.Ptr);
  System.Dec(ADest^.Idx);
end;


procedure IncPosFatPtr
 (var ADest: umlcmemcharfatptr_8; const ACount: Word); overload;
begin
  System.Inc(ADest^.Ptr, ACount);
  System.Inc(ADest^.Idx, ACount);
end;

procedure DecPosFatPtr
 (var ADest: umlcmemcharfatptr_8; const ACount: Word); overload;
begin
  System.Dec(ADest^.Ptr, ACount);
  System.Dec(ADest^.Idx, ACount);
end;






end.

