{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcmemnullstrs60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcmemnullstrs_rtti, umlcnullparser8s, umlcnullparser16s, 
  umlcnullparser32s, umlcnullparser64s, umlcnullstr8fatptrs, umlcnullstr8s, 
  umlcnullstr16s, umlcnullstr32s, umlcnullstr64s, umlcmemnullstrtypes;

implementation

end.
