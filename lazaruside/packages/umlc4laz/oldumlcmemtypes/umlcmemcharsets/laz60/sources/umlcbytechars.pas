(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbytechars;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement one single byte (s) based characters.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  dummy;

// ---

const

 MOD_umlcbytechars : TUMLCModule =
   ($C1,$05,$B2,$B9,$74,$51,$79,$48,$96,$0C,$67,$A9,$EE,$6A,$54,$20);

// ---

(* global constants *)

const
  bytenullchar = 0;

(* global standard functions *)

  function IsNull
    (const AValue: umlcbytechar): Boolean; overload;
  function IsAlpha
    (const AValue: umlcbytechar): Boolean; overload;
  function IsDigit
    (const AValue: umlcbytechar): Boolean; overload;
  function IsSpace
    (const AValue: umlcbytechar): Boolean; overload;
  function IsBlank
    (const AValue: umlcbytechar): Boolean; overload;

  function IsDecimal
    (const AValue: umlcbytechar): Boolean; overload;
  function IsHexa
    (const AValue: umlcbytechar): Boolean; overload;
  function IsOctal
    (const AValue: umlcbytechar): Boolean; overload;
  function IsBinary
    (const AValue: umlcbytechar): Boolean; overload;

  function IsUppercase
    (const AValue: umlcbytechar): Boolean; overload;
  function IsLowercase
    (const AValue: umlcbytechar): Boolean; overload;

(* global additional functions *)

  function CharToOrd
    (const AValue: umlcbytechar): Integer; overload;
  function OrdToChar
    (const AValue: Integer): umlcbytechar; overload;

  function UppercaseCopy
    (const AValue: umlcbytechar): umlcbytechar; overload;
  function LowercaseCopy
    (const AValue: umlcbytechar): umlcbytechar; overload;
  function TogglecaseCopy
    (const AValue: umlcbytechar): umlcbytechar; overload;

  function SameText
    (const A, B: umlcbytechar): Boolean; overload;

  function IfChar
    (const APredicate: Boolean; const A, B: umlcbytechar): umlcbytechar;

  function ReplaceChar
    (const AValue: umlcbytechar; const A, B: umlcbytechar): umlcbytechar; overload;
	
(* global operators *)

  procedure Assign
    (var   ADest:   umlcbytechar;
     const ASource: umlcbytechar); overload; // operator :=

  function Equal
    (const A, B: umlcbytechar): Boolean; overload; // operator =
  function Greater
    (const A, B: umlcbytechar): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcbytechar): Boolean; overload; // operator <
  function Different
    (const A, B: umlcbytechar): Boolean; overload; // operator <>
  function GreaterEqual
    (const A, B: umlcbytechar): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcbytechar): Boolean; overload; // operator <=

(* global procedures *)

  procedure Clear
    (var AValue: umlcbytechar); overload;

implementation

(* global standard functions *)

function IsNull
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    (AValue = 0);
end;

function IsAlpha
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    Windows.IsCharAlphaA(ansichar(AValue));
end;

function IsDigit
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    ((ansichar(AValue) >= '0') and (ansichar(AValue) <= '9'));
end;

function IsSpace
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    (AValue = 32);
end;

function IsBlank
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    (AValue = 32) or // space
    (AValue = 13) or // D.O.S. carriage-return
    (AValue = 10) or // D.O.S. line-feed
    (AValue = 12) or // page break
    (AValue = 08);   // tabulator
end;

function IsDecimal
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    ((ansichar(AValue) >= '0') and (ansichar(AValue) <= '9'));
end;

function IsHexa
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    (ansichar(AValue) >= '0') and (ansichar(AValue) <= '9') or
    (ansichar(AValue) >= 'A') and (ansichar(AValue) <= 'F') or
    (ansichar(AValue) >= 'a') and (ansichar(AValue) <= 'f');
end;

function IsOctal
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    (ansichar(AValue) >= '0') and (ansichar(AValue) <= '7');
end;

function IsBinary
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    (ansichar(AValue) = '0') or (ansichar(AValue) = '1');
end;

function IsUppercase
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    Windows.IsCharUpperA(ansichar(AValue));
end;

function IsLowercase
  (const AValue: umlcbytechar): Boolean;
begin
  Result :=
    Windows.IsCharLowerA(ansichar(AValue));
end;

(* global additional functions *)

function CharToOrd
  (const AValue: umlcbytechar): Integer;
begin
  Result :=
    Ord(AValue);
end;

function OrdToChar
  (const AValue: Integer): umlcbytechar;
begin
  Result :=
    umlcbytechar(AValue);
end;

function UppercaseCopy
  (const AValue: umlcbytechar): umlcbytechar;
begin
  Result :=
    AValue;
  Windows.CharUpperBuffA(@Result, 1);
  // Goal: Returns a uppercase copy of the given character.
  // Objetivo: Regresa una copia en mayusculas del caracter dado.
end;

function LowercaseCopy
  (const AValue: umlcbytechar): umlcbytechar;
begin
  Result := AValue;
  Windows.CharLowerBuffA(@Result, 1);
  // Goal: Returns a lowercase copy of the given character.
  // Objetivo: Regresa una copia en minusculas del caracter dado.
end;

function TogglecaseCopy
  (const AValue: umlcbytechar): umlcbytechar;
begin
  if (IsUppercase(AValue))
    then Result := LowercaseCopy(AValue)
    else Result := UppercaseCopy(AValue);
  // Goal: Returns a switched case copy of the given character.
  // Objetivo: Regresa una copia en caso intercambiado del caracter dado.
end;

function SameText
  (const A, B: umlcbytechar): Boolean;
begin
  Result :=
    (Lowercase(ansichar(A)) = Lowercase(ansichar(B)));
  // Goal: Returns if 2 characters are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 caracteres son iguales, ignorar caso sensitivo.
end;

function IfChar
  (const APredicate: Boolean; const A, B: umlcbytechar): umlcbytechar;
begin
  if (APredicate)
    then Result := A
    else Result := B;
  // Objetivo: Segun la condicion, regresar el caracter seleccionado.
  // Goal: Upon condition, return the select character.
end;

function ReplaceChar
  (const AValue: umlcbytechar; const A, B: umlcbytechar): umlcbytechar;
begin
  if (AValue = A)
    then Result := B
    else Result := AValue;
  // Objetivo: Reemplazar un caracter en especifico.
  // Goal: Upon condition, return the select character.  
end;

(* global operators *)

procedure Assign
  (var   ADest:   umlcbytechar;
   const ASource: umlcbytechar);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcbytechar): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: umlcbytechar): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcbytechar): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function Different
  (const A, B: umlcbytechar): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function GreaterEqual
  (const A, B: umlcbytechar): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcbytechar): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

(* global procedures *)

procedure Clear
  (var AValue: umlcbytechar);
begin
  AValue :=
    bytenullchar;
  // Goal: Clear a character.
  // Objetivo: Limpia un caracter.
end;





end.

