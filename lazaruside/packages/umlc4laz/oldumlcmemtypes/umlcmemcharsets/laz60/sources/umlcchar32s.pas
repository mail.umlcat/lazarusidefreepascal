(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcchar32s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement 32 bits based characters.
 ** Note:
 ** - The character encoding is neutral.
 ** - All characters have the same size
 ** - Data is stored as an unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  dummy;

// ---

const

  MOD_umlcchar32s : TUMLCModule =
    ($AB,$F9,$13,$46,$90,$8E,$FF,$42,$AF,$9B,$66,$DC,$3C,$47,$87,$1F);

// ---

(* global standard functions *)

  function IsNull
    (const AValue: umlcchar_32): Boolean; overload;
  function IsAlpha
    (const AValue: umlcchar_32): Boolean; overload;
  function IsDigit
    (const AValue: umlcchar_32): Boolean; overload;
  function IsSpace
    (const AValue: umlcchar_32): Boolean; overload;
  function IsBlank
    (const AValue: umlcchar_32): Boolean; overload;

  function IsDecimal
    (const AValue: umlcchar_32): Boolean; overload;
  function IsHexa
    (const AValue: umlcchar_32): Boolean; overload;
  function IsOctal
    (const AValue: umlcchar_32): Boolean; overload;
  function IsBinary
    (const AValue: umlcchar_32): Boolean; overload;

  function IsUppercase
    (const AValue: umlcchar_32): Boolean; overload;
  function IsLowercase
    (const AValue: umlcchar_32): Boolean; overload;

(* global additional functions *)

  function CharToOrd
    (const AValue: umlcchar_32): Integer; overload;
  function OrdToChar
    (const AValue: Integer): umlcchar_32; overload;

  function UppercaseCopy
    (const AValue: umlcchar_32): umlcchar_32; overload;
  function LowercaseCopy
    (const AValue: umlcchar_32): umlcchar_32; overload;
  function TogglecaseCopy
    (const AValue: umlcchar_32): umlcchar_32; overload;

  function SameText
    (const A, B: umlcchar_32): Boolean; overload;

  function IfChar
    (const APredicate: Boolean; const A, B: umlcchar_32): umlcchar_32;

  function ReplaceChar
    (const AValue: umlcchar_32; const A, B: umlcchar_32): umlcchar_32; overload;

(* global operators *)

  procedure Assign
    (var   ADest:   umlcchar_32;
     const ASource: umlcchar_32); overload; // operator :=

  function Compare
    (const A, B: umlcchar_32): umlctcomparison;

  function Different
    (const A, B: umlcchar_32): Boolean; overload; // operator <>
  function Equal
    (const A, B: umlcchar_32): Boolean; overload; // operator =

  function Greater
    (const A, B: umlcchar_32): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcchar_32): Boolean; overload; // operator <

  function GreaterEqual
    (const A, B: umlcchar_32): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcchar_32): Boolean; overload; // operator <=

(* global procedures *)

  procedure Clear
    (var AValue: umlcchar_32); overload;



implementation

(* global standard functions *)

function IsNull
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    (AValue = 0);
end;

function IsAlpha
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    Windows.IsCharAlphaA(ansichar(AValue));
end;

function IsDigit
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    ((ansichar(AValue) >= '0') and (ansichar(AValue) <= '9'));
end;

function IsSpace
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    (AValue = 32);
end;

function IsBlank
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    (AValue = 32) or // space
    (AValue = 13) or // D.O.S. carriage-return
    (AValue = 10) or // D.O.S. line-feed
    (AValue = 12) or // page break
    (AValue = 08);   // tabulator
end;

function IsDecimal
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    ((ansichar(AValue) >= '0') and (ansichar(AValue) <= '9'));
end;

function IsHexa
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    (ansichar(AValue) >= '0') and (ansichar(AValue) <= '9') or
    (ansichar(AValue) >= 'A') and (ansichar(AValue) <= 'F') or
    (ansichar(AValue) >= 'a') and (ansichar(AValue) <= 'f');
end;

function IsOctal
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    (ansichar(AValue) >= '0') and (ansichar(AValue) <= '7');
end;

function IsBinary
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    (ansichar(AValue) = '0') or (ansichar(AValue) = '1');
end;

function IsUppercase
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    Windows.IsCharUpperA(ansichar(AValue));
end;

function IsLowercase
  (const AValue: umlcchar_32): Boolean;
begin
  Result :=
    Windows.IsCharLowerA(ansichar(AValue));
end;

(* global additional functions *)

function CharToOrd
  (const AValue: umlcchar_32): Integer;
begin
  Result :=
    Ord(AValue);
end;

function OrdToChar
  (const AValue: Integer): umlcchar_32;
begin
  Result :=
    umlcchar_32(AValue);
end;

function UppercaseCopy
  (const AValue: umlcchar_32): umlcchar_32;
begin
  Result :=
    AValue;
  Windows.CharUpperBuffA(@Result, 1);
  // Goal: Returns a uppercase copy of the given character.
  // Objetivo: Regresa una copia en mayusculas del caracter dado.
end;

function LowercaseCopy
  (const AValue: umlcchar_32): umlcchar_32;
begin
  Result := AValue;
  Windows.CharLowerBuffA(@Result, 1);
  // Goal: Returns a lowercase copy of the given character.
  // Objetivo: Regresa una copia en minusculas del caracter dado.
end;

function TogglecaseCopy
  (const AValue: umlcchar_32): umlcchar_32;
begin
  if (IsUppercase(AValue))
    then Result := LowercaseCopy(AValue)
    else Result := UppercaseCopy(AValue);
  // Goal: Returns a switched case copy of the given character.
  // Objetivo: Regresa una copia en caso intercambiado del caracter dado.
end;

function SameText
  (const A, B: umlcchar_32): Boolean;
begin
  Result :=
    (Lowercase(ansichar(A)) = Lowercase(ansichar(B)));
  // Goal: Returns if 2 characters are equal, ignores sensitive case.
  // Objetivo: Regresa si 2 caracteres son iguales, ignorar caso sensitivo.
end;

function IfChar
  (const APredicate: Boolean; const A, B: umlcchar_32): umlcchar_32;
begin
  if (APredicate)
    then Result := A
    else Result := B;
  // Objetivo: Segun la condicion, regresar el caracter seleccionado.
  // Goal: Upon condition, return the select character.
end;

function ReplaceChar
  (const AValue: umlcchar_32; const A, B: umlcchar_32): umlcchar_32;
begin
  if (AValue = A)
    then Result := B
    else Result := AValue;
  // Objetivo: Reemplazar un caracter en especifico.
  // Goal: Upon condition, return the select character.
end;

(* global operators *)

procedure Assign
  (var   ADest:   umlcchar_32;
   const ASource: umlcchar_32);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcchar_32): umlccomparison;
begin
  Result := cmpEqual;
  if (A < B)
    then Result := cmpLower
  else if (A > B)
    then Result := cmpHigher;
end;

function Different
  (const A, B: umlcchar_32): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Equal
  (const A, B: umlcchar_32): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: umlcchar_32): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcchar_32): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcchar_32): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcchar_32): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

(* global procedures *)

procedure Clear
  (var AValue: umlcchar_32);
begin
  AValue :=
    nullchar_32;
  // Goal: Clear a character.
  // Objetivo: Limpia un caracter.
end;


end.

