(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharset8s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement a "Pascal" style set of characters.
 ** Note:
 ** - Each elements is a 8 bits character.
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as an unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcchar8s,
  dummy;

// ---

const

  MOD_umlccharset8s : TUMLCModule =
    ($78,$4F,$A1,$3E,$AA,$35,$43,$48,$BA,$93,$C9,$40,$07,$D6,$1C,$D7);

// ---

(* global functions *)

 function IsEmpty
   (const ASet: umlccharset_8): Boolean; overload;

 function IsMember
   (const ASet: umlccharset_8; const AItem: umlcchar_8): Boolean; overload;

 function IsSameMember
   (const ASet: umlccharset_8; const AItem: umlcchar_8): Boolean; overload;

 function RangeToSetCopy
   (const ALo, AHi: umlcchar_8): umlccharset_8; overload;

(* global procedures *)

procedure Clear
  (out ASet: umlccharset_8); overload;

procedure Include
  (var ASet: umlccharset_8; const AItem: umlcchar_8); overload;
procedure Exclude
  (var ASet: umlccharset_8; const AItem: umlcchar_8); overload;

procedure IncludeRange
  (var ASet: umlccharset_8; const ALo, AHi: umlcchar_8); overload;
procedure ExcludeRange
  (var ASet: umlccharset_8; const ALo, AHi: umlcchar_8); overload;

(* global operators *)

procedure Assign
  (out   ADest:   umlccharset_8;
   const ASource: umlccharset_8); overload; // operator :=

implementation

(* global functions *)

function IsEmpty
  (const ASet: umlccharset_8): Boolean;
begin
  Result :=
    umlcmemint_8(ASet[0]) = 0;
end;

function IsMember
  (const ASet: umlccharset_8; const AItem: umlcchar_8): Boolean;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1;
    Count := System.Length(ASet);
  Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsSameMember
  (const ASet: umlccharset_8; const AItem: umlcchar_8): Boolean; overload;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found :=
      umlcchar8s.SameText
        (umlcchar8s.LowercaseCopy(ASet[Index]), umlcchar8s.LowercaseCopy(AItem));
    Inc(Index);
  end;
  Result := Found;
end;

function RangeToSetCopy
  (const ALo, AHi: umlcchar_8): umlccharset_8;
var AItem, L, H: umlcchar_8; ACount: Word;
begin
  umlccharset8s.Clear(Result);

  if (ALo > AHi)
    then L := AHi
    else L := ALo;
  if (ALo > AHi)
    then H := ALo
    else H := AHi;

  ACount := 0;
  for AItem := L to H do
  begin
    Result[Succ(ACount)] := AItem;
    Inc(ACount);
  end;

  Result[0] := ACount;
end;


(* global procedures *)

procedure Clear
  (out ASet: umlccharset_8);
begin
  System.FillChar(ASet, sizeof(ASet), #0);
end;

procedure Include
  (var ASet: umlccharset_8; const AItem: umlcchar_8);
var AIndex: Byte;
begin
  AIndex := ASet[0];
  ASet[Succ(AIndex)] := AItem;
  ASet[0] := Succ(AIndex);
end;

procedure Exclude
  (var ASet: umlccharset_8; const AItem: umlcchar_8);
var I, Count, Last: Byte; Temp: umlccharset_8;
begin
  umlccharset8s.Clear(Temp);
  Count :=
    ASet[0];
  Last := 0;
  for I := 0 to Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := System.Pred(Last);
end;

procedure IncludeRange
  (var ASet: umlccharset_8; const ALo, AHi: umlcchar_8);
var AItem, L, H: umlcchar_8;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    Include(ASet, AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlccharset_8; const ALo, AHi: umlcchar_8);
var AItem, L, H: umlcchar_8;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  for AItem := L to H do
  begin
    Exclude(ASet, AItem);
  end;
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlccharset_8;
   const ASource: umlccharset_8);
begin
  System.Move(ASource, ADest, sizeof(ADest));
end;


end.

