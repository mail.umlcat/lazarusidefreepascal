(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharset64s;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement a "Pascal" style set of characters.
 ** Note:
 ** - Each elements is a 64 bits character.
 ** - The character encoding is neutral.
 ** - All characters have the same size.
 ** - Data is stored as an unsigned integer, not text.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes,
  umlcchar64s,
  dummy;

// ---

const

  MOD_umlccharset_64s : TUMLCModule =
    ($78,$34,$54,$D5,$6A,$79,$BB,$48,$AE,$58,$3B,$F5,$0C,$29,$99,$60);

// ---

(* global functions *)

function IsEmpty
  (const ASet: umlccharset_64): Boolean; overload;

function IsMember
  (const ASet: umlccharset_64; const AItem: umlcchar_64): Boolean; overload;

function IsSameMember
  (const ASet: umlccharset_64; const AItem: umlcchar_64): Boolean; overload;

function RangeToSetCopy
  (const ALo, AHi: umlcchar_64): umlccharset_64; overload;

(* global procedures *)

procedure Clear
  (out ASet: umlccharset_64); overload;

procedure Include
  (var ASet: umlccharset_64; const AItem: umlcchar_64); overload;
procedure Exclude
  (var ASet: umlccharset_64; const AItem: umlcchar_64); overload;

procedure IncludeRange
  (var ASet: umlccharset_64; const ALo, AHi: umlcchar_64); overload;
procedure ExcludeRange
  (var ASet: umlccharset_64; const ALo, AHi: umlcchar_64); overload;

(* global operators *)

procedure Assign
  (out   ADest:   umlccharset_64;
   const ASource: umlccharset_64); overload; // operator :=


implementation

(* global functions *)

function IsEmpty
  (const ASet: umlccharset_64): Boolean;
begin
  Result :=
    umlcmemint_64(ASet[0]) = 0;
end;

function IsMember
  (const ASet: umlccharset_64; const AItem: umlcchar_64): Boolean;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1;
    Count := System.Length(ASet);
  Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found := (ASet[Index] = AItem);
    Inc(Index);
  end;
  Result := Found;
end;

function IsSameMember
  (const ASet: umlccharset_64; const AItem: umlcchar_64): Boolean; overload;
var Index, Count: Integer; Found: Boolean;
begin
  Index := 1; Count := System.Length(ASet); Found := FALSE;
  while (Index <= Count) and (not Found) do
  begin
    Found :=
      umlcchar64s.Equal
        (umlcchar64s.LowercaseCopy(ASet[Index]), umlcchar64s.LowercaseCopy(AItem));
    Inc(Index);
  end;
  Result := Found;
end;

function RangeToSetCopy
  (const ALo, AHi: umlcchar_64): umlccharset_64;
var AItem, L, H: umlcchar_64; ACount: Word;
begin
  umlccharset64s.Clear(Result);

  if (ALo > AHi)
    then L := AHi
    else L := ALo;
  if (ALo > AHi)
    then H := ALo
    else H := AHi;

  ACount := 0;
  AItem := L;
  while (AItem <= H) do
  begin
    Result[Succ(ACount)] := AItem;
    Inc(ACount);

    Inc(AItem);
  end;

  Result[0] := ACount;
end;


(* global procedures *)

procedure Clear
  (out ASet: umlccharset_64);
begin
  System.FillChar(ASet, sizeof(ASet), #0);
end;

procedure Include
  (var ASet: umlccharset_64; const AItem: umlcchar_64);
var AIndex: Byte;
begin
  AIndex := ASet[0];
  ASet[Succ(AIndex)] := AItem;
  ASet[0] := Succ(AIndex);
end;

procedure Exclude
  (var ASet: umlccharset_64; const AItem: umlcchar_64);
var I, Count, Last: Byte; Temp: umlccharset_64;
begin
  umlccharset64s.Clear(Temp);
  Count :=
    ASet[0];
  Last := 0;
  for I := 0 to Pred(Count) do
  begin
    if (ASet[I] <> AItem) then
    begin
      Temp[Last] := AItem;
      Inc(Last);
    end;
  end;

  System.Move(Temp, ASet, sizeof(ASet));
  ASet[0] := Succ(Last);
end;

procedure IncludeRange
  (var ASet: umlccharset_64; const ALo, AHi: umlcchar_64);
var AItem, L, H: umlcchar_64;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  AItem := L;
  while (AItem <= H) do
  begin
    Include(ASet, AItem);

    Inc(AItem);
  end;
end;

procedure ExcludeRange
  (var ASet: umlccharset_64; const ALo, AHi: umlcchar_64);
var AItem, L, H: umlcchar_64;
begin
  L := IfChar((ALo > AHi), AHi, ALo);
  H := IfChar((ALo > AHi), ALo, AHi);

  AItem := L;
  while (AItem <= H) do
  begin
    Exclude(ASet, AItem);

    Inc(AItem);
  end;
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlccharset_64;
   const ASource: umlccharset_64);
begin
  System.Move(ASource, ADest, sizeof(ADest));
end;

end.

