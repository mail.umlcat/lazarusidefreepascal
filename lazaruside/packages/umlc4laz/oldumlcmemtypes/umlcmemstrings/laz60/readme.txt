readme.txt
==========

The "umlcmemstrings" folder contains the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This package is an extension to the related "umlcmemtypes" base / system library package,
with Text and Neutral character encoding operations.
