(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcbytestrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement one byte (s) character based text.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes, umlcmemstrtypes,
  umlcbytechars, umlcstring8s,
  dummy;

// ---

const

 MOD_umlcbytestrings : TUMLCModule =
   ($76,$F5,$94,$85,$A9,$A3,$5F,$47,$9B,$D0,$51,$BA,$A1,$C2,$67,$DE);

// ---

(* global standard functions *)

function IsEmpty
  (const ASource: umlcbytestring): Boolean; overload;

function IsStringOfChar
  (const S: umlcbytestring; C: umlcbytechar): Boolean; overload;

function Length
  (const ASource: umlcbytestring): Cardinal; overload;

function SafeIndexOf
  (const AValue: Cardinal): Cardinal;

(* global additional functions *)

function UppercaseCopy
  (const ASource: umlcbytestring): umlcbytestring; overload;
function LowercaseCopy
  (const ASource: umlcbytestring): umlcbytestring; overload;
function TogglecaseCopy
  (const ASource: umlcbytestring): umlcbytestring; overload;
function CapitalizeCopy
  (const ASource: umlcbytestring): umlcbytestring; overload;

function ConcatCharCopy
  (const A: umlcbytestring; const B: umlcbytechar): umlcbytestring; overload;
function ConcatStrCopy
  (var A, B: umlcbytestring): umlcbytestring; overload;

function StringOfChar
  (const ASource: umlcbytechar; const ACount: Byte): umlcbytestring;

(* global operators *)

procedure Assign
  (out   ADest:   umlcbytestring;
   const ASource: umlcbytestring); overload; // operator :=

function Equal
  (const A, B: umlcbytestring): Boolean; overload; // operator =
function Greater
  (const A, B: umlcbytestring): Boolean; overload; // operator >
function Lesser
  (const A, B: umlcbytestring): Boolean; overload; // operator <
function Different
  (const A, B: umlcbytestring): Boolean; overload; // operator <>
function GreaterEqual
  (const A, B: umlcbytestring): Boolean; overload; // operator >=
function LesserEqual
  (const A, B: umlcbytestring): Boolean; overload; // operator <=

(* global procedures *)

procedure Clear
  (out ADest: umlcbytestring);

implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcbytestring): Boolean;
begin
  Result :=
    umlcstring8s.IsEmpty(ASource);
end;

function IsStringOfChar
  (const S: umlcbytestring; C: umlcbytechar): Boolean;
begin
  Result :=
    umlcstring8s.IsStringOfChar(S, C);
end;

function Length
  (const ASource: umlcbytestring): Cardinal;
begin
  Result :=
    umlcstring8s.Length(ASource);
end;

(* global additional functions *)

function UppercaseCopy
  (const ASource: umlcbytestring): umlcbytestring;
begin
  Result :=
    umlcstring8s.UppercaseCopy(AValue);
end;

function LowercaseCopy
  (const ASource: umlcbytestring): umlcbytestring;
begin
  Result :=
    umlcstring8s.UppercaseCopy(AValue);
end;

function TogglecaseCopy
  (const ASource: umlcbytestring): umlcbytestring;
begin
  Result :=
    umlcstring8s.TogglecaseCopy(ASource);
end;

function CapitalizeCopy
  (const ASource: umlcbytestring): umlcbytestring;
begin
  Result :=
    umlcstring8s.CapitalizeCopy(ASource);
end;

function ConcatCharCopy
  (const A: umlcbytestring; const B: umlcbytechar): umlcbytestring;
begin
  Result :=
    umlcstring8s.ConcatCharCopy(A, B);
end;

function ConcatStrCopy
  (var A, B: umlcbytestring): umlcbytestring;
begin
  Result :=
    umlcstring8s.ConcatStrCopy(A, B);
end;

function StringOfChar
  (const ASource: umlcbytechar; const ACount: Byte): umlcbytestring;
begin
  Result :=
    umlcstring8s.StringOfChar(ASource, ACount);
end;

(* global operators *)

procedure Assign
  (out   ADest: umlcbytestring;
   const ASource: umlcbytestring);
begin
  umlcstring8s.Assign
     (ADest, ASource);
end;

function Equal
  (const A, B: umlcbytestring): Boolean;
begin
  Result :=
    umlcstring8s.Equal(A, B);
end;

function Greater
  (const A, B: umlcbytestring): Boolean;
begin
  Result :=
    umlcstring8s.Greater(A, B);
end;

function Lesser
  (const A, B: umlcbytestring): Boolean;
begin
  Result :=
    umlcstring8s.Lesser(A, B);
end;

function Different
  (const A, B: umlcbytestring): Boolean;
begin
  Result :=
    umlcstring8s.Different(A, B);
end;

function GreaterEqual
  (const A, B: umlcbytestring): Boolean;
begin
  Result :=
    umlcstring8s.GreaterEqual(A, B);
end;

function LesserEqual
  (const A, B: umlcbytestring): Boolean;
begin
  Result :=
    umlcstring8s.LesserEqual(A, B);
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcbytestring);
begin
  umlcstring8s.Clear
     (ADest);
end;


end.

