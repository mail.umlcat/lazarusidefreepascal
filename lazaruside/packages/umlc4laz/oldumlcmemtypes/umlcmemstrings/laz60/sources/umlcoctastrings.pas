(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcoctastrings;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement eight byte (s) character based text.
 ** The character encoding is neutral.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemtypes, umlcmemchartypes, umlcmemstrtypes,
  umlcoctachars, umlcstring64s,
  dummy;

// ---

const

 MOD_umlcoctastrings : TUMLCModule =
   ($9F,$43,$D8,$F2,$74,$94,$E5,$4F,$89,$D4,$A2,$46,$C1,$40,$C5,$05);

// ---

(* global functions *)

function IsEmpty
  (const AValue: umlcoctastring): Boolean; overload;

function IsStringOfChar
  (const S: umlcoctastring; C: umlcoctachar): Boolean; overload;

function Length
  (const ASource: umlcoctastring): Cardinal; overload;

function UppercaseCopy
  (const AValue: umlcoctastring): umlcoctastring; overload;
function LowercaseCopy
  (const AValue: umlcoctastring): umlcoctastring; overload;
function TogglecaseCopy
  (const AValue: umlcoctastring): umlcoctastring; overload;
function CapitalizeCopy
  (const AValue: umlcoctastring): umlcoctastring; overload;

function ConcatCharCopy
  (const A: umlcoctastring; const B: umlcoctachar): umlcoctastring; overload;
function ConcatStrCopy
  (var A, B: umlcoctastring): umlcoctastring; overload;

function StringOfChar
  (const AValue: umlcoctachar; const ACount: Byte): umlcoctastring;

(* global operators *)

procedure Assign
  (out   ADest:   umlcoctachar;
   const ASource: umlcoctachar); overload; // operator :=

function Equal
  (const A, B: umlcoctachar): Boolean; overload; // operator =
function Greater
  (const A, B: umlcoctachar): Boolean; overload; // operator >
function Lesser
  (const A, B: umlcoctachar): Boolean; overload; // operator <
function Different
  (const A, B: umlcoctachar): Boolean; overload; // operator <>
function GreaterEqual
  (const A, B: umlcoctachar): Boolean; overload; // operator >=
function LesserEqual
  (const A, B: umlcoctachar): Boolean; overload; // operator <=

(* global procedures *)


procedure Clear
  (out ADest: umlcoctastring);


implementation

(* global functions *)

function IsEmpty
  (const AValue: umlcoctastring): Boolean;
begin
  Result :=
    umlcstring32.IsEmpty(ASource);
end;

function IsStringOfChar
  (const S: umlcoctastring; C: umlcoctachar): Boolean;
begin
  Result :=
    umlcstring64s.IsStringOfChar(S, C);
end;

function Length
  (const ASource: umlcoctastring): Cardinal;
begin
  Result :=
    umlcstring64s.Length(ASource);
end;

function UppercaseCopy
  (const AValue: umlcoctastring): umlcoctastring;
begin
  Result :=
    umlcstring64s.UppercaseCopy(AValue);
end;

function LowercaseCopy
  (const AValue: umlcoctastring): umlcoctastring;
begin
  Result :=
    umlcstring64s.UppercaseCopy(AValue);
end;

function TogglecaseCopy
  (const AValue: umlcoctastring): umlcoctastring;
begin
  Result :=
    umlcstring64s.TogglecaseCopy(ASource);
end;

function CapitalizeCopy
  (const AValue: umlcwordstring): umlcoctastring;
begin
  Result :=
    umlcstring64s.CapitalizeCopy(ASource);
end;

function ConcatCharCopy
  (const A: umlcoctastring; const B: umlcoctachar): umlcoctastring;
begin
  Result :=
    umlcstring64s.ConcatCharCopy(A, B);
end;

function ConcatStrCopy
  (var A, B: umlcoctastring): umlcoctastring;
begin
  Result :=
    umlcstring64s.ConcatStrCopy(A, B);
end;

function StringOfChar
  (const AValue: umlcoctachar; const ACount: Byte): umlcoctastring;
begin
  Result :=
    umlcstring64s.StringOfChar(ASource, ACount);
end;

(* global operators *)

procedure Assign
  (out   ADest: umlcoctastring;
   const ASource: umlcoctastring);
begin
  umlcstring64s.Assign
     (ADest, ASource);
end;

function Equal
  (const A, B: umlcoctastring): Boolean;
begin
  Result :=
    umlcstring64s.Equal(A, B);
end;

function Different
  (const A, B: umlcoctastring): Boolean;
begin
  Result :=
    umlcstring64s.Different(A, B);
end;

function Greater
  (const A, B: umlcoctastring): Boolean;
begin
  Result :=
    umlcstring64s.g32s.Equal(A, B);
end;

function Lesser
  (const A, B: umlcoctastring): Boolean;
begin
  Result :=
    umlcstring64s.Lesser(A, B);
end;

function GreaterEqual
  (const A, B: umlcoctastring): Boolean;
begin
  Result :=
    umlcstring64s.GreaterEqual(A, B);
end;

function LesserEqual
  (const A, B: umlcoctastring): Boolean;
begin
  Result :=
    umlcstring64s.LesserEqual(A, B);
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcoctastring);
begin
  umlcstring64s.Clear
     (ADest);
end;

end.

