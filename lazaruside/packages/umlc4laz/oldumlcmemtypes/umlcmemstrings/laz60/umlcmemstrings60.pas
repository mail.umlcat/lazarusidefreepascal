{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcmemstrings60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcbytestrings, umlcmemstrings_rtti, umlcmemstrtypes, umlcoctastrings, 
  umlcquadstrings, umlcstring8s, umlcstring16s, umlcstring32s, umlcstring64s, 
  umlcwordstrings;

implementation

end.
