unit umlcmemdatetimebyptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To reimplement a Date and Time type and it's operations,
 ** by using pointers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcmemdatetimetypes,
  dummy;

// ---

const

 MOD_umlcmemdatetimebyptrs : TUMLCModule =
   ($2F,$14,$D3,$C9,$65,$85,$AF,$43,$B7,$60,$44,$68,$DE,$C9,$19,$A6);

// ---


implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemdatetime): pointer;
var P: umlcpmemdatetime;
begin
  System.GetMem(P, sizeof(umlcmemdatetime));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcmemdatetime));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcmemdatetime), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;



end.

