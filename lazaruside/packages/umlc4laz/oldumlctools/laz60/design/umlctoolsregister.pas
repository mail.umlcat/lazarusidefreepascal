unit umlctoolsregister;

interface
uses
  Classes,
  umlcfilefilters,
  umlcbookmngrs,
  umlcundomngrs,
  umlcdocs,
  umlcmsgctrls,
  {$IFDEF FPC}
  LResources,
  {$ENDIF}
  dummy;

  procedure Register;

implementation

{$IFDEF DELPHI}
{.R 'umlcfilefilters.lrs'}
{.R 'umlcbookmngrs.lrs'}

{.R 'umlcmsgctrls.lrs'}

{.R 'umlcundomngrs.lrs'}
{.R 'umlcdocs.lrs'}
{$ENDIF}

procedure Register;
const
  PaletteTools = 'UMLCat Tools';
begin
  RegisterComponents(PaletteTools, [TUMLCFileFiltersContainer]);
  RegisterComponents(PaletteTools, [TUMLCBookMarkMngr]);

  RegisterComponents(PaletteTools, [TUMLCMsgServer]);
  RegisterComponents(PaletteTools, [TUMLCSingleClientMsgServer]);
  RegisterComponents(PaletteTools, [TUMLCMsgClient]);
  RegisterComponents(PaletteTools, [TUMLCSingleServerMsgClient]);

  RegisterComponents(PaletteTools, [TUMLCUnDoManager]);

  RegisterComponents(PaletteTools, [TUMLCDocument]);
  RegisterComponents(PaletteTools, [TUMLCDelegateDocument]);
end;

initialization
{$IFDEF FPC}
{$I 'umlcfilefilters.lrs'}
{$I 'umlcbookmngrs.lrs'}

{$I 'umlcmsgctrls.lrs'}

{$I 'umlcundomngrs.lrs'}
{$I 'umlcdocs.lrs'}
{$ENDIF}
end.

