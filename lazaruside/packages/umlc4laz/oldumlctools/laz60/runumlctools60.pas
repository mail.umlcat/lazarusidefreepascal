{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit runumlctools60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcactcntrls, umlcbookmngrs, umlccomponents, umlcdocs, umlcdocsres, 
  umlcfilefilters, umlcmsgctrls, umlcmsgdlgstrs, umlcmsgdlgtypes, 
  umlcmsgtypes, umlcressrchmngrs, umlcressrchtxtdlgs, umlcsrchtypes, 
  umlcstdevents, umlctreestates, umlcundomngrs, umlcconsoles;

implementation

end.
