(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file UMLC.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcundomngrs;

interface
uses
  SysUtils,
  Classes,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcnormobjects,
  umlccomponents,
  dummy;

type

  TUMLCTextOperationCode =
    (txopNone,
     txopBackupText, txopRestoreText,
     txopUnDoRemoveText, txopUnDoInsertText);

(* TUMLCTextOperation *)

  TUMLCTextOperation =
    class(TCollectionItem)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FOperation: TUMLCTextOperationCode;
    FTextIndex: Integer;
    FCaretIndex: Integer;
    FText: string;
  public
    (* public declarations *)

    constructor Create(ACollection: TCollection); override;

    property Operation: TUMLCTextOperationCode
      read FOperation write FOperation;
    property CaretIndex: Integer
      read FCaretIndex write FCaretIndex;
    property TextIndex: Integer
      read FTextIndex write FTextIndex;
    property Text: string
      read FText write FText;
  end;

(* TUMLCTextOperations *)

  TUMLCTextOperations =
    class(TCollection)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)
  end;

  TTextChangeEvent =
    procedure (Sender: TObject; const Item: TUMLCTextOperation)
    of object;

(* TCustomUMLCUnDoManager *)

  TCustomUMLCUnDoManager =
    class(TComponent)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FOnUnDo: TTextChangeEvent;
    FOnReDo: TTextChangeEvent;

    FBackupList:  TUMLCTextOperations;
    FRestoreList: TUMLCTextOperations;

    procedure DelegateOnUnDo(const Item: TUMLCTextOperation);
    procedure DelegateOnReDo(const Item: TUMLCTextOperation);
  public
    (* public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure BackupText(CaretIndex, TextIndex: Integer; Data: string);
    procedure RestoreText(CaretIndex, TextIndex: Integer; Data: string);

    procedure InsertText(CaretIndex, TextIndex: Integer; Data: string);
    procedure RemoveText(CaretIndex, TextIndex: Integer; Data: string);

    function UnDo: Boolean;
    function ReDo: Boolean;

    property BackupList: TUMLCTextOperations
      read FBackupList write FBackupList;
    property RestoreList: TUMLCTextOperations
      read FRestoreList write FRestoreList;
  published
    (* published declarations *)

    property OnUnDo: TTextChangeEvent
      read FOnUnDo write FOnUnDo;
    property OnReDo: TTextChangeEvent
      read FOnReDo write FOnReDo;
  end;

(* TUMLCUnDoManager *)

  TUMLCUnDoManager =
    class(TCustomUMLCUnDoManager)

    (* TCustomUMLCUnDoManager: *)

    property OnUnDo;
    property OnReDo;
  end;

implementation

(* TUMLCTextOperation *)

constructor TUMLCTextOperation.Create(ACollection: TCollection);
begin
  inherited Create(ACollection);

  FOperation := txopNone;

  FCaretIndex := -1;
  // posicion del caqret antes de la modificacion, comienza en 0
  // position of caret before modification, starts in 0

  FTextIndex := -1;
  // ubicacion de buffer en donde el texto fue cambiado, comienza en 0
  // location in buffer where text was changed, starts in 0

  FText := '';
  // text that was modified
  // texto que fue modificado
end;

(* TUMLCTextOperations *)

(* TCustomUMLCUnDoManager *)

procedure TCustomUMLCUnDoManager.DelegateOnUnDo
  (const Item: TUMLCTextOperation);
begin
  if (Assigned(FOnUnDo))
    then FOnUnDo(Self, Item);
end;

procedure TCustomUMLCUnDoManager.DelegateOnReDo
  (const Item: TUMLCTextOperation);
begin
  if (Assigned(FOnReDo))
    then FOnReDo(Self, Item);
end;

constructor TCustomUMLCUnDoManager.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FBackupList  := TUMLCTextOperations.Create(TUMLCTextOperation);
  FRestoreList := TUMLCTextOperations.Create(TUMLCTextOperation);

  FOnUnDo:= nil;
  FOnReDo:= nil;
end;

destructor TCustomUMLCUnDoManager.Destroy;
begin
  FRestoreList.Free;
  FBackupList.Free;
  inherited Destroy;
end;

procedure TCustomUMLCUnDoManager.BackupText
  (CaretIndex, TextIndex: Integer; Data: string);
var AItem: TUMLCTextOperation;
begin
  AItem := (BackupList.Add as TUMLCTextOperation);
  AItem.Operation := txopRestoreText;
  AItem.CaretIndex := CaretIndex;
  AItem.TextIndex := TextIndex;
  AItem.Text := Data;
  (* Objetivo: Respaldar una porcion de texto que no cambio su longuitud .*)
  (* Goal: Backup a portion of text that didn*t change its length .*)
end;

procedure TCustomUMLCUnDoManager.RestoreText
  (CaretIndex, TextIndex: Integer; Data: string);
var AItem: TUMLCTextOperation;
begin
  AItem := (BackupList.Add as TUMLCTextOperation);
  AItem.Operation := txopBackupText;
  AItem.CaretIndex := CaretIndex;
  AItem.TextIndex := TextIndex;
  AItem.Text := Data;
  (* Objetivo: Restaurar una porcion de texto que no cambio su longuitud .*)
  (* Goal: Restore a portion of text that didn*t change its length .*)
end;

procedure TCustomUMLCUnDoManager.InsertText
  (CaretIndex, TextIndex: Integer; Data: string);
var AItem: TUMLCTextOperation;
begin
  AItem := (BackupList.Add as TUMLCTextOperation);
  AItem.Operation := txopUnDoInsertText;
  AItem.CaretIndex := CaretIndex;
  AItem.TextIndex := TextIndex;
  AItem.Text := Data;
end;

procedure TCustomUMLCUnDoManager.RemoveText
  (CaretIndex, TextIndex: Integer; Data: string);
var AItem: TUMLCTextOperation;
begin
  AItem := (BackupList.Add as TUMLCTextOperation);
  AItem.Operation := txopUnDoRemoveText;
  AItem.CaretIndex := CaretIndex;
  AItem.TextIndex := TextIndex;
  AItem.Text := Data;
end;

function TCustomUMLCUnDoManager.UnDo: Boolean;
var ACount: Integer; Source, Dest: TUMLCTextOperation;
begin
  ACount := BackupList.Count;
  Result := (BackupList.Count > 0);
  if (Result) then
  begin
    Source := (BackupList.Items[Pred(ACount)] as TUMLCTextOperation);

    Dest := (RestoreList.Add as TUMLCTextOperation);
    case (Source.Operation) of
      txopUnDoRemoveText: Dest.Operation := txopUnDoInsertText;
      txopUnDoInsertText: Dest.Operation := txopUnDoRemoveText;
      txopBackUpText:     Dest.Operation := txopRestoreText;
      txopRestoreText:    Dest.Operation := txopBackUpText;
      else Dest.Operation := txopNone;
    end;

    Dest.CaretIndex := Source.CaretIndex;
    Dest.TextIndex := Source.TextIndex;
    Dest.Text := Source.Text;
    // mover el elemento a la lista de restaurar
    // move the item to RestoreList*s list

    DelegateOnUnDo(Source);
  end;
end;

function TCustomUMLCUnDoManager.ReDo: Boolean;
var ACount: Integer; AItem: TUMLCTextOperation;
begin
  ACount := RestoreList.Count;
  Result := (RestoreList.Count > 0);
  AItem  := (RestoreList.Items[Pred(ACount)] as TUMLCTextOperation);
  DelegateOnReDo(AItem);
end;

end.
