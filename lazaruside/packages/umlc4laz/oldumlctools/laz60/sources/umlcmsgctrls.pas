(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file UMLC.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmsgctrls;

interface
uses
  SysUtils, 
  Classes,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlclists,
  //umlcreclists,
  umlcactcntrls,
  umlcguidstrs,
  umlcmsgtypes,
  dummy;

const

  // ---

  ID_TUMLCMsgServer : TUMLCType =
    ($08,$4F,$B6,$24,$B7,$92,$44,$48,$B6,$88,$2A,$6E,$D2,$99,$B0,$5B);

  ID_TUMLCSingleClientMsgServer : TUMLCType =
    ($C1,$51,$E2,$1E,$BB,$DF,$87,$43,$8C,$90,$67,$33,$87,$63,$3D,$ED);

  ID_TUMLCMsgClient : TUMLCType =
    ($0D,$5F,$9C,$4E,$FC,$15,$90,$40,$8F,$DD,$7A,$A4,$F8,$6B,$50,$B2);

  ID_TUMLCSingleServerMsgClient : TUMLCType =
    ($0D,$0A,$41,$8E,$DD,$E5,$5C,$4D,$A9,$93,$B2,$7D,$F7,$36,$D9,$11);

  // ---

type

(* TUMLCMsgServer *)

  TUMLCMsgServer = class(TUMLCActivatedComponent, IUMLCMessageServer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FClients: TUMLCMsgClientList;
  protected
    (* Protected declarations *)

    function getClients(): TUMLCMsgClientList;

    procedure setClients(const AValue: TUMLCMsgClientList);
  protected
    (* Protected declarations *)

    function CreateClients(): TUMLCMsgClientList; virtual;
  public
    (* Public declarations *)

    procedure InsertClient
      (const AClient: IUMLCMessageClient);
    procedure RemoveClient
      (const AClient: IUMLCMessageClient);
  public
    (* Public declarations *)

    //procedure InsertClient(const AClient: IUMLCMessageClient);
    //procedure RemoveClient(const AClient: IUMLCMessageClient);

    function AsComponent(): TComponent;

    procedure SendMessage
      (const AMsgRec: TUMLCMessageParamsRecord);
    procedure SendMessageSingle
      (const AClient: IUMLCMessageClient; const AMsgRec: TUMLCMessageParamsRecord);
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
    destructor Destroy(); override;
  public
    (* Public declarations *)

    (* Read-Only properties *)

    function ClientsCount(): Integer;
    function HasClients(): Boolean;

    (* Never Published declarations *)

    property Clients: TUMLCMsgClientList
      read getClients write setClients;
  end;

(* TUMLCSingleClientMsgServer *)

  TUMLCSingleClientMsgServer = class(TUMLCMsgServer, IUMLCSingleMessageServer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function CurrentClient(): IUMLCMessageClient;
    function CreateClients(): TUMLCMsgClientList; override;
  public
    (* Public declarations *)

    procedure SendMessageCurrent
      (const AMsgRec: TUMLCMessageParamsRecord);
  end;

(* TUMLCMsgClient *)

  TUMLCMsgClient = class(TUMLCActivatedComponent, IUMLCMessageClient)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FServer: TUMLCMsgServer;
    FOnAnswerMessage: TUMLCMsgEventHandler;

    function getServer(): TUMLCMsgServer; virtual;

    procedure setServer(const AValue: TUMLCMsgServer); virtual;
  protected
    (* Protected declarations *)

    procedure DelegateOnAnswerMessage(const AMsgRec: TUMLCMessageParamsRecord);

    procedure Notification
      (AComponent: TComponent; Operation: TOperation); override;
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
  public
    (* Public declarations *)

    procedure AnswerMessage
     (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    function AsComponent(): TComponent;
  protected
    (* Protected declarations *)

    property Server: TUMLCMsgServer
      read getServer write setServer;
  published
    (* Published declarations *)

    //property Server: TUMLCMsgServer
    //  read getServer write setServer;
    property OnAnswerMessage: TUMLCMsgEventHandler
      read FOnAnswerMessage write FOnAnswerMessage;
  end;

(* TUMLCSingleServerMsgClient *)

  TUMLCSingleServerMsgClient =
    class(
    TUMLCActivatedComponent,
    IUMLCMessageClient,
    IUMLCSingleServerMessageClient)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FServer: IUMLCMessageServer;
    FOnAnswerMessage: TUMLCMsgEventHandler;

    function getServer(): IUMLCMessageServer;

    procedure setServer(const AValue: IUMLCMessageServer);
  protected
    (* Protected declarations *)

    procedure DelegateOnAnswerMessage(const AMsgRec: TUMLCMessageParamsRecord);

    procedure Notification
      (AComponent: TComponent; Operation: TOperation); override;
  protected
    (* Protected declarations *)

    property Server: IUMLCMessageServer
      read getServer write setServer;
  public
    (* Public declarations *)

    procedure AnswerMessage
     (const AMsgRec: TUMLCMessageParamsRecord); virtual;

    function AsComponent(): TComponent;

    //property Server: IUMLCMessageServer
    //  read getServer write setServer;
  end;

implementation

(* TUMLCMsgServer *)

function TUMLCMsgServer.getClients(): TUMLCMsgClientList;
begin
  Result := FClients
  // Goal: "Clients" property get method .
  // Objetivo: Metodo lectura para propiedad "Clients".
end;

procedure TUMLCMsgServer.setClients(const AValue: TUMLCMsgClientList);
begin
  FClients := AValue;
  // Goal: "Clients" property set method .
  // Objetivo: Metodo escritura para propiedad "Clients".
end;

function TUMLCMsgServer.CreateClients(): TUMLCMsgClientList;
begin
  Result := TUMLCMsgClientList.Create();
  Result.RecordSize := System.SizeOf(TUMLCMsgItemRec);
end;

procedure TUMLCMsgServer.InsertClient
  (const AClient: IUMLCMessageClient);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  if (Clients.IndexOf(AClient) = IndexNotFound) then
  begin
    // perform subscription
    Clients.Insert(AClient);

    // confirm subscription to client
    umlcguidstrs.DoubleStrToGUID
      (msgServerAssign, AMsgRec.Message);

    AMsgRec.Sender  := Self;
    AMsgRec.Param   := AClient;
    SendMessageSingle(AClient, AMsgRec);
  end;
end;

procedure TUMLCMsgServer.RemoveClient
  (const AClient: IUMLCMessageClient);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  if (Clients.IndexOf(AClient) = IndexNotFound) then
  begin
    // confirm cancelation of subscription to client
    umlcguidstrs.DoubleStrToGUID
      (msgServerDeassign, AMsgRec.Message);

    AMsgRec.Sender  := Self;
    AMsgRec.Param   := AClient;
    SendMessageSingle(AClient, AMsgRec);

    // perform cancelation of subscription
    Clients.Remove(AClient);
  end;
end;

function TUMLCMsgServer.AsComponent(): TComponent;
begin
  Result := Self;
end;

procedure TUMLCMsgServer.SendMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
var I, ALast: Integer; Client: IUMLCMessageClient;
begin
  if (Clients <> nil) then
  begin
    ALast := (Clients.Count - 1);
    if (HasClients()) then
    begin
      for I := 0 to ALast do
      begin
        Client := Clients[i];
        Client.AnswerMessage(AMsgRec);
      end;
    end;
  end;
  // Goal: Notifies clients that server has changed.
  // Objetivo: Notificar a los clientes que el servidor ha cambiado.
end;

procedure TUMLCMsgServer.SendMessageSingle
  (const AClient: IUMLCMessageClient; const AMsgRec: TUMLCMessageParamsRecord);
begin
  if (AClient <> nil) then
  begin
    AClient.AnswerMessage(AMsgRec);
  end;
end;

constructor TUMLCMsgServer.Create
  (AOwner: TComponent);
begin
  //inherited Create(AOwner);
  FClients := Self.CreateClients();
  // Goal: To prepare the navigator.
  // Objetivo: Preparar el navegador.
end;

destructor TUMLCMsgServer.Destroy();
begin
  FClients.Empty();
  FClients.Free();
  //inherited Destroy();
  // Goal: To unprepare the navigator.
  // Objetivo: Despreparar el navegador.
end;

function TUMLCMsgServer.ClientsCount(): Integer;
begin
  Result := Clients.Count;
end;

function TUMLCMsgServer.HasClients(): Boolean;
begin
  Result := (Clients.Count > 0);
end;

(* TUMLCSingleClientMsgServer *)

function TUMLCSingleClientMsgServer.CurrentClient(): IUMLCMessageClient;
var Index: Integer;
begin
  Index  := (Clients as TUMLCCurrentMsgClientList).CurrentIndex;
  Result := Clients.Items[Index];
end;

procedure TUMLCSingleClientMsgServer.SendMessageCurrent
  (const AMsgRec: TUMLCMessageParamsRecord);
var Client: IUMLCMessageClient;
begin
  Client := CurrentClient();
  if (Client <> nil) then
  begin
    Client.AnswerMessage(AMsgRec);
  end;
end;

function TUMLCSingleClientMsgServer.CreateClients(): TUMLCMsgClientList;
begin
  Result := TUMLCCurrentMsgClientList.Create();
end;

(* TUMLCMsgClient *)

procedure TUMLCMsgClient.DelegateOnAnswerMessage
 (const AMsgRec: TUMLCMessageParamsRecord);
begin
  if (Self.FOnAnswerMessage <> nil) then
  begin
    Self.FOnAnswerMessage(AMsgRec);
  end;
end;

procedure TUMLCMsgClient.AnswerMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  Self.DelegateOnAnswerMessage(AMsgRec);
end;

function TUMLCMsgClient.AsComponent(): TComponent;
begin
  Result := Self;
end;

function TUMLCMsgClient.getServer(): TUMLCMsgServer;
begin
  Result :=
    Self.FServer;
  // Goal: "Server" property set method.
  // Objetivo: Metodo lectura propiedad "Server".
end;

procedure TUMLCMsgClient.setServer(const AValue: TUMLCMsgServer);
begin
  if (Self.FServer <> AValue) then
  begin
    if (Self.FServer <> nil) then
    begin
      Self.FServer.RemoveClient(Self);
    end;
    // erase client from previous server
    // borrar cliente de servidor anterior

    Self.FServer := AValue;
    // update reference
    // actualizar referencia

    if (Self.FServer <> nil) then
    begin
      Self.FServer.InsertClient(Self);
    end;
    // insert client in new server
    // insertar cliente en servidor nuevo
  end;
  // Goal: "Server" property set method.
  // Objetivo: Metodo escritura propiedad "Server".
end;

procedure TUMLCMsgClient.Notification
  (AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if ((Operation = opRemove) and (AComponent = FServer)) then
  begin
    if (Self.FServer <> nil) then
    begin
      Self.FServer := nil;
    end;
  end;
  // Goal: To detect & update when associated components,
  // have been removed.

  // Objetivo: Detectar y actualizar cuando,
  // los componentes asociados se han removido.
end;

constructor TUMLCMsgClient.Create
  (AOwner: TComponent);
begin
  inherited;
  Self.FServer := nil;
end;

(* TUMLCSingleServerMsgClient *)

function TUMLCSingleServerMsgClient.getServer(): IUMLCMessageServer;
begin
  Result := Self.FServer;
  // Goal: "Server" property set method.
  // Objetivo: Metodo lectura propiedad "Server".
end;

procedure TUMLCSingleServerMsgClient.setServer
  (const AValue: IUMLCMessageServer);
var AClient: IUMLCMessageClient;
begin
  if (Self.FServer <> AValue) then
  begin
    if (Self.FServer <> nil) then
    begin
      AClient := IUMLCMessageClient(Self);
      FServer.RemoveClient(AClient);
    end;
    // erase client from previous server
    // borrar cliente de servidor anterior

    Self.FServer := AValue;
    // update reference
    // actualizar referencia

    if (FServer <> nil) then
    begin
      Self.FServer.InsertClient(Self);
    end;
    // insert client in new server
    // insertar cliente en servidor nuevo
  end;
  // Goal: "Server" property set method.
  // Objetivo: Metodo escritura propiedad "Server".
end;

procedure TUMLCSingleServerMsgClient.DelegateOnAnswerMessage
 (const AMsgRec: TUMLCMessageParamsRecord);
begin
  if (FOnAnswerMessage <> nil) then
  begin
    FOnAnswerMessage(AMsgRec);
  end;
end;

procedure TUMLCSingleServerMsgClient.Notification
  (AComponent: TComponent; Operation: TOperation);
var FComponent: TComponent;
begin
  inherited Notification(AComponent, Operation);

  if (FServer <> nil) then
  begin
    FComponent := FServer.AsComponent();
    if ((Operation = opRemove) and (AComponent = FComponent)) then
    begin
      FServer := nil;
    end;
  end;
  // Goal: To detect & update when associated components,
  // have been removed.

  // Objetivo: Detectar y actualizar cuando,
  // los componentes asociados se han removido.
end;

procedure TUMLCSingleServerMsgClient.AnswerMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
begin
  DelegateOnAnswerMessage(AMsgRec);
end;

function TUMLCSingleServerMsgClient.AsComponent(): TComponent;
begin
  Result := Self;
end;



end.
