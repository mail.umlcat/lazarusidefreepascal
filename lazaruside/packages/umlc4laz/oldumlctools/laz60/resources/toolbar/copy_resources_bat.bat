rem echo "Hello World"
rem @ECHO OFF

cd "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\resources\toolbar\umlcbookmngrs"
copy "*.lrs" "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\design\*.lrs"

cd "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\resources\toolbar\umlcdocs"
copy "*.lrs" "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\design\*.lrs"

cd "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\resources\toolbar\umlcfilefilters"
copy "*.lrs" "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\design\*.lrs"

cd "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\resources\toolbar\umlcmsgctrls"
copy "*.lrs" "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\design\*.lrs"

cd "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\resources\toolbar\umlcundomngrs"
copy "*.lrs" "C:\softdev\lazarus\packages\umlc4laz\umlctools\laz60\design\*.lrs"

PAUSE
