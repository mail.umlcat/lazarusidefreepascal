(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file UMLC.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdbcompdatasets;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids, umlcguidstrs,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlcactivatedcontrols,
  umlcmsgtypes, umlcmsgctrls,
  DB,
  umlcdbs, umlcdbdatasets, umlcdbcmds,
  dummy;
  
  // toxdo: "*custom*"
  
const

  // ---

  ID_TUMLCCustomDBComposedDataset : TUMLCType =
   ($1E,$EE,$9D,$A1,$DC,$1F,$0D,$46,$B5,$1E,$22,$6D,$74,$32,$D7,$E1);

  // ---


type

  (* TUMLCCustomDBComposedDataset *)

  TUMLCCustomDBComposedDataset =
    class(TCustomUMLCDBDataset)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    //FClients: TUMLCMsgClientList;
  protected
    (* Protected declarations *)

    function getInsertSQL(): TStrings; virtual;
    function getUpdateSQL(): TStrings; virtual;
    function getDeleteSQL(): TStrings; virtual;

    procedure setInsertSQL
      (const ASQL: TStrings); virtual;
    procedure setUpdateSQL
      (const ASQL: TStrings); virtual;
    procedure setDeleteSQL
      (const ASQL: TStrings); virtual;
  public
    (* Public declarations *)

    constructor Create
      (AOwner: TComponent); override;
    destructor Destroy(); override;
  protected
    (* Protected declarations *)

    property InsertSQL: TStrings
      read getInsertSQL write setInsertSQL;
	  
    property UpdateSQL: TStrings
      read getUpdateSQL write setUpdateSQL;
	  
    property DeleteSQL: TStrings
      read getDeleteSQL write setDeleteSQL;
  end;



implementation

(* TUMLCCustomDBComposedDataset *)

constructor TUMLCCustomDBComposedDataset.Create
  (AOwner: TComponent);
begin
  inherited Create(AOwner);
  //Self.FServer := nil;
end;

destructor TUMLCCustomDBComposedDataset.Destroy();
begin
  //Self.FServer := nil;
  inherited Destroy();
end;

function TUMLCCustomDBComposedDataset.getInsertSQL(): TStrings;
begin
  //Result :=
  //  Self.FServer;
  
  Result := nil;
  // Goal: "InsertSQL" property set method.
  // Objetivo: Metodo lectura propiedad "InsertSQL".
end;

function TUMLCCustomDBComposedDataset.getUpdateSQL(): TStrings;
begin
  //Result :=
  //  Self.FServer;
  
  Result := nil;
  // Goal: "UpdateSQL" property set method.
  // Objetivo: Metodo lectura propiedad "UpdateSQL".
end;

function TUMLCCustomDBComposedDataset.getDeleteSQL(): TStrings;
begin
  //Result :=
  //  Self.FServer;
  
  Result := nil;
  // Goal: "DeleteSQL" property set method.
  // Objetivo: Metodo lectura propiedad "DeleteSQL".
end;

procedure TUMLCCustomDBComposedDataset.setInsertSQL
    (const ASQL: TStrings);
begin
  if (ASQL <> nil) then
  begin
    Self.DoNothing()
  end;
  // Goal: "InsertSQL" property set method.
  // Objetivo: Metodo escritura para propiedad "InsertSQL".
end;

procedure TUMLCCustomDBComposedDataset.setUpdateSQL
    (const ASQL: TStrings);
begin
  if (ASQL <> nil) then
  begin
    Self.DoNothing()
  end;
  // Goal: "UpdateSQL" property set method.
  // Objetivo: Metodo escritura para propiedad "UpdateSQL".
end;

procedure TUMLCCustomDBComposedDataset.setDeleteSQL
    (const ASQL: TStrings);
begin
  if (ASQL <> nil) then
  begin
    Self.DoNothing()
  end;
  // Goal: "DeleteSQL" property set method.
  // Objetivo: Metodo escritura para propiedad "DeleteSQL".
end;

end.
