{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit runumlcdb;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcdbs, umlcdbdatasets, umlcdbtables, umlcdbcmds, LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('runumlcdb', @Register);
end.
