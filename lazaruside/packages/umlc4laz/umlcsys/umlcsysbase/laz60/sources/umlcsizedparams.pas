(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsizedparams;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysConst, SysUtils, Math,
  umlclibc,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcsizedparams : TUMLCModule =
   ($05,$A9,$5B,$F1,$36,$88,$35,$41,$83,$9C,$B1,$1A,$25,$84,$52,$87);

// ---

(* global types *)

type
  umlcsizedparameter = record
    ItemData: pointer;
    ItemSize: Cardinal;
  end;
  umlctsizedparameter = (* alias of *) umlcsizedparameter;
  umlcpsizedparameter = ^umlcsizedparameter;

type
  umlcsizedparameters = (* redefine *) type pointer;

type
  umlctsizedparametersarray = array[0..65535] of umlcpsizedparameter;
  umlcpsizedparametersarray = ^umlctsizedparametersarray;

type
  umlcsizedparametersheader = record
    // how many items can be stored
    ItemsMaxCount:  Cardinal;

    // how many items are currently stored
    ItemsCurrCount: Cardinal;

    // ...
  end;
  umlctsizedparametersheader = (* alias of *) umlcsizedparametersheader;
  umlcpsizedparametersheader = ^umlcsizedparametersheader;

type
  umlcsizedparametersrecord = record
    Header: umlctsizedparametersheader;
    Items:  umlcpsizedparametersarray;
  end;
  umlctsizedparametersrecord = (* alias of *) umlcsizedparametersrecord;
  umlcpsizedparametersrecord = ^umlcsizedparametersrecord;

(* global functions *)

function paramsreadmaxcount
  (const AParams: umlcsizedparameters): Cardinal;

function paramsreadcurrcount
  (const AParams: umlcsizedparameters): Cardinal;

function paramscreate
  (const AMaxCount: Cardinal): umlcsizedparameters;

(* global procedures *)

procedure paramsdrop
  (var AParams: umlcsizedparameters);

implementation

(* global functions *)

function paramsreadmaxcount
  (const AParams: umlcsizedparameters): Cardinal;
var AParamsRecord: umlcpsizedparametersrecord;
begin
  AParamsRecord :=
    umlcpsizedparametersrecord(AParams);

  Result :=
    AParamsRecord^.Header.ItemsMaxCount;
end;

function paramsreadcurrcount
  (const AParams: umlcsizedparameters): Cardinal;
var AParamsRecord: umlcpsizedparametersrecord;
begin
  AParamsRecord :=
    umlcpsizedparametersrecord(AParams);

  Result :=
    AParamsRecord^.Header.ItemsCurrCount;
end;

function paramscreate
  (const AMaxCount: Cardinal): umlcsizedparameters;
var AParamsRecord: umlcpsizedparametersrecord;
    AItemsSize:    Cardinal;
begin
  Result := nil;

  // allocate full record
  System.GetMem
    (AParamsRecord, sizeof(umlctsizedparametersrecord));

  AItemsSize :=
    (AMaxCount * sizeof(umlcpsizedparameter));

  // allocate items
  System.GetMem
    (AParamsRecord^.Items, AItemsSize);

  // clear items
  umlclibc.FillByte
    (AParamsRecord^.Items, AItemsSize, 0);

  // add final count
  AParamsRecord^.Header.ItemsMaxCount :=
    AMaxCount;

  AParamsRecord^.Header.ItemsCurrCount :=
    0;

  Result :=
    AParamsRecord;
end;

(* global procedures *)

procedure paramsdrop
  (var AParams: umlcsizedparameters);
var AParamsRecord: umlcpsizedparametersrecord;
    AItemsSize:    Cardinal;
begin
  if (AParams <> nil) then
  begin
    AParamsRecord :=
      umlcpsizedparametersrecord(AParams);

    // Items are removed separatly

    AItemsSize :=
      (AParamsRecord^.Header.ItemsMaxCount * sizeof(umlcpsizedparameter));

    // deallocate items
    System.FreeMem
      (AParamsRecord^.Items, AItemsSize);

    // deallocate full record
    System.FreeMem
      (AParamsRecord, sizeof(umlcpsizedparametersrecord));
  end;
end;


end.

