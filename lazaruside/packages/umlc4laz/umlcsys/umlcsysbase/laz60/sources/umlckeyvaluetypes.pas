(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlckeyvaluetypes;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  dummy;

// ---

const

 MOD_umlckeyvalues : TUMLCModule =
  ($A1,$B1,$E3,$B1,$14,$07,$5A,$4C,$8E,$63,$01,$13,$53,$3E,$F5,$DC);

 // ---

 const

 // ---

 ID_TUMLCANSIShortKeyValueItem : TUMLCType =
   ($58,$4B,$E9,$A2,$9E,$2A,$BF,$4D,$8E,$20,$0C,$12,$A4,$C2,$45,$79);

 ID_TUMLCANSIShortDictionaryItem : TUMLCType =
   ($67,$6D,$D9,$A4,$FC,$36,$04,$47,$85,$B2,$DE,$CD,$14,$63,$64,$34);

 ID_TUMLCANSIShortTypedDictionaryItem : TUMLCType =
  ($39,$37,$37,$58,$5D,$89,$F6,$46,$B4,$BC,$AF,$4C,$9A,$38,$64,$E4);

// ---

  ID_TUMLCWideShortKeyValueItem : TUMLCType =
    ($48,$08,$DF,$98,$9B,$B7,$9D,$45,$9B,$14,$3E,$EE,$7D,$DC,$77,$5A);

  ID_TUMLCWideShortDictionaryItem : TUMLCType =
    ($DF,$CE,$BD,$2A,$5D,$83,$7A,$4E,$94,$6F,$59,$3C,$8A,$CE,$D8,$DC);

  ID_TUMLCWideShortTypedDictionaryItem : TUMLCType =
    ($A6,$23,$1C,$BE,$3B,$7A,$E7,$4A,$B2,$75,$EC,$2D,$79,$9F,$5E,$78);

(* global types *)

type
  TUMLCANSIShortKeyValueItem = record
    Key:   umlcansishortstring;
  end;

  TUMLCANSIShortDictionaryItem = record
    Key:   umlcansishortstring;
    Value: umlcansishortstring;
  end;

  TUMLCANSIShortTypedDictionaryItem = record
    Key:   umlcansishortstring;
    Value: umlcansishortstring;
    Typed: umlcansishortstring;
  end;

  PUMLCANSIShortKeyValueItem        = ^TUMLCANSIShortKeyValueItem;
  PUMLCANSIShortDictionaryItem      = ^TUMLCANSIShortDictionaryItem;
  PUMLCANSIShortTypedDictionaryItem = ^TUMLCANSIShortTypedDictionaryItem;

  umlcansishortkeyvalueitem         = ^TUMLCANSIShortKeyValueItem;
  umlcansishortdictionaryitem       = ^TUMLCANSIShortDictionaryItem;
  umlcansishorttypeddictionaryitem  = ^TUMLCANSIShortTypedDictionaryItem;

// ---

type
  TUMLCWideShortKeyValueItem = record
    Key:   umlcwideshortstring;
  end;

  TUMLCWideShortDictionaryItem = record
    Key:   umlcwideshortstring;
    Value: umlcwideshortstring;
  end;

  TUMLCWideShortTypedDictionaryItem = record
    Key:   umlcwideshortstring;
    Value: umlcwideshortstring;
    Typed: umlcwideshortstring;
  end;

  PUMLCWideShortKeyValueItem        = ^TUMLCWideShortKeyValueItem;
  PUMLCWideShortDictionaryItem      = ^TUMLCWideShortDictionaryItem;
  PUMLCWideShortTypedDictionaryItem = ^TUMLCWideShortTypedDictionaryItem;

  umlcwideshortkeyvalueitem         = ^TUMLCWideShortKeyValueItem;
  umlcwideshortdictionaryitem       = ^TUMLCWideShortDictionaryItem;
  umlcwideshorttypeddictionaryitem  = ^TUMLCWideShortTypedDictionaryItem;

implementation


end.

