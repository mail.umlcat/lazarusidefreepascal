(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcxnums;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  Classes, SysUtils,
  umlclibc,
  umlcmodules, umlctypes,
  umlcstdtypes,
  dummy;

// ---

const

 MOD_umlcxnums : TUMLCModule =
   ($5B,$8A,$27,$15,$F4,$81,$74,$4B,$92,$2F,$25,$53,$C0,$F4,$46,$8F);

// ---

const

  // ---

  ID_TUMLCComplexNumberPointer : TUMLCType =
    ($D7,$2E,$A3,$06,$7E,$0B,$DB,$48,$AB,$71,$12,$83,$05,$C7,$FE,$D7);

  ID_PUMLCComplexNumberPointer : TUMLCType =
    ($2D,$A0,$14,$7E,$C8,$F9,$29,$45,$9A,$FC,$19,$0A,$A3,$DA,$60,$05);

  // ---

  ID_TUMLCIntegerComplexNumber : TUMLCType =
    ($A6,$B6,$E1,$F0,$39,$C4,$69,$47,$A1,$61,$8D,$A2,$53,$A9,$6E,$B0);
  ID_PUMLCIntegerComplexNumber : TUMLCType =
    ($25,$2B,$3A,$7D,$93,$54,$D7,$43,$9B,$3C,$87,$D3,$F7,$C1,$F6,$30);

  // ---

  ID_TUMLCDoubleComplexNumber : TUMLCType =
    ($A7,$F9,$89,$28,$7D,$71,$53,$40,$A9,$CF,$B1,$C4,$74,$39,$3D,$32);
  ID_PUMLCDoubleComplexNumber : TUMLCType =
    ($58,$22,$44,$2E,$63,$9F,$8E,$4D,$91,$10,$98,$86,$5E,$DB,$CC,$C0);

  // ---

  ID_TUMLCExtendedComplexNumber : TUMLCType =
    ($1B,$54,$83,$16,$9B,$70,$B3,$4B,$A9,$DF,$F9,$2D,$A6,$17,$06,$95);
  ID_PUMLCExtendedComplexNumber : TUMLCType =
    ($2C,$6A,$49,$45,$9B,$54,$CC,$4E,$81,$20,$49,$BD,$03,$EF,$6E,$78);

  // ---

type

(* TComplexNumberPointer *)

  TComplexNumberPointer = record
    Real:      Pointer;
    Imaginary: Pointer;
  end;
  PComplexNumberPointer = ^TComplexNumberPointer;

(* TUMLCIntegerComplexNumber *)

  TUMLCIntegerComplexNumber = record
    Real:      Integer;
    Imaginary: Integer;
  end;
  PUMLCIntegerComplexNumber = ^TUMLCIntegerComplexNumber;

(* TUMLCDoubleComplexNumber *)

  TUMLCDoubleComplexNumber = record
    Real:      Double;
    Imaginary: Integer;
  end;
  PUMLCDoubleComplexNumber = ^TUMLCDoubleComplexNumber;

(* TUMLCExtendedComplexNumber *)

  TUMLCExtendedComplexNumber = record
    Real:      Extended;
    Imaginary: Integer;
  end;
  PUMLCExtendedComplexNumber = ^TUMLCExtendedComplexNumber;

(* TUMLCIntegerComplexNumber *)

  procedure IntCplxPtrOf
    (out   ADest:   TComplexNumberPointer;
     const ASource: TUMLCIntegerComplexNumber);

  procedure IntCplxImaginaryOf
    (const ASource: TUMLCIntegerComplexNumber;
     out   ADest:   Integer);

  procedure IntCplxRealOf
    (const ASource: TUMLCIntegerComplexNumber;
     out   ADest:   Integer);

  procedure IntCplxClear
    (out ADest: TUMLCIntegerComplexNumber);

  procedure IntCplxAssign
    (out   ADest:   TUMLCIntegerComplexNumber;
     const ASource: TUMLCIntegerComplexNumber);

  procedure IntCplxEncode
    (out   ADest: TUMLCIntegerComplexNumber;
     const AReal: Integer;
     const AImaginary: Integer);

  procedure IntCplxDecode
    (const ASource: TUMLCIntegerComplexNumber;
     out   AReal:   Integer;
     out   AImaginary: Integer);

  procedure IntCplxSum
    (out   ADest: TUMLCIntegerComplexNumber;
     const A, B:  TUMLCIntegerComplexNumber);

  function IntCplxAreEqual
    (const A, B: TUMLCIntegerComplexNumber): Boolean;

(* TUMLCDoubleComplexNumber *)

  procedure DblCplxPtrOf
    (out   ADest:   TComplexNumberPointer;
     const ASource: TUMLCDoubleComplexNumber);

  procedure DblCplxImaginaryOf
    (const ASource: TUMLCDoubleComplexNumber;
     out   ADest:   Integer);

  procedure DblCplxRealOf
    (const ASource: TUMLCDoubleComplexNumber;
     out   ADest:   Double);

  procedure DblCplxClear
    (out ADest: TUMLCDoubleComplexNumber);

  procedure DblCplxAssign
    (out   ADest:   TUMLCDoubleComplexNumber;
     const ASource: TUMLCDoubleComplexNumber);

  procedure DblCplxEncode
    (out   ADest: TUMLCDoubleComplexNumber;
     const AReal: Double;
     const AImaginary: Integer);

  procedure DblCplxDecode
    (const ASource: TUMLCDoubleComplexNumber;
     out   AReal:   Double;
     out   AImaginary: Integer);

  procedure DblCplxSum
    (out   ADest: TUMLCDoubleComplexNumber;
     const A, B:  TUMLCDoubleComplexNumber);

  function DblCplxAreEqual
    (const A, B: TUMLCDoubleComplexNumber): Boolean;

(* TUMLCExtendedComplexNumber *)

  procedure ExtCplxPtrOf
    (out   ADest:   TComplexNumberPointer;
     const ASource: TUMLCExtendedComplexNumber);

  procedure ExtCplxImaginaryOf
    (const ASource: TUMLCExtendedComplexNumber;
     out   ADest:   Integer);

  procedure ExtCplxRealOf
    (const ASource: TUMLCExtendedComplexNumber;
     out   ADest:   Extended);

  procedure ExtCplxClear
    (out ADest: TUMLCExtendedComplexNumber);

  procedure ExtCplxAssign
    (out   ADest:   TUMLCExtendedComplexNumber;
     const ASource: TUMLCExtendedComplexNumber);

  procedure ExtCplxEncode
    (out   ADest: TUMLCExtendedComplexNumber;
     const AReal: Extended;
     const AImaginary: Integer);

  procedure ExtCplxDecode
    (const ASource: TUMLCExtendedComplexNumber;
     out   AReal:   Extended;
     out   AImaginary: Integer);

  procedure ExtCplxSum
    (out   ADest: TUMLCExtendedComplexNumber;
     const A, B:  TUMLCExtendedComplexNumber);

  function ExtCplxAreEqual
    (const A, B: TUMLCExtendedComplexNumber): Boolean;

implementation

(* TUMLCIntegerComplexNumber *)

procedure IntCplxPtrOf
  (out   ADest:   TComplexNumberPointer;
   const ASource: TUMLCIntegerComplexNumber);
begin
  ADest.Real      := @ASource.Real;
  ADest.Imaginary := @ASource.Imaginary;
end;

procedure IntCplxImaginaryOf
  (const ASource: TUMLCIntegerComplexNumber;
   out   ADest:   Integer);
begin
  ADest := ASource.Imaginary;
end;

procedure IntCplxRealOf
  (const ASource: TUMLCIntegerComplexNumber;
   out   ADest:   Integer);
begin
  ADest := ASource.Real;
end;

procedure IntCplxClear
  (out ADest: TUMLCIntegerComplexNumber);
begin
  umlclibc.FillByte(ADest, Sizeof(TUMLCIntegerComplexNumber), 0);
end;

procedure IntCplxAssign
  (out   ADest:   TUMLCIntegerComplexNumber;
   const ASource: TUMLCIntegerComplexNumber);
begin
  ADest.Real      := ASource.Real;
  ADest.Imaginary := ASource.Imaginary;
end;

procedure IntCplxEncode
  (out   ADest: TUMLCIntegerComplexNumber;
   const AReal: Integer;
   const AImaginary: Integer);
begin
  ADest.Real := AReal;
  ADest.Imaginary := AImaginary;
end;

procedure IntCplxDecode
  (const ASource: TUMLCIntegerComplexNumber;
   out   AReal:   Integer;
   out   AImaginary: Integer);
begin
  AReal      := ASource.Real;
  AImaginary := ASource.Imaginary;
end;

procedure IntCplxSum
  (out   ADest: TUMLCIntegerComplexNumber;
   const A, B:  TUMLCIntegerComplexNumber);
begin
  ADest.Real :=
    (A.Real + B.Real);
  ADest.Imaginary :=
    (A.Imaginary + B.Imaginary);
end;

function IntCplxAreEqual
  (const A, B: TUMLCIntegerComplexNumber): Boolean;
begin
  Result :=
    ((A.Real = B.Real) and (A.Imaginary = B.Imaginary));
end;

(* TUMLCDoubleComplexNumber *)

procedure DblCplxPtrOf
  (out   ADest:   TComplexNumberPointer;
   const ASource: TUMLCDoubleComplexNumber);
begin
  ADest.Real      := @ASource.Real;
  ADest.Imaginary := @ASource.Imaginary;
end;

procedure DblCplxImaginaryOf
  (const ASource: TUMLCDoubleComplexNumber;
   out   ADest:   Integer);
begin
  ADest := ASource.Imaginary;
end;

procedure DblCplxRealOf
  (const ASource: TUMLCDoubleComplexNumber;
   out   ADest:   Double);
begin
  ADest := ASource.Real;
end;

procedure DblCplxClear
  (out ADest: TUMLCDoubleComplexNumber);
begin
  umlclibc.FillByte(ADest, Sizeof(TUMLCIntegerComplexNumber), 0);
end;

procedure DblCplxAssign
  (out   ADest:   TUMLCDoubleComplexNumber;
   const ASource: TUMLCDoubleComplexNumber);
begin
  ADest.Real      := ASource.Real;
  ADest.Imaginary := ASource.Imaginary;
end;

procedure DblCplxEncode
  (out   ADest: TUMLCDoubleComplexNumber;
   const AReal: Double;
   const AImaginary: Integer);
begin
  ADest.Real := AReal;
  ADest.Imaginary := AImaginary;
end;

procedure DblCplxDecode
  (const ASource: TUMLCDoubleComplexNumber;
   out   AReal:   Double;
   out   AImaginary: Integer);
begin
  AReal      := ASource.Real;
  AImaginary := ASource.Imaginary;
end;

procedure DblCplxSum
  (out   ADest: TUMLCDoubleComplexNumber;
   const A, B:  TUMLCDoubleComplexNumber);
begin
  ADest.Real :=
    (A.Real + B.Real);
  ADest.Imaginary :=
    (A.Imaginary + B.Imaginary);
end;

function DblCplxAreEqual
  (const A, B: TUMLCDoubleComplexNumber): Boolean;
begin
  Result :=
    ((A.Real = B.Real) and (A.Imaginary = B.Imaginary));
end;

(* TUMLCExtendedComplexNumber *)

procedure ExtCplxPtrOf
  (out   ADest:   TComplexNumberPointer;
   const ASource: TUMLCExtendedComplexNumber);
begin
  ADest.Real      := @ASource.Real;
  ADest.Imaginary := @ASource.Imaginary;
end;

procedure ExtCplxImaginaryOf
  (const ASource: TUMLCExtendedComplexNumber;
   out   ADest:   Integer);
begin
  ADest := ASource.Imaginary;
end;

procedure ExtCplxRealOf
  (const ASource: TUMLCExtendedComplexNumber;
   out   ADest:   Extended);
begin
  ADest := ASource.Imaginary;
end;

procedure ExtCplxClear
  (out ADest: TUMLCExtendedComplexNumber);
begin
  umlclibc.FillByte(ADest, Sizeof(TUMLCIntegerComplexNumber), 0);
end;

procedure ExtCplxAssign
  (out   ADest:   TUMLCExtendedComplexNumber;
   const ASource: TUMLCExtendedComplexNumber);
begin
  ADest.Real      := ASource.Real;
  ADest.Imaginary := ASource.Imaginary;
end;

procedure ExtCplxEncode
  (out   ADest: TUMLCExtendedComplexNumber;
   const AReal: Extended;
   const AImaginary: Integer);
begin
  ADest.Real := AReal;
  ADest.Imaginary := AImaginary;
end;

procedure ExtCplxDecode
  (const ASource: TUMLCExtendedComplexNumber;
   out   AReal:   Extended;
   out   AImaginary: Integer);
begin
  AReal      := ASource.Real;
  AImaginary := ASource.Imaginary;
end;

procedure ExtCplxSum
  (out   ADest: TUMLCExtendedComplexNumber;
   const A, B:  TUMLCExtendedComplexNumber);
begin
  ADest.Real :=
    (A.Real + B.Real);
  ADest.Imaginary :=
    (A.Imaginary + B.Imaginary);
end;

function ExtCplxAreEqual
  (const A, B: TUMLCExtendedComplexNumber): Boolean;
begin
  Result :=
    ((A.Real = B.Real) and (A.Imaginary = B.Imaginary));
end;


//procedure UnitConstructor();
//begin
  //umlcmodules.RegisterModule
  //  (MOD_None, MOD_umlcglobal);
//end;

//procedure UnitDestructor();
//begin
  //
//end;

//initialization
  //UnitConstructor;
//finalization
  //UnitDestructor;
end.

