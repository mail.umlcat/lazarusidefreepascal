(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuuids;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysUtils,
  Classes,
  umlclibc,
  dummy;

// ---

//const
//
// MOD_umlcuuids : TUMLCModule =
//   ($7C,$B4,$A1,$60,$A7,$CE,$5C,$43,$9E,$1A,$3B,$0E,$E5,$27,$41,$FC);

// ---

(**
 ** Description:
 ** This unit supports function for Global Unique Identifiers.
 **
 ** This unit, DOES NOT have strings conversion functions.
 **)

 (* Global Types *)

type
  TUUID = array[0 .. 15] of byte;
  PUUID = ^TUUID;

(* Global Functions *)

  function IsEmptyUUID
    (const A: array of byte): Boolean;
  function AreEqualUUID
    (const A, B: TUUID): Boolean;

  procedure EmptyUUID
    (out AUUID: TUUID);
  procedure CreateUUID
    (out AUUID: TUUID);

  procedure ClearUUID
    (var AUUID: TUUID);

implementation

function IsEmptyUUID
  (const A: array of byte): Boolean;
var Match: Boolean; I, C: Integer;
begin
  Result := false;

  I := 0;
  C := sizeof(TUUID);
  Match := true;
  while (Match or (I < C)) do
  begin
    Match :=
      (A[I] = 0);

    Inc(I);
  end;
end;

function AreEqualUUID
  (const A, B: TUUID): Boolean;
begin
  Result :=
    (umlclibc.CompareByte(A, B, SizeOf(TUUID)) = 0);
end;

procedure EmptyUUID
  (out AUUID: TUUID);
begin
  umlclibc.FillByte((* var *) AUUID, SizeOf(TUUID), 0);
end;

procedure CreateUUID
  (out AUUID: TUUID);
var AGUID: TGUID;
begin
  if (SysUtils.CreateGUID((* out *) AGUID) <> 0) then
  begin
    EmptyUUID((* out *) AUUID);
  end else
  begin
    System.Move(AGUID, AUUID, SizeOf(TUUID));
  end;
end;

procedure ClearUUID
  (var AUUID: TUUID);
begin
  umlclibc.FillByte((* var *) AUUID, sizeof(TUUID), 0);
end;

//procedure UnitConstructor();
//begin
  //umlcmodules.RegisterModule
  //  (MOD_None, MOD_umlcglobal);
//end;

//procedure UnitDestructor();
//begin
  //
//end;

//initialization
  //UnitConstructor;
//finalization
  //UnitDestructor;
end.

