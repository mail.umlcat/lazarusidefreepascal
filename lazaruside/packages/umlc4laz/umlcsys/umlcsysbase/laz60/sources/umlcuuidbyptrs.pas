(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuuidbyptrs;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysConst, SysUtils, Math,
  umlclibc,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuuids,
  dummy;

// ---

const

  MOD_umlcuuidbyptrs : TUMLCModule =
    ($B5,$F2,$03,$02,$36,$19,$70,$47,$9F,$E3,$59,$E9,$44,$01,$6D,$62);

// ---


(* global functions *)

function ConstToPtr
  (const AValue: TUUID): pointer;

procedure DropPtr
  (var ADestPtr: pointer);


implementation

(* global functions *)

function ConstToPtr
  (const AValue: TUUID): pointer;
var P: puuid;
begin
  System.GetMem(P, sizeof(TUUID));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(TUUID));
end;

//procedure UnitConstructor();
//begin
  //umlcmodules.RegisterModule
  //  (MOD_None, MOD_umlcglobal);
//end;

//procedure UnitDestructor();
//begin
  //
//end;

//initialization
  //UnitConstructor;
//finalization
  //UnitDestructor;
end.

