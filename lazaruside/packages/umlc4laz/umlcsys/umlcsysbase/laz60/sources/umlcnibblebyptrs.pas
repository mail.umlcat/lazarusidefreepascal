(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnibblebyptrs;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  dummy;

// ---

const

 MOD_umlcnibblebyptrs : TUMLCModule =
   ($08,$C4,$05,$A9,$A9,$57,$13,$47,$99,$06,$5C,$C7,$9A,$55,$06,$63);

// ---


implementation




end.

