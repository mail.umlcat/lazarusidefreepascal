(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnibblebuffers;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

interface

uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  dummy;

// ---

const

 MOD_umlcnibblebuffers : TUMLCModule =
   ($10,$FA,$46,$77,$20,$84,$62,$4F,$BB,$C9,$1E,$52,$37,$04,$29,$18);

// ---




implementation




end.

