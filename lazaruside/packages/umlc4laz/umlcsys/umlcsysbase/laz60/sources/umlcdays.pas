(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdays;

{$IFDEF FPC}
  {$mode objfpc}{$H+}
{$ENDIF}

{ Espanol }

  // Objetivo: Provee constantes para manejo de dias.

{ English }

  // Goal: Provides constants for days management.

interface
uses
  Classes, SysUtils,
  dummy;

type

  TDay =
  ( dyNone,
    dySun, dyMon, dyTue, dyWed, dyThu, dyFri, dySat);

  function ValidateDay(Value: Integer): Integer;
  function SafeDay(const Value: TDay): TDay;

implementation

function ValidateDay(Value: Integer): Integer;
begin
  if (Value < 1)
    then Result := 1
  else if (Value > 31)
    then Result := 31
  else Result := Value
  // Goal: Checks that a day number is between 1 and 31.
  // Objetivo: Revisa que el numero de dia este entre 1 y 31.
end;

function SafeDay(const Value: TDay): TDay;
begin
  Result := Value;
  if (Result < Low(TDay))
    then Result := Low(TDay);
  if (Result > High(TDay))
    then Result := High(TDay);
  // Goal: Checks that a day number is between 0 and 7.
  // Objetivo: Revisa que el numero de dia este entre 0 y 7.
end;



end.
