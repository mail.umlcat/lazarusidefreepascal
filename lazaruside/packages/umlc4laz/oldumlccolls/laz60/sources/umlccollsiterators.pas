unit umlccollsiterators;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcnormobjects,
  umlcstdobjs,
  umlccollsenums,
  dummy;

type

  (* IUMLCIterator *)

  IUMLCIterator = interface(IUMLCHalfCloneableObject)
    ['{337210D8-A81A-5449-8A95-FA3EAA69AD6C}']

    { interface declarations }

    procedure Start();
    procedure Stop();

    function IsDone: Boolean;

    procedure MoveNext();
  end;

  (* IUMLCListIterator *)

  IUMLCListIterator = interface(IUMLCIterator)
    ['{BE96C824-756F-0542-81B9-656B141322D7}']

    { interface declarations }

    function ListDirection(): ListDirectionsEnum;
  end;

  (* IUMLCTreeIterator *)

  IUMLCTreeIterator = interface(IUMLCIterator)
    ['{81737477-DEAA-7640-BCFC-2CB9F1832E88}']

    { interface declarations }

    function TreeDirection(): TreeDirectionsEnum;
  end;

  (* TUMLCIterator *)

  TUMLCIterator = class(TUMLCHalfCloneableObject, IUMLCIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FCount: Integer;
  protected
    (* protected declarations *)

    procedure InternalStart(); virtual;
    procedure InternalStop(); virtual;

    function InternalIsDone: Boolean; virtual;

    procedure InternalMoveNext(); virtual;
  public
    (* public declarations *)

    procedure Start();
    procedure Stop();

    function IsDone: Boolean;

    procedure MoveNext();
  end;

  (* TUMLCListIterator *)

  TUMLCListIterator = class(TUMLCIterator, IUMLCListIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function InternalListDirection(): ListDirectionsEnum; virtual;
  public
    (* public declarations *)

    function ListDirection(): ListDirectionsEnum;
  end;

  (* TUMLCTreeIterator *)

  TUMLCTreeIterator = class(TUMLCIterator, IUMLCTreeIterator)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function InternalTreeDirection(): TreeDirectionsEnum; virtual;
  public
    (* public declarations *)

    function TreeDirection(): TreeDirectionsEnum;
  end;

implementation

(* TUMLCIterator *)

procedure TUMLCIterator.InternalStart();
begin
  Self.DoNothing();
  Self.FCount := 0;
end;

procedure TUMLCIterator.InternalStop();
begin
  Self.FCount := 0;
  Self.DoNothing();
end;

function TUMLCIterator.InternalIsDone: Boolean;
begin
  Result := true;
end;

procedure TUMLCIterator.InternalMoveNext();
begin
  Self.FCount :=
    (Self.FCount + 1);
end;

procedure TUMLCIterator.Start();
begin
  Self.InternalStart();
end;

procedure TUMLCIterator.Stop();
begin
  Self.InternalStop();
end;

function TUMLCIterator.IsDone: Boolean;
begin
  Result := Self.InternalIsDone();
end;

procedure TUMLCIterator.MoveNext();
begin
  Self.InternalMoveNext();
end;

(* TUMLCListIterator *)

function TUMLCListIterator.InternalListDirection(): ListDirectionsEnum;
begin
  Result := listdirections_none;
end;

function TUMLCListIterator.ListDirection(): ListDirectionsEnum;
begin
  Result := Self.InternalListDirection();
end;

(* TUMLCTreeIterator *)

function TUMLCTreeIterator.InternalTreeDirection(): TreeDirectionsEnum;
begin
  Result := treedirections_none;
end;

function TUMLCTreeIterator.TreeDirection(): TreeDirectionsEnum;
begin
  Result := Self.InternalTreeDirection();
end;

end.

