(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnormvectorlists;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  umlcnormlists,
  umlcnormarrays,
  umlcnormvectors,
  dummy;

const

  // ---

  ID_IUMLCNormObjVectorList : TUMLCType =
    ($8B,$80,$3D,$3D,$96,$23,$C0,$48,$9F,$60,$F7,$32,$25,$25,$D4,$1F);

  ID_TUMLCNormObjVectorList : TUMLCType =
    ($8C,$59,$62,$BF,$57,$CC,$F8,$49,$BD,$41,$57,$E2,$AB,$AE,$0F,$B5);


  // ---

type

  IUMLCNormObjVectorList =
    interface(
      IUMLCHalfNormalizedObject
      // , IUMLCNormObjArray
      // , IUMLCNormObjVector
      // , IUMLCNormObjList
    )
    ['{8B803D3D-9623-C048-9F60-F7322525D41F}']

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;

    // ---

    function IsValidIndex
      (const AIndex: LongInt): Boolean;

    function readSize(): LongInt;

    function getAt
      (const AIndex: LongInt): TUMLCHalfNormalizedObject;

    procedure setAt
      (const AIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure DeleteAt
      (const AIndex: LongInt);

    procedure ExtractAt
      (const AIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure Clear();

    // ---

    function IsEmpty(): Boolean;

    procedure Add
      (var AItem: TUMLCHalfNormalizedObject);

    procedure Remove();

    function Extract
      (): TUMLCHalfNormalizedObject;
  end;

type

//(* TUMLCNormObjVectorListIterator *)
//
//  TUMLCNormObjVectorListIterator = (* forward *) class;

(* TUMLCNormObjVectorList *)

  TUMLCNormObjVectorList =
    class(
      TUMLCInternalCloneableObject,
      IUMLCNormObjVectorList)
  private
    (* private declarations *)

  protected
    (* protected declarations *)

    FAssignedSize: Boolean;

    FSize: LongInt;
  public
    (* public declarations *)

    constructor Create(); override;

    constructor CreateByCount
      (const ACount: LongInt); virtual;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;
  public
    (* public declarations *)

    procedure AssignSize
      (const ASize: LongInt);

    function IsValidIndex
      (const AIndex: LongInt): Boolean;
  public
    (* public declarations *)

    function readSize(): LongInt;

    function getAt
      (const AIndex: LongInt): TUMLCHalfNormalizedObject;

    procedure setAt
      (const AIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);
  public
    (* public declarations *)

    procedure DeleteAt
      (const AIndex: LongInt);

    procedure ExtractAt
      (const AIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure Clear();
  public
    (* public declarations *)

    function IsEmpty(): Boolean;

    procedure Add
      (var AItem: TUMLCHalfNormalizedObject);

    procedure Remove();

    function Extract
      (): TUMLCHalfNormalizedObject;
  end;


implementation

(* TUMLCNormObjVectorList *)

constructor TUMLCNormObjVectorList.Create();
begin
  inherited Create();

  Self.FAssignedSize := false;
  Self.FSize := 0;
end;

constructor TUMLCNormObjVectorList.CreateByCount
  (const ACount: LongInt);
begin
  inherited Create();

  Self.FAssignedSize :=
    (ACount > 0);
  if(Self.FAssignedSize) then
  begin
    Self.FSize := ACount;
  end;
end;

destructor TUMLCNormObjVectorList.Destroy();
begin
  inherited Destroy();
end;

function TUMLCNormObjVectorList.readDimCount(): LongInt;
begin
  Result := 1;
end;

function TUMLCNormObjVectorList.readFullCount(): LongInt;
begin
  Result := 0;
end;

procedure TUMLCNormObjVectorList.AssignSize
  (const ASize: LongInt);
begin
  if (not Self.FAssignedSize) then
  begin
    Self.FSize := ASize;
    Self.FAssignedSize := true;
  end;
end;

function TUMLCNormObjVectorList.IsValidIndex
  (const AIndex: LongInt): Boolean;
begin
  Result:= false;
end;

function TUMLCNormObjVectorList.readSize(): LongInt;
begin
  Result:= 0;
end;

function TUMLCNormObjVectorList.getAt
  (const AIndex: LongInt): TUMLCHalfNormalizedObject;
begin
  Result:= nil;
end;

procedure TUMLCNormObjVectorList.setAt
  (const AIndex: LongInt;
   var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjVectorList.DeleteAt
  (const AIndex: LongInt);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjVectorList.ExtractAt
  (const AIndex: LongInt;
   var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjVectorList.Clear();
begin
  //Self.DoNothing();
end;

function TUMLCNormObjVectorList.IsEmpty(): Boolean;
begin
  Result := false;
end;

procedure TUMLCNormObjVectorList.Add
  (var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjVectorList.Remove();
begin
  //Self.DoNothing();
end;


function TUMLCNormObjVectorList.Extract(): TUMLCHalfNormalizedObject;
begin
  Result := nil;
end;

end.

