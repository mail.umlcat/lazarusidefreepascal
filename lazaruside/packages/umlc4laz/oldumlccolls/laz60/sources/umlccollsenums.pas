unit umlccollsenums;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  dummy;

type
  ListDirectionsEnum =
  (
    listdirections_none,
    listdirections_forward,
    listdirections_backward
  );

  TreeDirectionsEnum =
  (
    treedirections_none,
    treedirections_prefix,
    treedirections_posfix,
    treedirections_infix
  );

  SortModesEnum =
  (
    SortModes_none,
    SortModes_asc,
    SortModes_desc
  );

implementation

end.

