(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnormmatrices;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlccomparisons,
  umlcnormobjects,
  umlcstdobjs,
  umlclists,
  umlccollsenums,
  umlccollsiterators,
  umlcnormarrays,
  dummy;

const

  // ---

  ID_IUMLCNormObjMatrix : TUMLCType =
    ($A5,$5A,$7A,$AF,$89,$1C,$BF,$4E,$81,$B0,$AB,$DD,$8F,$29,$A2,$0F);

  ID_TUMLCNormObjMatrix : TUMLCType =
    ($80,$F6,$0A,$ED,$2E,$F9,$92,$47,$84,$27,$6C,$77,$15,$96,$A0,$27);

  // ---

type

  IUMLCNormObjMatrix =
    interface(
      IUMLCHalfNormalizedObject
      // , IUMLCNormObjArray
    )
    ['{A55A7AAF-891C-BF4E-81B0-ABDD8F29A20F}']

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;

    // ---

    function IsValidIndex
      (const ACol, ARow: LongInt): Boolean;

    function readColSize(): LongInt;

    function readRowSize(): LongInt;

    function getAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt): TUMLCHalfNormalizedObject;

    procedure setAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure DeleteAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt);

    procedure ExtractAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure Clear();
  end;

type

//(* TUMLCNormObjMatrixIterator *)
//
//  TUMLCNormObjMatrixIterator = (* forward *) class;

(* TUMLCNormObjMatrix *)

  TUMLCNormObjMatrix =
    class(TUMLCInternalCloneableObject, IUMLCNormObjMatrix)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FAssignedColSize: Boolean;
    FAssignedRowSize: Boolean;

    FColSize: LongInt;
    FRowSize: LongInt;
  public
    (* public declarations *)

    constructor Create(); override;

    constructor CreateBySize
      (const AColSize: LongInt;
       const ARowSize: LongInt); virtual;

    destructor Destroy(); override;
  public
    (* public declarations *)

    function readDimCount(): LongInt;

    function readFullCount(): LongInt;
  public
    (* public declarations *)

    procedure AssignColSize
      (const AColSize: LongInt);

    procedure AssignRowSize
      (const ARowSize: LongInt);

    function IsValidIndex
      (const ACol, ARow: LongInt): Boolean;
  public
    (* public declarations *)

    function readColSize(): LongInt;

    function readRowSize(): LongInt;
  public
    (* public declarations *)

    function getAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt): TUMLCHalfNormalizedObject;

    procedure setAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);
  public
    (* public declarations *)

    procedure DeleteAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt);

    procedure ExtractAt
      (const AColIndex: LongInt;
       const ARowIndex: LongInt;
       var AItem: TUMLCHalfNormalizedObject);

    procedure Clear();
  end;

implementation

(* TUMLCNormObjMatrix *)

constructor TUMLCNormObjMatrix.Create();
begin
  inherited Create();

  Self.FAssignedColSize := false;
  Self.FColSize := 0;
  Self.FAssignedRowSize := false;
  Self.FRowSize := 0;
end;

constructor TUMLCNormObjMatrix.CreateBySize
  (const AColSize: LongInt;
   const ARowSize: LongInt);
begin
  inherited Create();

  Self.FAssignedColSize :=
    (AColSize > 0);
  if(Self.FAssignedColSize) then
  begin
    Self.FColSize := AColSize;
  end;

  Self.FAssignedRowSize :=
    (ARowSize > 0);
  if(Self.FAssignedRowSize) then
  begin
    Self.FRowSize := ARowSize;
  end;
end;

destructor TUMLCNormObjMatrix.Destroy();
begin
  //Self.FRowSize := ARowSize;
  inherited Destroy();
end;

function TUMLCNormObjMatrix.readDimCount(): LongInt;
begin
  Result := 2;
end;

function TUMLCNormObjMatrix.readFullCount(): LongInt;
begin
  Result := 0;
end;

function TUMLCNormObjMatrix.IsValidIndex
  (const ACol, ARow: LongInt): Boolean;
begin
  Result := false;
end;

procedure TUMLCNormObjMatrix.AssignColSize
  (const AColSize: LongInt);
begin
  if (not Self.FAssignedColSize) then
  begin
    Self.FColSize := AColSize;
    Self.FAssignedColSize := true;
  end;
end;

procedure TUMLCNormObjMatrix.AssignRowSize
  (const ARowSize: LongInt);
begin
  if (not Self.FAssignedRowSize) then
  begin
    Self.FRowSize := ARowSize;
    Self.FAssignedRowSize := true;
  end;
end;

function TUMLCNormObjMatrix.readColSize(): LongInt;
begin
  Result:=
    Self.FColSize;
end;

function TUMLCNormObjMatrix.readRowSize(): LongInt;
begin
  Result:=
    Self.FRowSize;
end;

function TUMLCNormObjMatrix.getAt
  (const AColIndex: LongInt;
   const ARowIndex: LongInt): TUMLCHalfNormalizedObject;
begin
  Result:=
    nil;
end;

procedure TUMLCNormObjMatrix.setAt
  (const AColIndex: LongInt;
   const ARowIndex: LongInt;
   var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjMatrix.DeleteAt
  (const AColIndex: LongInt;
   const ARowIndex: LongInt);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjMatrix.ExtractAt
  (const AColIndex: LongInt;
   const ARowIndex: LongInt;
   var AItem: TUMLCHalfNormalizedObject);
begin
  //Self.DoNothing();
end;

procedure TUMLCNormObjMatrix.Clear();
begin
  //Self.DoNothing();
end;

end.

