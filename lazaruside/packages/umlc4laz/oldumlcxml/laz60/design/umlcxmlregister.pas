unit umlcxmlregister;

interface
uses
  Classes,
  //umlcfilefilters,
  //umlcbookmngrs,
  //umlcundomngrs,
  //umlcdocs,
  //umlcmsgctrls,
  {$IFDEF FPC}
  LResources,
  {$ENDIF}
  dummy;

  procedure Register;

implementation

{$IFDEF DELPHI}
{.R 'umlcfilefilters.lrs'}
{.R 'umlcbookmngrs.lrs'}

{.R 'umlcmsgctrls.lrs'}

{.R 'umlcundomngrs.lrs'}
{.R 'umlcdocs.lrs'}
{$ENDIF}

procedure Register;
//const
//  XMLPalette = 'UMLCat Tools';
begin
  //RegisterComponents(XMLPalette, [TUMLCFileFiltersContainer]);
  //RegisterComponents(XMLPalette, [TUMLCBookMarkMngr]);
  //
  //RegisterComponents(XMLPalette, [TUMLCMsgServer]);
  //RegisterComponents(XMLPalette, [TUMLCSingleClientMsgServer]);
  //RegisterComponents(XMLPalette, [TUMLCMsgClient]);
  //RegisterComponents(XMLPalette, [TUMLCSingleServerMsgClient]);
  //
  //RegisterComponents(XMLPalette, [TUMLCUnDoManager]);
  //
  //RegisterComponents(XMLPalette, [TUMLCDocument]);
  //RegisterComponents(XMLPalette, [TUMLCDelegateDocument]);
end;

initialization
{$IFDEF FPC}
{.I 'umlcfilefilters.lrs'}
{.I 'umlcbookmngrs.lrs'}

{.I 'umlcmsgctrls.lrs'}

{.I 'umlcundomngrs.lrs'}
{.I 'umlcdocs.lrs'}
{$ENDIF}
end.

