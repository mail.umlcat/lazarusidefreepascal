(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)
 
unit umlctagattrdocs;

interface

uses
  Windows, SysUtils, Classes,
  umlctreenodes, umlctreecntrs,
  umlctagprops, umlctagstyles,
  dummy;

type

(* TUMLCTagAttributesItem *)

  TUMLCTagAttributesItem = class(TUMLCContainerTreeNode)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FKeyword: string;
    FStyle:   TUMLCTagStyle;

    FValue:   string;
    // valor unico almacenado en etiqueta como bloques de texto
    // single value stored in tag such text blocks

    FUsesBreak: Boolean;
    // generar salto de linea despues de marcadores inicio o final de bloque ?
    // generate a line break after block*s start or finish markers ?

    FProperties: TUMLCTagProperties;
    // varios valores almacenados en propiedades
    // '<body forecolor = "%CCAA00" backcolor = "%AF07B3"> '

    // several values stored in properties such
    // '<body forecolor = "%CCAA00" backcolor = "%AF07B3"> '
  public
    (* public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  public
    (* public declarations *)

    function ToStartText: string; dynamic;
    function ToFinishText: string; dynamic;
    function ToText: string; dynamic;

    function PropByKeyword(const Keyword: string): TUMLCTagProperty;
    function RegisterProperty(const Keyword: string): TUMLCTagProperty;
    function RegisterPropertyValue
      (const Keyword, Value: string): TUMLCTagProperty;

    procedure CopyPropTo(const Dest: TUMLCTagAttributesItem);
  published
    (* published declarations *)

    property UsesBreak: Boolean
      read FUsesBreak write FUsesBreak;
    property Keyword: string
      read FKeyword write FKeyword;
    property Value: string
      read FValue write FValue;
    property Style: TUMLCTagStyle
      read FStyle write FStyle;
    property Properties: TUMLCTagProperties
      read FProperties write FProperties;
  end;

(* TUMLCTagAttributesCollection *)

  TUMLCTagAttributesCollection = class(TUMLCContainerTreeCollection)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  published
    (* published declarations *)
  end;

(* TCustomUMLCTagAttributesTreeContainer *)

  TCustomUMLCTagAttributesTreeContainer = class(TCustomUMLCTreeContainer)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    function CreateCollectionByClass: TUMLCContainerTreeCollection; override;
  public
    (* public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

(* TUMLCTagAttributesTreeContainer *)

  TUMLCTagAttributesTreeContainer = class(TCustomUMLCTagAttributesTreeContainer)
  published
    (* published declarations *)

    { TCustomUMLCTreeContainer: }

    (*
    property OnInsert;
    property OnRemove;
    property OnUpdateName;
    property OnUpdateText;
    *)
  end;

implementation

(* TUMLCTagAttributesItem *)

procedure TUMLCTagAttributesItem.DoCreate();
begin
  inherited DoCreate();

  FKeyword := '';
  FUsesBreak := FALSE;
  FStyle := tsNone;

  FValue   := '';
  FProperties := TUMLCTagProperties.Create(TUMLCTagProperty);
end;

procedure TUMLCTagAttributesItem.DoDestroy();
begin
  FProperties.Free();
  FValue   := '';

  FStyle := tsNone;
  FKeyword := '';
  inherited DoDestroy();
end;

function TUMLCTagAttributesItem.ToStartText: string;
begin
  Result := FKeyword;
  // to-do: include properties
end;

function TUMLCTagAttributesItem.ToFinishText: string;
begin
  Result := FKeyword;
  // to-do: include properties
end;

function TUMLCTagAttributesItem.ToText: string;
begin
  Result := FKeyword;
  // to-do: include properties
end;

function TUMLCTagAttributesItem.PropByKeyword(const Keyword: string): TUMLCTagProperty;
var Found: Boolean; AIndex: Integer;
begin
  AIndex := 0; Found := FALSE; Result := nil;
  while ((AIndex < Count) and (not Found)) do
  begin
    Result := (FProperties.Items[AIndex] as TUMLCTagProperty);
    Found  := ANSISameText(Result.Keyword, Keyword);
    System.Inc(AIndex);
  end;
  if (not Found)
    then Result := nil;
end;

function TUMLCTagAttributesItem.RegisterProperty(const Keyword: string): TUMLCTagProperty;
begin
  Result := PropByKeyword(Keyword);
  if (Result = nil) then
  begin
    Result := (FProperties.Add as TUMLCTagProperty);
    Result.Keyword := Keyword;
  end;
end;

function TUMLCTagAttributesItem.RegisterPropertyValue
  (const Keyword, Value: string): TUMLCTagProperty;
begin
  Result := PropByKeyword(Keyword);
  if (Result = nil) then
  begin
    Result := (FProperties.Add as TUMLCTagProperty);
    Result.Keyword := Keyword;
    Result.Value   := Value;
  end;
end;

procedure TUMLCTagAttributesItem.CopyPropTo(const Dest: TUMLCTagAttributesItem);
var I: Integer; S, D: TUMLCTagProperty;
begin
  Dest.Properties.Clear();

  for I := 0 to (Self.Properties.Count - 1) do
  begin
    S := Self.Properties.PropByIndex(i);
    D := Dest.RegisterProperty(S.Keyword);
    D.Value := S.Value;
  end;
end;

(* TUMLCTagAttributesCollection *)

function TUMLCTagAttributesCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCTagAttributesItem.Create();
  Result.DoCreate();
end;

procedure TUMLCTagAttributesCollection.DoCreate();
begin
  inherited DoCreate();
  {Your Code...}
end;

procedure TUMLCTagAttributesCollection.DoDestroy();
begin
  {Your Code...}
  inherited DoDestroy();
end;

(* TCustomUMLCTagAttributesTreeContainer *)

function TCustomUMLCTagAttributesTreeContainer.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TUMLCTagAttributesCollection.Create();
end;

constructor TCustomUMLCTagAttributesTreeContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  {Your Code...}
end;

destructor TCustomUMLCTagAttributesTreeContainer.Destroy();
begin
  {Your Code...}
  inherited Destroy();
end;

end.
 
