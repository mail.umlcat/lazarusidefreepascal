(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

unit umlcxmltagansisymbols;

interface
uses
  SysUtils,
  umlcxmltagTokens;

type
  TUMLCXMLTagANSISymbol = record
    Token:      TUMLCXMLTagToken;
    Text:       ANSIstring;
    StartRow,
    StartCol,
    FinishRow,
    FinishCol:  Integer;
  end;
  PUMLCXMLTagANSISymbol = ^TUMLCXMLTagANSISymbol;

  procedure Clear(var Symbol: TUMLCXMLTagANSISymbol);

implementation

procedure Clear(var Symbol: TUMLCXMLTagANSISymbol);
begin
  System.FillChar(Symbol, SizeOf(Symbol), 0);
end;

end.
