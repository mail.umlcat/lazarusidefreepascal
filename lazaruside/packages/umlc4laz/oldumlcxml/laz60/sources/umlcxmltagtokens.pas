(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

unit umlcxmltagtokens;

interface
uses
  umlcAnsiCharSets, 
  umlcStrings, 
  umlcStrParsers, 
  umlcTextConsts, 
  umlcAnsiCharSetConsts;

type
  TUMLCXMLTagToken = 
  (
  xmltagtkNone,      // Sin definir
  xmltagtkAttribute, // attribute*s qualified identifier
  xmltagtkAssign,    // '='
  xmltagtkValue,     // attribute*s value
  xmltagtkSpace,     // space character
  xmltagtkEoLn,      // End Of Line marker
  xmltagtkEoPg,      // End Of Page marker
  xmltagtkEoF        // End Of File marker
  );
  PUMLCXMLTagToken = ^TUMLCXMLTagToken;

  function TokenToStr({copy} Value: TUMLCXMLTagToken): string;
  function StrToToken(const Value: string): TUMLCXMLTagToken;


implementation

const EnumToStrArray: array[TUMLCXMLTagToken] of string =
(
  'xmltagtkNone',
  'xmltagtkAttribute',
  'xmltagtkAssign',
  'xmltagtkValue',
  'xmltagtkSpace',
  'xmltagtkEoLn',
  'xmltagtkEoPg',
  'xmltagtkEoF'
);

function TokenToStr({copy} Value: TUMLCXMLTagToken): string;
begin
  if (Value < Low(TUMLCXMLTagToken))
    then Value := Low(TUMLCXMLTagToken);
  if (Value > High(TUMLCXMLTagToken))
    then Value := High(TUMLCXMLTagToken);
  Result := EnumToStrArray[Value];
end;

function StrToToken(const Value: string): TUMLCXMLTagToken;
var i: TUMLCXMLTagToken; Found: Boolean;
begin
  i := Low(TUMLCXMLTagToken); Found := FALSE;
  while ((i <= High(TUMLCXMLTagToken)) and (not Found)) do
  begin
    Found := SameText(EnumToStrArray[i], Value);
    Inc(i);
  end;

  if (Found)
    then Result := Pred(i)
    else Result := Low(TUMLCXMLTagToken);
end;

end.
