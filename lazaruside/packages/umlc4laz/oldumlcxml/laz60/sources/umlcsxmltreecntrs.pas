(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

unit umlcsxmltreecntrs;

{$mode objfpc}{$H+}

interface
uses
  Classes, SysUtils,
  umlcguids,
  umlcguidstrs,
  umlcmsgtypes,
  umlctreenodes,
  umlctreecntrs,
  umlcmsgtreecntrs,
  umlcxmlfileansisymbols,
  umlcXMLFileTreeNodeTokens,
  dummy;

  // --> Simple eXtensible Markup Language
  // --> X.M.L. tags without attributes
  // --> "Think Different" --> "Think Simple"

(**
 ** Description:
 ** This unit declares collections of treenodes,
 ** that store Simple X.M.L. parsed tokens.
 **)

const
  msgTreeNodeAfterChangeSymbol
    = '{5D7621DC-FDF38B46-8DBD1AB3-4B385EC9}';
  msgTreeNodeAfterChangeTextValue
    = '{E11EA271-C8182F46-88943547-542C58E7}';
  msgTreeNodeAfterChangeTreeToken
    = '{D3983359-C867C24B-9D9A0C31-3699D24D}';

type

(* TUMLCSXMLTreeNode *)

  TUMLCSXMLTreeNode = class(TUMLCMsgContainerTreeNode)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    (* field declarations *)

    FSymbol: TUMLCXMLFileANSISymbol;

    FCanInsert: Boolean;
    FCanEdit:   Boolean;
    FCanRemove: Boolean;

    FNewLineAfter: Boolean;
  protected
    (* Protected declarations *)

    (* accesors declarations *)

    function getSymbol(): TUMLCXMLFileANSISymbol;
    function getTextValue(): string;
    function getTreeToken(): TUMLCXMLFileTreeNodeToken;
    function getCanEdit(): Boolean; virtual;
    function getCanInsert(): Boolean; virtual;
    function getCanRemove(): Boolean; virtual;
    function getNewLineAfter(): Boolean; virtual;

    procedure setSymbol(const AValue: TUMLCXMLFileANSISymbol);
    procedure setTextValue(const AValue: string);
    procedure setTreeToken(const AValue: TUMLCXMLFileTreeNodeToken);
    procedure setCanRemove(const AValue: Boolean); virtual;
    procedure setCanEdit(const AValue: Boolean); virtual;
    procedure setCanInsert(const AValue: Boolean); virtual;
    procedure setNewLineAfter(const AValue: Boolean); virtual;
  protected
    (* Protected declarations *)

    procedure ConfirmedCopyTo(var ADestNode: TUMLCTreeNode); override;
    procedure ConfirmedMoveTo(var ADestNode: TUMLCTreeNode); override;
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  public
    (* Public declarations *)

    property Symbol: TUMLCXMLFileANSISymbol
      read getSymbol write setSymbol;

    // can subitems be added to this node
    property CanInsert: Boolean
      read getCanInsert write setCanInsert;
    // can remove this node
    property CanRemove: Boolean
      read getCanRemove write setCanRemove;
    // can chnage the value of this node
    property CanEdit: Boolean
      read getCanEdit write setCanEdit;

    property NewLineAfter: Boolean
      read getNewLineAfter write setNewLineAfter;

    // "TextValue" will store the full parsed value,
    // "Text" will only store a short readable version of the token
    property TextValue: string
      read getTextValue write setTextValue;

    property TreeToken: TUMLCXMLFileTreeNodeToken
      read getTreeToken write setTreeToken;
  end;

(* TCustomUMLCSXMLTreeCollection *)

  TCustomUMLCSXMLTreeCollection = class(TUMLCMsgContainerTreeCollection)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* Public declarations *)
  end;

(* TCustomUMLCSXMLTreeContainer *)

  TCustomUMLCSXMLTreeContainer = class(TCustomUMLCMsgTreeContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Friend Protected declarations *)

    function CreateCollectionByClass(): TUMLCContainerTreeCollection; override;
  public
    (* Public declarations *)
  end;

implementation

(* TUMLCSXMLTreeNode *)

function TUMLCSXMLTreeNode.getSymbol(): TUMLCXMLFileANSISymbol;
begin
  Result := FSymbol;
end;

function TUMLCSXMLTreeNode.getTextValue(): string;
begin
  Result := FSymbol.Text;
end;

function TUMLCSXMLTreeNode.getTreeToken(): TUMLCXMLFileTreeNodeToken;
begin
  Result := FSymbol.TreeToken;
end;

function TUMLCSXMLTreeNode.getCanEdit(): Boolean;
begin
  Result := FCanEdit;
end;

function TUMLCSXMLTreeNode.getCanInsert(): Boolean;
begin
  Result := FCanInsert;
end;

function TUMLCSXMLTreeNode.getCanRemove(): Boolean;
begin
  Result := FCanRemove;
end;

function TUMLCSXMLTreeNode.getNewLineAfter(): Boolean;
begin
  Result := FNewLineAfter;
end;

procedure TUMLCSXMLTreeNode.setSymbol(const AValue: TUMLCXMLFileANSISymbol);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  FSymbol := AValue;

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeSymbol, AMsgRec.Message);

  AMsgRec.Sender  := Self;
  AMsgRec.Param   := @FSymbol;

  Self.SendMessage(AMsgRec);
end;

procedure TUMLCSXMLTreeNode.setTextValue(const AValue: string);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  FSymbol.Text := AValue;

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeTextValue, AMsgRec.Message);

  AMsgRec.Sender  := Self;
  AMsgRec.Param   := @FSymbol;

  Self.SendMessage(AMsgRec);
end;

procedure TUMLCSXMLTreeNode.setTreeToken(const AValue: TUMLCXMLFileTreeNodeToken);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  FSymbol.TreeToken := AValue;

  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeTreeToken, AMsgRec.Message);

  AMsgRec.Sender  := Self;
  AMsgRec.Param   := @FSymbol;

  Self.SendMessage(AMsgRec);
end;

procedure TUMLCSXMLTreeNode.setCanEdit(const AValue: Boolean);
begin
  FCanEdit := AValue;
end;

procedure TUMLCSXMLTreeNode.setCanInsert(const AValue: Boolean);
begin
  FCanInsert := AValue;
end;

procedure TUMLCSXMLTreeNode.setNewLineAfter(const AValue: Boolean);
begin

end;

procedure TUMLCSXMLTreeNode.setCanRemove(const AValue: Boolean);
begin
  FCanRemove := AValue;
end;

procedure TUMLCSXMLTreeNode.ConfirmedCopyTo
  (var ADestNode: TUMLCTreeNode);
var ThisDestNode: TUMLCSXMLTreeNode;
begin
  // perform copy of fields specific to parent class
  inherited ConfirmedCopyTo(ADestNode);

  // cast to current type
  ThisDestNode := TUMLCSXMLTreeNode(ADestNode);

  // perform copy of fields specific to this class
  umlcxmlfileansisymbols.Assign(ThisDestNode.FSymbol, Self.FSymbol);

  ThisDestNode.FCanInsert    := Self.FCanInsert;
  ThisDestNode.FCanEdit      := Self.FCanEdit;
  ThisDestNode.FCanRemove    := Self.FCanRemove;
  ThisDestNode.FNewLineAfter := Self.FNewLineAfter;
end;

procedure TUMLCSXMLTreeNode.ConfirmedMoveTo
  (var ADestNode: TUMLCTreeNode);
var ThisDestNode: TUMLCSXMLTreeNode;
begin
  // perform move of fields specific to parent class
  inherited ConfirmedMoveTo(ADestNode);

  // cast to current type
  ThisDestNode := TUMLCSXMLTreeNode(ADestNode);

  // perform move of fields specific to this class
  umlcxmlfileansisymbols.Move(ThisDestNode.FSymbol, Self.FSymbol);

  ThisDestNode.FCanInsert := Self.FCanInsert;
  Self.FCanInsert      := false;
  ThisDestNode.FCanEdit   := Self.FCanEdit;
  Self.FCanEdit        := false;
  ThisDestNode.FCanRemove := Self.FCanRemove;
  Self.FCanRemove      := false;
  ThisDestNode.FNewLineAfter := Self.FNewLineAfter;
  Self.FNewLineAfter      := false;
end;

procedure TUMLCSXMLTreeNode.DoCreate();
begin
  inherited DoCreate();
  umlcxmlfileansisymbols.Clear(FSymbol);
end;

procedure TUMLCSXMLTreeNode.DoDestroy();
begin
  umlcxmlfileansisymbols.Clear(FSymbol);
  inherited DoDestroy();
end;

(* TCustomUMLCSXMLTreeCollection *)

function TCustomUMLCSXMLTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCSXMLTreeNode.Create();
  Result.DoCreate();
end;

(* TCustomUMLCSXMLTreeContainer *)

function TCustomUMLCSXMLTreeContainer.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TCustomUMLCSXMLTreeCollection.Create();
  Result.DoCreate();
  // Goal: Create inheretable (polimorphic) collection.
  // Objetivo: Crear coleccion heredable (polimorfica).
end;

end.

