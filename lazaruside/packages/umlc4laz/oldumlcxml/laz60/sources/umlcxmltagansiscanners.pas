(*****************************************************************************
 *                                                                           *
 *  This file is part of the UMLCat Component Library.                       *
 *                                                                           *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution,    *
 *  for details about the copyright.                                         *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
 **)

unit umlcxmltagansiscanners;

interface

uses
  SysUtils, Classes,
  umlcstreams, umlcansitextstreams,
  umlcansistrings,
  umlcscanners,
  umlcscannerstates,
  umlcxmltagtokens,
  umlcxmltaggroups,
  umlcxmltagansisymbols,
  dummy;

type

(* TCustomUMLCXMLTagANSIScanner *)

  TCustomUMLCXMLTagANSIScanner = class(TCustomUMLCScanner)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FCurrentSymbol: TUMLCXMLTagANSISymbol;
    FBackupSymbol:  TUMLCXMLTagANSISymbol;

    UsePrevToken: Boolean;
  protected
    (* Protected declarations *)

    function getCurrentSymbol: TUMLCXMLTagANSISymbol; virtual;
    function getStream: TCustomUMLCANSITextBasedStream; virtual;

    procedure setCurrentSymbol(const Value: TUMLCXMLTagANSISymbol); virtual;
    procedure setStream(const Value: TCustomUMLCANSITextBasedStream); virtual;
  protected
    (* Protected declarations *)

    function InternalNext: Boolean; virtual;
    function ScanNext: Boolean;
  public
    (* Public declarations *)

    function Start: Boolean; override;
    function Next: Boolean; override;
    function Finish: Boolean; override;
  public
    (* Public declarations *)

    property CurrentSymbol: TUMLCXMLTagANSISymbol
      read getCurrentSymbol write setCurrentSymbol;

    property Stream: TCustomUMLCANSITextBasedStream
      read getStream write setStream;
  end;

(* TUMLCXMLTagANSIScanner *)

  TUMLCXMLTagANSIScanner = class(TCustomUMLCXMLTagANSIScanner)
  published
    (* published declarations *)

    (* TCustomUMLCScanner: *)

//    property InternalOptions;
    property Stream;

    (* TCustomUMLCXMLTagANSIScanner: *)
  end;

implementation

{$include 'umlcxmltagmatrix.incpas'}

(* TCustomUMLCXMLTagANSIScanner *)

function TCustomUMLCXMLTagANSIScanner.getCurrentSymbol: TUMLCXMLTagANSISymbol;
begin
  Result := FCurrentSymbol;
end;

function TCustomUMLCXMLTagANSIScanner.getStream: TCustomUMLCANSITextBasedStream;
begin
  Result := (FStream as TCustomUMLCANSITextBasedStream);
end;

procedure TCustomUMLCXMLTagANSIScanner.setCurrentSymbol
  (const Value: TUMLCXMLTagANSISymbol);
begin
  FCurrentSymbol := Value;
end;

procedure TCustomUMLCXMLTagANSIScanner.setStream(const Value: TCustomUMLCANSITextBasedStream);
begin
  if (FStream <> Value) then
  begin
    FStream := Value;
  end;
end;

function TCustomUMLCXMLTagANSIScanner.InternalNext: Boolean;
var C: ANSIchar; G: TUMLCXMLTagGroup; CurrentState: TUMLCState;
begin
  umlcxmltagansisymbols.Clear(FCurrentSymbol);

  FCurrentSymbol.StartRow := FCurrentRow;
  FCurrentSymbol.StartCol := FCurrentCol;
  C := #0;

  CurrentState := stStart;
  repeat
    Stream.GetChar(C);
    G := CharToGroup(C);
    CurrentState := NextState(CurrentState, G);

    if (G = xmltaggrEoLn) then
    begin
      FCurrentCol := 1;
      System.Inc(FCurrentRow);
    end else
    begin
      System.Inc(FCurrentCol);
    end;
    // actualizar posicion de caracteres
    // update characters location

    if CanSave(CurrentState)
      then umlcansistrings.Concat(FCurrentSymbol.Text, C);
    if CanMove(CurrentState)
      then Stream.NextChar;
  until IsTerminal(CurrentState);

  FCurrentSymbol.FinishRow := FCurrentRow;
  FCurrentSymbol.FinishCol := FCurrentCol;
  FCurrentSymbol.Token := StateToToken(CurrentState);

  Result := not (CurrentState < stStart);
end;

function TCustomUMLCXMLTagANSIScanner.ScanNext: Boolean;
var CanContinue: Boolean;
begin
  umlcxmltagansisymbols.Clear(FBackupSymbol);
  repeat
    Result := InternalNext;
    // obtener "EoF", "EoPg", "EoLn", "espacio" como simbolos independientes
    // obtain "EoF", "EoPg", "EoLn", "space" as independent symbols

    CanContinue :=
      (FCurrentSymbol.Token = xmltagtkSpace) or
      (FCurrentSymbol.Token = xmltagtkEoLn) or
      (FCurrentSymbol.Token = xmltagtkEoPg);
  until ((not CanContinue) or (not Result))
end;

function TCustomUMLCXMLTagANSIScanner.Start: Boolean;
begin
  FCurrentRow := 1;
  FCurrentCol := 1;

  Result := FStream.Connect;
end;

function TCustomUMLCXMLTagANSIScanner.Next: Boolean;
begin
  if (UsePrevToken) then
  begin
    FCurrentSymbol := FBackupSymbol;
    UsePrevToken := FALSE;
    Result := TRUE;
  end else Result := ScanNext;
end;

function TCustomUMLCXMLTagANSIScanner.Finish: Boolean;
begin
  Result := FStream.Disconnect;
end;

end.
