(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctreeiterators;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  contnrs,
  umlciterators,
  umlctreenodes,
  dummy;

(**
 ** This unit implements concrete classes,
 ** in order to support the "Iterator" Design Pattern,
 ** that are specific to the tree structure.
 **
 ** This iterator is intended to be used with a "while" loop,
 ** instead of a "for" loop, since the "for" loop used,
 ** in the GoF book is not the same in Pascal.
 **
 ** ...
 **)

 type

   (* TUMLCAbstractTreeIterator *)

   TUMLCAbstractTreeIterator = class(TUMLCAbstractIterator)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     FCollection: TUMLCTreeCollection;
   public
     (* Public declarations *)

     function Collection(): TUMLCTreeCollection;

     constructor Create(); override;
     destructor Destroy(); override;
   end;

   (* TUMLCAbstractTreeInmediateIterator *)

   TUMLCAbstractTreeInmediateIterator = class(TUMLCAbstractIterator)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)
   public
     (* Public declarations *)
   end;

   (* TUMLCTreeInmediateForEachIterator *)

   TUMLCTreeInmediateForEachIterator = class(TUMLCAbstractIterator)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)
   public
     (* Public declarations *)

     procedure Start(); override;
     procedure Stop(); override;

     function MoveNext(): Boolean; override;
     function Current(): TObject; override;
   end;

   (* TUMLCTreeInmediateForBackIterator *)

   TUMLCTreeInmediateForBackIterator = class(TUMLCAbstractIterator)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)
   public
     (* Public declarations *)

     procedure Start(); override;
     procedure Stop(); override;

     function MoveNext(): Boolean; override;
     function Current(): TObject; override;
   end;

   (* TUMLCTreeUpIterator *)

   TUMLCTreeUpIterator = class(TUMLCAbstractTreeIterator)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)
   public
     (* Public declarations *)

     procedure Start(); override;
     procedure Stop(); override;

     function MoveNext(): Boolean; override;
     function Current(): TObject; override;
   end;

   (* TUMLCAbstractTreeIteratorRecursive *)

   TUMLCAbstractTreeIteratorRecursive = class(TUMLCAbstractTreeIterator)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     FStack: TObjectStack;
   public
     (* Public declarations *)

     procedure Start(); override;
     procedure Stop(); override;

     function MoveNext(): Boolean; override;
     function Current(): TObject; override;
   end;

   (* TUMLCTreePrefixIterator *)

   TUMLCTreePrefixIterator = class(TUMLCAbstractTreeIteratorRecursive)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)
   public
     (* Public declarations *)

     procedure Start(); override;
     procedure Stop(); override;

     function MoveNext(): Boolean; override;
     function Current(): TObject; override;
   end;

   (* TUMLCTreePosfixIterator *)

   TUMLCTreePosfixIterator = class(TUMLCAbstractTreeIteratorRecursive)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)
   public
     (* Public declarations *)

     procedure Start(); override;
     procedure Stop(); override;

     function MoveNext(): Boolean; override;
     function Current(): TObject; override;
   end;

   (* TUMLCTreePrefixPosfixIterator *)

   TUMLCTreePrefixPosfixIterator = class(TUMLCAbstractTreeIteratorRecursive)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)
   public
     (* Public declarations *)

     procedure Start(); override;
     procedure Stop(); override;

     function MoveNext(): Boolean; override;
     function Current(): TObject; override;
   end;

implementation

(* TUMLCTreeInmediateForBackIterator *)

procedure TUMLCTreeInmediateForBackIterator.Start;
begin
  //inherited Start();
end;

procedure TUMLCTreeInmediateForBackIterator.Stop;
begin
  //inherited Stop();
end;

function TUMLCTreeInmediateForBackIterator.MoveNext: Boolean;
begin
  Result := false;
end;

function TUMLCTreeInmediateForBackIterator.Current: TObject;
begin
  Result := nil;
end;

(* TUMLCTreeInmediateForEachIterator *)

procedure TUMLCTreeInmediateForEachIterator.Start();
begin
  //inherited Start();
end;

procedure TUMLCTreeInmediateForEachIterator.Stop();
begin
  //inherited Stop();
end;

function TUMLCTreeInmediateForEachIterator.MoveNext: Boolean;
begin
  Result := false;
end;

function TUMLCTreeInmediateForEachIterator.Current: TObject;
begin
  Result := nil;
end;

(* TUMLCTreeUpIterator *)

procedure TUMLCTreeUpIterator.Start();
begin
  //inherited Start();
end;

procedure TUMLCTreeUpIterator.Stop();
begin
  //inherited Stop();
end;

function TUMLCTreeUpIterator.MoveNext: Boolean;
begin
  Result := false;
end;

function TUMLCTreeUpIterator.Current: TObject;
begin
  Result := nil;
end;

(* TUMLCTreePrefixPosfixIterator *)

procedure TUMLCTreePrefixPosfixIterator.Start();
begin
  inherited Start();
end;

procedure TUMLCTreePrefixPosfixIterator.Stop();
begin
  inherited Stop();
end;

function TUMLCTreePrefixPosfixIterator.MoveNext: Boolean;
begin
  Result := false;
end;

function TUMLCTreePrefixPosfixIterator.Current: TObject;
begin
  Result := nil;
end;

(* TUMLCTreePosfixIterator *)

procedure TUMLCTreePosfixIterator.Start();
begin
  inherited Start();
end;

procedure TUMLCTreePosfixIterator.Stop();
begin
  inherited Stop();
end;

function TUMLCTreePosfixIterator.MoveNext: Boolean;
begin
  Result := false;
end;

function TUMLCTreePosfixIterator.Current: TObject;
begin
  Result := nil;
end;

(* TUMLCTreePrefixIterator *)

procedure TUMLCTreePrefixIterator.Start();
begin
  inherited Start();
end;

procedure TUMLCTreePrefixIterator.Stop();
begin
  inherited Stop();
end;

function TUMLCTreePrefixIterator.MoveNext(): Boolean;
begin
  Result := false;
end;

function TUMLCTreePrefixIterator.Current(): TObject;
begin
  Result := nil;
end;

(* TUMLCAbstractTreeIteratorRecursive *)

procedure TUMLCAbstractTreeIteratorRecursive.Start;
begin
  if (FStack <> nil) then
  begin
    FStack.Free();
    FStack := nil;
  end;

  FStack := TObjectStack.Create();
end;

procedure TUMLCAbstractTreeIteratorRecursive.Stop;
begin
  FStack.Free();
end;

function TUMLCAbstractTreeIteratorRecursive.MoveNext: Boolean;
begin
  Result := false;
end;

function TUMLCAbstractTreeIteratorRecursive.Current: TObject;
begin
  Result := nil;
end;

(* TUMLCAbstractTreeIterator *)

function TUMLCAbstractTreeIterator.Collection(): TUMLCTreeCollection;
begin
  Result := FCollection;
end;

constructor TUMLCAbstractTreeIterator.Create();
begin
  inherited Create();
end;

destructor TUMLCAbstractTreeIterator.Destroy();
begin
  inherited Destroy();
end;

end.

