(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlckeyvaluelisttreenodes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  //umlclists,
  umlctreenodes,
  umlckeyvaluemodes,
  umlconlystringkeyvaluelists,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 **
 ** It was not designed with generics collections.
 **
 ** Additionally, a "KeyValueList" property and helper functions are provided,
 ** Note: Its a list of "KeyValue" items, property,
 ** not a "KeyValue" property, itself.
 **)

type

(* TUMLCKeyValueListTreeNode *)

  TUMLCKeyValueListTreeNode = class(TUMLCTreeNode)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    _KeyValueList : TUMLCOnlyStringKeyValueList;
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;

    function KeyValueList(): TUMLCOnlyStringKeyValueList;
  end;

(* TUMLCKeyValueListTreeCollection *)

  TUMLCKeyValueListTreeCollection = class(TUMLCTreeCollection)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* Public declarations *)
  end;

implementation

(* TUMLCKeyValueListTreeNode *)

procedure TUMLCKeyValueListTreeNode.DoCreate();
begin
  inherited DoCreate();

  _KeyValueList := TUMLCOnlyStringKeyValueList.Create();
end;

procedure TUMLCKeyValueListTreeNode.DoDestroy();
begin
  _KeyValueList.Empty();
  _KeyValueList.Free();

  inherited DoDestroy();
end;

// "read-only"
function TUMLCKeyValueListTreeNode.KeyValueList(): TUMLCOnlyStringKeyValueList;
begin
  Result := Self._KeyValueList;
end;

(* TUMLCKeyValueListTreeCollection *)

function TUMLCKeyValueListTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCKeyValueListTreeNode.Create();
  Result.DoCreate();
end;

end.

