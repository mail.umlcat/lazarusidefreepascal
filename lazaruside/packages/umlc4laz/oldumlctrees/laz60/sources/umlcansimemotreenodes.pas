(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansimemotreenodes;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlccomparisons,
  umlcansimemos,
  umlctreenodes,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 **
 ** It was not designed with generics collections.
 **
 ** Additionally, a "Text" property and helper functions are provided,
 **
 ** The "Text" property is a special string type,
 ** similar to the standard pascal string,
 ** but, supports more than 256 bytes, includes a field for length.
 **)

 type

  (* TUMLCANSIMemoTreeNode *)

    TUMLCANSIMemoTreeNode = class(TUMLCTreeNode)
    private
      (* Private declarations *)
    protected
      (* Protected declarations *)

      FText: ansimemo;

      (* Accessors declarations *)

      function getText(): ansimemo; virtual;

      procedure setText(const AValue: ansimemo); virtual;
    public
      (* Public declarations *)

      function IsEmpty(): Boolean;

      function EqualAs(const AStr: ansimemo): Boolean;
      function SameAs(const AStr: ansimemo): Boolean;

      function StartsWith(const ASubStr: ansimemo): Boolean;
      function FinishesWith(const ASubStr: ansimemo): Boolean;

      function ComparesWith(const AStr: ansimemo): TComparison;

      procedure Clear();
    public
      (* Public declarations *)

      property Text: ansimemo
       read getText write setText;
    end;

  (* TUMLCANSIMemoTreeCollection *)

    TUMLCANSIMemoTreeCollection = class(TUMLCTreeCollection)
    private
      (* Private declarations *)
    protected
      (* Protected declarations *)

      function CreateNodeByClass(): TUMLCTreeNode; override;
    public
      (* Friend Protected declarations *)
    public
      (* Public declarations *)
    end;


implementation

(* TUMLCANSIMemoTreeNode *)

function TUMLCANSIMemoTreeNode.getText(): ansimemo;
begin
  Result := FText;
end;

procedure TUMLCANSIMemoTreeNode.setText(const AValue: ansimemo);
//var ACollection: TUMLCNullStringTreeCollection;
begin
  (*
  if (EqualAs(FText, AValue)) then
  begin
    ACollection := TUMLCNullStringTreeCollection(InternalCollection);
    ACollection.RequestChangeText(Self, AValue);
  end;
  *)
end;

function TUMLCANSIMemoTreeNode.IsEmpty(): Boolean;
begin
  Result := umlcansimemos.IsEmpty(FText);
end;

function TUMLCANSIMemoTreeNode.EqualAs(const AStr: ansimemo): Boolean;
begin
  Result := umlcansimemos.Equal(FText, AStr);
  // Goal: Case Sensitive comparison.
end;

function TUMLCANSIMemoTreeNode.SameAs(const AStr: ansimemo): Boolean;
begin
  Result := umlcansimemos.SameText(FText, AStr);
  // Goal: Case Insensitive comparison.
end;

function TUMLCANSIMemoTreeNode.StartsWith(const ASubStr: ansimemo): Boolean;
begin
  Result := umlcansimemos.StartsWith(ASubStr, FText);
end;

function TUMLCANSIMemoTreeNode.FinishesWith(const ASubStr: ansimemo): Boolean;
begin
  Result := umlcansimemos.FinishesWith(ASubStr, FText);
end;

function TUMLCANSIMemoTreeNode.ComparesWith(const AStr: ansimemo): TComparison;
begin
  Result := umlcansimemos.Compare(AStr, FText);
end;

procedure TUMLCANSIMemoTreeNode.Clear();
begin
  umlcansimemos.Clear(Self.FText);
end;

(* TUMLCANSIMemoTreeCollection *)

function TUMLCANSIMemoTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCANSIMemoTreeNode.Create();
  Result.DoCreate();
end;



end.

