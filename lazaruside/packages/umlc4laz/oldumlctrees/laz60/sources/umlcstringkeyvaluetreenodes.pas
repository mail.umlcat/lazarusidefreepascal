(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstringkeyvaluetreenodes;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlcguids,
  umlctypes, umlcstdtypes,
  umlcstrings,
  umlctreenodes,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 **
 ** It was not designed with generics collections.
 **
 ** Additionally, a "Key" & "Value" pair of properties,
 ** and helper functions are provided,
 ** Note: Its not a "KeyValue" list property.
 **)


const

 // ---

 ID_TUMLCStringKeyValueTreeNode : TUMLCType =
   ($81,$22,$AB,$A4,$D4,$92,$A6,$47,$B0,$4E,$D6,$8F,$54,$CA,$9A,$0B);

 ID_TUMLCStringKeyValueTreeCollection : TUMLCType =
   ($CE,$41,$3B,$79,$E8,$EC,$17,$4E,$9A,$DE,$2D,$C2,$83,$10,$1D,$27);

 // ---


 type

 (* TUMLCStringKeyValueTreeNode *)

   TUMLCStringKeyValueTreeNode = class(TUMLCTreeNode)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     _Key:   string;
     _Value: string;
   protected
     (* Protected declarations *)

   public
     (* Public declarations *)

     (* accesor declarations *)

     function getKey(): string; virtual;
     procedure setKey(const AValue: string); virtual;

     function getValue(): string; virtual;
     procedure setValue(const AValue: string); virtual;
   public
     (* Public declarations *)

     procedure DoCreate(); override;
     procedure DoDestroy(); override;
   public
     (* Public declarations *)

     function MatchKey(const AKey: string): Boolean;
     function MatchValue(const AValue: string): Boolean;

     function EqualKey(const AKey: string): Boolean;
     function EqualValue(const AValue: string): Boolean;

     function SameKey(const AKey: string): Boolean;
     function SameValue(const AValue: string): Boolean;

     function InsertByKey
       (const AKey: string): TUMLCStringKeyValueTreeNode;
     function InsertByKeyValue
       (const AKey, AValue: string): TUMLCStringKeyValueTreeNode;
   public
     (* Public declarations *)

     property Key: string
       read getKey write setKey;

     property Value: string
       read getValue write setValue;
   end;

 (* TUMLCStringKeyValueTreeCollection *)

   TUMLCStringKeyValueTreeCollection = class(TUMLCTreeCollection)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     _AllowDuplicates: boolean;
     _IgnoreKeyCase: boolean;
     _IgnoreValueCase: boolean;
   protected
     (* Protected declarations *)

     (* accesor declarations *)

     function getAllowDuplicates(): boolean;
     function getIgnoreKeyCase(): boolean;
     function getIgnoreValueCase(): boolean;

     procedure setAllowDuplicates(const AValue: boolean);
     procedure setIgnoreKeyCase(const AValue: boolean);
     procedure setIgnoreValueCase(const AValue: boolean);
   protected
     (* Protected declarations *)

     function CreateNodeByClass(): TUMLCTreeNode; override;
   public
     (* Friend Protected declarations *)

     function InternalMatchKeys(const A, B: string): Boolean;
     function InternalMatchValues(const A, B: string): Boolean;
   public
     (* Public declarations *)

     function NodeOfKey(const AKey: string): TUMLCStringKeyValueTreeNode;
     function NodeOfValue(const AValue: string): TUMLCStringKeyValueTreeNode;

     function KeyFound(const AKey: string): Boolean;
     function ValueFound(const AValue: string): Boolean;

     function ReplaceFirstKey(const APrevKey, ANewKey: string): Boolean;
     function ReplaceFirstValueByKey(const AKey, AValue: string): Boolean;
     function ReplaceFirstValue(const APrevValue, ANewValue: string): Boolean;
     function ReplaceAllValues(const APrevValue, ANewValue: string): Boolean;
   public
     (* Public declarations *)

     property AllowDuplicates: boolean
       read getAllowDuplicates write setAllowDuplicates;
     property IgnoreKeyCase: boolean
       read getIgnoreKeyCase write setIgnoreKeyCase;
     property IgnoreValueCase: boolean
       read getIgnoreValueCase write setIgnoreValueCase;
   end;

implementation

(* TUMLCStringKeyValueTreeNode *)

function TUMLCStringKeyValueTreeNode.getKey(): string;
begin
  Result := Self._Key;
end;

procedure TUMLCStringKeyValueTreeNode.setKey(const AValue: string);
begin
  Self._Key := AValue;
end;

function TUMLCStringKeyValueTreeNode.getValue(): string;
begin
  Result := Self._Value;
end;

procedure TUMLCStringKeyValueTreeNode.setValue(const AValue: string);
begin
  Self._Value := AValue;
end;

procedure TUMLCStringKeyValueTreeNode.DoCreate();
begin
  inherited DoCreate();

  Self._Key   := '';
  Self._Value := '';
end;

procedure TUMLCStringKeyValueTreeNode.DoDestroy();
begin
  Self._Value := '';
  Self._Key   := '';

  inherited DoDestroy();
end;

function TUMLCStringKeyValueTreeNode.MatchKey(const AKey: string): Boolean;
var ACollection: TUMLCStringKeyValueTreeCollection;
begin
  ACollection := TUMLCStringKeyValueTreeCollection(InternalCollection);
  Result := ACollection.InternalMatchKeys(Self._Key, AKey);
end;

function TUMLCStringKeyValueTreeNode.MatchValue(const AValue: string): Boolean;
var ACollection: TUMLCStringKeyValueTreeCollection;
begin
  ACollection := TUMLCStringKeyValueTreeCollection(InternalCollection);
  Result := ACollection.InternalMatchKeys(Self._Value, AValue);
end;

function TUMLCStringKeyValueTreeNode.EqualKey(const AKey: string): Boolean;
begin
  Result := (Self._Key = AKey);
end;

function TUMLCStringKeyValueTreeNode.EqualValue(const AValue: string): Boolean;
begin
  Result := (Self._Value = AValue);
end;

function TUMLCStringKeyValueTreeNode.SameKey(const AKey: string): Boolean;
begin
  Result := umlcstrings.SameText(Self._Key, AKey);
end;

function TUMLCStringKeyValueTreeNode.SameValue(const AValue: string): Boolean;
begin
  Result := umlcstrings.SameText(Self._Value, AValue);
end;

function TUMLCStringKeyValueTreeNode.InsertByKey
  (const AKey: string): TUMLCStringKeyValueTreeNode;
var CanInsert: Boolean;
begin
  Result := Self.InsertByKeyValue(AKey, '');
end;

function TUMLCStringKeyValueTreeNode.InsertByKeyValue
  (const AKey, AValue: string): TUMLCStringKeyValueTreeNode;
var CanInsert: Boolean; ACollection: TUMLCStringKeyValueTreeCollection;
begin
  Result := nil;

  CanInsert := true;
  ACollection := TUMLCStringKeyValueTreeCollection(InternalCollection);
  if (not ACollection.AllowDuplicates) then
  begin
    CanInsert := (not MatchKey(AKey));
  end;

  if (CanInsert) then
  begin
    Result := (Self.Insert() as TUMLCStringKeyValueTreeNode);
    Result.Key   := AKey;
    Result.Value := AValue;
  end;
end;

(* TUMLCStringKeyValueTreeCollection *)

function TUMLCStringKeyValueTreeCollection.getAllowDuplicates(): boolean;
begin
  Result := Self._AllowDuplicates;
end;

function TUMLCStringKeyValueTreeCollection.getIgnoreKeyCase(): boolean;
begin
  Result := Self._IgnoreKeyCase;
end;

function TUMLCStringKeyValueTreeCollection.getIgnoreValueCase(): boolean;
begin
  Result := Self._IgnoreValueCase;
end;

procedure TUMLCStringKeyValueTreeCollection.setAllowDuplicates(const AValue: boolean);
begin
  Self._AllowDuplicates := AValue;
end;

procedure TUMLCStringKeyValueTreeCollection.setIgnoreKeyCase(const AValue: boolean);
begin
  Self._IgnoreKeyCase := AValue;
end;

procedure TUMLCStringKeyValueTreeCollection.setIgnoreValueCase(const AValue: boolean);
begin
  Self._IgnoreValueCase := AValue;
end;

function TUMLCStringKeyValueTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCStringKeyValueTreeNode.Create();
  Result.DoCreate();
end;

function TUMLCStringKeyValueTreeCollection.InternalMatchKeys
  (const A, B: string): Boolean;
begin
  Result := false;
  if (IgnoreKeyCase) then
  begin
    Result := umlcstrings.SameText(A, B);
  end else
  begin
    Result := (A = B);
  end;
end;

function TUMLCStringKeyValueTreeCollection.InternalMatchValues
  (const A, B: string): Boolean;
begin
  Result := false;
  if (IgnoreValueCase) then
  begin
    Result := umlcstrings.SameText(A, B);
  end else
  begin
    Result := (A = B);
  end;
end;

function TUMLCStringKeyValueTreeCollection.NodeOfKey
  (const AKey: string): TUMLCStringKeyValueTreeNode;
begin
  Result := nil;
end;

function TUMLCStringKeyValueTreeCollection.NodeOfValue
  (const AValue: string): TUMLCStringKeyValueTreeNode;
begin
  Result := nil;
end;

function TUMLCStringKeyValueTreeCollection.KeyFound(const AKey: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyValueTreeCollection.ValueFound
  (const AValue: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyValueTreeCollection.ReplaceFirstKey
  (const APrevKey, ANewKey: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyValueTreeCollection.ReplaceFirstValueByKey
  (const AKey, AValue: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyValueTreeCollection.ReplaceFirstValue
  (const APrevValue, ANewValue: string): Boolean;
begin
  Result := false;
end;

function TUMLCStringKeyValueTreeCollection.ReplaceAllValues
  (const APrevValue, ANewValue: string): Boolean;
begin
  Result := false;
end;

end.

