(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcidtreenodes;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlctreenodes,
  umlctreecntrs,
  dummy;

type

(* TUMLCIdTreeNode *)

  TUMLCIdTreeNode = class(TUMLCContainerTreeNode)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function getIdentifier(): string;
    procedure setIdentifier(const Value: string);
  public
    (* Public declarations *)

    property Identifier: string
      read getIdentifier write setIdentifier;
  end;

(* TUMLCIdTreeCollection *)

  TUMLCIdTreeCollection = class(TUMLCContainerTreeCollection)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)
  public
    (* Public declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  end;

(* TUMLCIdTreeContainer *)

  TUMLCIdTreeContainer = class(TCustomUMLCTreeContainer)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function CreateCollectionByClass(): TUMLCContainerTreeCollection; override;
  public
    (* Public declarations *)
  end;

implementation

(* TUMLCIdTreeNode *)

function TUMLCIdTreeNode.getIdentifier(): string;
begin
  Result := Self.Text;
end;

procedure TUMLCIdTreeNode.setIdentifier(const Value: string);
begin
  Self.Text := Value;
end;

(* TUMLCIdTreeCollection *)

function TUMLCIdTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCIdTreeNode.Create();
  Result.DoCreate();
end;

(* TUMLCIdTreeContainer *)

function TUMLCIdTreeContainer.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TUMLCIdTreeCollection.Create();
  Result.DoCreate();
end;

end.

