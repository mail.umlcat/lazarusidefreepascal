(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmsgtreecntrs;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  umlcguids,
  umlcguidstrs,
  umlclists,
  umlcmsgtypes,
  umlctreenodes,
  umlctreecntrs,
  dummy;

(**
 ** Description:
 ** This unit contains several classes.
 **
 ** The main class is "TCustomUMLCMsgTreeContainer",
 ** a non-visual component that contains a hierarchical,
 ** tree structure.
 **
 ** Additionaly, it supports the Subject part,
 ** of the Subject-Observer Pattern,
 ** and its observers, maybe visual controls, or not.
 **)

 // default messages

 const
   msgTreeNodeBeforeInsert
     = '{8A1BB14E-F285D14A-A8272FEB-631A0181}';
   msgTreeNodeAfterInsert
     = '{324D2E02-79A3024D-B8975C52-6010CCA0}';

   msgTreeNodeBeforeRemove
     = '{72BE8F90-66EE5A46-87C345F4-A121FD05}';
   msgTreeNodeAfterRemove
     = '{3F9715ED-32BFB648-8E0A2197-7F10A164}';

   msgTreeNodeBeforeChangeText
     = '{663499E3-BF0AAB45-90AB21AE-FBFCD728}';
   msgTreeNodeAfterChangeText
     = '{690F1DDE-A82C1E49-A548E4AF-08FEB6B4}';

   msgTreeNodeBeforeChangeSelected
     = '{7EF47716-E5129340-8FB19308-8577B33A}';
   msgTreeNodeAfterChangeSelected
     = '{85E39117-921E3148-BC5CBDB0-341164A4}';

type

(* TUMLCMsgContainerTreeNode *)

  TUMLCMsgContainerTreeNode = class(TUMLCContainerTreeNode)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    procedure SendMessage
      (const AMsgRec: TUMLCMessageParamsRecord);
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  end;

(* TUMLCMsgContainerTreeCollection *)

  TUMLCMsgContainerTreeCollection = class(TUMLCContainerTreeCollection)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;

    procedure SendMessage
      (const AMsgRec: TUMLCMessageParamsRecord);
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;
  end;

 (* TCustomUMLCMsgTreeContainer *)

   TCustomUMLCMsgTreeContainer =
     class(TCustomUMLCTreeContainer, IUMLCMessageServer)
   private
     (* Private declarations *)
   protected
     (* Protected declarations *)

     FClients: TUMLCMsgClientList;
   protected
     (* Protected declarations *)

     (* accesors declarations *)

     function getClients(): TUMLCMsgClientList;

     procedure setClients(const AValue: TUMLCMsgClientList);
   protected
     (* Protected declarations *)

     function CreateCollectionByClass(): TUMLCContainerTreeCollection; override;

     function CreateClients(): TUMLCMsgClientList; virtual;
   public
     (* Friend Protected declarations *)

     procedure NotifyBeforeChangeText
       (const ANode: TUMLCContainerTreeNode;
        const AText: string); override;
     procedure NotifyAfterChangeText
       (const ANode: TUMLCContainerTreeNode); override;

     procedure NotifyBeforeChangeSelected
       (const ANode: TUMLCContainerTreeNode;
        const ASelected: Boolean); override;
     procedure NotifyAfterChangeSelected
       (const ANode: TUMLCContainerTreeNode); override;

     procedure NotifyBeforeInsert
       (const AParentNode, ANode: TUMLCContainerTreeNode;
        const AIndex: Integer); override;
     procedure NotifyAfterInsert
       (const AParentNode, ANode: TUMLCContainerTreeNode;
        const AIndex: Integer); override;

     procedure NotifyBeforeRemove
       (const ANode: TUMLCContainerTreeNode); override;
     procedure NotifyAfterRemove
       (const ANode: TUMLCContainerTreeNode); override;

     procedure NotifyBeforeEmpty
       (const ANode: TUMLCContainerTreeNode); override;
     procedure NotifyAfterEmpty
       (const ANode: TUMLCContainerTreeNode); override;
   public
     (* Public declarations *)

     constructor Create(AOwner: TComponent); override;
     destructor Destroy(); override;
   public
     (* Public declarations *)

     procedure InsertClient(const AClient: IUMLCMessageClient);
     procedure RemoveClient(const AClient: IUMLCMessageClient);

     function AsComponent(): TComponent;

     procedure SendMessage
       (const AMsgRec: TUMLCMessageParamsRecord);
     procedure SendMessageSingle
       (const AClient: IUMLCMessageClient;
        const AMsgRec: TUMLCMessageParamsRecord);
   public
     (* Public declarations *)

     (* Read-Only properties *)

     function ClientsCount(): Integer;
     function HasClients(): Boolean;

     (* Never Published declarations *)

     property Clients: TUMLCMsgClientList
       read getClients write setClients;
   end;

 (* TUMLCMsgTreeContainer *)

   TUMLCMsgTreeContainer = class(TCustomUMLCMsgTreeContainer)
   published
     (* Published declarations *)

     (* TCustomUMLCTreeContainer: *)

     (* TCustomUMLCMsgTreeContainer: *)
   end;

implementation

(* TUMLCMsgContainerTreeNode *)

procedure TUMLCMsgContainerTreeNode.SendMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
var ACollection: TUMLCMsgContainerTreeCollection;
begin
  ACollection := TUMLCMsgContainerTreeCollection(Self.InternalCollection);
  ACollection.SendMessage(AMsgRec);
end;

procedure TUMLCMsgContainerTreeNode.DoCreate();
begin
  inherited DoCreate();
end;

procedure TUMLCMsgContainerTreeNode.DoDestroy();
begin
  inherited DoDestroy();
end;

(* TUMLCMsgContainerTreeCollection *)

function TUMLCMsgContainerTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCMsgContainerTreeNode.Create();
  Result.DoCreate();
end;

procedure TUMLCMsgContainerTreeCollection.SendMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
var AMsgContainer: TCustomUMLCMsgTreeContainer;
begin
  AMsgContainer := TCustomUMLCMsgTreeContainer(Self.Container);
  AMsgContainer.SendMessage(AMsgRec);
end;

procedure TUMLCMsgContainerTreeCollection.DoCreate();
begin
  inherited DoCreate();
end;

procedure TUMLCMsgContainerTreeCollection.DoDestroy();
begin
  inherited DoDestroy();
end;

(* TCustomUMLCMsgTreeContainer *)

function TCustomUMLCMsgTreeContainer.getClients(): TUMLCMsgClientList;
begin
  Result := FClients
  // Goal: "Clients" property get method .
  // Objetivo: Metodo lectura para propiedad "Clients".
end;

procedure TCustomUMLCMsgTreeContainer.setClients(const AValue: TUMLCMsgClientList);
begin
  FClients := AValue;
  // Goal: "Clients" property set method .
  // Objetivo: Metodo escritura para propiedad "Clients".
end;

function TCustomUMLCMsgTreeContainer.CreateCollectionByClass(): TUMLCContainerTreeCollection;
begin
  Result := TUMLCMsgContainerTreeCollection.Create();
  Result.DoCreate();
  // Goal: Create inheretable (polimorphic) collection.
  // Objetivo: Crear coleccion heredable (polimorfica).
end;

procedure TCustomUMLCMsgTreeContainer.NotifyBeforeChangeText
  (const ANode: TUMLCContainerTreeNode; const AText: string);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeChangeText, AMsgRec.Message);

  AMsgRec.Sender  := ANode;
  AMsgRec.Param   := @AText;
  SendMessage(AMsgRec);
end;

procedure TCustomUMLCMsgTreeContainer.NotifyAfterChangeText
  (const ANode: TUMLCContainerTreeNode);
var AMsgRec: TUMLCMessageParamsRecord; AText: string;
begin
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeText, AMsgRec.Message);

  AText := ANode.Text;
  AMsgRec.Sender  := ANode;
  AMsgRec.Param   := @AText;
  SendMessage(AMsgRec);
end;

procedure TCustomUMLCMsgTreeContainer.NotifyBeforeChangeSelected
  (const ANode: TUMLCContainerTreeNode; const ASelected: Boolean);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeChangeSelected, AMsgRec.Message);

  AMsgRec.Sender  := ANode;
  AMsgRec.Param   := @ASelected;
  SendMessage(AMsgRec);
end;

procedure TCustomUMLCMsgTreeContainer.NotifyAfterChangeSelected
  (const ANode: TUMLCContainerTreeNode);
var AMsgRec: TUMLCMessageParamsRecord; ASelected: Boolean;
begin
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterChangeSelected, AMsgRec.Message);

  ASelected := ANode.Selected;
  AMsgRec.Sender  := ANode;
  AMsgRec.Param   := @ASelected;
  SendMessage(AMsgRec);
end;

procedure TCustomUMLCMsgTreeContainer.NotifyBeforeInsert
  (const AParentNode, ANode: TUMLCContainerTreeNode; const AIndex: Integer);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  // notify before insertion to clients
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeInsert, AMsgRec.Message);

  AMsgRec.Sender  := AParentNode;
  AMsgRec.Param   := ANode;
  SendMessage(AMsgRec);
end;

procedure TCustomUMLCMsgTreeContainer.NotifyAfterInsert
  (const AParentNode, ANode: TUMLCContainerTreeNode; const AIndex: Integer);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  // notify after insertion to clients
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterInsert, AMsgRec.Message);

  AMsgRec.Sender  := AParentNode;
  AMsgRec.Param   := ANode;
  SendMessage(AMsgRec);
end;

procedure TCustomUMLCMsgTreeContainer.NotifyBeforeRemove
  (const ANode: TUMLCContainerTreeNode);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  // notify before remotion to clients
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeRemove, AMsgRec.Message);

  AMsgRec.Sender  := ANode;
  AMsgRec.Param   := ANode;
  SendMessage(AMsgRec);
end;

procedure TCustomUMLCMsgTreeContainer.NotifyAfterRemove
  (const ANode: TUMLCContainerTreeNode);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  // notify before remotion to clients
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterRemove, AMsgRec.Message);

  AMsgRec.Sender  := ANode;
  AMsgRec.Param   := ANode;
  SendMessage(AMsgRec);
end;

procedure TCustomUMLCMsgTreeContainer.NotifyBeforeEmpty
  (const ANode: TUMLCContainerTreeNode);
//var AMsgRec: TUMLCMessageParamsRecord;
begin
  inherited NotifyBeforeEmpty(ANode);
  (*
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeBeforeEmpty, AMsgRec.Message);

  AMsgRec.Sender  := ANode;
  AMsgRec.Param   := ANode;
  SendMessage(AMsgRec);
  *)
end;

procedure TCustomUMLCMsgTreeContainer.NotifyAfterEmpty
  (const ANode: TUMLCContainerTreeNode);
//var AMsgRec: TUMLCMessageParamsRecord;
begin
  inherited NotifyAfterEmpty(ANode);
  (*
  umlcguidstrs.DoubleStrToGUID
    (msgTreeNodeAfterEmpty, AMsgRec.Message);

  AMsgRec.Sender  := ANode;
  AMsgRec.Param   := ANode;
  SendMessage(AMsgRec);
  *)
end;

function TCustomUMLCMsgTreeContainer.CreateClients(): TUMLCMsgClientList;
begin
  Result := TUMLCMsgClientList.Create();
  Result.RecordSize := System.SizeOf(TUMLCMsgItemRec);
end;

constructor TCustomUMLCMsgTreeContainer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FClients := CreateClients();
end;

destructor TCustomUMLCMsgTreeContainer.Destroy;
begin
  FClients.Free();
  inherited Destroy();
end;

procedure TCustomUMLCMsgTreeContainer.InsertClient
  (const AClient: IUMLCMessageClient);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  if (Clients.IndexOf(AClient) = IndexNotFound) then
  begin
    // perform subscription
    Clients.Insert(AClient);

    // confirm subscription to client
    umlcguidstrs.DoubleStrToGUID
      (msgServerAssign, AMsgRec.Message);

    AMsgRec.Sender  := Self;
    AMsgRec.Param   := AClient;
    SendMessageSingle(AClient, AMsgRec);
  end;
end;

procedure TCustomUMLCMsgTreeContainer.RemoveClient
  (const AClient: IUMLCMessageClient);
var AMsgRec: TUMLCMessageParamsRecord;
begin
  if (Clients.IndexOf(AClient) = IndexNotFound) then
  begin
    // confirm cancelation of subscription to client
    umlcguidstrs.DoubleStrToGUID
      (msgServerDeassign, AMsgRec.Message);

    AMsgRec.Sender  := Self;
    AMsgRec.Param   := AClient;
    SendMessageSingle(AClient, AMsgRec);

    // perform cancelation of subscription
    Clients.Remove(AClient);
  end;
end;

function TCustomUMLCMsgTreeContainer.AsComponent: TComponent;
begin
  Result := Self;
end;

procedure TCustomUMLCMsgTreeContainer.SendMessage
  (const AMsgRec: TUMLCMessageParamsRecord);
var I, ALast: Integer; EachClient: IUMLCMessageClient;
begin
  if (Clients <> nil) then
  begin
    ALast := (Clients.Count - 1);
    if (HasClients()) then
    begin
      for I := 0 to ALast do
      begin
        EachClient := Clients[i];
        EachClient.AnswerMessage(AMsgRec);
      end;
    end;
  end;
  // Goal: Notifies clients that server has changed.
  // Objetivo: Notificar a los clientes que el servidor ha cambiado.
end;

procedure TCustomUMLCMsgTreeContainer.SendMessageSingle
  (const AClient: IUMLCMessageClient; const AMsgRec: TUMLCMessageParamsRecord);
begin
  if (AClient <> nil) then
  begin
    AClient.AnswerMessage(AMsgRec);
  end;
end;

function TCustomUMLCMsgTreeContainer.ClientsCount(): Integer;
begin
  Result := Clients.Count;
end;

function TCustomUMLCMsgTreeContainer.HasClients(): Boolean;
begin
  Result := (Clients.Count > 0);
end;

end.

