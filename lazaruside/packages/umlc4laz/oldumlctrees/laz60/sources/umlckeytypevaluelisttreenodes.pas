(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlckeytypevaluelisttreenodes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  //umlclists,
  umlctreenodes,
  umlconlystringkeytypevaluelists,
  dummy;

(**
 ** Description:
 ** This unit contains several in order to support an non visual,
 ** hierarchical ( "tree" ) collection of data.
 **
 ** It was not designed with generics collections.
 **
 ** Additionally, a "KeyTypeValueList" property and helper functions are provided,
 ** Note: Its a list of "KeyTypeValue" items, property,
 ** not a "KeyTypeValue" property, itself.
 **)

type

(* TUMLCKeyTypeValueListTreeNode *)

  TUMLCKeyTypeValueListTreeNode = class(TUMLCTreeNode)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    FKeyTypeValueList : TUMLCOnlyStringKeyTypeValueList;
  public
    (* Public declarations *)

    procedure DoCreate(); override;
    procedure DoDestroy(); override;

    function KeyTypeValueList(): TUMLCOnlyStringKeyTypeValueList;
  end;

(* TUMLCKeyTypeValueListTreeCollection *)

  TUMLCKeyTypeValueListTreeCollection = class(TUMLCTreeCollection)
  private
    (* Private declarations *)

  protected
    (* Protected declarations *)

    function CreateNodeByClass(): TUMLCTreeNode; override;
  public
    (* Public declarations *)
  end;

implementation

(* TUMLCKeyTypeValueListTreeNode *)

procedure TUMLCKeyTypeValueListTreeNode.DoCreate();
begin
  inherited DoCreate();

  FKeyTypeValueList := TUMLCOnlyStringKeyTypeValueList.Create();
end;

procedure TUMLCKeyTypeValueListTreeNode.DoDestroy();
begin
  FKeyTypeValueList.Empty();
  FKeyTypeValueList.Free();

  inherited DoDestroy();
end;

// "read-only"
function TUMLCKeyTypeValueListTreeNode.KeyTypeValueList(): TUMLCOnlyStringKeyTypeValueList;
begin
  Result := Self.FKeyTypeValueList;
end;

(* TUMLCKeyTypeValueListTreeCollection *)

function TUMLCKeyTypeValueListTreeCollection.CreateNodeByClass(): TUMLCTreeNode;
begin
  Result := TUMLCKeyTypeValueListTreeNode.Create();
  Result.DoCreate();
end;

end.

