(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsint8s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 8 bits signed integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint8s : TUMLCModule =
   ($B3,$74,$8F,$DA,$E3,$DC,$EC,$44,$8F,$EB,$EC,$DB,$52,$54,$40,$7F);

// ---

(* global functions *)




(* global procedures *)



(* global operators *)

  procedure Assign
    (out   ADest:   umlcsint_8;
     const ASource: umlcsint_8); overload; // operator :=

  function Equal
    (const A, B: umlcsint_8): Boolean; overload; // operator =
  function Different
    (const A, B: umlcsint_8): Boolean; overload; // operator <>
  function Greater
    (const A, B: umlcsint_8): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcsint_8): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcsint_8): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcsint_8): Boolean; overload; // operator <=


implementation

(* global functions *)




(* global procedures *)



(* global operators *)

procedure Assign
  (out   ADest:   umlcsint_8;
   const ASource: umlcsint_8);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcsint_8): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcsint_8): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: umlcsint_8): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcsint_8): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcsint_8): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcsint_8): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;





end.

