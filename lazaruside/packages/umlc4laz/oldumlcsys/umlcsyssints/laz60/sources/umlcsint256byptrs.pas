unit umlcsint256byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint256ptrs : TUMLCModule =
   ($CE,$DF,$8F,$5F,$43,$15,$8F,$48,$BB,$C0,$7E,$51,$DC,$D1,$0F,$0B);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcsint_256): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_256): pointer;
var P: umlcpsint_256;
begin
  System.GetMem(P, sizeof(umlcsint_256));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcsint_256));
end;



end.

