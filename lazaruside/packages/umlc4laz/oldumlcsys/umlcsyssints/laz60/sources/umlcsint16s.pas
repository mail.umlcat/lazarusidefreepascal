unit umlcsint16s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 16 bits signed integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint16s : TUMLCModule =
   ($62,$86,$A2,$6D,$99,$9E,$11,$4C,$B2,$D3,$E9,$56,$A5,$27,$2D,$49);

 // ---

 (* global functions *)




 (* global procedures *)



 (* global operators *)

   procedure Assign
     (out   ADest:   umlcsint_16;
      const ASource: umlcsint_16); overload; // operator :=

   function Equal
     (const A, B: umlcsint_16): Boolean; overload; // operator =
   function Different
     (const A, B: umlcsint_16): Boolean; overload; // operator <>
   function Greater
     (const A, B: umlcsint_16): Boolean; overload; // operator >
   function Lesser
     (const A, B: umlcsint_16): Boolean; overload; // operator <
   function GreaterEqual
     (const A, B: umlcsint_16): Boolean; overload; // operator >=
   function LesserEqual
     (const A, B: umlcsint_16): Boolean; overload; // operator <=


implementation

(* global functions *)




(* global procedures *)


(* global operators *)

procedure Assign
  (out   ADest:   umlcsint_16;
   const ASource: umlcsint_16);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcsint_16): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcsint_16): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: umlcsint_16): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcsint_16): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcsint_16): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcsint_16): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

end.

