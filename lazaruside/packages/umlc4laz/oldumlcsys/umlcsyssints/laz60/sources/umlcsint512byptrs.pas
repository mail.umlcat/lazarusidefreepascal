unit umlcsint512byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint512ptrs : TUMLCModule =
   ($A1,$53,$B7,$8C,$B8,$9B,$38,$4B,$A1,$3F,$BB,$BE,$D7,$C0,$96,$0A);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcsint_512): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_512): pointer;
var P: umlcpsint_512;
begin
  System.GetMem(P, sizeof(umlcsint_512));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcsint_512));
end;



end.

