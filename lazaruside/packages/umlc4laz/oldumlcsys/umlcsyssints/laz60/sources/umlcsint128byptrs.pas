unit umlcsint128byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcsinttypes,
  dummy;

// ---

const

 MOD_umlcsint128ptrs : TUMLCModule =
   ($3D,$2C,$CF,$66,$DC,$6D,$E0,$4E,$B7,$7E,$29,$E5,$58,$82,$EA,$35);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcsint_128): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcsint_128): pointer;
var P: umlcpsint_128;
begin
  System.GetMem(P, sizeof(umlcsint_128));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcsint_128));
end;



end.

