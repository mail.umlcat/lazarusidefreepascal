{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsyssints60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcsyssints_rtti, umlcintbyptrs, umlcsint8byptrs, umlcsint8s, 
  umlcsint16byptrs, umlcsint16s, umlcsint32byptrs, umlcsint32s, 
  umlcsint64byptrs, umlcsint64s, umlcsint128byptrs, umlcsint128s, 
  umlcsint256byptrs, umlcsint256s, umlcsint512byptrs, umlcsint512s, 
  umlcsint8buffers;

implementation

end.
