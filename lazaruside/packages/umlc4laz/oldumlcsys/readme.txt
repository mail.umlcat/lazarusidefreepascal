readme.txt
==========

The "umlcsys" folder is part of the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This folder contains several packages for defining types and operations,
as an extension for the (FreePascal / Delphi) predefined type system.

