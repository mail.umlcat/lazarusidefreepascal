unit umlcstdtimestampsbyptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement a Timestamp type and it's operations,
 ** by using pointers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstddatetimetypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcstdtimestampsbyptrs : TUMLCModule =
   ($7D,$D1,$73,$A2,$26,$58,$2B,$4A,$94,$A6,$53,$5A,$B0,$CD,$CC,$F5);

// ---

(* global functions *)

function ConstToPtr
 (const AValue: umlcstdtimestamp): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

function IsEmpty
 (const ASource: pointer): Boolean; overload;

(* global procedures *)

procedure Clear
 (out ADest: pointer); overload;


implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcstdtimestamp): pointer;
var P: umlcpstdtimestamp;
begin
  System.GetMem(P, sizeof(umlcstdtimestamp));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcstdtimestamp));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcstdtimestamp), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;



end.

