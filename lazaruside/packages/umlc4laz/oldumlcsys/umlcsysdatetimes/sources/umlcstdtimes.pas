(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdtimes;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlccomparisons,
  umlcstddatetimetypes,
  dummy;

// ---

const

 MOD_umlcsystimes : TUMLCModule =
   ($71,$BA,$84,$9D,$41,$9F,$3A,$40,$88,$C1,$2C,$E0,$BC,$97,$9D,$19);

// ---

(* global functions *)

function TimeToDateTime
  (const AValue: umlcstdtime): umlcdatetime;
function DateTimeToTime
  (const AValue: umlcdatetime): umlcstdtime;

function TryEncodeTime
  (out AValue: umlcstdtime; const AHour, AMin, ASec, AMSec: Word): Boolean;
function EncodeTime
  (const AHour, AMin, ASec, AMSec: Word): umlcstdtime;

(* global procedures *)

procedure DecodeTime
  (const AValue: umlcstdtime;
   out   AHour:  Word;
   out   AMin:   Word;
   out   ASec:   Word;
   out   AMSec:  Word);

implementation

(* global functions *)

function TimeToDateTime
  (const AValue: umlcstdtime): TDateTime;
var Temp: umlctimestamp;
begin
  Temp.Date := DateDelta;
  //Temp.Time := LongInt(AValue);
  Result :=
    SysUtils.TimeStampToDateTime(Temp);
  // Goal: Used internally to return a datetime value from a time value.
  // Objetivo: Usado internamente para regresar un valor fechatiempo de un valor tiempo.
end;

function DateTimeToTime
  (const AValue: umlcdatetime): umlcstdtime;
begin
  Result :=
    SysUtils.DateTimeToTimeStamp(AValue).Time;
  // Goal: Used internally to return a time value from a datetime value.
  // Objetivo: Usado internamente para regresar un valor tiempo de un valor fechatiempo.
end;

function TryEncodeTime
  (out AValue: umlcstdtime; const AHour, AMin, ASec, AMSec: Word): Boolean;
var Temp: umlcdatetime;
begin
  umlcdebug.DebugWriteLn('TryEncodeTime');

  Result := TRUE;
  Temp := System.TDateTime(NoDateTime);
  if ((AHour < 24) and (AMin < 60) and (ASec < 60) and (AMSec < 1000)) then
  begin
    Temp :=
      SysUtils.EncodeTime(AHour, AMin, ASec, AMSec);

    //Temp :=
    //  ((AHour * 3600000) +
    //   (AMin * 60000) +
    //   (ASec * 1000) + AMSec);
    //
    //umlcdebug.DebugWriteLn('AHour: [' + IntToStr(AHour) + ']');
    //umlcdebug.DebugWriteLn('AMin: [' + IntToStr(AMin) + ']');
    //umlcdebug.DebugWriteLn('ASec: [' + IntToStr(ASec) + ']');
    //umlcdebug.DebugWriteLn('Temp: [' + FloatToStr(Temp) + ']');
    //umlcdebug.DebugWriteEoLn();
    //
    //Temp :=
    //  (Temp / SysUtils.MSecsPerDay);
    //
    //umlcdebug.DebugWriteLn('Temp: [' + FloatToStr(Temp) + ']');
    //umlcdebug.DebugWriteEoLn();
  end;

  AValue := Temp

  //AValue :=
  //  umlcstdtimes.DateTimeToTime(Temp);
  // Goal: Groups hours, minutes, seconds & milliseconds into
  // a complete time value.

  // Objetivo: Agrupa horas, minutos, segundos y milisegundos en
  // un valor tiempo completo.
end;

function EncodeTime
  (const AHour, AMin, ASec, AMSec: Word): umlcstdtime;
begin
  if (not umlcstdtimes.TryEncodeTime
    (Result, AHour, AMin, ASec, AMSec)) then
  begin
    // raise error
  end;
  // Goal: Groups hours, minutes, seconds & milliseconds into
  // a complete time value.

  // Objetivo: Agrupa horas, minutos, segundos y milisegundos en
  // un valor tiempo completo.
end;

(* global procedures *)

procedure DecodeTime
  (const AValue: umlcstdtime;
   out   AHour:  Word;
   out   AMin:   Word;
   out   ASec:   Word;
   out   AMSec:  Word);
var ATimeValue: Double;
begin
  //umlcdebug.DebugWriteLn('DecodeTime');
  //
  //umlcdebug.DebugWriteLn('AValue: [' + FloatToStr(AValue) + ']');
  //umlcdebug.DebugWriteEoLn();

  SysUtils.DecodeTime
    (AValue, AHour, AMin, ASec, AMSec);

  //ATimeValue :=
  //  (AValue * SysUtils.MSecsPerDay);
  //
  //umlcdebug.DebugWriteLn('ATimeValue: [' + FloatToStr(ATimeValue) + ']');
  //umlcdebug.DebugWriteEoLn();
  //
  //AHour := Trunc(ATimeValue / 3600000);
  //AMin  := Trunc(ATimeValue / 60000);
  //ASec  := Trunc(ATimeValue / 1000);
  //AMSec := Trunc((AHour + AMin + ASec) - ATimeValue);


end;


end.

