unit umlcstddatetimesbyptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement a Date and Time type and it's operations,
 ** by using pointers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstddatetimetypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcstddatetimesbyptrs : TUMLCModule =
   ($F5,$56,$D6,$1F,$70,$F2,$E1,$47,$AF,$07,$32,$93,$1C,$B6,$58,$E4);

// ---

(* global functions *)

function ConstToPtr
  (const AValue: umlcstddatetime): pointer;

procedure DropPtr
  (var ADestPtr: pointer);

function IsEmpty
  (const ASource: pointer): Boolean; overload;

(* global procedures *)

procedure Clear
  (out ADest: pointer); overload;

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcstddatetime): pointer;
var P: umlcpstddatetime;
begin
  System.GetMem(P, sizeof(umlcstddatetime));
  P^ := AValue;
  Result := pointer(P);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcstddatetime));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(umlcstddatetime), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;


end.

