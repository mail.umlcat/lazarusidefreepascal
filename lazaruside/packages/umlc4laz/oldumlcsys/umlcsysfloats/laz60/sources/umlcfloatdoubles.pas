(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatdoubles;

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  dummy;

// ---

const

 MOD_umlcfloat_doubles : TUMLCModule =
   ($1E,$4A,$26,$F1,$7B,$F8,$9D,$4F,$96,$81,$73,$B1,$48,$A3,$D4,$C5);

// ---

(* global standard functions *)

  function IsEmpty
    (const ASource: umlcfloat_double): Boolean; overload;

(* global standard procedures *)

  procedure Clear
    (out ADest: umlcfloat_double); overload;

(* global operators *)

  procedure Assign
    (out   ADest:   umlcfloat_double;
     const ASource: umlcfloat_double); overload; // operator :=

  function Compare
    (const A, B: umlcfloat_double): umlctcomparison;

  function Different
    (const A, B: umlcfloat_double): Boolean; overload; // operator <>
  function Equal
    (const A, B: umlcfloat_double): Boolean; overload; // operator =

  function Greater
    (const A, B: umlcfloat_double): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcfloat_double): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcfloat_double): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcfloat_double): Boolean; overload; // operator <=

implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcfloat_double): Boolean;
begin
  Result :=
    (ASource = 0);
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcfloat_double);
begin
  System.FillByte(ADest, sizeof(umlcfloat_double), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcfloat_double;
   const ASource: umlcfloat_double);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcfloat_double): umlccomparison;
begin
  Result := cmpEqual;
  if (A < B)
    then Result := cmpLower
  else if (A > B)
    then Result := cmpHigher;
end;

function Different
  (const A, B: umlcfloat_double): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Equal
  (const A, B: umlcfloat_double): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: umlcfloat_double): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcfloat_double): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcfloat_double): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcfloat_double): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;

end.


