(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsysfloats_rtti;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules,
  dummy;


// ---

const

  MOD_runumlcfloats_rtti : TUMLCModule =
    ($19,$2E,$69,$2C,$C6,$1A,$C3,$41,$BE,$12,$18,$53,$F7,$08,$E4,$BE);

// ---


implementation

end.

