(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatreals;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  dummy;

// ---

const

 MOD_umlcfloat_reals : TUMLCModule =
   ($B2,$76,$01,$51,$34,$93,$A1,$4E,$81,$74,$89,$A9,$8F,$BF,$89,$09);

 // ---

 (* global standard functions *)

   function IsEmpty
     (const ASource: umlcfloat_real): Boolean; overload;

 (* global standard procedures *)

   procedure Clear
     (out ADest: umlcfloat_real); overload;

(* global operators *)

  procedure Assign
    (out   ADest:   umlcfloat_real;
     const ASource: umlcfloat_real); overload; // operator :=

  function Compare
    (const A, B: umlcfloat_real): umlctcomparison;

  function Different
    (const A, B: umlcfloat_real): Boolean; overload; // operator <>
  function Equal
    (const A, B: umlcfloat_real): Boolean; overload; // operator =

  function Greater
    (const A, B: umlcfloat_real): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcfloat_real): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcfloat_real): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcfloat_real): Boolean; overload; // operator <=


implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcfloat_real): Boolean;
begin
  Result :=
    (ASource = 0);
end;



(* global standard procedures *)

procedure Clear
  (out ADest: umlcfloat_real);
begin
  System.FillByte(ADest, sizeof(umlcfloat_real), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcfloat_real;
   const ASource: umlcfloat_real);
begin
  ADest := ASource;
end;

function Compare
  (const A, B: umlcfloat_real): umlccomparison;
begin
  Result := cmpEqual;
  if (A < B)
    then Result := cmpLower
  else if (A > B)
    then Result := cmpHigher;
end;

function Different
  (const A, B: umlcfloat_real): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Equal
  (const A, B: umlcfloat_real): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Greater
  (const A, B: umlcfloat_real): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcfloat_real): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcfloat_real): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcfloat_real): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;


end.

