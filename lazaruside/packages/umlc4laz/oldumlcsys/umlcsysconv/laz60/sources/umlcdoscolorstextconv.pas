(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdoscolorstextconv;

{$mode objfpc}{$H+}

interface
uses
{$IFDEF MSWINDOWS}
  Graphics,
{$ENDIF}
{$IFDEF LINUX}
  QGraphics,
{$ENDIF}
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlcresdoscolors,
  umlcdoscolors,
  dummy;

// ---

const

  MOD_umlcdoscolorstextconv : TUMLCModule =
   ($13,$A4,$89,$6F,$71,$D9,$13,$4A,$A0,$74,$28,$FF,$06,$2C,$F3,$9B);

// ---

const
  strdosclBlack         = 'dosclBlack';
  strdosclBlue          = 'dosclBlue';
  strdosclGreen         = 'dosclGreen';
  strdosclCyan          = 'dosclCyan';
  strdosclRed           = 'dosclRed';
  strdosclMagenta       = 'dosclMagenta';
  strdosclBrown         = 'dosclBrown';
  strdosclLightGray     = 'dosclLightGray';
  strdosclDarkGray      = 'dosclDarkGray';
  strdosclLightBlue     = 'dosclLightBlue';
  strdosclLightGreen    = 'dosclLightGreen';
  strdosclLightCyan     = 'dosclLightCyan';
  strdosclLightRed      = 'dosclLightRed';
  strdosclLightMagenta  = 'dosclLightMagenta';
  strdosclYellow        = 'dosclYellow';
  strdosclWhite         = 'dosclWhite';

  function DOSColorToStr
    (const AValue: doscolor): string;
  function DOSColorToText
    (const AValue: doscolor): string;

  function StrToDOSColor
    (const AValue: string): doscolor;
  function TextToDOSColor
    (const AValue: string): doscolor;

  function ColorToStr
    (const AValue: TColor): string;
  function StrToColor
    (const AValue: string): TColor;

implementation

type
  TDOSColorNames = array[doscolor] of string;

const
  DOSColorToStrArray: TDOSColorNames =
   ( strdosclBlack,    strdosclBlue,         strdosclGreen,
     strdosclCyan,     strdosclRed,          strdosclMagenta,
     strdosclBrown,    strdosclLightGray,    strdosclDarkGray,
     strdosclBlue,     strdosclLightGreen,   strdosclLightCyan,
     strdosclLightRed, strdosclLightMagenta, strdosclYellow,
     strdosclWhite );

const
  DOSColorToTextArray: TDOSColorNames =
   ( textdosclBlack,    textdosclBlue,         textdosclGreen,
     textdosclCyan,     textdosclRed,          textdosclMagenta,
     textdosclBrown,    textdosclLightGray,    textdosclDarkGray,
     textdosclBlue,     textdosclLightGreen,   textdosclLightCyan,
     textdosclLightRed, textdosclLightMagenta, textdosclYellow,
     textdosclWhite );

function DOSColorToStr
  (const AValue: doscolor): string;
begin
  Result := DOSColorToStrArray[AValue];
  // Goal: To cast a "doscolor" AValue to a string.
  // Objetivo: Convertir un valor "doscolor" a un valor "string".
end;

function DOSColorToText
  (const AValue: doscolor): string;
begin
  Result := DOSColorToTextArray[AValue];
  // Goal: To cast a "doscolor" AValue to a readable text.
  // Objetivo: Convertir un valor "doscolor" a un texto legible.
end;

function MatchDOSColor
  (const AValue: string; const DOSColorNames: TDOSColorNames): Tdoscolor;
var i: doscolor; Found: Boolean;
begin
  i := Low(doscolor); Found := FALSE;
  while (i <= High(Tdoscolor)) and (not Found) do
  begin
    Found := SameText(DOSColorNames[i], AValue);
    Inc(i);
  end;

  if (Found)
    then Result := Pred(i)
    else Result := Low(doscolor);
  // Goal: Locates a doscolor by its name in a given array.
end;

function StrToDOSColor
  (const AValue: string): doscolor;
begin
  Result := MatchDOSColor(AValue, DOSColorToStrArray);
  // Goal: To cast a "string" AValue to a "doscolor" AValue.
  // Objetivo: Convertir un valor "string" a un valor "doscolor".
end;

function TextToDOSColor
  (const AValue: string): doscolor;
begin
  Result := MatchDOSColor(AValue, DOSColorToTextArray);
  // Goal: To cast a readable text to a "doscolor" AValue.
  // Objetivo: Convertir un texto legible a un valor "doscolor".
end;

function ColorToStr
  (const AValue: TColor): string;
begin
  Result := DOSColorToStr( ColorToDOSColor(AValue) );
  // Goal: To cast a "TColor" AValue to a string.
  // Objetivo: Convertir un valor "TColor" a un valor "string".
end;

function StrToColor
  (const AValue: string): TColor;
begin
  Result := DOSColorToColor( StrToDOSColor(AValue) );
  // Goal: To cast a "string" AValue to a "TColor" AValue.
  // Objetivo: Convertir un valor "string" a un valor "TColor".
end;

end.

