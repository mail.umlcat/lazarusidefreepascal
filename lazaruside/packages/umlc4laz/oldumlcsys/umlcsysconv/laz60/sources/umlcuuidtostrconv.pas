(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuuidtostrconv;

{$mode objfpc}{$H+}

interface
uses
  Classes,
  SysUtils,
  umlcmodules, umlctypes,
  umlcuuids,
  umlchexas,
  umlcansicharsets,
  umlcansicharsetconsts,
  umlcmaskarrays,
  dummy;

// ---

const

 MOD_umlcuuidtostrconv : TUMLCModule =
   ($ED,$6A,$62,$19,$0A,$4A,$AF,$4A,$95,$35,$CE,$F6,$DE,$46,$09,$11);

// ---

(**
 ** Description:
 ** This unit supports text functions for
 ** Global / Universal Unique Identifiers.
 **
 ** There are several string representations,
 ** all based on hexadecimal digits. The "hyphen" character,
 ** or "minus" operator characters are used as separators,
 ** and "curly brackets" character as delimiters.
 **
 ** - Single.
 ** 32 characters. Only hexadecimal digits,
 ** without separators, or delimiters.
 ** Example: "21EC20203AEA1069A2DD08002B30309D"
 **
 ** - Double
 ** 37 characters. Hexadecimal digits, with separators, and delimiters.
 ** The digits are grouped in groups of 8 digits, each.
 ** Example: "{21EC2020-3AEA1069-A2DD0800-2B30309D}"
 **
 ** + Canonical / Long
 ** 38 characters. Hexadecimal digits, with separators, and delimiters.
 ** The digits are grouped as:
 ** - 8 digits,
 ** - 4 digits,
 ** - 4 digits,
 ** - 4 digits,
 ** - 12 digits
 ** Example: "{21EC2020-3AEA-1069-A2DD-08002B30309D}"
 **
 ** This is the most commonly used representation.
 **
 ** + Pascal Array
 ** It's a single dimension array constant in (Procedural) Pascal.
 **
 ** + C Array
 ** It's a single dimension array constant in (Plain / Pure) C.
 **)

 resourcestring
   resUUIDSingleStrOverflow = 'Single String UUID overflow';
   resUUIDDoubleStrOverflow = 'Double String UUID overflow';
   resUUIDLongStrOverflow   = 'Long String UUID overflow';

 (*
 resource
   resUUIDSingleStrOverflow: string = '';
   resUUIDDoubleStrOverflow: string = '';
   resUUIDLongStrOverflow:   string = '';
 *)

const
  SingleUUIDStrMaxSize = 32;
  DoubleUUIDStrMaxSize = 37;
  LongUUIDStrMaxSize   = 38;
  PascalArrayUUIDStrMaxSize = 66;
  PlainCArrayUUIDStrMaxSize = 82;

 (* String types *)

 type
   TSingleUUIDStr = string[SingleUUIDStrMaxSize];
 type
   PSingleUUIDStr = ^TSingleUUIDStr;

 type
   TDoubleUUIDStr = string[DoubleUUIDStrMaxSize];
 type
   PDoubleUUIDStr = ^TDoubleUUIDStr;

 type
   TLongUUIDStr = string[LongUUIDStrMaxSize];
 type
   PLongUUIDStr = ^TLongUUIDStr;

 (* Array types *)

 type
   TSingleUUIDArray = array[0 .. 31] of ansichar;
 type
   PSingleUUIDArray = ^TSingleUUIDArray;
 type
   TDoubleUUIDArray = array[0 .. 36] of ansichar;
 type
   PDoubleUUIDArray = ^TDoubleUUIDArray;
 type
   TLongUUIDArray = array[0 .. 37] of ansichar;
 type
   PLongUUIDArray = ^TLongUUIDArray;

   (* Other types *)

 type
   EUUIDOverflow = exception;

  (* Global Functions *)

  function IndexNonSingleUUIDStr
    (const ASource: shortstring): Integer;
  function IndexNonDoubleUUIDStr
    (const ASource: shortstring): Integer;
  function IndexNonLongUUIDStr
    (const ASource: shortstring): Integer;

  function IndexNonSingleUUIDAnsiCharPtr
    (const ASource: PChar): Integer;
  function IndexNonDoubleUUIDAnsiCharPtr
    (const ASource: PChar): Integer;
  function IndexNonLongUUIDAnsiCharPtr
    (const ASource: PChar): Integer;

  function IsSingleUUIDStr
    (const ASource: TSingleUUIDStr): Boolean;
  function IsDoubleUUIDStr
    (const ASource: TDoubleUUIDStr): Boolean;
  function IsLongUUIDStr
    (const ASource: TLongUUIDStr): Boolean;

  function IsSingleUUIDAnsiCharPtr
    (const ASource: PChar): Boolean;
  function IsDoubleUUIDAnsiCharPtr
    (const ASource: PChar): Boolean;
  function IsLongUUIDAnsiCharPtr
    (const ASource: PChar): Boolean;

  function TrySingleStrToUUID
    (const ASource: TSingleUUIDStr; var ADest: TUUID): Boolean;
  function TryDoubleStrToUUID
    (const ASource: TDoubleUUIDStr; var ADest: TUUID): Boolean;
  function TryLongStrToUUID
    (const ASource: TLongUUIDStr; var ADest: TUUID): Boolean;

  function TrySingleStrToUUIDAnsiCharPtr
    (const ASource: PChar; var ADest: TUUID): Boolean;
  function TryDoubleStrToUUIDAnsiCharPtr
    (const ASource: PChar; var ADest: TUUID): Boolean;
  function TryLongStrToUUIDAnsiCharPtr
    (const ASource: PChar; var ADest: TUUID): Boolean;

  procedure SingleStrToUUID
    (const ASource: TSingleUUIDStr; var ADest: TUUID);
  procedure DoubleStrToUUID
    (const ASource: TSingleUUIDStr; var ADest: TUUID);
  procedure LongStrToUUID
    (const ASource: TLongUUIDStr; var ADest: TUUID);

  procedure SingleStrToUUIDAnsiCharPtr
    (const ASource: PChar; var ADest: TUUID);
  procedure DoubleStrToUUIDAnsiCharPtr
    (const ASource: PChar; var ADest: TUUID);
  procedure LongStrToUUIDAnsiCharPtr
    (const ASource: PChar; var ADest: TUUID);

  procedure ClearSingleUUIDStr
    (out ADest: TSingleUUIDStr);

  procedure UUIDToSingleStr
    (const ASource: TUUID; out ADest: TSingleUUIDStr);
  procedure UUIDToDoubleStr
    (const ASource: TUUID; out ADest: TDoubleUUIDStr);
  procedure UUIDToLongStr
    (const ASource: TUUID; var ADest: TLongUUIDStr);

  procedure UUIDToSingleAnsiCharPtr
    (const ASource: TUUID; var ADest: PChar);
  procedure UUIDToDoubleAnsiCharPtr
    (const ASource: TUUID; var ADest: PChar);
  procedure UUIDToLongAnsiCharPtr
    (const ASource: TUUID; var ADest: PChar);

  procedure UUIDToPascalByteArray
    (const ASource: TUUID; var ADest: ansistring);
  procedure UUIDToPlainCByteArray
    (const ASource: TUUID; var ADest: ansistring);

implementation

function IndexNonSingleUUIDStr
  (const ASource: shortstring): Integer;
var ALen: Integer; AMask: PMaskArray;
begin
  Result := -1;

  ALen := Length(ASource);
  if (ALen = SingleUUIDStrMaxSize) then
  begin
    Result := IndexNonHexShortStr(ASource, ALen);
  end;
  // Goal: Returns the index of the first character,
  // in the given parameter, that doesn't match
  // the required mask or pattern,
  // of the indicated UUID, string format.
  // If all characters match,
  // then, "-1" is returned.
end;

function IndexNonDoubleUUIDStr
  (const ASource: shortstring): Integer;
var ALen: Integer; AMask: PMaskArray;
begin
  Result := -1;

  ALen := Length(ASource);
  if (ALen = DoubleUUIDStrMaxSize) then
  begin
    AMask := NewClearMaskArray(ALen);
    // indicate all characters are hexadecimal digits
    FillMaskArray(AMask, ALen);

    // mark skip initial delimiter
    MaskArraySetAt(AMask, ALen, 0, false);

    // mark skip first separator
    MaskArraySetAt(AMask, ALen, 9, false);

    // mark skip second separator
    MaskArraySetAt(AMask, ALen, 18, false);

    // mark skip third separator
    MaskArraySetAt(AMask, ALen, 27, false);

    // mark skip final delimiter
    MaskArraySetAt(AMask, ALen, (ALen - 1), false);

    // compare only the characters indicated by the mask array
    Result := umlchexas.IndexNonHexaStrByMask(ASource, AMask, ALen);
  end;
  // Goal: Returns the index of the first character,
  // in the given parameter, that doesn't match
  // the required mask or pattern,
  // of the indicated UUID, string format.
  // If all characters match,
  // then, "-1" is returned.
end;

function IndexNonLongUUIDStr
  (const ASource: shortstring): Integer;
var ALen: Integer; AMask: PMaskArray;
begin
  Result := -1;

  ALen := Length(ASource);
  if (ALen = DoubleUUIDStrMaxSize) then
  begin
    AMask := NewClearMaskArray(ALen);
    // indicate all characters are hexadecimal digits
    FillMaskArray(AMask, ALen);

    // mark skip initial delimiter
    MaskArraySetAt(AMask, ALen, 0, false);

    // mark skip first separator
    MaskArraySetAt(AMask, ALen, 9, false);

    // mark skip second separator
    MaskArraySetAt(AMask, ALen, 14, false);

    // mark skip third separator
    MaskArraySetAt(AMask, ALen, 19, false);

    // mark skip final delimiter
    MaskArraySetAt(AMask, ALen, (ALen - 1), false);

    // compare only the characters indicated by the mask array
    Result := umlchexas.IndexNonHexaStrByMask(ASource, AMask, ALen);
  end;
  // Goal: Returns the index of the first character,
  // in the given parameter, that doesn't match
  // the required mask or pattern,
  // of the indicated UUID, string format.
  // If all characters match,
  // then, "-1" is returned.
end;

function IndexNonSingleUUIDAnsiCharPtr(const ASource: PChar): Integer;
begin
  Result := -1;
  // Goal: Returns the index of the first character,
  // in the given parameter, that doesn't match
  // the required mask or pattern,
  // of the indicated UUID, string format.
  // If all characters match,
  // then, "-1" is returned.
end;

function IndexNonDoubleUUIDAnsiCharPtr(const ASource: PChar): Integer;
begin
  Result := -1;
  // Goal: Returns the index of the first character,
  // in the given parameter, that doesn't match
  // the required mask or pattern,
  // of the indicated UUID, string format.
  // If all characters match,
  // then, "-1" is returned.
end;

function IndexNonLongUUIDAnsiCharPtr(const ASource: PChar): Integer;
begin
  Result := -1;
  // Goal: Returns the index of the first character,
  // in the given parameter, that doesn't match
  // the required mask or pattern,
  // of the indicated UUID, string format.
  // If all characters match,
  // then, "-1" is returned.
end;

function IsSingleUUIDStr
  (const ASource: TSingleUUIDStr): Boolean;
begin
  Result := (IndexNonSingleUUIDStr(ASource) = -1);
end;

function IsDoubleUUIDStr
  (const ASource: TDoubleUUIDStr): Boolean;
begin
  Result := (IndexNonDoubleUUIDStr(ASource) = -1);
end;

function IsLongUUIDStr(const ASource: TLongUUIDStr): Boolean;
begin
  Result := (IndexNonLongUUIDStr(ASource) = -1);
end;

function IsSingleUUIDAnsiCharPtr(const ASource: PChar): Boolean;
begin
  Result := (IndexNonSingleUUIDAnsiCharPtr(ASource) = -1);
end;

function IsDoubleUUIDAnsiCharPtr(const ASource: PChar): Boolean;
begin
  Result := (IndexNonDoubleUUIDAnsiCharPtr(ASource) = -1);
end;

function IsLongUUIDAnsiCharPtr(const ASource: PChar): Boolean;
begin
  Result := (IndexNonLongUUIDAnsiCharPtr(ASource) = -1);
end;

function TrySingleStrToUUID
  (const ASource: TSingleUUIDStr; var ADest: TUUID): Boolean;
var S1, S2: PChar; D: PByte; ACount, K: Integer; S: ansistring;
begin
  Result := IsSingleUUIDStr(ASource);
  if (Result) then
  begin
    umlcuuids.ClearUUID(ADest);

    // --> prepare pointers
    D  := @ADest;
    // characters will be copied 2, at a time,
    // since each byte is converted as 2 hexadecimal digits
    S1 := @(ASource[1]);
    S2 := @(ASource[2]);

    ACount := 16;
    K  := 0;
    while (K < ACount) do
    begin
      // merge 2 digits into a byte
      S  := S1^ + S2^;
      D^ := HexToByte(S);

      Inc(D);

      Inc(S1, 2);
      Inc(S2, 2);

      Inc(K);
    end;
  end;
  // Goal: To convert a partial readeable text into a UUID type.
end;

function TryDoubleStrToUUID
  (const ASource: TDoubleUUIDStr; var ADest: TUUID): Boolean;
var S1, S2: PChar; D: PByte; ACount, I, J: Integer; S: ansistring;
    U: PUUID;
begin
  Result := false;
  Result := IsDoubleUUIDStr(ASource);
  if (Result) then
  begin
    ClearUUID(ADest);

    // --> prepare pointers
    D  := @ADest;
    U  := @ADest;
    // characters will be copied 2, at a time,
    // since each byte is converted as 2 hexadecimal digits
    S1 := @(ASource[2]);
    S2 := @(ASource[3]);
    // first character is skipped, is initial delimiter

    // each group
    for I := 1 to 4 do
    begin
      for J := 1 to 4 do
      begin
        // merge 2 digits into a byte
        S  := S1^ + S2^;
        D^ := HexToByte(S);

        Inc(D);

        Inc(S1, 2);
        Inc(S2, 2);
      end;

      if (I < 4) then
      begin
        // skip separator
        Inc(S1, 1);
        Inc(S2, 1);
      end;
    end;
  end;
  // Goal: To convert a full readeable text into a UUID type.
end;

function TryLongStrToUUID
  (const ASource: TLongUUIDStr; var ADest: TUUID): Boolean;
var S1, S2: PChar; D: PByte; ACount, I, J: Integer; S: ansistring;
    U: PUUID;
begin
  Result := false;
  Result := IsLongUUIDStr(ASource);
  if (Result) then
  begin
    ClearUUID(ADest);

    // --> prepare pointers
    D  := @ADest;
    U  := @ADest;
    // characters will be copied 2, at a time,
    // since each byte is converted as 2 hexadecimal digits
    S1 := @(ASource[2]);
    S2 := @(ASource[3]);
    // first character is skipped, is initial delimiter

    // first 4 bytes
    for I := 1 to 4 do
    begin
      // merge 2 digits into a byte
      S  := S1^ + S2^;
      D^ := HexToByte(S);

      Inc(D);

      Inc(S1, 2);
      Inc(S2, 2);
    end;

    // skip separator
    Inc(S1, 1);
    Inc(S2, 1);

    // next 3 groups
    for J := 1 to 3 do
    begin
      // 2 bytes each
      for I := 1 to 2 do
      begin
        // merge 2 digits into a byte
        S  := S1^ + S2^;
        D^ := HexToByte(S);

        Inc(D);

        Inc(S1, 2);
        Inc(S2, 2);

        // skip separator
        Inc(S1, 1);
        Inc(S2, 1);
      end;
    end;

    // last 6 bytes
    for I := 1 to 6 do
    begin
      // merge 2 digits into a byte
      S  := S1^ + S2^;
      D^ := HexToByte(S);

      Inc(D);

      Inc(S1, 2);
      Inc(S2, 2);
    end;
  end;
  // Goal: To convert a full readeable text into a UUID type.
end;

function TrySingleStrToUUIDAnsiCharPtr
  (const ASource: PChar; var ADest: TUUID): Boolean;
begin
  Result := false;
end;

function TryDoubleStrToUUIDAnsiCharPtr
  (const ASource: PChar; var ADest: TUUID): Boolean;
begin
  Result := false;
end;

function TryLongStrToUUIDAnsiCharPtr
  (const ASource: PChar; var ADest: TUUID): Boolean;
begin
  Result := false;
end;

procedure SingleStrToUUID(const ASource: TSingleUUIDStr; var ADest: TUUID);
begin
  if (not TrySingleStrToUUID(ASource, ADest)) then
  begin
     raise EUUIDOverflow.Create(resUUIDSingleStrOverflow);
  end;
  // Goal: To convert a partial readeable text into a UUID type.
end;

procedure DoubleStrToUUID(const ASource: TSingleUUIDStr; var ADest: TUUID);
begin
  if (not TryDoubleStrToUUID(ASource, ADest)) then
  begin
     raise EUUIDOverflow.Create(resUUIDDoubleStrOverflow);
  end;
  // Goal: To convert a full readeable text into a UUID type.
end;

procedure LongStrToUUID
  (const ASource: TLongUUIDStr; var ADest: TUUID);
begin
  if (not TryLongStrToUUID(ASource, ADest)) then
  begin
     raise EUUIDOverflow.Create(resUUIDLongStrOverflow);
  end;
  // Goal: To convert a full readeable text into a UUID type.
end;

procedure SingleStrToUUIDAnsiCharPtr
  (const ASource: PChar; var ADest: TUUID);
begin
  // ...
end;

procedure DoubleStrToUUIDAnsiCharPtr
  (const ASource: PChar; var ADest: TUUID);
begin
  // ...
end;

procedure LongStrToUUIDAnsiCharPtr
  (const ASource: PChar; var ADest: TUUID);
begin
  // ...
end;

procedure ClearSingleUUIDStr
  (out ADest: TSingleUUIDStr);
begin
  FillZeroShortStr(ADest, SizeOf(TSingleUUIDStr));
  // Goal: Returns a UUID null value with a short textual representation.
end;

procedure UUIDToSingleStr
  (const ASource: TUUID; out ADest: TSingleUUIDStr);
var S: shortstring;
    D1, D2: PChar; P: PByte; K, ACount: Integer;
begin
  // clear result
  FillZeroShortStr(ADest, SizeOf(TSingleUUIDStr));

  ACount := 16;

  // --> prepare pointers
  P  := @ASource;
  // characters will be copied 2, at a time,
  // since each byte is converted as 2 hexadecimal digits
  D1 := @(ADest[1]);
  D2 := @(ADest[2]);

  K  := 0;
  while (K < ACount) do
  begin
    S  := umlchexas.ByteToHexStr(P^);
    D1^ := S[1];
    D2^ := S[2];

    Inc(D1, 2);
    Inc(D2, 2);
    Inc(P);
    Inc(K);
  end;

  // ...
  SetLength(ADest, SingleUUIDStrMaxSize);
  // Goal: To convert a UUID type into a partial readeable text.
end;

procedure UUIDToDoubleStr
  (const ASource: TUUID; out ADest: TDoubleUUIDStr);
var S: shortstring; I, J: Integer;
    D1, D2: PChar; P: PByte; ACount: Integer;
begin
  // --> fill with '0' character
  FillZeroShortStr(ADest, SizeOf(TDoubleUUIDStr));

  // --> prepare pointers
  P  := @ASource;
  // characters will be copied 2, at a time,
  // since each byte is converted as 2 hexadecimal digits
  D1 := @(ADest[1]);
  D2 := @(ADest[2]);

  // --> add initial delimiter
  D1 := @(ADest[1]);
  D1^ := '{';
  Inc(D1, 1);
  Inc(D2, 1);

  // --> add groups
  for I := 1 to 4 do
  begin
    for J := 1 to 4 do
    begin
      S  := umlchexas.ByteToHexStr(P^);
      D1^ := S[1];
      D2^ := S[2];

      Inc(D1, 2);
      Inc(D2, 2);
      Inc(P);
    end;

    // add separator
    if (I < 4) then
    begin
      D1^ := '-';
      Inc(D1, 1);
      Inc(D2, 1);
    end;
  end;

  // --> add final delimiter
  D1^ := '}';

  // ...
  SetLength(ADest, DoubleUUIDStrMaxSize);

  // Goal: To convert a UUID type into a full readeable text.
end;

procedure UUIDToLongStr
  (const ASource: TUUID; var ADest: TLongUUIDStr);
var S: shortstring; I, J: Integer;
    D1, D2: PChar; P: PByte; ACount: Integer;
begin
  // --> fill with '0' character
  FillZeroShortStr(ADest, SizeOf(TLongUUIDStr));

  // --> prepare pointers
  P  := @ASource;
  // characters will be copied 2, at a time,
  // since each byte is converted as 2 hexadecimal digits
  D1 := @(ADest[1]);
  D2 := @(ADest[2]);

  // --> add initial delimiter
  D1 := @(ADest[1]);
  D1^ := '{';
  Inc(D1, 1);
  Inc(D2, 1);

  // --> add groups

  // first group of 4 bytes
  for I := 1 to 4 do
  begin
    S  := umlchexas.ByteToHexStr(P^);
    D1^ := S[1];
    D2^ := S[2];

    Inc(D1, 2);
    Inc(D2, 2);
    Inc(P);
  end;

  // add first group separator
  D1^ := '-';
  Inc(D1, 1);
  Inc(D2, 1);

  // 3 groups of 2 bytes
  for I := 1 to 3 do
  begin
    for J := 1 to 2 do
    begin
      S  := umlchexas.ByteToHexStr(P^);
      D1^ := S[1];
      D2^ := S[2];

      Inc(D1, 2);
      Inc(D2, 2);
      Inc(P);
    end;

    // add each group separator
    D1^ := '-';
    Inc(D1, 1);
    Inc(D2, 1);
  end;

  // last group of 6 bytes
  for I := 1 to 6 do
  begin
    S  := umlchexas.ByteToHexStr(P^);
    D1^ := S[1];
    D2^ := S[2];

    Inc(D1, 2);
    Inc(D2, 2);
    Inc(P);
  end;

  // --> add final delimiter
  D1^ := '}';

  // ...
  SetLength(ADest, LongUUIDStrMaxSize);
  // Goal: To convert a UUID type into a full readeable text.
end;

procedure UUIDToSingleAnsiCharPtr
  (const ASource: TUUID; var ADest: PChar);
begin
  // ...
end;

procedure UUIDToDoubleAnsiCharPtr
  (const ASource: TUUID; var ADest: PChar);
begin
  // ...
end;

procedure UUIDToLongAnsiCharPtr
  (const ASource: TUUID; var ADest: PChar);
begin
  // ...
end;

procedure UUIDToPascalByteArray
  (const ASource: TUUID; var ADest: ansistring);
var S: string; K: Integer;
    D1, D2, D3, D4: PChar; P: PByte; ACount: Integer;
begin
  // --> fill with '0' character
  ACount := PascalArrayUUIDStrMaxSize;
  FillZeroAnsiStr(ADest, ACount);

  // --> prepare pointers
  P  := @ASource;
  // characters will be copied 2, at a time,
  // since each byte is converted as 2 hexadecimal digits
  // additionally a third character, a colon, will be included
  // additionally a fourth character, a money sign, will be included
  D1 := @(ADest[1]);
  D2 := @(ADest[2]);
  D3 := @(ADest[3]);
  D4 := @(ADest[4]);

  // --> add initial delimiter
  D1 := @(ADest[1]);
  D1^ := '(';
  Inc(D1, 1);
  Inc(D2, 1);
  Inc(D3, 1);
  Inc(D4, 1);

  ACount := 16;
  K  := 0;
  while (K < ACount) do
  begin
    S  := umlchexas.ByteToHexStr(P^);
    D1^ := '$';
    D2^ := S[1];
    D3^ := S[2];

    if (K < (ACount - 1)) then
    begin
      D4^ := ',';
    end else
    begin
      D4^ := ')';
    end;

    Inc(D1, 4);
    Inc(D2, 4);
    Inc(D3, 4);
    Inc(D4, 4);

    Inc(P);
    Inc(K);
  end;

  // --> add final delimiter
  D1^ := ';';
end;

procedure UUIDToPlainCByteArray(const ASource: TUUID; var ADest: ansistring);
var S: string; K: Integer;
    P: PByte; ACount: Integer;
    D1, D2, D3, D4, D5: PChar;
begin
  // --> fill with '0' character
  ACount := PlainCArrayUUIDStrMaxSize;
  FillZeroAnsiStr(ADest, ACount);

  // --> prepare pointers
  P  := @ASource;
  // characters will be copied 2, at a time,
  // since each byte is converted as 2 hexadecimal digits
  D1 := @(ADest[1]); // "0"
  D2 := @(ADest[2]); // "x"
  D3 := @(ADest[3]); // 0 ... F
  D4 := @(ADest[4]); // 0 ... F
  D5 := @(ADest[5]); // ',' or ')'

  // --> add initial delimiter
  D1 := @(ADest[1]);
  D1^ := '{';
  Inc(D1, 1);
  Inc(D2, 1);
  Inc(D3, 1);
  Inc(D4, 1);
  Inc(D5, 1);

  ACount := 16;
  K  := 0;
  while (K < ACount) do
  begin
    S  := umlchexas.ByteToHexStr(P^);
    D1^ := '0';
    D2^ := 'x';
    D3^ := S[1];
    D4^ := S[2];

    if (K < (ACount - 1)) then
    begin
      D5^ := ',';
    end else
    begin
      D5^ := '}';
    end;

    Inc(D1, 5);
    Inc(D2, 5);
    Inc(D3, 5);
    Inc(D4, 5);
    Inc(D5, 5);

    Inc(P);
    Inc(K);
  end;

  // --> add final delimiter
  D1^ := ';';
end;

end.

