(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcfloatrealtostrconv;

{$mode objfpc}{$H+}

interface
uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdfloattypes,
  umlcfloatreals,
  umlcansimemos,
  umlcansicharsets,
  umlcansicharsetconsts,
  dummy;

// ---

const

 MOD_umlcfloat_realstextconv : TUMLCModule =
   ($B2,$76,$01,$51,$34,$93,$A1,$4E,$81,$74,$89,$A9,$8F,$BF,$89,$09);

// ---

(* global functions *)

  function StrToCurrDef
    (const AValue: string; const ADefValue: umlcfloat_currency): umlcfloat_currency;

  function AnsiMemoToCurrDef
    (const AValue: ansimemo; const ADefValue: umlcfloat_currency): umlcfloat_currency; overload;




implementation

(* global functions *)

function StrToCurrDef
  (const AValue: string; const ADefValue: umlcfloat_currency): umlcfloat_currency;
var i, l: byte; AnyError: Boolean;
begin
  I := 1;
  L :=
    System.Length(AValue);
  AnyError := L < 1;
  // Revisar longuitud primero

  if (L > 1) then
  while ((i<=L) and not (AnyError)) do
  begin
    AnyError :=
      not IsMember(CurrSet, AValue[i]);
    Inc(i);
  end;
  // Under construction

  if (AnyError)
    then Result := ADefValue
    else Result := SysUtils.StrToCurr(AValue);
  // Goal: Returns the currency value of "Value" with type protection.
  // Objetivo: Regresa el valor moneda de "Value" con proteccion a errores.
end;


function ansimemoToCurrDef
  (const AValue: ansimemo; const ADefValue: umlcfloat_currency): umlcfloat_currency;
begin
  Result :=
    StrToFloatDef(umlcansimemos.MemoToStr(AValue), ADefValue);
  // Goal: Returns the currency value of "Value" with type protection.
  // Objetivo: Regresa el valor moneda de "Value" con proteccion a errores.
end;


end.

