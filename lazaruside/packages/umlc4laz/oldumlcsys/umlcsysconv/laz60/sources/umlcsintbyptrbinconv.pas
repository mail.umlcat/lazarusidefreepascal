unit umlcsintbyptrbinconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcsinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcbins,
  dummy;

// ---

const

  MOD_umlcsintbyptrbinconv : TUMLCModule =
    ($27,$55,$9B,$73,$52,$C5,$84,$45,$8B,$6C,$1D,$D1,$67,$FB,$38,$54);

// ---

(* global functions *)



(* global procedures *)

procedure UIntPtrToBinStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatSInt8PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt16PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt32PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt64PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt128PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt256PtrToBinStr
  (out ADest: string; const ASource: pointer);

procedure ConcatSInt512PtrToBinStr
  (out ADest: string; const ASource: pointer);


implementation

(* global functions *)



(* global procedures *)

procedure UIntPtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_8(ASource);

    // ...
end;

procedure UInt16ToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_16(ASource);

  // ...
end;

procedure UInt32ToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('UInt32ToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 2;
      Remainder := Number mod 2;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure UInt64ToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_64(ASource);

  // ...
end;

procedure UInt128ToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_128(ASource);

  // ...
end;

procedure UInt256ToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_256(ASource);

  // ...
end;

procedure UInt512ToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_512(ASource);

  // ...
end;

procedure UIntToBinStr
  (out ADest: string; const ASource: pointer);
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Cardinal) = sizeof(umlcsint_32))
    then UInt32ToBinStr(ADest, ASource)

  //if (sizeof(Cardinal) = sizeof(umlcsint_8))
  //  then UInt8ToBinStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_16))
  //  then UInt16ToBinStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_32))
  //  then UInt32ToBinStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_64))
  //  then UInt64ToBinStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_128))
  //  then UInt128ToBinStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_256))
  //  then UInt256ToBinStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_512))
  //  then UInt512ToBinStr(ADest, ASource);
end;

// ---

procedure ConcatSInt8PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_8;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_8(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt8PtrToBinStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 2;
      Remainder := Number mod 2;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcbins.DecToBinChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt16PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_16;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_16(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt16PtrToBinStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 2;
      Remainder := Number mod 2;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcbins.DecToBinChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt32PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt32PtrToBinStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 2;
      Remainder := Number mod 2;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcbins.DecToBinChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt64PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_64;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_64(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt64PtrToBinStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 2;
      Remainder := Number mod 2;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcbins.DecToBinChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt128PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatSInt128PtrToBinStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_128);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcbins.ByteToBinStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt256PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatSInt256PtrToBinStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_256);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcbins.ByteToBinStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt512PtrToBinStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatSInt512PtrToBinStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_512);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcbins.ByteToBinStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;



end.

