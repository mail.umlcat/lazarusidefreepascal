unit umlcdecs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for decimal base representation numbers.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstdtypes, umlcconvtypes, umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  dummy;

// ---

const

 MOD_umlcdecs : TUMLCModule =
   ($07,$6E,$03,$17,$CD,$FE,$40,$40,$AC,$09,$78,$54,$DA,$81,$68,$C2);

// ---


function IsDecimalDigit
  (const AValue: ansichar): Boolean;

procedure ConcatCardinalToDecStr
  (out   ADestStr:     string;
   const ASrcCardinal: Cardinal);

procedure CardinalToDecStr
  (out   ADestStr:     string;
   const ASrcCardinal: Cardinal);

procedure ConcatIntegerToDecStr
  (out   ADestStr:     string;
   const ASrcInteger: Integer);

procedure IntegerToDecStr
  (out   ADestStr:    string;
   const ASrcInteger: Integer);

  function TryDecStrToUInt8
    (var ADest: umlcuint_8; const ASource: umlcdecstring): Boolean;
  function TryDecStrToUInt16
    (var ADest: umlcuint_16; const ASource: umlcdecstring): Boolean;
  function TryDecStrToUInt32
    (var ADest: umlcuint_32; const ASource: umlcdecstring): Boolean;
  function TryDecStrToUInt64
    (var ADest: umlcuint_64; const ASource: umlcdecstring): Boolean;

  function TryDecStrToUInt128
    (var ADest: umlcuint_128; const ASource: umlcdecstring): Boolean;
  function TryDecStrToUInt256
    (var ADest: umlcuint_256; const ASource: umlcdecstring): Boolean;
  function TryDecStrToUInt512
    (var ADest: umlcuint_512; const ASource: umlcdecstring): Boolean;

  procedure UInt8ToDecStr
    (var ADest: umlcdecstring; const ASource: umlcuint_8);
  procedure UInt16ToDecStr
    (var ADest: umlcdecstring; const ASource: umlcuint_16);
  procedure UInt32ToDecStr
    (var ADest: umlcdecstring; const ASource: umlcuint_32);
  procedure UInt64ToDecStr
    (var ADest: umlcdecstring; const ASource: umlcuint_64);

  procedure UInt128ToDecStr
    (var ADest: umlcdecstring; const ASource: umlcuint_128);
  procedure UInt256ToDecStr
    (var ADest: umlcdecstring; const ASource: umlcuint_256);
  procedure UInt512ToDecStr
    (var ADest: umlcdecstring; const ASource: umlcuint_512);

implementation

function IsDecimalDigit
  (const AValue: ansichar): Boolean;
begin
  Result :=
    ((AValue >= '0') or (AValue <= '9'));
end;

procedure ConcatCardinalToDecStr
  (out   ADestStr:     string;
   const ASrcCardinal: Cardinal);
var number: Byte;
    Count: Byte;
    i, j: Byte;
    Temp1, temp2: string;
begin
  //umlcdebug.DebugWriteLn('ASrcCardinal : [' + IntToStr(ASrcCardinal) + ']');
  //umlcdebug.DebugWriteEoLn();

  umlcstrings.Clear(Temp1);
  if (ASrcCardinal <> 0) then
  begin
    number :=
      ASrcCardinal;
    while (number > 0) do
    begin
      j := ((number mod 10) + 48);

      //umlcdebug.DebugWriteLn('letter : [' + chr(j) + ']');
      //umlcdebug.DebugWriteEoLn();

      temp1 := temp1 + chr(j);

      //umlcdebug.DebugWriteLn('temp1 : [' + temp1 + ']');
      //umlcdebug.DebugWriteEoLn();

      number := number div 10;
    end;

    umlcdebug.DebugWriteLn('temp1 : [' + temp1 + ']');
    umlcdebug.DebugWriteEoLn();

    umlcstrings.Clear(Temp2);
    Count :=
      System.Length(temp1);
    for i := 1 to Count do
    begin
      Temp2 := Temp2 + temp1[Count - i + 1];

      umlcdebug.DebugWriteLn('Temp2 : [' + Temp2 + ']');
      umlcdebug.DebugWriteEoLn();
    end;
    Temp1 := Temp2;
  end else
  begin
    Temp1 := '0';
  end;

  umlcstrings.ConcatStr(ADestStr, Temp1);
end;

procedure CardinalToDecStr
  (out   ADestStr:     string;
   const ASrcCardinal: Cardinal);
begin
  ADestStr := '';
  umlcstrings.Clear(ADestStr);

  umlcdecs.ConcatCardinalToDecStr
    (ADestStr, ASrcCardinal);
end;

procedure ConcatIntegerToDecStr
  (out   ADestStr:     string;
   const ASrcInteger: Integer);
var UnsignedNum: Byte;
    Count: Byte;
    i, j: Byte;
    Temp1, temp2: string;
    SignSet: Boolean;
begin
  //umlcdebug.DebugWriteLn('ASrcInteger : [' + IntToStr(ASrcInteger) + ']');
  //umlcdebug.DebugWriteEoLn();

  umlcstrings.Clear(Temp1);
  if (ASrcInteger <> 0) then
  begin
    SignSet :=
      ((ASrcInteger and $80000000) <> 0);

    UnsignedNum :=
      Abs(ASrcInteger);

    while (UnsignedNum > 0) do
    begin
      j := ((UnsignedNum mod 10) + 48);

      //umlcdebug.DebugWriteLn('letter : [' + chr(j) + ']');
      //umlcdebug.DebugWriteEoLn();

      temp1 := temp1 + chr(j);

      //umlcdebug.DebugWriteLn('temp1 : [' + temp1 + ']');
      //umlcdebug.DebugWriteEoLn();

      UnsignedNum := UnsignedNum div 10;
    end;

    //umlcdebug.DebugWriteLn('temp1 : [' + temp1 + ']');
    //umlcdebug.DebugWriteEoLn();

    if (SignSet) then
    begin
      umlcstrings.ConcatChar(ADestStr, '-');
    end else
    begin
      umlcstrings.ConcatChar(ADestStr, '+');
    end;

    umlcstrings.Clear(Temp2);
    Count :=
      System.Length(temp1);
    for i := 1 to Count do
    begin
      Temp2 := Temp2 + temp1[Count - i + 1];

      //umlcdebug.DebugWriteLn('Temp2 : [' + Temp2 + ']');
      //umlcdebug.DebugWriteEoLn();
    end;
    Temp1 := Temp2;
  end else
  begin
    Temp1 := '+0';
  end;

  umlcstrings.ConcatStr(ADestStr, Temp1);
end;

procedure IntegerToDecStr
  (out   ADestStr:    string;
   const ASrcInteger: Integer);
begin
  ADestStr := '';
  umlcstrings.Clear(ADestStr);

  umlcdecs.ConcatIntegerToDecStr
    (ADestStr, ASrcInteger);
end;

function TryDecStrToUInt8
(var ADest: umlcuint_8; const ASource: umlcdecstring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryDecStrToUInt16
(var ADest: umlcuint_16; const ASource: umlcdecstring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryDecStrToUInt32
(var ADest: umlcuint_32; const ASource: umlcdecstring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryDecStrToUInt64
(var ADest: umlcuint_64; const ASource: umlcdecstring): Boolean;
begin
  Result := false;
  ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryDecStrToUInt128
(var ADest: umlcuint_128; const ASource: umlcdecstring): Boolean;
begin
  Result := false;
  //ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryDecStrToUInt256
(var ADest: umlcuint_256; const ASource: umlcdecstring): Boolean;
begin
  Result := false;
  //  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

function TryDecStrToUInt512
(var ADest: umlcuint_512; const ASource: umlcdecstring): Boolean;
begin
  Result := false;
  //ADest  := 0;

  Result := (ASource <> '');
  if (Result) then
  begin
    // @toxdo: ...
  end;
end;

procedure UInt8ToDecStr
  (var ADest: umlcdecstring; const ASource: umlcuint_8);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt16ToDecStr
  (var ADest: umlcdecstring; const ASource: umlcuint_16);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt32ToDecStr
  (var ADest: umlcdecstring; const ASource: umlcuint_32);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt64ToDecStr
  (var ADest: umlcdecstring; const ASource: umlcuint_64);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt128ToDecStr
  (var ADest: umlcdecstring; const ASource: umlcuint_128);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt256ToDecStr
  (var ADest: umlcdecstring; const ASource: umlcuint_256);
begin
  ADest := '';
  // @toxdo: ...
end;

procedure UInt512ToDecStr
  (var ADest: umlcdecstring; const ASource: umlcuint_512);
begin
  ADest := '';
  // @toxdo: ...
end;


end.

