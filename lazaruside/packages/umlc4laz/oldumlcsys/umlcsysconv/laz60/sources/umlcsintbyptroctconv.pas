(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcsintbyptroctconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcsinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcoctals,
  dummy;

// ---

const

  MOD_umlcsintbyptroctconv : TUMLCModule =
    ($D3,$72,$27,$95,$30,$F2,$1D,$4D,$B6,$BA,$89,$CC,$5E,$B3,$E2,$EB);

// ---

(* global functions *)

(* global procedures *)

procedure UIntPtrToOctStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatSInt8PtrToOctStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt16PtrToOctStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt32PtrToOctStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt64PtrToOctStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt128PtrToOctStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt256PtrToOctStr
  (var ADest: string; const ASource: pointer);

procedure ConcatSInt512PtrToOctStr
  (var ADest: string; const ASource: pointer);


implementation

(* global functions *)



(* global procedures *)

procedure UInt8ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_8(ASource);

    // ...
end;

procedure UInt16ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_16(ASource);

  // ...
end;

procedure UInt32ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('UInt32ToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure UInt64ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_64(ASource);

  // ...
end;

procedure UInt128ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_128(ASource);

  // ...
end;

procedure UInt256ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_256(ASource);

  // ...
end;

procedure UInt512ToOctStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpsint_512(ASource);

  // ...
end;

procedure UIntPtrToOctStr
  (out ADest: string; const ASource: pointer);
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Cardinal) = sizeof(umlcsint_32))
    then UInt32ToOctStr(ADest, ASource)

  //if (sizeof(Cardinal) = sizeof(umlcsint_8))
  //  then UInt8ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_16))
  //  then UInt16ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_32))
  //  then UInt32ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_64))
  //  then UInt64ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_128))
  //  then UInt128ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_256))
  //  then UInt256ToOctStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcsint_512))
  //  then UInt512ToOctStr(ADest, ASource);
end;

// ---

procedure ConcatSInt8PtrToOctStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_8;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_8(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;
//
//  umlcdebug.DebugWriteLn('ConcatSInt8PtrToOctStr');
//  umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
//  umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcoctals.DecToOctalChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt16PtrToOctStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_16;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_16(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt16PtrToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt32PtrToOctStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  umlcdebug.DebugWriteLn('ConcatSInt32PtrToOctStr');
  umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

      umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt64PtrToOctStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: umlcpsint_64;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpsint_64(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatSInt64PtrToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 8;
      Remainder := Number mod 8;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlcOctals.DecToOctalChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt128PtrToOctStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatSInt128PtrToOctStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_128);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcoctals.ByteToOctalStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt256PtrToOctStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatSInt256PtrToOctStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_256);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcoctals.ByteToOctalStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatSInt512PtrToOctStr
  (var ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatSInt512PtrToOctStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcsint_512);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlcoctals.ByteToOctalStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;



end.

