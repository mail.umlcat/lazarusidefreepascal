(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdtimetostrbyptrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for pointers to Time type variables.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstddatetimetypes,
  umlcstdtimes,
  umlcstrings,
  dummy;

// ---

const

  MOD_umlcstdtimetostrbyptrconv : TUMLCModule =
    ($D3,$49,$E7,$C0,$48,$A1,$A8,$4F,$9C,$E6,$06,$7B,$21,$F1,$B3,$C5);


// ---

(* global functions *)

function TryParseTimePtrToStr
  (out ADest: string; const ASource: pointer): Boolean;

// ---

function TryConcatTimePtrToStr
  (var ADest: string; const ASource: pointer): Boolean;

(* global procedures *)

procedure ParseTimePtrToStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatTimePtrToStr
  (var ADest: string; const ASource: pointer);



implementation

(* global functions *)

function TryParseTimePtrToStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpstdtime;
    AHour, AMin, ASec, AMSec: Word;
    IsEvening: Boolean;
begin
  ADest  := '';
  umlcstrings.Clear(ADest);
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpstdtime(ASource);
    umlcstdtimes.DecodeTime
      (ASourcePtr^, AHour, AMin, ASec, AMSec);

    IsEvening :=
      (AHour > 12);
    if (IsEvening) then
    begin
      System.Dec(AHour, 12);
    end;

    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AHour));
    umlcstrings.ConcatChar
      (ADest, ':');
    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AMin));
    umlcstrings.ConcatChar
      (ADest, ':');
    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(ASec));
    umlcstrings.ConcatChar
      (ADest, ':');
    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AMSec));

    if (IsEvening) then
    begin
      umlcstrings.ConcatStr
        (ADest, 'pm');
    end else
    begin
      umlcstrings.ConcatStr
        (ADest, 'am');
    end;
  end;
end;

// ---

function TryConcatTimePtrToStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpstdtime;
    AHour, AMin, ASec, AMSec: Word;
    IsEvening: Boolean;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpstdtime(ASource);
    umlcstdtimes.DecodeTime
      (ASourcePtr^, AHour, AMin, ASec, AMSec);

    IsEvening :=
      (AHour > 12);
    if (IsEvening) then
    begin
      System.Dec(AHour, 12);
    end;

    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AHour));
    umlcstrings.ConcatChar
      (ADest, ':');
    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AMin));
    umlcstrings.ConcatChar
      (ADest, ':');
    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(ASec));
    umlcstrings.ConcatChar
      (ADest, ':');
    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AMSec));

    if (IsEvening) then
    begin
      umlcstrings.ConcatStr
        (ADest, 'pm');
    end else
    begin
      umlcstrings.ConcatStr
        (ADest, 'am');
    end;
  end;
end;

(* global procedures *)

procedure ParseTimePtrToStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryParseTimePtrToStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---

procedure ConcatTimePtrToStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatTimePtrToStr
    (ADest, ASource)) then
  begin
    //
  end;
end;


end.

