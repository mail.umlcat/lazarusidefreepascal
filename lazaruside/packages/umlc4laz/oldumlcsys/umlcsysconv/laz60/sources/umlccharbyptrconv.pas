unit umlccharbyptrconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcchars,
  dummy;

// ---

const

  MOD_umlccharbyptrconv : TUMLCModule =
    ($BA,$75,$70,$7D,$AB,$EA,$D0,$48,$A8,$5C,$F5,$71,$AF,$DA,$28,$96);

// ---

(* global functions *)



(* global procedures *)

procedure CharPtrToStr
  (out ADest: string; const ASource: pointer);



implementation

(* global functions *)



(* global procedures *)

procedure CharPtrToStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pchar;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := pchar(ASource);

  umlcstrings.ConcatChar(ADest, ASourcePtr^);
end;



end.

