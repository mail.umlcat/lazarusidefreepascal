unit umlcconvtypes;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcstdnullstrtypes,
  //umlcdebug,
  dummy;

// ---

const

  MOD_umlcconvtypes : TUMLCModule =
    ($C3,$96,$88,$41,$39,$C1,$2A,$46,$9E,$1F,$C0,$C8,$0D,$F1,$87,$F6);

// ---

(* global types *)

type
  umlcptrtostringconvfunctor =
    procedure (* ^ *)
      (out ADest: string; const ASource: pointer);

type
  umlcptrtoansistringconvfunctor =
    procedure (* ^ *)
      (out ADest: ansistring; const ASource: pointer);

type
  umlcptrtoansishortstringconvfunctor =
    procedure (* ^ *)
      (out ADest: umlcansishortstring; const ASource: pointer);

type
  umlcdecimal = byte;

type

  // ## deprecated
  // ## {
  umlcdecchar       = (* alias of *) ansichar;
  umlcdecstring     = (* alias of *) ansistring;
  // ## }

  // ## new
  // ## {
  umlcdecansichar   = (* alias of *) ansichar;
  umlcdecansistring = (* alias of *) ansistring;

  umlcdecwidechar   = (* alias of *) widechar;
  umlcdecwidestring = (* alias of *) widestring;
  // ## }

type
  // ## deprecated
  // ## {
  umlchexachar     = (* alias of *) widechar;
  umlchexastring   = (* alias of *) widestring;
  // ## }

  // ## new
  // ## {
  umlchexansichar     = (* alias of *) ansichar;
  umlchexansistring   = (* alias of *) widestring;

  umlchexwidechar     = (* alias of *) widechar;
  umlchexwidestring   = (* alias of *) widestring;
  // ## }

type
  // ## deprecated
  // ## {
  umlcoctalchar    = (* alias of *) ansichar;
  umlcoctalstring  = (* alias of *) ansistring;
  // ## }

  // ## new
  // ## {
  umlcoctalansichar    = (* alias of *) ansichar;
  umlcoctalansistring  = (* alias of *) ansistring;

  umlcoctalwidechar    = (* alias of *) widechar;
  umlcoctalwidestring  = (* alias of *) widestring;
  // ## }

type
  // ## deprecated
  // ## {
  umlcbinarychar   = (* alias of *) ansichar;
  umlcbinarystring = (* alias of *) ansistring;
  // ## }

  // ## new
  // ## {
  umlcbinaryansichar   = (* alias of *) ansichar;
  umlcbinaryansistring = (* alias of *) ansistring;

  umlcbinarywidechar   = (* alias of *) ansichar;
  umlcbinarywidestring = (* alias of *) ansistring;
  // ## }

implementation




end.

