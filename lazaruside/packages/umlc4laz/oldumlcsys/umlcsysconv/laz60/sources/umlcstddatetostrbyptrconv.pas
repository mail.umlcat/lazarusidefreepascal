(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstddatetostrbyptrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for pointers to Date type variables.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstddatetimetypes,
  umlcstddates,
  umlcstrings,
  umlcstrparsers,
  dummy;

// ---

const

  MOD_umlcsintbyptroctconv : TUMLCModule =
    ($40,$68,$FB,$98,$26,$48,$9B,$4D,$A9,$D4,$E2,$F0,$A4,$99,$93,$15);


// ---

(* global functions *)

function TryParseDatePtrToStr
  (out ADest: string; const ASource: pointer): Boolean;

// ---

function TryConcatDatePtrToStr
  (var ADest: string; const ASource: pointer): Boolean;

(* global procedures *)

procedure ParseDatePtrToStr
  (out ADest: string; const ASource: pointer);

// ---

procedure ConcatDatePtrToStr
  (var ADest: string; const ASource: pointer);





implementation

(* global functions *)

function TryParseDatePtrToStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpstddate;
    AYear, AMonth, ADay: Word;
begin
  ADest := '';
  umlcstrings.Clear(ADest);

  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpstddate(ASource);
    umlcstddates.DecodeDate
      (ASourcePtr^, AYear, AMonth, ADay);

    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AYear));
    umlcstrings.ConcatChar
      (ADest, '/');
    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AMonth));
    umlcstrings.ConcatChar
      (ADest, '/');
    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(ADay));
  end;
end;

// ---

function TryConcatDatePtrToStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpstddate;
    AYear, AMonth, ADay: Word;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpstddate(ASource);
    umlcstddates.DecodeDate
      (ASourcePtr^, AYear, AMonth, ADay);

    //umlcdebug.DebugWriteLn('TryConcatDatePtrToStr');
    //umlcdebug.DebugWriteLn('AYear: [' + IntToStr(AYear) + ']');
    //umlcdebug.DebugWriteLn('AMonth: [' + IntToStr(AMonth) + ']');
    //umlcdebug.DebugWriteLn('ADay: [' + IntToStr(ADay) + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatStr
      (ADest, SysUtils.IntToStr(AYear));
    umlcstrings.ConcatChar
      (ADest, '/');
    umlcstrings.ConcatStr
      (ADest, umlcstrings.AlignRightCopy(SysUtils.IntToStr(AMonth), 2, '0'));
    umlcstrings.ConcatChar
      (ADest, '/');
    umlcstrings.ConcatStr
      (ADest, umlcstrings.AlignRightCopy(SysUtils.IntToStr(ADay), 2, '0'));
  end;
end;

(* global procedures *)

procedure ParseDatePtrToStr
  (out ADest: string; const ASource: pointer);
begin
  if (not TryParseDatePtrToStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---

procedure ConcatDatePtrToStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatDatePtrToStr
    (ADest, ASource)) then
  begin
    //
  end;
end;



end.

