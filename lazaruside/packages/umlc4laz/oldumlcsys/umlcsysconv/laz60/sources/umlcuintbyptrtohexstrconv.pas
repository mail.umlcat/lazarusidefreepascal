(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuintbyptrtohexstrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for pointers to memory based unsigned integers,
 ** using hexadecimal base representation numbers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlchexas,
  dummy;

// ---

const

  MOD_umlcuintbyptrtohexstrconv : TUMLCModule =
    ($C6,$C3,$FF,$32,$81,$DF,$48,$4F,$AA,$36,$E3,$3A,$93,$F2,$0D,$24);

// ---

(* global functions *)

function TryParseUInt8PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryParseUInt16PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryParseUInt32PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryParseUInt64PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryParseUInt128PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryParseUInt256PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryParseUInt512PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;

function TryParseUIntPtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;

(* global procedures *)

procedure ParseUInt8PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ParseUInt16PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ParseUInt32PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ParseUInt64PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ParseUInt128PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ParseUInt256PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ParseUInt512PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ParseUIntPtrToHexStr
  (out ADest: string; const ASource: pointer);

// ---

function TryConcatInt8PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt16PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt32PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt64PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt128PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt256PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;

function TryConcatInt512PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;

// ---

procedure ConcatInt8PtrToHexStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt16PtrToHexStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt32PtrToHexStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt64PtrToHexStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt128PtrToHexStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt256PtrToHexStr
  (var ADest: string; const ASource: pointer);

procedure ConcatInt512PtrToHexStr
  (var ADest: string; const ASource: pointer);

implementation

(* global functions *)

function TryParseUInt8PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_8;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_8(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('Int8PtrToHexStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 16;
        Remainder := Number mod 16;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := chr(Remainder + 48);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatChar(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryParseUInt16PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_16;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_16(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('Int8PtrToHexStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 16;
        Remainder := Number mod 16;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := chr(Remainder + 48);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatChar(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryParseUInt32PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    umlcstrings.Clear(ADest);

    ASourcePtr := umlcpuint_32(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('Int32ToOctStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 16;
        Remainder := Number mod 16;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := chr(Remainder + 48);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatChar(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryParseUInt64PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_64;
begin
  umlcstrings.Clear(ADest);
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_64(ASource);

    // ...
  end;
end;

function TryParseUInt128PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_128;
begin
  umlcstrings.Clear(ADest);
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_128(ASource);

    // ...
  end;
end;

function TryParseUInt256PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_256;
begin
  umlcstrings.Clear(ADest);
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_256(ASource);

    // ...
  end;
end;

function TryParseUInt512PtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_512;
begin
  umlcstrings.Clear(ADest);
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_512(ASource);

    // ...
  end;
end;

function TryParseUIntPtrToHexStr
  (out ADest: string; const ASource: pointer): Boolean;
begin
  umlcstrings.Clear(ADest);
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    if (sizeof(Integer) = sizeof(umlcuint_8))
      then ParseUInt8PtrToHexStr(ADest, ASource)
    else if (sizeof(Integer) = sizeof(umlcuint_16))
      then ParseUInt16PtrToHexStr(ADest, ASource)
    else if (sizeof(Integer) = sizeof(umlcuint_32))
      then ParseUInt32PtrToHexStr(ADest, ASource)
    //else if (sizeof(Integer) = sizeof(umlcuint_64))
    //  then ParseUInt64PtrToHexStr(ADest, ASource)
    //else if (sizeof(Integer) = sizeof(umlcuint_128))
    //  then ParseUInt128PtrToHexStr(ADest, ASource)
    //else if (sizeof(Integer) = sizeof(umlcuint_256))
    //  then ParseUInt256PtrToHexStr(ADest, ASource)
    //else if (sizeof(Integer) = sizeof(umlcuint_512))
    //  then ParseUInt512PtrToHexStr(ADest, ASource);
  end;
end;

(* global procedures *)

procedure ParseUInt8PtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtohexstrconv.TryParseUInt8PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt16PtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtohexstrconv.
     TryParseUInt16PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

//procedure Int32PtrToHexStr
//  (out ADest: string; const ASource: pointer);
//var ASourcePtr: umlcuintbyptr32;
//begin
//  umlcstrings.Clear(ADest);
//
//  ASourcePtr := umlcuintbyptr32(ASource);
//
//  ADest :=
//    SysUtils.IntToHex(ASourcePtr^, 8);
//end;

procedure ParseUInt32PtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtohexstrconv.
     TryParseUInt32PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt64PtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtohexstrconv.
     TryParseUInt64PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt128PtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtohexstrconv.
     TryParseUInt128PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt256PtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtohexstrconv.
     TryParseUInt256PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt512PtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtohexstrconv.
     TryParseUInt512PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUIntPtrToHexStr
  (out ADest: string; const ASource: pointer);
begin
  if (not umlcuintbyptrtohexstrconv.
     TryParseUIntPtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---

function TryConcatInt8PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_8;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_8(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatInt8PtrToHexStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 16;
        Remainder := Number mod 16;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlchexas.DecToHexChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();

    //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
  end;
end;

function TryConcatInt16PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_16;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_16(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatInt16PtrToHexStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 16;
        Remainder := Number mod 16;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlchexas.DecToHexChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();

    //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
  end;
end;

function TryConcatInt32PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_32;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_32(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatInt32PtrToHexStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 16;
        Remainder := Number mod 16;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlchexas.DecToHexChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();

    //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
  end;
end;

function TryConcatInt64PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: umlcpuint_64;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := umlcpuint_64(ASource);

    Count  := 0;
    Number := ASourcePtr^;
    Division  := 0;
    Remainder := 0;

    //umlcdebug.DebugWriteLn('ConcatInt64PtrToHexStr');
    //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
    //umlcdebug.DebugWriteEoLn();

    repeat
      if (Number > 0) then
      begin
        Division  := Number div 16;
        Remainder := Number mod 16;
        Number    := Division;

        //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
        //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

        Temp[Count] := umlchexas.DecToHexChar(Remainder);

        //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
        //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
        //umlcdebug.DebugWriteEoLn();

        System.Inc(Count);
      end;
    until (Number < 1);

    for I := 0 to System.Pred(Count) do
    begin
      //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
      //umlcdebug.DebugWriteEoLn();

      umlcstrings.ConcatCharBack(ADest, Temp[I]);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();

    //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
  end;
end;

function TryConcatInt128PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatInt128PtrToHexStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_128);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlchexas.ByteToHexStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatInt256PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatInt256PtrToHexStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_256);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlchexas.ByteToHexStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

function TryConcatInt512PtrToHexStr
  (var ADest: string; const ASource: pointer): Boolean;
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  Result :=
    (ASource <> nil);
  if (Result) then
  begin
    ASourcePtr := pbyte(ASource);

    //umlcdebug.DebugWriteLn('ConcatInt512PtrToHexStr');
    //umlcdebug.DebugWriteEoLn();

    Count :=
      sizeof(umlcuint_512);

    //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
    //umlcdebug.DebugWriteEoLn();

    I := 0;
    for I := 0 to System.Pred(Count) do
    begin
      B :=
        umlchexas.ByteToHexStr(ASourcePtr^);
      umlcstrings.ConcatStrBack(ADest, B);

      //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(ASourcePtr);
    end;

    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteEoLn();
  end;
end;

// ---

procedure ConcatInt8PtrToHexStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatInt8PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatInt16PtrToHexStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatInt16PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatInt32PtrToHexStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatInt32PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatInt64PtrToHexStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatInt64PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatInt128PtrToHexStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatInt128PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatInt256PtrToHexStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatInt256PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatInt512PtrToHexStr
  (var ADest: string; const ASource: pointer);
begin
  if (not TryConcatInt512PtrToHexStr(ADest, ASource)) then
  begin
    // raise error
  end;
end;



end.

