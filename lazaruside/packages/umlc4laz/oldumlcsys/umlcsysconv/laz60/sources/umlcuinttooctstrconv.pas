(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcuinttooctstrconv;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Text Formatting operations,
 ** for memory based unsigned integers,
 ** using octal base representation numbers.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlcoctals,
  dummy;

// ---

const

  MOD_umlcInttooctstrconv : TUMLCModule =
    ($AE,$28,$9A,$1A,$7B,$BC,$2D,$4F,$8A,$4F,$89,$08,$C3,$E2,$C8,$B5);

// ---

(* global functions *)

function TryParseUInt8ToOctStr
  (out ADest: string; const ASource: umlcuint_8): Boolean;

function TryParseUInt16ToOctStr
  (out ADest: string; const ASource: umlcuint_16): Boolean;

function TryParseUInt32ToOctStr
  (out ADest: string; const ASource: umlcuint_32): Boolean;

function TryParseUInt64ToOctStr
  (out ADest: string; const ASource: umlcuint_64): Boolean;

function TryParseUInt128ToOctStr
  (out ADest: string; const ASource: umlcuint_128): Boolean;

function TryParseUInt256ToOctStr
  (out ADest: string; const ASource: umlcuint_256): Boolean;

function TryParseUInt512ToOctStr
  (out ADest: string; const ASource: umlcuint_512): Boolean;

// ---

function TryConcatUInt8ToOctStr
  (var ADest: string; const ASource: umlcuint_8): Boolean;

function TryConcatUInt16ToOctStr
  (var ADest: string; const ASource: umlcuint_16): Boolean;

function TryConcatUInt32ToOctStr
  (var ADest: string; const ASource: umlcuint_32): Boolean;

function TryConcatUInt64ToOctStr
  (var ADest: string; const ASource: umlcuint_64): Boolean;

function TryConcatUInt128ToOctStr
  (var ADest: string; const ASource: umlcuint_128): Boolean;

function TryConcatUInt256ToOctStr
  (var ADest: string; const ASource: umlcuint_256): Boolean;

function TryConcatUInt512ToOctStr
  (var ADest: string; const ASource: umlcuint_512): Boolean;

(* global procedures *)

procedure ParseUInt8ToOctStr
  (out ADest: string; const ASource: umlcuint_8);

procedure ParseUInt16ToOctStr
  (out ADest: string; const ASource: umlcuint_16);

procedure ParseUInt32ToOctStr
  (out ADest: string; const ASource: umlcuint_32);

procedure ParseUInt64ToOctStr
  (out ADest: string; const ASource: umlcuint_64);

procedure ParseUInt128ToOctStr
  (out ADest: string; const ASource: umlcuint_128);

procedure ParseUInt256ToOctStr
  (out ADest: string; const ASource: umlcuint_256);

procedure ParseUInt512ToOctStr
  (out ADest: string; const ASource: umlcuint_512);

// ---

procedure ConcatUInt8ToOctStr
  (var ADest: string; const ASource: umlcuint_8);

procedure ConcatUInt16ToOctStr
  (var ADest: string; const ASource: umlcuint_16);

procedure ConcatUInt32ToOctStr
  (var ADest: string; const ASource: umlcuint_32);

procedure ConcatUInt64ToOctStr
  (var ADest: string; const ASource: umlcuint_64);

procedure ConcatUInt128ToOctStr
  (var ADest: string; const ASource: umlcuint_128);

procedure ConcatUInt256ToOctStr
  (var ADest: string; const ASource: umlcuint_256);

procedure ConcatUInt512ToOctStr
  (var ADest: string; const ASource: umlcuint_512);

implementation

(* global functions *)

function TryParseUInt8ToOctStr
  (out ADest: string; const ASource: umlcuint_8): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt16ToOctStr
  (out ADest: string; const ASource: umlcuint_16): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt32ToOctStr
  (out ADest: string; const ASource: umlcuint_32): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt64ToOctStr
  (out ADest: string; const ASource: umlcuint_64): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt128ToOctStr
  (out ADest: string; const ASource: umlcuint_128): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt256ToOctStr
  (out ADest: string; const ASource: umlcuint_256): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryParseUInt512ToOctStr
  (out ADest: string; const ASource: umlcuint_512): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

// ---

function TryConcatUInt8ToOctStr
  (var ADest: string; const ASource: umlcuint_8): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt16ToOctStr
  (var ADest: string; const ASource: umlcuint_16): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt32ToOctStr
  (var ADest: string; const ASource: umlcuint_32): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt64ToOctStr
  (var ADest: string; const ASource: umlcuint_64): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt128ToOctStr
  (var ADest: string; const ASource: umlcuint_128): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt256ToOctStr
  (var ADest: string; const ASource: umlcuint_256): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

function TryConcatUInt512ToOctStr
  (var ADest: string; const ASource: umlcuint_512): Boolean;
//var Number, Division, Remainder: Integer;
//    Temp: array[0..255] of char;
//    I, Count: Cardinal;
begin

end;

(* global procedures *)

procedure ParseUInt8ToOctStr
  (out ADest: string; const ASource: umlcuint_8);
begin
  if (not TryParseUInt8ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt16ToOctStr
  (out ADest: string; const ASource: umlcuint_16);
begin
  if (not TryParseUInt16ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt32ToOctStr
  (out ADest: string; const ASource: umlcuint_32);
begin
  if (not TryParseUInt32ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt64ToOctStr
  (out ADest: string; const ASource: umlcuint_64);
begin
  if (not TryParseUInt64ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt128ToOctStr
  (out ADest: string; const ASource: umlcuint_128);
begin
  if (not TryParseUInt128ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt256ToOctStr
  (out ADest: string; const ASource: umlcuint_256);
begin
  if (not TryParseUInt256ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ParseUInt512ToOctStr
  (out ADest: string; const ASource: umlcuint_512);
begin
  if (not TryParseUInt512ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

// ---


procedure ConcatUInt8ToOctStr
  (var ADest: string; const ASource: umlcuint_8);
begin
  if (not TryConcatUInt8ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt16ToOctStr
  (var ADest: string; const ASource: umlcuint_16);
begin
  if (not TryConcatUInt16ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt32ToOctStr
  (var ADest: string; const ASource: umlcuint_32);
begin
  if (not TryConcatUInt32ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt64ToOctStr
  (var ADest: string; const ASource: umlcuint_64);
begin
  if (not TryConcatUInt64ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt128ToOctStr
  (var ADest: string; const ASource: umlcuint_128);
begin
  if (not TryConcatUInt128ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt256ToOctStr
  (var ADest: string; const ASource: umlcuint_256);
begin
  if (not TryConcatUInt256ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;

procedure ConcatUInt512ToOctStr
  (var ADest: string; const ASource: umlcuint_512);
begin
  if (not TryConcatUInt512ToOctStr
    (ADest, ASource)) then
  begin
    // raise error
  end;
end;


end.

