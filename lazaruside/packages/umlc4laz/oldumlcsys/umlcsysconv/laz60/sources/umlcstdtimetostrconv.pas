(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdtimetostrconv;

{$mode objfpc}{$H+}

interface
uses
{$IFDEF MSWINDOWS}
  Windows, Messages,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  Math,
  SysUtils,
  umlcmodules, umlctypes,
  umlcdebug,
  umlccomparisons,
  umlcstdtypes,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcstdnullstrtypes,
  umlcstddatetimetypes,
  umlctimes,
  umlcansicharsets,
  umlcansinullstrs, //umlcansinullparsers,
  dummy;

// ---

const

  MOD_umlcstdtimetostrconv : TUMLCModule =
    ($CC,$FF,$62,$56,$73,$CE,$1D,$41,$83,$ED,$B9,$39,$36,$3C,$02,$29);

// ---

(* global constants *)

const DefaultTimeFormatSingleton: string = 'HH:MM:SSap';

(* global properties *)

function getCurrentTimeFormat(): string;

procedure setCurrentTimeFormat
  (const AValue: string);

(* global functions *)

function FormatTimeToStr
  (const Format: string; AValue: umlcstdtime): string;
function FormatStrToTime
  (const Format: string; AValue: string): umlcstdtime;

function StrToTimeDef
  (const S: string; AValue: umlcstdtime): umlcstdtime;
function StrToTime
  (const AValue: string): umlcstdtime;
function TimeToStr
  (const AValue: umlcstdtime): string;

procedure DecodeTimeFormat
  (const AValue: string; var AHourFmt, AMinFmt, ASecFmt, AMSecFmt: ansicharset);

(* global variables *)

var
  HourFormat, MinFormat, SecFormat, MSecFormat: ansicharset;

implementation

(* global variables *)

var FCurrentTimeFormat: string;

(* global properties *)

function getCurrentTimeFormat(): string;
begin
  Result := FCurrentTimeFormat;
end;

procedure setCurrentTimeFormat
  (const AValue: string);
begin
  FCurrentTimeFormat := AValue;
end;

(* global functions *)

function FormatTimeToStr
  (const Format: string; AValue: umlcstdtime): string;
begin
  Result :=
    SysUtils.FormatDateTime(Format, umlctimes.TimeToDateTime(AValue));
  // Goal: Returns a string representation of the given time value.
  // Objetivo: Regresa una representacion alfanumerica del valor tiempo dado.
end;

function FormatStrToTime
  (const Format: string; AValue: string): umlcstdtime;
//var FmtStr, FmtPtr: ansinullstring; Token: string;
//    Hour, Min, Sec, MSec: Word;
begin
  Result :=
    umlcstdtimetostrconv.StrToTime(AValue);

(*
  FmtStr := StrToNullStr(Format); FmtPtr := FmtStr;
    case FmtPtr^ of
      'h', 'H':
      begin
        Token := umlcansinullstrs.ExtractWhile(FmtPtr, HourFormat);
      end;
      'm', 'M':
      begin
        Token := umlcansinullstrs.ExtractWhile(FmtPtr, MinFormat);
      end;
      's', 'S':
      begin
        Token := umlcansinullstrs.ExtractWhile(FmtPtr, SecFormat);
      end;
      't', 'T':
      begin
        Token := umlcansinullstrs.ExtractWhile(FmtPtr, MSecFormat);
      end;
    end;
  Result := NoTime;
  FreeNullStr(FmtStr);
*)
  // Goal: Returns a time value from the given string.
  // Objetivo: Regresa un valor tiempo a partir de la cadena dada.
end;

function StrToTimeDef
  (const S: string; AValue: umlcstdtime): umlcstdtime;
begin
  try
    Result := umlcstdtimetostrconv.StrToTime(S);
  except
    Result := AValue;
  end;
  // Goal: Returns a time value from a string.
  // Objetivo: Regresa un valor tiempo de una cadena.
end;

function StrToTime
  (const AValue: string): umlcstdtime;
begin
  if (AValue = '')
    then Result := NoTime
    else Result := DateTimeToTime(SysUtils.StrToTime(AValue));
  // Goal: Returns a time value from a string.
  // Objetivo: Regresa un valor tiempo a partir de una cadena.
end;

function TimeToStr
  (const AValue: umlcstdtime): string;
begin
  if (umlctimes.Equal(AValue, NoTime))
    then Result := '00:00:00'
    else Result := SysUtils.TimeToStr(TimeToDateTime(AValue));
  // Goal: Returns a string from a time value.
  // Objetivo: Regresa una cadena a partir de un valor tiempo.
end;

procedure DecodeTimeFormat
  (const AValue: string; var AHourFmt, AMinFmt, ASecFmt, AMSecFmt: ansicharset);
//var S: ansinullstring;
begin
  //S := StrToNullStr(AValue);
  //  AHourFmt :=
  //    umlcansinullparsers.ParseEqualCharSetWhile(S, HourFormat);
  //  // obtain hour*s format
  //  // obtener formato para hora
  //
  //  AMinFmt :=
  //    umlcansinullparsers.ParseEqualCharSetWhile(S, MinFormat);
  //  // obtain minutes*s format
  //  // obtener formato para horas
  //
  //  ASecFmt :=
  //    umlcansinullparsers.ParseEqualCharSetWhile(S, SecFormat);
  //  // obtain seconds* format
  //  // obtener formato para segundos
  //
  //  AMSecFmt :=
  //    umlcansinullparsers.ParseEqualCharSetWhile(S, MSecFormat);
  //  // obtain seconds* format
  //  // obtener formato para segundos
  //FreeNullStr(S);
  // Goal: Splits a time format into standalone formats.
  // Objetivo: Dividir un formato tiempo en formatos separados.
end;

procedure UnitConstructor;
begin
  HourFormat := 'hH';
  // HourFormat := ['h', 'H'];

  MinFormat := 'mM';
  // MinFormat  := ['m', 'M'];

  SecFormat := 'sS';
  // SecFormat  := ['s', 'S'];

  MSecFormat := 'tT';
  // MSecFormat := ['t', 'T'];

  FCurrentTimeFormat :=
    DefaultTimeFormatSingleton;
end;

procedure UnitDestructor;
begin
  MSecFormat := '';
  SecFormat  := '';
  MinFormat  := '';
  HourFormat := '';
end;

initialization
  UnitConstructor;
finalization
  UnitDestructor;
end.

