(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmonthstextconv;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  //umlcuuids,
  umlcmodules, umlctypes,
  umlcmonths,
  umlcresmonths,
  dummy;

// ---

const

 MOD_umlcmonthstextconv : TUMLCModule =
   ($F8,$22,$C1,$F7,$96,$AE,$11,$42,$A7,$A5,$25,$F3,$9A,$AA,$0D,$6A);

// ---

const
  strmoNone      = 'moNone';
  strmoJanuary   = 'moJanuary';
  strmoFebruary  = 'moFebruary';
  strmoMarch     = 'moMarch';
  strmoApril     = 'moApril';
  strmoMay       = 'moMay';
  strmoJune      = 'moJune';
  strmoJuly      = 'moJuly';
  strmoAugust    = 'moAugust';
  strmoSeptember = 'moSeptember';
  strmoOctober   = 'moOctober';
  strmoNovember  = 'moNovember';
  strmoDecember  = 'moDecember';

  function MonthToStr(const AValue: TMonth): string;
  function MonthToShortText(const AValue: TMonth): string;
  function MonthToLongText(const AValue: TMonth): string;

  function StrToMonth(const AValue: string): TMonth;
  function ShortTextToMonth(const AValue: string): TMonth;
  function LongTextToMonth(const AValue: string): TMonth;


implementation

type
  TMonthNames = array[TMonth] of string;
  PMonthNames = ^TMonthNames;

const
   MonthToStrArray: TMonthNames =
   (strmoNone,
    strmoJanuary, strmoFebruary, strmoMarch,
    strmoApril, strmoMay, strmoJune,
    strmoJuly, strmoAugust, strmoSeptember,
    strmoOctober, strmoNovember, strmoDecember);

const
   MonthToShortTextArray: TMonthNames =
   (shortmoNone,
    shortmoJanuary, shortmoFebruary, shortmoMarch,
    shortmoApril, shortmoMay, shortmoJune,
    shortmoJuly, shortmoAugust, shortmoSeptember,
    shortmoOctober, shortmoNovember, shortmoDecember);

const
   MonthToLongTextArray: TMonthNames =
   (longmoNone,
    longmoJanuary, longmoFebruary, longmoMarch,
    longmoApril, longmoMay, longmoJune,
    longmoJuly, longmoAugust, longmoSeptember,
    longmoOctober, longmoNovember, longmoDecember);

function MonthToStr(const AValue: TMonth): string;
begin
  Result :=
    MonthToStrArray[AValue];
  // Goal: To cast a "Month" value to a "string" value.
  // Objetivo: Convertir un valor "Month" a un valor "string".
end;

function MonthToShortText(const AValue: TMonth): string;
begin
  Result :=
    MonthToShortTextArray[AValue];
  // Goal: To cast a "Month" value to a "string" value
  // (short description).

  // Objetivo: Convertir un valor "Month" a un valor "string"
  // (descripcion corta).
end;

function MonthToLongText(const AValue: TMonth): string;
begin
  Result :=
    MonthToLongTextArray[AValue];
  // Goal: To cast a "Month" value to a "string" value
  // (Long description).

  // Objetivo: Convertir un valor "Month" a un valor "string"
  // (descripcion larga).
end;

function MatchMonth
 (const AValue: string; const MonthNames: TMonthNames): TMonth;
var i: TMonth; Found: Boolean;
begin
  i := Low(TMonth); Found := FALSE;
  while ((i <= High(TMonth)) and (not Found)) do
  begin
    Found := SameText(MonthNames[i], AValue);
    Inc(i);
  end;

  if (Found)
    then Result := Pred(i)
    else Result := Low(TMonth);
  // Goal: Locates a month by its name in a given array.
end;

function StrToMonth(const AValue: string): TMonth;
begin
  Result :=
    MatchMonth(AValue, MonthToStrArray);
  // Goal: To cast a "string" value to a "Month" value.
  // Objetivo: Convertir un valor "string" a un valor "Month".
end;

function ShortTextToMonth(const AValue: string): TMonth;
begin
  Result :=
    MatchMonth(AValue, MonthToShortTextArray);
  // Goal: To cast a "string" value to a "Month" value (short description).

  // Objetivo: Convertir un valor "string" a un valor "Month"
  // (descripcion corta).
end;

function LongTextToMonth(const AValue: string): TMonth;
begin
  Result :=
    MatchMonth(AValue, MonthToLongTextArray);
  // Goal: To cast a "string" value to a "Month" value (Long description).

  // Objetivo: Convertir un valor "string" a un valor "Month"
  // (descripcion corta).
end;


end.

