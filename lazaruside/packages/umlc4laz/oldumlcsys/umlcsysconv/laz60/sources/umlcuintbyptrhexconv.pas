unit umlcuintbyptrhexconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuinttypes,
  umlcmaskarrays,
  umlcstrings,
  umlchexas,
  dummy;

// ---

const

  MOD_umlcuintbyptrhexconv : TUMLCModule =
    ($6E,$62,$93,$5F,$05,$52,$0D,$44,$B0,$DC,$84,$1F,$01,$08,$88,$3C);

// ---

(* global functions *)



(* global procedures *)

procedure UIntPtrToHexStr
  (out ADest: string; const ASource: pointer);


// ---

procedure ConcatUInt8PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt16PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt32PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt64PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt128PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt256PtrToHexStr
  (out ADest: string; const ASource: pointer);

procedure ConcatUInt512PtrToHexStr
  (out ADest: string; const ASource: pointer);

implementation

(* global functions *)



(* global procedures *)

procedure UIntPtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_8;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_8(ASource);

    // ...
end;

procedure UInt16ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_16;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_16(ASource);

  // ...
end;

procedure UInt32ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_32;
    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('MemInt32ToOctStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := chr(Remainder + 48);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatChar(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure UInt64ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_64;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_64(ASource);

  // ...
end;

procedure UInt128ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_128;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_128(ASource);

  // ...
end;

procedure UInt256ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_256;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_256(ASource);

  // ...
end;

procedure UInt512ToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_512;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpuint_512(ASource);

  // ...
end;

procedure UIntToHexStr
  (out ADest: string; const ASource: pointer);
begin
  umlcstrings.Clear(ADest);

  if (sizeof(Cardinal) = sizeof(umlcuint_32))
    then UInt32ToHexStr(ADest, ASource)

  //if (sizeof(Cardinal) = sizeof(umlcuint_8))
  //  then UInt8ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_16))
  //  then UInt16ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_32))
  //  then UInt32ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_64))
  //  then UInt64ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_128))
  //  then UInt128ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_256))
  //  then UInt256ToHexStr(ADest, ASource)
  //else if (sizeof(Cardinal) = sizeof(umlcuint_512))
  //  then UInt512ToHexStr(ADest, ASource);
end;


// ---

procedure ConcatUInt8PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_8;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpuint_8(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt8PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

procedure ConcatUInt16PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_16;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpuint_16(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt16PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

procedure ConcatUInt32PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_32;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpuint_32(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt32PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

procedure ConcatUInt64PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpuint_64;

    Number, Division, Remainder: Integer;
    Temp: array[0..255] of char;
    I, Count: Cardinal;
begin
  ASourcePtr := umlcpuint_64(ASource);

  Count  := 0;
  Number := ASourcePtr^;
  Division  := 0;
  Remainder := 0;

  //umlcdebug.DebugWriteLn('ConcatUInt64PtrToHexStr');
  //umlcdebug.DebugWriteLn('Number : [' + IntToStr(Number) + ']');
  //umlcdebug.DebugWriteEoLn();

  repeat
    if (Number > 0) then
    begin
      Division  := Number div 16;
      Remainder := Number mod 16;
      Number    := Division;

      //umlcdebug.DebugWriteLn('Division : [' + IntToStr(Division) + ']');
      //umlcdebug.DebugWriteLn('Remainder : [' + IntToStr(Remainder) + ']');

      Temp[Count] := umlchexas.DecToHexChar(Remainder);

      //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
      //umlcdebug.DebugWriteLn('Digit : [' + Temp[Count] + ']');
      //umlcdebug.DebugWriteEoLn();

      System.Inc(Count);
    end;
  until (Number < 1);

  for I := 0 to System.Pred(Count) do
  begin
    //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
    //umlcdebug.DebugWriteLn('Digit : [' + Temp[I] + ']');
    //umlcdebug.DebugWriteEoLn();

    umlcstrings.ConcatCharBack(ADest, Temp[I]);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();

  //umlcstrings.ConcatStr(ADest, umlchexas.ByteToHex(ASourcePtr^));
end;

procedure ConcatUInt128PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatUInt128PtrToHexStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_128);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatUInt256PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  umlcdebug.DebugWriteLn('ConcatUInt256PtrToHexStr');
  umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_256);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;

procedure ConcatUInt512PtrToHexStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: pbyte;
    B: string;
    I, Count: LongWord;
begin
  ASourcePtr := pbyte(ASource);

  //umlcdebug.DebugWriteLn('ConcatUInt512PtrToHexStr');
  //umlcdebug.DebugWriteEoLn();

  Count :=
    sizeof(umlcuint_512);

  //umlcdebug.DebugWriteLn('Count : [' + IntToStr(Count) + ']');
  //umlcdebug.DebugWriteEoLn();

  I := 0;
  for I := 0 to System.Pred(Count) do
  begin
    B :=
      umlchexas.ByteToHexStr(ASourcePtr^);
    umlcstrings.ConcatStrBack(ADest, B);

    //umlcdebug.DebugWriteLn('Digit : [' + B + ']');
    //umlcdebug.DebugWriteEoLn();

    System.Inc(ASourcePtr);
  end;

  //umlcdebug.DebugWriteLn('ADest : [' + ADest + ']');
  //umlcdebug.DebugWriteEoLn();
end;



end.

