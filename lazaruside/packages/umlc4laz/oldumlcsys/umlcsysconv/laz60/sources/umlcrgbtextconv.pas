(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcrgbtextconv;

interface
uses
  umlcrgbs,
  dummy;

  procedure RGBRecToHex
    (const ASource: TUMLCRGBRecord; var ADest: string);

  procedure HexToRGBRec
    (const ASource: string; var ADest: TUMLCRGBRecord);

  procedure ARGBRecToHex
    (const ASource: TUMLCARGBRecord; var ADest: string);

  procedure HexToARGBRec
    (const ASource: string; var ADest: TUMLCARGBRecord);

implementation

procedure RGBRecToHex
  (const ASource: TUMLCRGBRecord; var ADest: string);
begin
  // ...
end;

procedure HexToRGBRec
  (const ASource: string; var ADest: TUMLCRGBRecord);
begin
  // ...
end;

procedure ARGBRecToHex
  (const ASource: TUMLCARGBRecord; var ADest: string);
begin
  // ...
end;

procedure HexToARGBRec
  (const ASource: string; var ADest: TUMLCARGBRecord);
begin
  // ...
end;



end.
