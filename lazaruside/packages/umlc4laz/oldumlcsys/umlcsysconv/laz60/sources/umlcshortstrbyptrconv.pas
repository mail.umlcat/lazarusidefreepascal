unit umlcshortstrbyptrconv;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcstdstrtypes,
  umlcmaskarrays,
  umlcstrings,
  umlcansishortstrings,
  umlcshortstrbyptrs,
  dummy;

// ---

const

  MOD_umlcshortstrbyptrconv : TUMLCModule =
    ($D8,$E3,$21,$94,$1D,$4F,$82,$45,$B4,$9F,$6A,$0E,$9E,$B3,$A8,$8C);

// ---

(* global functions *)



(* global procedures *)

procedure ShortStrPtrToStr
  (out ADest: string; const ASource: pointer);


implementation

(* global functions *)



(* global procedures *)

procedure ShortStrPtrToStr
  (out ADest: string; const ASource: pointer);
var ASourcePtr: umlcpansishortstring;
begin
  umlcstrings.Clear(ADest);

  ASourcePtr := umlcpansishortstring(ASource);

  //umlcstrings.ConcatStr(ADest, ASourcePtr^);
end;

end.

