(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtype256byptrs;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 256 bits segments' pointers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  dummy;

// ---

const

 MOD_umlcmemtype256byptrs : TUMLCModule =
  ($EA,$11,$FC,$EF,$DE,$26,$19,$4E,$89,$C7,$2F,$45,$8F,$F2,$E9,$C7);

 // ---

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemtype_256): pointer;

procedure DropPtr
  (var ADestPtr: pointer);

   function IsEmpty
     (const ASource: pointer): Boolean; overload;

 (* global procedures *)

   procedure Clear
     (out ADest: pointer); overload;




 (* global operators *)

   procedure Assign
     (out   ADest:   pointer;
      const ASource: pointer); overload; // operator :=

   function Compare
     (const A, B: pointer): umlctcomparison;

   function Equal
     (const A, B: pointer): Boolean; overload; // operator =
   function Different
     (const A, B: pointer): Boolean; overload; // operator <>

   function Greater
     (const A, B: pointer): Boolean; overload; // operator >
   function Lesser
     (const A, B: pointer): Boolean; overload; // operator <
   function GreaterEqual
     (const A, B: pointer): Boolean; overload; // operator >=
   function LesserEqual
     (const A, B: pointer): Boolean; overload; // operator <=

implementation

(* global functions *)

function ConstToPtr
  (const AValue: umlcmemtype_256): pointer;
var P: umlcpmemtype_256;
begin
  System.GetMem(P, sizeof(umlcmemtype_256));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcmemtype_256));
end;

function IsEmpty
  (const ASource: pointer): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

(* global procedures *)

procedure Clear
  (out ADest: pointer);
begin
  System.FillByte(ADest^, sizeof(pointer), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;





(* global operators *)

procedure Assign
  (out   ADest:   pointer;
   const ASource: pointer);
begin
  System.Move(ASource^, ADest^, sizeof(umlcmemtype_256));
end;

function Compare
  (const A, B: pointer): umlccomparison;
var C, D: umlcpmemtype_256;
begin
  C := umlcpmemtype_256(A);
  D := umlcpmemtype_256(B);
  //if (C^ = D^)
  //  then Result := cmpEqual
  //else if (C^ < D^)
  //  then Result := cmpLower
  //else Result := cmpHigher;
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemtype_256));
end;

function Equal
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_256;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemtype_256(A);
  D := umlcpmemtype_256(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemtype_256));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_256;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemtype_256(A);
  D := umlcpmemtype_256(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemtype_256));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_256;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemtype_256(A);
  D := umlcpmemtype_256(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemtype_256));
  Result :=
    (ErrorCode <> cmpHigher);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_256;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemtype_256(A);
  D := umlcpmemtype_256(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemtype_256));
  Result :=
    (ErrorCode <> cmpLower);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_256;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemtype_256(A);
  D := umlcpmemtype_256(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemtype_256));
  Result :=
  (ErrorCode = cmpHigher) or (ErrorCode = cmpEqual);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: pointer): Boolean;
var C, D: umlcpmemtype_256;
    ErrorCode: umlctcomparison;
begin
  C := umlcpmemtype_256(A);
  D := umlcpmemtype_256(B);
  ErrorCode :=
    System.CompareByte(C^, D^, sizeof(umlcmemtype_256));
  Result :=
    (ErrorCode = cmpLower) or (ErrorCode = cmpEqual);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;





end.

