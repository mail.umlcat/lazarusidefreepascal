(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtype256s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 256 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemtype64s,
  umlcmemtype128s,
  dummy;

// ---

const

 MOD_umlcmemtype256s : TUMLCModule =
  ($09,$96,$1B,$C5,$97,$ED,$1D,$41,$BA,$64,$F7,$05,$F0,$E7,$DC,$92);

// ---

(* "namespaced" redefinitions *)

type

 umlcmemtype256s__umlcmemtype_256 = (* alias of *) umlcmemtype_256;

(* global functions *)

function IsEmpty
  (const ASource: umlcmemtype_256): Boolean; overload;

procedure Min
  (out   ADest: umlcmemtype_256;
   const A:     umlcmemtype_256;
   const B:     umlcmemtype_256); overload;

procedure Max
  (out   ADest: umlcmemtype_256;
   const A:     umlcmemtype_256;
   const B:     umlcmemtype_256); overload;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_256); overload;

procedure Encode64To256
  (out   ADest: umlcmemtype_256;
   const AA:    umlcmemtype_64;
   const AB:    umlcmemtype_64;
   const AC:    umlcmemtype_64;
   const AD:    umlcmemtype_64);

procedure Encode128To256
  (out   ADest: umlcmemtype_256;
   const AA:    umlcmemtype_128;
   const AB:    umlcmemtype_128);

procedure Decode256To64
  (out   AA:      umlcmemtype_64;
   out   AB:      umlcmemtype_64;
   out   AC:      umlcmemtype_64;
   out   AD:      umlcmemtype_64;
   const ASource: umlcmemtype_256);

procedure Decode256To128
  (out   AA:      umlcmemtype_128;
   out   AB:      umlcmemtype_128;
   const ASource: umlcmemtype_256);

procedure AssignMem8
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_8);

procedure AssignMem16
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_16);

procedure AssignMem32
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_32);

procedure AssignMem64
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_64);

procedure AssignMem128
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_128);

(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_256); overload; // operator :=

function Equal
  (const A, B: umlcmemtype_256): Boolean; overload; // operator =
function Different
  (const A, B: umlcmemtype_256): Boolean; overload; // operator <>

function NotOp
  (const AValue: umlcmemtype_256): umlcmemtype_256; overload; // operator not
function NegTwoOp
  (const AValue: umlcmemtype_256): umlcmemtype_256; overload; // operator negtwo

function ShlOp
  (const AValue: umlcmemtype_256;
   const AShift: umlcmemtype_256): umlcmemtype_256; overload; // operator xor
function ShrOp
  (const AValue: umlcmemtype_256;
   const AShift: umlcmemtype_256): umlcmemtype_256; overload; // operator xor

function AndOp
  (const A, B: umlcmemtype_256): umlcmemtype_256; overload; // operator and
function OrOp
  (const A, B: umlcmemtype_256): umlcmemtype_256; overload; // operator or
function XorOp
  (const A, B: umlcmemtype_256): umlcmemtype_256; overload; // operator xor

procedure Substract
  (out   ADest: umlcmemtype_256;
   const A, B:  umlcmemtype_256);

implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmemtype_256): Boolean;
var Match: Boolean; I: Cardinal; P: PByte;
begin
  Match := true; I := sizeof(ASource);
  P := PByte(@ASource);

  while ((Match) and (I > 0)) do
  begin
    Match :=
      (P^ = 0);
    Dec(I);
    Inc(p);
  end;

  Result :=
    Match;
end;

procedure Min
  (out   ADest: umlcmemtype_256;
   const A:     umlcmemtype_256;
   const B:     umlcmemtype_256);
begin
  //ADest := 0;
  // @toxdo: ...
end;

procedure Max
  (out   ADest: umlcmemtype_256;
   const A:     umlcmemtype_256;
   const B:     umlcmemtype_256);
begin
  //ADest := 0;
  // @toxdo: ...
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_256);
begin
  System.FillByte(ADest, sizeof(umlcmemtype_256), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

procedure Encode64To256
  (out   ADest: umlcmemtype_256;
   const AA:    umlcmemtype_64;
   const AB:    umlcmemtype_64;
   const AC:    umlcmemtype_64;
   const AD:    umlcmemtype_64);
begin
  umlcmemtype256s.Clear(ADest);
  System.Move(AA, ADest.Value[0], sizeof(umlcmemtype_64));
  System.Move(AB, ADest.Value[1], sizeof(umlcmemtype_64));
  System.Move(AC, ADest.Value[2], sizeof(umlcmemtype_64));
  System.Move(AD, ADest.Value[3], sizeof(umlcmemtype_64));
end;

procedure Encode128To256
  (out   ADest: umlcmemtype_256;
   const AA:    umlcmemtype_128;
   const AB:    umlcmemtype_128);
begin
  umlcmemtype256s.Clear(ADest);
  System.Move(AA, ADest.Value[0], sizeof(umlcmemtype_128));
  System.Move(AB, ADest.Value[1], sizeof(umlcmemtype_128));
end;

procedure Decode256To64
  (out   AA:      umlcmemtype_64;
   out   AB:      umlcmemtype_64;
   out   AC:      umlcmemtype_64;
   out   AD:      umlcmemtype_64;
   const ASource: umlcmemtype_256);
begin
  AA := 0;
  AB := 0;
  AC := 0;
  AD := 0;
  System.Move(ASource.Value[0], AA, sizeof(umlcmemtype_64));
  System.Move(ASource.Value[1], AB, sizeof(umlcmemtype_64));
  System.Move(ASource.Value[2], AC, sizeof(umlcmemtype_64));
  System.Move(ASource.Value[3], AD, sizeof(umlcmemtype_64));
end;

procedure Decode256To128
  (out   AA:      umlcmemtype_128;
   out   AB:      umlcmemtype_128;
   const ASource: umlcmemtype_256);
begin
  umlcmemtype128s.Clear(AA);
  umlcmemtype128s.Clear(AB);
  System.Move(ASource.Value[0], AA, sizeof(umlcmemtype_128));
  System.Move(ASource.Value[1], AB, sizeof(umlcmemtype_128));
end;

procedure AssignMem8
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_8);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem16
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_16);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem32
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_32);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem64
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_64);
begin
  Clear(ADest);
  // @toxdo: ...
end;

procedure AssignMem128
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_128);
begin
  Clear(ADest);
  // @toxdo: ...
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_256;
   const ASource: umlcmemtype_256);
begin
  System.Move(ASource, ADest, sizeof(umlcmemtype_256));
end;

function Equal
  (const A, B: umlcmemtype_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcmemtype_256));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcmemtype_256): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcmemtype_256));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function NotOp
  (const AValue: umlcmemtype_256): umlcmemtype_256;
begin
  Clear(Result);
  // @toxdo: ...
end;

function NegTwoOp
  (const AValue: umlcmemtype_256): umlcmemtype_256;
begin
  Clear(Result);
  // @toxdo: ...
end;

function ShlOp
  (const AValue: umlcmemtype_256;
   const AShift: umlcmemtype_256): umlcmemtype_256;
begin
  Clear(Result);
  // @toxdo: ...
end;

function ShrOp
  (const AValue: umlcmemtype_256;
   const AShift: umlcmemtype_256): umlcmemtype_256;
begin
  Clear(Result);
  // @toxdo: ...
end;

function AndOp
  (const A, B: umlcmemtype_256): umlcmemtype_256;
begin
  Clear(Result);
  // @toxdo: ...
end;

function OrOp
  (const A, B: umlcmemtype_256): umlcmemtype_256;
begin
  Clear(Result);
  // @toxdo: ...
end;

function XorOp
  (const A, B: umlcmemtype_256): umlcmemtype_256;
begin
  Clear(Result);
  // @toxdo: ...
end;

procedure Substract
  (out   ADest: umlcmemtype_256;
   const A, B:  umlcmemtype_256);
begin
  Clear(ADest);
  // @toxdo: ...
end;



end.

