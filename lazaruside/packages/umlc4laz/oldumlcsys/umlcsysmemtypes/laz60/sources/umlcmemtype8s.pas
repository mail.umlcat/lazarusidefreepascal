(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtype8s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 8 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  dummy;

// ---

const

 MOD_umlcmemtype8s : TUMLCModule =
  ($C7,$43,$52,$95,$83,$19,$6A,$44,$89,$D0,$F1,$54,$F8,$66,$C6,$7A);

// ---

(* "namespaced" redefinitions *)

type

  umlcmemtype8s__umlcmemtype_8 = (* alias of *) umlcmemtype_8;

(* global functions *)

function IsEmpty
  (const ASource: umlcmemtype_8): Boolean; overload;

function Min
  (const A: umlcmemtype_8;
   const B: umlcmemtype_8): umlcmemtype_8; overload;

function Max
  (const A: umlcmemtype_8;
   const B: umlcmemtype_8): umlcmemtype_8; overload;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_8); overload;

(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_8;
   const ASource: umlcmemtype_8); overload; // operator :=

function Equal
  (const A, B: umlcmemtype_8): Boolean; overload; // operator =
function Different
  (const A, B: umlcmemtype_8): Boolean; overload; // operator <>

function NotOp
  (const AValue: umlcmemtype_8): umlcmemtype_8; overload; // operator not
function NegTwoOp
  (const AValue: umlcmemtype_8): umlcmemtype_8; overload; // operator negtwo

function ShlOp
  (const AValue: umlcmemtype_8;
   const AShift: umlcmemtype_8): umlcmemtype_8; overload; // operator xor
function ShrOp
  (const AValue: umlcmemtype_8;
   const AShift: umlcmemtype_8): umlcmemtype_8; overload; // operator xor

function AndOp
  (const A, B: umlcmemtype_8): umlcmemtype_8; overload; // operator and
function OrOp
  (const A, B: umlcmemtype_8): umlcmemtype_8; overload; // operator or
function XorOp
  (const A, B: umlcmemtype_8): umlcmemtype_8; overload; // operator xor

implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmemtype_8): Boolean;
begin
  Result :=
    (umlcmemtype_8(ASource) = 0);
end;

function Min
  (const A: umlcmemtype_8;
   const B: umlcmemtype_8): umlcmemtype_8;
begin
  if (A < B)
    then Result := A
    else Result := B;
end;

function Max
  (const A: umlcmemtype_8;
   const B: umlcmemtype_8): umlcmemtype_8;
begin
  if (A > B)
    then Result := A
    else Result := B;
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_8);
begin
  ADest := 0;
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_8;
   const ASource: umlcmemtype_8);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcmemtype_8): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcmemtype_8): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function NotOp
  (const AValue: umlcmemtype_8): umlcmemtype_8;
begin
  Result := (Not AValue);
end;

function NegTwoOp
  (const AValue: umlcmemtype_8): umlcmemtype_8;
begin
  Result := 0;
  // @toxdo: ...
end;

function ShlOp
  (const AValue: umlcmemtype_8;
   const AShift: umlcmemtype_8): umlcmemtype_8;
begin
  Result := (AValue shl AShift);
end;

function ShrOp
  (const AValue: umlcmemtype_8;
   const AShift: umlcmemtype_8): umlcmemtype_8;
begin
  Result := (AValue shr AShift);
end;

function AndOp
  (const A, B: umlcmemtype_8): umlcmemtype_8;
begin
  Result := (A and B);
end;

function OrOp
  (const A, B: umlcmemtype_8): umlcmemtype_8;
begin
  Result := (A or B);
end;

function XorOp
  (const A, B: umlcmemtype_8): umlcmemtype_8;
begin
  Result := (A xor B);
end;



end.

