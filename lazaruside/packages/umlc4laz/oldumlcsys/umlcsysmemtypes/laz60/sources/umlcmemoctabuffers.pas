(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemoctabuffers;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in words segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  umlcmemoctas,
  dummy;

// ---

const

  MOD_umlcmemoctabuffers: TUMLCModule =
    ($9D,$F5,$AD,$E5,$52,$BD,$77,$4E,$BB,$46,$5D,$84,$FA,$3D,$CA,$77);

// ---


implementation




end.

