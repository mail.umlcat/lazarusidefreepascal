(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcmemtype16s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations to handle memory in 16 bits segments.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcmemtypes,
  dummy;
// ---

const

 MOD_umlcmemtype16s : TUMLCModule =
  ($75,$EE,$1F,$1D,$8B,$E1,$5B,$4E,$AA,$0D,$C6,$7D,$74,$F9,$73,$82);

// ---

(* "namespaced" redefinitions *)

type

 umlcmemtype16s__umlcmemtype_16 = (* alias of *) umlcmemtypes__umlcmemtype_16;

(* global functions *)

function IsEmpty
  (const ASource: umlcmemtype_16): Boolean; overload;

function Min
  (const A: umlcmemtype_16;
   const B: umlcmemtype_16): umlcmemtype_16; overload;

function Max
  (const A: umlcmemtype_16;
   const B: umlcmemtype_16): umlcmemtype_16; overload;

(* global procedures *)

  procedure Clear
    (out ADest: umlcmemtype_16); overload;

(* global functions *)

  function NewItem
    (): Pointer; overload;
  function NewArray
    (const ACount: Cardinal): Pointer; overload;

(* global procedures *)

  procedure ClearArray
    (var   AValue: Pointer;
	 const ACount: Cardinal); overload;

  procedure DropItem
    (var AValue: Pointer); overload;

  procedure DropArray
    (var   AValue: Pointer;
     const ACount: Cardinal); overload;
	 
(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_16;
   const ASource: umlcmemtype_16); overload; // operator :=

function Equal
  (const A, B: umlcmemtype_16): Boolean; overload; // operator =
function Different
  (const A, B: umlcmemtype_16): Boolean; overload; // operator <>

function NotOp
  (const AValue: umlcmemtype_16): umlcmemtype_16; overload; // operator not
function NegTwoOp
  (const AValue: umlcmemtype_16): umlcmemtype_16; overload; // operator negtwo

function ShlOp
  (const AValue: umlcmemtype_16;
   const AShift: umlcmemtype_16): umlcmemtype_16; overload; // operator xor
function ShrOp
  (const AValue: umlcmemtype_16;
   const AShift: umlcmemtype_16): umlcmemtype_16; overload; // operator xor

function AndOp
  (const A, B: umlcmemtype_16): umlcmemtype_16; overload; // operator and
function OrOp
  (const A, B: umlcmemtype_16): umlcmemtype_16; overload; // operator or
function XorOp
  (const A, B: umlcmemtype_16): umlcmemtype_16; overload; // operator xor

implementation

(* global standard functions *)

function IsEmpty
  (const ASource: umlcmemtype_16): Boolean;
begin
  Result :=
    (ASource = 0);
end;

function Min
  (const A: umlcmemtype_16;
   const B: umlcmemtype_16): umlcmemtype_16;
begin
  if (A < B)
    then Result := A
    else Result := B;
end;

function Max
  (const A: umlcmemtype_16;
   const B: umlcmemtype_16): umlcmemtype_16;
begin
  if (A > B)
    then Result := A
    else Result := B;
end;

(* global procedures *)

procedure Clear
  (out ADest: umlcmemtype_16);
begin
  ADest := 0;
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;




(* global functions *)

function NewItem
  (): Pointer;
begin
  Result := nil;
  System.GetMem(Result, sizeof(umlcmemtype_16));
end;

function NewArray
  (const ACount: Cardinal): Pointer;
var ASize: Cardinal;
begin
  Result := nil;
  ASize := sizeof(umlcmemtype_16) * ACount;
  System.GetMem(Result, ASize);
end;

(* global procedures *)

procedure ClearArray
  (var   AValue: Pointer;
   const ACount: Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(AValue)) then
  begin
    ASize := sizeof(umlcmemtype_16) * ACount; 
    System.FillByte(AValue^, ASize, 0);
  end;
end;

procedure DropItem
  (var AValue: Pointer);
begin
  if (Assigned(AValue)) then
  begin
    System.FreeMem(AValue, sizeof(umlcmemtype_16));
  end;
end;

procedure DropArray
  (var   AValue: Pointer;
   const ACount: Cardinal);
var ASize: Cardinal;
begin
  if (Assigned(AValue)) then
  begin
    ASize := sizeof(umlcmemtype_16) * ACount; 
    System.FreeMem(AValue, ASize);
  end;
end;


(* global operators *)

procedure Assign
  (out   ADest:   umlcmemtype_16;
   const ASource: umlcmemtype_16);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcmemtype_16): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcmemtype_16): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function NotOp
  (const AValue: umlcmemtype_16): umlcmemtype_16;
begin
  Result := (Not AValue);
end;

function NegTwoOp
  (const AValue: umlcmemtype_16): umlcmemtype_16;
begin
  Result := 0;
  // @toxdo: ...
end;

function ShlOp
  (const AValue: umlcmemtype_16;
   const AShift: umlcmemtype_16): umlcmemtype_16;
begin
  Result := (AValue shl AShift);
end;

function ShrOp
  (const AValue: umlcmemtype_16;
   const AShift: umlcmemtype_16): umlcmemtype_16;
begin
  Result := (AValue shr AShift);
end;

function AndOp
  (const A, B: umlcmemtype_16): umlcmemtype_16;
begin
  Result := (A and B);
end;

function OrOp
  (const A, B: umlcmemtype_16): umlcmemtype_16;
begin
  Result := (A or B);
end;

function XorOp
  (const A, B: umlcmemtype_16): umlcmemtype_16;
begin
  Result := (A xor B);
end;





end.

