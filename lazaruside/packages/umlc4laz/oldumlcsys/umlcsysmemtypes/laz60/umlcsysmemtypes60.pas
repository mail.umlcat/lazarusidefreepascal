{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsysmemtypes60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcmembyte2dbuffers, umlcmembytebuffers, umlcmembytes, 
  umlcmemocta2dbuffers, umlcmemoctabuffers, umlcmemoctas, 
  umlcmemquad2Dbuffers, umlcmemquadbuffers, umlcmemquads, umlcmemtype128s, 
  umlcmemtype8s, umlcmemtypes, umlcmemword2dbuffers, umlcmemwordbuffers, 
  umlcmemwords, umlcsysmemtypes_rtti, umlcmemtype8byptrs, umlcmemtype16byptrs, 
  umlcmemtype128byptrs, umlcmemtype32byptrs, umlcmemtype256s, 
  umlcmemtype256byptrs, umlcmemtype64byptrs;

implementation

end.
