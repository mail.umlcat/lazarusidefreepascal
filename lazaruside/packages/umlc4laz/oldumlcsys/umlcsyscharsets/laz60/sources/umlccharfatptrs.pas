(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlccharfatptrs;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Implementation of a composed pointer with an offset.
 ** The character encoding is undefined.
 **************************************************************************
 **)

interface
uses
  SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdstrtypes,
  umlccharfatptrtypes,
  dummy;

// ---

const

 MOD_umlccharfatptrs : TUMLCModule =
   ($AD,$73,$32,$45,$49,$56,$6F,$46,$BA,$D8,$99,$C2,$37,$ED,$27,$24);

// ---

(* global functions *)

function Assigned
  (const ASourcePtr: umlccharfatptr): Boolean;

function PtrToFatPtr
  (const ASourcePtr: pchar): umlccharfatptr;

function DuplicateCopy
  (const ASourcePtr: umlccharfatptr): umlccharfatptr;

function Succ
  (const ASourcePtr: umlccharfatptr): umlccharfatptr;

function Pred
  (const ASourcePtr: umlccharfatptr): umlccharfatptr;

(* global procedures *)

procedure EncodeFatPtr
  (out   ADestFatPtr:  umlccharfatptr;
   const ASourcePtr:   pchar;
   const ASourceIndex: word);

procedure DecodeFatPtr
  (const ASourceFatPtr: umlccharfatptr;
   out   ADestPtr:      pchar;
   out   ADestIndex:    word);

procedure Inc
  (var ADestFatPtr: umlccharfatptr);

procedure Dec
  (var ADestFatPtr: umlccharfatptr);

(* global operators *)

procedure Assign
  (out   ADestFatPtr:   umlccharfatptr;
   const ASourceFatPtr: umlccharfatptr); // operator :=

function Equal
  (const A, B: umlccharfatptr): Boolean; // operator =

function Different
  (const A, B: umlccharfatptr): Boolean; // operator <>



implementation

(* global functions *)

function Assigned
  (const ASourcePtr: umlccharfatptr): Boolean;
begin
  Result :=
    (ASourcePtr <> nil) and
    (ASourcePtr^.Ptr <> nil);
end;

function PtrToFatPtr
  (const ASourcePtr: pchar): umlccharfatptr;
begin
  Result :=
    System.New(umlccharfatptr);
  Result^.Ptr := ASourcePtr;
  Result^.Idx := 0;
end;

function DuplicateCopy
  (const ASourcePtr: umlccharfatptr): umlccharfatptr;
begin
  Result :=
    System.New(umlccharfatptr);
  Result^.Ptr := ASourcePtr^.Ptr;
  Result^.Idx := ASourcePtr^.Idx;
end;

function Succ
  (const ASourcePtr: umlccharfatptr): umlccharfatptr;
begin
  Result :=
    umlccharfatptrs.DuplicateCopy(ASourcePtr);
  System.Inc(Result^.Ptr);
  System.Inc(Result^.Idx);
end;

function Pred
  (const ASourcePtr: umlccharfatptr): umlccharfatptr;
begin
  Result :=
    umlccharfatptrs.DuplicateCopy(ASourcePtr);
  System.Dec(Result^.Ptr);
  System.Dec(Result^.Idx);
end;

(* global procedures *)

procedure EncodeFatPtr
  (out   ADestFatPtr:  umlccharfatptr;
   const ASourcePtr:   pchar;
   const ASourceIndex: word);
begin
  ADestFatPtr :=
    System.New(umlccharfatptr);
  ADestFatPtr^.Ptr := ASourcePtr;
  ADestFatPtr^.Idx := ASourceIndex;
end;

procedure DecodeFatPtr
  (const ASourceFatPtr: umlccharfatptr;
   out   ADestPtr:      pchar;
   out   ADestIndex:    word);
begin
  ADestPtr   := ASourceFatPtr^.Ptr;
  ADestIndex := ASourceFatPtr^.Idx;
end;

procedure Inc
  (var ADestFatPtr: umlccharfatptr);
begin
  System.Inc(ADestFatPtr^.Ptr);
  System.Inc(ADestFatPtr^.Idx);
end;

procedure Dec
  (var ADestFatPtr: umlccharfatptr);
begin
  System.Dec(ADestFatPtr^.Ptr);
  System.Dec(ADestFatPtr^.Idx);
end;




(* global operators *)

procedure Assign
  (out   ADestFatPtr:   umlccharfatptr;
   const ASourceFatPtr: umlccharfatptr);
begin
  ADestFatPtr^.Ptr := ASourceFatPtr^.Ptr;
  ADestFatPtr^.Idx := ASourceFatPtr^.Idx;
end;

function Equal
  (const A, B: umlccharfatptr): Boolean;
begin
  Result :=
    (umlccharfatptrs.Assigned(A) and umlccharfatptrs.Assigned(B));
  if (Result) then
  begin
    Result :=
      ((A^.Ptr = B^.Ptr) and
       (A^.Idx = B^.Idx));
  end;
end;

function Different
  (const A, B: umlccharfatptr): Boolean;
begin
  Result :=
    (umlccharfatptrs.Assigned(A) and umlccharfatptrs.Assigned(B));
  if (Result) then
  begin
    Result :=
      ((A^.Ptr <> A^.Ptr) and
       (B^.Idx <> B^.Idx));
  end;
end;





end.

