unit umlcstdnullstrtypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To define and implement,
 ** several encoding and memory size, null character terminated,
 ** text (string) types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlcstdchartypes,
  dummy;

// ---

const

 MOD_umlcstdnullstrtypes : TUMLCModule =
   ($C2,$DA,$4C,$A8,$83,$D4,$A1,$44,$9D,$CA,$C6,$46,$1F,$96,$89,$3A);

// ---

(* global types *)


type
 ansinullstring = pansichar;

type
 widenullstring = pwidechar;

implementation

procedure UnitConstructor();
begin
  //
end;

procedure UnitDestructor();
begin
  //
end;

initialization
  //UnitConstructor();
finalization
  //UnitDestructor();
end.

