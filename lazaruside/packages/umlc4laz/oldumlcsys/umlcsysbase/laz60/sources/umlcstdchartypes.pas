(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdchartypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To define and implement,
 ** several encoding and memory size text (string) types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  dummy;

// ---

const

 MOD_umlcstdchartypes : TUMLCModule =
   ($2F,$A0,$60,$2A,$C2,$16,$A1,$42,$9E,$D9,$F4,$89,$12,$BB,$47,$82);

// ---

(* global types *)

type
  umlcansichar = (* alias of *) ansichar;

type
  umlctansichar = (* alias of *) ansichar;
  umlcpansichar = (* alias of *) pansichar;

type
  umlcwidechar  = (* alias of *) widechar;

type
  umlctwidechar = (* alias of *) umlcwidechar;
  umlcpwidechar = ^umlcwidechar;

type
  charset     = (* redefine *) type string;
  umlccharset = (* alias of *) charset;

type
  ansicharset     = (* redefine *) array[0..255] of ansichar;
  umlcansicharset = (* alias of *) ansicharset;

type
  widecharset     = (* redefine *) array[0..255] of umlcwidechar;
  umlcwidecharset = (* alias of *) widecharset;

(* global constants *)

const
  ansinullchar = #0;

const
  widenullchar = #0;


implementation




end.

