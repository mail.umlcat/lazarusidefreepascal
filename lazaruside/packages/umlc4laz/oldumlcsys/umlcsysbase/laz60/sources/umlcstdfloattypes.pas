(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdfloattypes;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** To implement numerical floating point / real types.
 **************************************************************************
 **)

interface

uses
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  dummy;

// ---

const

 MOD_umlcstdfloattypes : TUMLCModule =
   ($EB,$A3,$3F,$8F,$76,$8F,$D3,$43,$85,$24,$86,$F1,$2F,$C4,$34,$67);

// ---

(* global standard types *)

type

  umlcfloat_single   = (* alias of *) single;
  umlcfloat_double   = (* alias of *) double;
  umlcfloat_extended = (* alias of *) extended;
  umlcfloat_real     = (* alias of *) real;
  umlcfloat_currency = (* alias of *) currency;

type

  umlctfloat_single   = (* alias of *) umlcfloat_single;
  umlctfloat_double   = (* alias of *) umlcfloat_double;
  umlctfloat_real     = (* alias of *) umlcfloat_real;
  umlctfloat_extended = (* alias of *) umlcfloat_extended;
  umlctfloat_currency = (* alias of *) umlcfloat_currency;

type

  umlcpfloat_single   = ^umlcfloat_single;
  umlcpfloat_double   = ^umlcfloat_double;
  umlcpfloat_real     = ^umlcfloat_real;
  umlcpfloat_extended = ^umlcfloat_extended;
  umlcpfloat_currency = ^umlcfloat_currency;

  //type
  //
  //  umlchalffloat   = (* alias of *) byte;
  //  umlcsinglefloat = (* alias of *) single;
  //  umlcdoublefloat = (* alias of *) double;
  //  umlcquadfloat   = (* alias of *) qword;
  //  umlcoctafloat   = (* alias of *) packed record Value: array[0..15] of byte; end;
  //  umlclongfloat   = (* alias of *) packed record Value: array[0..31] of byte; end;
  //

  // Las funciones "Float" utilizan "Double",
  // porque el tipo "Float" no existe !!!
  // "TFloatField" y "TCurrencyField" usan el tipo "Double" !!!

  // "Float" functions use "Double",
  // because "Float" type doesn't exist !!!
  // "TFloatField" & "TCurrencyField" use the "Double" type !!!

type
  TDBFloat = Double;
  PDBFloat = ^Double;

type
  TDBCurrency = Double;
  PDBCurrency = ^Double;

type
  umlcdbfloat  = TDBFloat;
  umlctdbfloat = umlcdbfloat;
  umlcpdbfloat = ^umlcdbfloat;

type
  umlcdbcurrency  = TDBCurrency;
  umlctdbcurrency = umlcdbcurrency;
  umlcpdbcurrency = ^umlcdbcurrency;

implementation



end.

