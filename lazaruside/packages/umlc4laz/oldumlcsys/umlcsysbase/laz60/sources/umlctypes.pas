(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlccat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctypes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcuuids,
  dummy;

// ---

//const
//
// MOD_umlcuuidstextconv : TUMLCModule =
//   ($6D,$71,$C8,$26,$FB,$85,$28,$4E,$90,$92,$72,$F5,$7C,$D3,$4B,$0B);

// ---

type
  TUMLCType =
    type TUUID;


  // ---

const
  UnknownType : TUMLCType =
    ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) ;

  ID_UnknownType : TUMLCType =
    ( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ) ;

  // ---

  function IsEmptyType
    (const AType: TUMLCType): Boolean;
  function AreEqualTypes
    (const A, B: TUMLCType): Boolean;

  procedure ClearType
    (out AType: TUMLCType);
  procedure AssignType
    (out A: TUMLCType; const B: TUMLCType);

implementation

function IsEmptyType
  (const AType: TUMLCType): Boolean;
begin
  Result :=
     IsEmptyUUID(AType);
end;

function AreEqualTypes
  (const A, B: TUMLCType): Boolean;
begin
  Result :=
    (System.CompareByte(A, B, SizeOf(TUMLCType)) = 0);
end;

procedure ClearType
  (out AType: TUMLCType);
begin
  System.FillByte((* var *) AType, sizeof(TUMLCType), 0);
end;

procedure AssignType
  (out A: TUMLCType; const B: TUMLCType);
begin
  System.Move((* const *) B, (* var *) A, sizeof(TUMLCType));
end;

//initialization
  //
end.

