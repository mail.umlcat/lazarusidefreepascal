unit umlcuuidbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlcdebug,
  umlcuuids,
  dummy;

// ---

const

  MOD_umlcuuidbyptrs : TUMLCModule =
    ($B5,$F2,$03,$02,$36,$19,$70,$47,$9F,$E3,$59,$E9,$44,$01,$6D,$62);

// ---


(* global functions *)

function ConstToPtr
  (const AValue: TUUID): pointer;

procedure DropPtr
  (var ADestPtr: pointer);


implementation

(* global functions *)

function ConstToPtr
  (const AValue: TUUID): pointer;
var P: puuid;
begin
  System.GetMem(P, sizeof(TUUID));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(TUUID));
end;



end.

