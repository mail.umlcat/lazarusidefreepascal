{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcsysuints60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcsysuints__rtti, umlccardbyptrs, umlcuint8byptrs, umlcuint8s, 
  umlcuint16byptrs, umlcuint16s, umlcuint32byptrs, umlcuint32s, 
  umlcuint64byptrs, umlcuint64s, umlcuint128byptrs, umlcuint128s, 
  umlcuint256byptrs, umlcuint256s, umlcuint512byptrs, umlcuint512s, 
  umlcuint8buffers;

implementation

end.
