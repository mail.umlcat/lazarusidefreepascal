unit umlcuint64byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint64ptrs : TUMLCModule =
   ($B8,$9D,$03,$7A,$F7,$BA,$D2,$44,$AE,$F6,$CB,$B2,$01,$C3,$3A,$8E);

// ---

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;

function ConstToPtr
 (const AValue: umlcuint_64): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;
var P: umlcpuint_64 absolute ASource;
begin
  Result := (P <> nil);
  if (Result) then
  begin
    Result := (P^ = 0);
  end;
end;

function ConstToPtr
  (const AValue: umlcuint_64): pointer;
var P: umlcpuint_64;
begin
  System.GetMem(P, sizeof(umlcuint_64));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcuint_64));
end;



end.

