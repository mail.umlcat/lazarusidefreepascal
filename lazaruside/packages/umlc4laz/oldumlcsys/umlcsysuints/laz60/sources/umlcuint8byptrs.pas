unit umlcuint8byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint8s : TUMLCModule =
  ($D1,$59,$02,$C2,$6D,$44,$94,$4A,$87,$B6,$1E,$AF,$2F,$BC,$ED,$77);

// ---


(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;

function ConstToPtr
  (const AValue: umlcuint_8): pointer;

procedure DropPtr
  (var ADestPtr: pointer);

implementation

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;
var P: umlcpuint_8 absolute ASource;
begin
  Result := (P <> nil);
  if (Result) then
  begin
    Result := (P^ = 0);
  end;
end;

function ConstToPtr
  (const AValue: umlcuint_8): pointer;
var P: umlcpuint_8;
begin
  System.GetMem(P, sizeof(umlcuint_8));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcuint_8));
end;


end.

