unit umlcuint512byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint512ptrs : TUMLCModule =
   ($5D,$36,$CF,$60,$AB,$1B,$13,$48,$B2,$06,$2D,$0A,$F8,$EA,$35,$6D);

// ---

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;

function ConstToPtr
 (const AValue: umlcuint_512): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;
var P: umlcpuint_512 absolute ASource;
begin
  Result := (P <> nil);
  if (Result) then
  begin
    //Result := (P^ = 0);
  end;
end;

function ConstToPtr
  (const AValue: umlcuint_512): pointer;
var P: umlcpuint_512;
begin
  System.GetMem(P, sizeof(umlcuint_512));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcuint_512));
end;



end.

