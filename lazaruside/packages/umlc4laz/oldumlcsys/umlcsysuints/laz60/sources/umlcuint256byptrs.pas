unit umlcuint256byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint256ptrs : TUMLCModule =
   ($DA,$6D,$DF,$4B,$69,$83,$D5,$4F,$98,$E8,$5A,$D0,$38,$0C,$0C,$F1);

// ---

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;

function ConstToPtr
 (const AValue: umlcuint_256): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;
var P: umlcpuint_256 absolute ASource;
begin
  Result := (P <> nil);
  if (Result) then
  begin
    //Result := (P^ = 0);
  end;
end;

function ConstToPtr
  (const AValue: umlcuint_256): pointer;
var P: umlcpuint_256;
begin
  System.GetMem(P, sizeof(umlcuint_256));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcuint_256));
end;



end.

