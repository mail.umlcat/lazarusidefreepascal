unit umlcuint512s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 512 bits unsigned integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  umlcuint64s,
  umlcuint128s,
  umlcuint256s,
  dummy;

// ---

const

 MOD_umlcuint512s : TUMLCModule =
   ($88,$A5,$03,$6E,$DD,$A4,$D3,$44,$AE,$90,$C5,$60,$80,$4A,$2D,$A1);

// ---

(* global functions *)





(* global procedures *)

procedure Clear
  (out ADest: umlcuint_512); overload;

procedure Encode128To512
  (out   ADest: umlcuint_512;
   const AA:    umlcuint_128;
   const AB:    umlcuint_128;
   const AC:    umlcuint_128;
   const AD:    umlcuint_128);

procedure Encode256To512
  (out   ADest: umlcuint_512;
   const AA:    umlcuint_256;
   const AB:    umlcuint_256);

procedure Decode512To128
  (out   AA:      umlcuint_128;
   out   AB:      umlcuint_128;
   out   AC:      umlcuint_128;
   out   AD:      umlcuint_128;
   const ASource: umlcuint_512);

procedure Decode512To256
  (out   AA:      umlcuint_256;
   out   AB:      umlcuint_256;
   const ASource: umlcuint_512);

(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_512;
   const ASource: umlcuint_512); overload; // operator :=

function Equal
  (const A, B: umlcuint_512): Boolean; overload; // operator =
function Different
  (const A, B: umlcuint_512): Boolean; overload; // operator <>
function Greater
  (const A, B: umlcuint_512): Boolean; overload; // operator >
function Lesser
  (const A, B: umlcuint_512): Boolean; overload; // operator <
function GreaterEqual
  (const A, B: umlcuint_512): Boolean; overload; // operator >=
function LesserEqual
  (const A, B: umlcuint_512): Boolean; overload; // operator <=

implementation

(* global functions *)




(* global procedures *)

procedure Clear
  (out ADest: umlcuint_512);
begin
  System.FillByte(ADest, sizeof(umlcuint_512), 0);
  // Goal: Clear a value.
  // Objetivo: Limpia un valor.
end;

procedure Encode128To512
  (out   ADest: umlcuint_512;
   const AA:    umlcuint_128;
   const AB:    umlcuint_128;
   const AC:    umlcuint_128;
   const AD:    umlcuint_128);
begin
  umlcuint512s.Clear(ADest);
  System.Move(AA, ADest.Value[0], sizeof(umlcuint_128));
  System.Move(AB, ADest.Value[2], sizeof(umlcuint_128));
  System.Move(AC, ADest.Value[4], sizeof(umlcuint_128));
  System.Move(AD, ADest.Value[6], sizeof(umlcuint_128));
end;

procedure Encode256To512
  (out   ADest: umlcuint_512;
   const AA:    umlcuint_256;
   const AB:    umlcuint_256);
begin
  umlcuint512s.Clear(ADest);
  System.Move(AA, ADest.Value[0], sizeof(umlcuint_256));
  System.Move(AB, ADest.Value[1], sizeof(umlcuint_256));
end;

procedure Decode512To128
  (out   AA:      umlcuint_128;
   out   AB:      umlcuint_128;
   out   AC:      umlcuint_128;
   out   AD:      umlcuint_128;
   const ASource: umlcuint_512);
begin
  umlcuint128s.Clear(AA);
  umlcuint128s.Clear(AB);
  umlcuint128s.Clear(AC);
  umlcuint128s.Clear(AD);
  System.Move(ASource.Value[0], AA, sizeof(umlcuint_128));
  System.Move(ASource.Value[2], AB, sizeof(umlcuint_128));
  System.Move(ASource.Value[4], AC, sizeof(umlcuint_128));
  System.Move(ASource.Value[6], AD, sizeof(umlcuint_128));
end;

procedure Decode512To256
  (out   AA:      umlcuint_256;
   out   AB:      umlcuint_256;
   const ASource: umlcuint_512);
begin
  umlcuint256s.Clear(AA);
  umlcuint256s.Clear(AB);
  System.Move(ASource.Value[0], AA, sizeof(umlcuint_256));
  System.Move(ASource.Value[1], AB, sizeof(umlcuint_256));
end;

(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_512;
   const ASource: umlcuint_512);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcuint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_512));
  Result :=
    (ErrorCode = cmpEqual);
  // Goal: Returns if 2 values are equal.
  // Objetivo: Regresa si 2 valores son iguales.
end;

function Different
  (const A, B: umlcuint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_512));
  Result :=
    (ErrorCode <> cmpEqual);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Greater
  (const A, B: umlcuint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_512));
  Result :=
    (ErrorCode <> cmpHigher);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function Lesser
  (const A, B: umlcuint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_512));
  Result :=
    (ErrorCode <> cmpLower);
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function GreaterEqual
  (const A, B: umlcuint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_512));
  Result :=
    ((ErrorCode = cmpHigher) or (ErrorCode = cmpEqual));
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;

function LesserEqual
  (const A, B: umlcuint_512): Boolean;
var ErrorCode: umlctcomparison;
begin
  ErrorCode :=
    System.CompareByte(A, B, sizeof(umlcuint_512));
  Result :=
    ((ErrorCode = cmpLower) or (ErrorCode = cmpEqual));
  // Goal: Returns if 2 values are different.
  // Objetivo: Regresa 2 valores son diferentes.
end;





end.

