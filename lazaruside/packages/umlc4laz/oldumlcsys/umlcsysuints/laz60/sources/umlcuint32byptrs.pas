unit umlcuint32byptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint32ptrs : TUMLCModule =
   ($B0,$69,$46,$C8,$80,$86,$D8,$40,$B9,$DE,$72,$C8,$A9,$9F,$49,$AB);

// ---

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;

function ConstToPtr
 (const AValue: umlcuint_32): pointer;

procedure DropPtr
 (var ADestPtr: pointer);

implementation

(* global functions *)

function IsEmpty
  (const ASource: pointer): boolean;
var P: umlcpuint_32 absolute ASource;
begin
  Result := (P <> nil);
  if (Result) then
  begin
    Result := (P^ = 0);
  end;
end;

function ConstToPtr
  (const AValue: umlcuint_32): pointer;
var P: umlcpuint_32;
begin
  System.GetMem(P, sizeof(umlcuint_32));
  P^ := AValue;
  Result := P;
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  System.FreeMem(ADestPtr, sizeof(umlcuint_32));
end;



end.

