unit umlcuint16s;

{$mode objfpc}{$H+}

interface

(**
 **************************************************************************
 ** Description:
 ** Operations for 16 bits unsigned integers.
 **************************************************************************
 **)

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons, umlcuinttypes,
  dummy;

// ---

const

 MOD_umlcuint16s : TUMLCModule =
   ($CB,$FA,$32,$DD,$C5,$1B,$94,$4E,$A3,$93,$FD,$04,$CF,$CB,$2B,$54);

// ---

(* global functions *)





(* global procedures *)



(* global operators *)

  procedure Assign
    (out   ADest:   umlcuint_16;
     const ASource: umlcuint_16); overload; // operator :=

  function Equal
    (const A, B: umlcuint_16): Boolean; overload; // operator =
  function Different
    (const A, B: umlcuint_16): Boolean; overload; // operator <>
  function Greater
    (const A, B: umlcuint_16): Boolean; overload; // operator >
  function Lesser
    (const A, B: umlcuint_16): Boolean; overload; // operator <
  function GreaterEqual
    (const A, B: umlcuint_16): Boolean; overload; // operator >=
  function LesserEqual
    (const A, B: umlcuint_16): Boolean; overload; // operator <=

implementation

(* global functions *)




(* global procedures *)



(* global operators *)

procedure Assign
  (out   ADest:   umlcuint_16;
   const ASource: umlcuint_16);
begin
  ADest := ASource;
end;

function Equal
  (const A, B: umlcuint_16): Boolean;
begin
  Result :=
    (A = B);
  // Goal: Returns if 2 characters are equal.
  // Objetivo: Regresa si 2 caracteres son iguales.
end;

function Different
  (const A, B: umlcuint_16): Boolean;
begin
  Result := (A <> B);
  // Goal: Returns if "A <> B".
  // Objetivo: Regresa si "A <> B".
end;

function Greater
  (const A, B: umlcuint_16): Boolean;
begin
  Result := (A > B);
  // Goal: Returns if "A > B".
  // Objetivo: Regresa si "A > B".
end;

function Lesser
  (const A, B: umlcuint_16): Boolean;
begin
  Result := (A < B);
  // Goal: Returns if "A < B".
  // Objetivo: Regresa si "A < B".
end;

function GreaterEqual
  (const A, B: umlcuint_16): Boolean;
begin
  Result := (A >= B);
  // Goal: Returns if "A >= B".
  // Objetivo: Regresa si "A >= B".
end;

function LesserEqual
  (const A, B: umlcuint_16): Boolean;
begin
  Result := (A <= B);
  // Goal: Returns if "A <= B".
  // Objetivo: Regresa si "A <= B".
end;





end.

