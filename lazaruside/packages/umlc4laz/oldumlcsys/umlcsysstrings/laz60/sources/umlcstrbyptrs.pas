unit umlcstrbyptrs;

{$mode objfpc}{$H+}

interface

uses
  SysConst, SysUtils, Math,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstrings,
  dummy;

// ---

const

 MOD_umlcstrptrs : TUMLCModule =
   ($39,$53,$15,$F7,$E4,$B8,$0A,$43,$AA,$B0,$1F,$E8,$24,$A0,$EC,$7F);

// ---

(* global functions *)

function ConstToPtr
  (const AValue: string): pointer;

procedure DropPtr
  (var ADestPtr: pointer);

implementation

(* global functions *)

function ConstToPtr
  (const AValue: string): pointer;
begin
  Result :=
    SysUtils.NewStr(AValue);
end;

procedure DropPtr
  (var ADestPtr: pointer);
begin
  SysUtils.DisposeStr(PString(ADestPtr));
end;


end.

