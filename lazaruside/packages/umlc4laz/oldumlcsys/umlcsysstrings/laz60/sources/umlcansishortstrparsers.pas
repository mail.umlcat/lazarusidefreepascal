(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansishortstrparsers;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Quick Text Search and Parsing operations,
 ** for FreePascal's Default pascal 256 items array strings.
 ** The character encoding is A.N.S.I.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcansichars,
  umlcansicharsets,
  umlcansicharsetconsts,
  umlcansishortstrings,
  dummy;

// ---

const

  MOD_umlcansishortstrparsers : TUMLCModule =
    ($4E,$FA,$E4,$F5,$9A,$76,$2A,$43,$AF,$D8,$CD,$A0,$5A,$BC,$CF,$55);

// ---

(* global functions *)

  function MatchesCharSet
    (const AHaystack: umlcansishortstring; const ValidChars: ansicharset): umlcansishortstring; overload;

  function IsStringInSet
    (const AHaystack: umlcansishortstring; const ValidChars: ansicharset): Boolean; overload;

  function IsWildcard
     (const ASource: umlcansishortstring): Boolean;

  function IsIdentifier
    (const ASource: umlcansishortstring): Boolean;

  function CharPosOfEqual
    (const S: umlcansishortstring; const SubStr: ansichar): Word;
  function CharPosOfEqualReverse
    (const S: umlcansishortstring; const SubStr: ansichar): Word;

  function CharPosOfSame
    (const S: umlcansishortstring; const SubStr: ansichar): Word;
  function CharPosOfSameReverse
    (const S: umlcansishortstring; const SubStr: ansichar): Word;

  function CharSetPosOfEqual
    (const S: umlcansishortstring; const Items: ansicharset): Word;
  function CharSetPosOfReverseEqual
    (const S: umlcansishortstring; const Items: ansicharset): Word;

  function CharSetPosOfSame
    (const S: umlcansishortstring; const Items: ansicharset): Word;
  function CharSetPosOfReverseSame
    (const S: umlcansishortstring; const Items: ansicharset): Word;

  function StrPosOfReverse
    (const SubStr, FullStr: umlcansishortstring): Word; overload;
  function StrPosOfSameReverse
    (const SubStr, FullStr: umlcansishortstring): Word; overload;

  function TryStrPosOf
    (const AHaystack:  umlcansishortstring;
     const ANeedleStr: umlcansishortstring;
     out   AIndex:     Word): Boolean;
  function TryStrPosOfSame
    (const AHaystack:  umlcansishortstring;
     const ANeedleStr: umlcansishortstring;
     out   AIndex:     Word): Boolean;

  function TryStrAtPosOf
    (const AHaystack:    umlcansishortstring;
     const ANeedle:      umlcansishortstring;
     const ASourceIndex: byte;
     out   ADestIndex:   byte): Boolean;

  function StrAtPosOf
   (const AHaystack: umlcansishortstring;
    const ANeedle:   umlcansishortstring;
    const AIndex:    byte): byte;

  function StrPosOf
    (const AHaystack:  umlcansishortstring;
     const ANeedleStr: umlcansishortstring): Word;
  function StrPosOfSame
    (const AHaystack:  umlcansishortstring;
     const ANeedleStr: umlcansishortstring): Word;

  function StartsWithEqualChar
    (const AHaystack: umlcansishortstring; const ANeedle: ansichar): Boolean;

  function StartsWithSameChar
    (const AHaystack: umlcansishortstring; const ANeedle: ansichar): Boolean;

  function StartsWithEqualStr
    (const AHaystack: umlcansishortstring; const ANeedleStr: umlcansishortstring): Boolean;

  function StartsWithSameStr
    (const AHaystack: umlcansishortstring; const ANeedleStr: umlcansishortstring): Boolean;

  function StartsWithEqualCharSet
    (const AHaystack: umlcansishortstring; const ANeedleSet: ansicharset): Boolean;

  function StartsWithSameCharSet
    (const AHaystack: umlcansishortstring; const ANeedleSet: ansicharset): Boolean;

  function FinishesWithEqualChar
    (const AHaystack: umlcansishortstring; const ANeedleChar: umlcansishortstring): Boolean;

  function FinishesWithSameChar
    (const AHaystack: umlcansishortstring; const ANeedleChar: umlcansishortstring): Boolean;

  function FinishesWithEqualCharSet
   (const AHaystack: umlcansishortstring; const ANeedleSet: ansicharset): Boolean;

  function FinishesWithSameCharSet
   (const AHaystack: umlcansishortstring; const ANeedleSet: ansicharset): Boolean;

  function FinishesWithEqualStr
    (const AHaystack: umlcansishortstring; const ANeedleStr: umlcansishortstring): Boolean;

  function FinishesWithSameStr
    (const AHaystack: umlcansishortstring; const ANeedleStr: umlcansishortstring): Boolean;

  function ReplaceCharByCharCopy
    (const AHaystack: umlcansishortstring; ASource, ADest: ansichar): umlcansishortstring; overload;
  function ReplaceCharSetByCharCopy
    (const AHaystack: umlcansishortstring; ASource: umlcansicharset; ADest: ansichar): umlcansishortstring; overload;

  function RemoveSingleCharCopy
   (const ASource: umlcansishortstring; IgnoreChar: ansichar): umlcansishortstring;
  function RemoveCharSetCopy
   (const ASource: umlcansishortstring; IgnoreChars: umlcansicharset): umlcansishortstring;

  function CharCountOf
    (const AHaystack: umlcansishortstring; ANeedleChar: ansichar): Byte;
  function CharSetCountOf
    (const AHaystack: umlcansishortstring; ANeedleSet: ansicharset): Byte;

  function ReplaceAllStrByStrCopy
    (const AHaystack: umlcansishortstring;
     const ASource:   umlcansishortstring;
     const ADest:     umlcansishortstring): umlcansishortstring;

  function ReplaceSingleStrByStrCopy
    (const AHaystack: umlcansishortstring;
     const ASource:   umlcansishortstring;
     const ADest:     umlcansishortstring): umlcansishortstring;

  function RemoveSingleStrCopy
    (const AHaystack: umlcansishortstring;
     const ANeedle:   umlcansishortstring): umlcansishortstring;

  function RemoveAllStrCopy
    (const AHaystack: umlcansishortstring;
     const ANeedle:   umlcansishortstring): umlcansishortstring;

  function ReplaceForwardCopy
    (const AHaystack, Source, Dest: umlcansishortstring): umlcansishortstring;
  function ReplaceForwardCopySame
    (const AHaystack, Source, Dest: umlcansishortstring): umlcansishortstring;

  function ReplaceBackwardCopy
    (const AHaystack, Source, Dest: umlcansishortstring): umlcansishortstring;
  function ReplaceBackwardCopySame
    (const AHaystack, Source, Dest: umlcansishortstring): umlcansishortstring;

  function ReplaceForwardALLCopy
    (const AHaystack, ASourceNeedle, ADestNeedle: umlcansishortstring): umlcansishortstring;
  function ReplaceBackwardALLCopy
    (const AHaystack, ASourceNeedle, ADestNeedle: umlcansishortstring): umlcansishortstring;

  procedure ReplaceForwardPos
    (var AHaystack: umlcansishortstring;
     var AIndex: Byte; const ASourceNeedle, ADestNeedle: umlcansishortstring);
  procedure ReplaceForwardPosSame
    (var AHaystack: umlcansishortstring;
     var AIndex: Byte; const ASourceNeedle, ADestNeedle: umlcansishortstring);

  procedure ReplaceBackwardPos
    (var AHaystack: umlcansishortstring;
     var AIndex: Byte; const ASourceNeedle, ADestNeedle: umlcansishortstring);
  procedure ReplaceBackwardPosSame
    (var AHaystack: umlcansishortstring;
     var AIndex: Byte; const ASourceNeedle, ADestNeedle: umlcansishortstring);

  function DeleteForwardCopy
    (const AHaystack, ANeedle: umlcansishortstring): umlcansishortstring;
  function DeleteBackwardCopy
    (const AHaystack, ANeedle: umlcansishortstring): umlcansishortstring;

  function DeleteALLCopy
    (const AHaystack, ANeedle: umlcansishortstring): umlcansishortstring;

  function CopyFrom
    (const ASource: umlcansishortstring;
     const AStart:  Byte;
     const AFinish: Byte): umlcansishortstring;

  function ParseFrom
    (const ASource: umlcansishortstring;
     var   AStart:  Byte;
     const AFinish: Byte): umlcansishortstring;


  function SkipChar
    (const ASource:    umlcansishortstring;
     const AIndex:     Byte;
     const AValidChar: ansichar): Byte;

  function SkipCharWhile
    (const ASource:    umlcansishortstring;
     const AIndex:     Byte;
     const AValidChar: ansichar): Byte;

  function SkipCharUntil
    (const ASource:   umlcansishortstring;
     const AIndex:    Byte;
     const BreakChar: ansichar): Byte;

  function SkipCharSetWhile
    (const ASource:     umlcansishortstring;
     const AIndex:      Byte;
     const AValidChars: ansicharset): Byte;

  function SkipCharSetUntil
    (const ASource:    umlcansishortstring;
     const AIndex:     Byte;
     const BreakChars: ansicharset): Byte;

  function SkipToken
    (const ASource:      umlcansishortstring;
     var   AStartIndex:  Byte;
     var   AFinishIndex: Byte): umlcansishortstring;

  function SkipDigit
    (const ASource:      umlcansishortstring;
     var   AStartIndex:  Byte;
     var   AFinishIndex: Byte): umlcansishortstring;

  function SkipLetter
    (const ASource:      umlcansishortstring;
     var   AStartIndex:  Byte;
     var   AFinishIndex: Byte): umlcansishortstring;

(* global procedures *)

  procedure ReplaceSingleStrByStrAt
    (var   AHaystack: umlcansishortstring;
     var   Index:     Byte;
     const ASource:   umlcansishortstring;
     const ADest:     umlcansishortstring);

  function CppEscToStr
    (const ASource: umlcansishortstring): umlcansishortstring;
  function StrToCppEsc
    (const ASource: umlcansishortstring): umlcansishortstring;

implementation

(* global functions *)

function MatchesCharSet
  (const AHaystack: umlcansishortstring; const ValidChars: ansicharset): umlcansishortstring;
var CanContinue: Boolean; AIndex, ALen: Word; Temp: umlcansishortstring;
begin
  umlcansishortstrings.Clear(Result);

  if (not umlcansishortstrings.IsEmpty(AHaystack)) then
  begin
    umlcansishortstrings.Clear(Temp);

    AIndex := 1;
    ALen   := umlcansishortstrings.Length(AHaystack);

    CanContinue := TRUE;
    while ((AIndex < ALen) and CanContinue) do
    begin
      CanContinue :=
        umlcansicharsets.IsMember(ValidChars, AHaystack[AIndex]);

      if (CanContinue) then
      begin
        Temp := Temp + AHaystack[AIndex];
      end;

      Inc(AIndex);
    end;

    if (CanContinue) then
    begin
      Result := Temp;
    end;
  end;
  // Goal: Moves a null terminated umlcansishortstring pointer,
  // as long as each character, matches the valid set.

  // Objetivo: Mueve un apuntador cadena terminada en nulo,
  // mientras cada caracter, coincide con el conjunto valido.
end;

function IsStringInSet
  (const AHaystack: umlcansishortstring; const ValidChars: ansicharset): Boolean;
begin
  Result :=
    (umlcansishortstrparsers.MatchesCharSet(AHaystack, ValidChars) <> AHaystack);
  // Goal: Returns if all the characters, a umlcansishortstring are valid.
  // Objetivo: Regresa si todos los caracteres en una cadena son validos.
end;

function IsWildcard
   (const ASource: umlcansishortstring): Boolean;
begin
  Result :=
    umlcansishortstrparsers.IsStringInSet(ASource, WildcardSet);
  // Goal: Returns if a umlcansishortstring is a wildcard.
  // Objetivo: Regresa si una cadena es un comodin.
end;

function IsIdentifier
  (const ASource: umlcansishortstring): Boolean;
var I, ACount: Integer;
begin
  Result := False;
  ACount :=
    umlcansishortstrings.Length(ASource);
  if (ACount = 0) or not umlcansicharsets.IsMember(AlphaSet, ASource[1])
    then Exit;
  for I := 2 to ACount do
  begin
    if (not umlcansicharsets.IsMember(IDSet, ASource[I]))
      then Exit;
  end;
  Result := TRUE;
  // Goal: To return if a umlcansishortstring is a valid identifier.
  // Objetivo: Regresar si una cadena es un identificador valido.
end;
function CharPosOfEqual
  (const S: umlcansishortstring; const SubStr: ansichar): Word;
var Len: Word; Found: Boolean;
begin
  Len :=
    Pred(umlcansishortstrings.Length(S));
  Result := 0; Found := FALSE;

  while ((not Found) and (Result < Len)) do
  begin
    Found :=
      umlcansichars.Equal(S[Result], SubStr);
    Inc(Result);
  end;

  if (Found)
    then Dec(Result)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena.

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharPosOfEqualReverse
  (const S: umlcansishortstring; const SubStr: ansichar): Word;
var Found: Boolean;
begin
  Result :=
    umlcansishortstrings.Length(S);
  Found  := FALSE;

  while ((not Found) and (Result > 0)) do
  begin
    Found :=
      umlcansichars.Equal(S[Result], SubStr);
    Dec(Result);
  end;

  if (Found)
    then Inc(Result)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el final de la cadena.

  // Goal: Returns the first location of the given character,
  // from the finish of the string.
end;

function CharPosOfSame
  (const S: umlcansishortstring; const SubStr: ansichar): Word;
var Len: Word; Found: Boolean;
begin
  Len :=
    Pred(umlcansishortstrings.Length(S));
  Result := 0; Found := FALSE;

  while ((not Found) and (Result < Len)) do
  begin
    Found :=
      umlcansichars.SameText(S[Result], SubStr);
    Inc(Result);
  end;

  if (Found)
    then Dec(Result)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena.

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharPosOfSameReverse
  (const S: umlcansishortstring; const SubStr: ansichar): Word;
var Found: Boolean;
begin
  Result :=
    umlcansishortstrings.Length(S);
  Found  := FALSE;

  while ((not Found) and (Result > 0)) do
  begin
    Found :=
      umlcansichars.SameText(S[Result], SubStr);
    Dec(Result);
  end;

  if (Found)
    then Inc(Result)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el final de la cadena.

  // Goal: Returns the first location of the given character,
  // from the finish of the string.
end;

function CharSetPosOfEqual
  (const S: umlcansishortstring; const Items: ansicharset): Word;
var AIndex, Len: Word; Found: Boolean;
begin
  Found := FALSE;
  Len   := System.Length(S);
  AIndex := 1;

  while ((AIndex <= Len) and (not Found)) do
  begin
    Found :=
      umlcansicharsets.IsMember(Items, S[AIndex]);
    Inc(AIndex);
  end;

  if (Found)
    then Result := Pred(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharSetPosOfReverseEqual
  (const S: umlcansishortstring; const Items: ansicharset): Word;
var AIndex: Word; Found: Boolean;
begin
  Found := FALSE;
  AIndex := System.Length(S);

  while ((AIndex > 0) and (not Found)) do
  begin
    Found :=
      umlcansicharsets.IsMember(Items, S[AIndex]);
    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;
function CharSetPosOfSame
  (const S: umlcansishortstring; const Items: ansicharset): Word;
var AIndex, Len: Word; Found: Boolean;
begin
  Found := FALSE;
  Len   := System.Length(S);
  AIndex := 1;

  while ((AIndex <= Len) and (not Found)) do
  begin
    Found :=
      umlcansicharsets.IsSameMember(Items, S[AIndex]);
    Inc(AIndex);
  end;

  if (Found)
    then Result := Pred(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function CharSetPosOfReverseSame
  (const S: umlcansishortstring; const Items: ansicharset): Word;
var AIndex: Word; Found: Boolean;
begin
  Found := FALSE;
  AIndex := System.Length(S);

  while ((AIndex > 0) and (not Found)) do
  begin
    Found :=
      umlcansicharsets.IsSameMember(Items, S[AIndex]);
    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa la primera posicion del caracter dado,
  // desde el inicio de la cadena}

  // Goal: Returns the first location of the given character,
  // from the start of the string.
end;

function StrPosOfReverse
  (const SubStr, FullStr: umlcansishortstring): Word;
var Len, AIndex: Byte; Found: Boolean;
begin
  Len := umlcansishortstrings.Length(FullStr);
  // obtain length of full string
  // obtener longuitud de cadena completa

  AIndex := Len;
  // "AIndex" indicates from which character in full string starts analysis
  // "AIndex" indica de cual caracter eb la cadena completa comienza el analisis

  Found := FALSE;
  while ((not Found) and (AIndex > 0)) do
  begin
    Found :=
      umlcansishortstrparsers.StartsWithEqualStr
        (System.Copy(FullStr, AIndex, Len), SubStr);

    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function StrPosOfSameReverse
  (const SubStr, FullStr: umlcansishortstring): Word;
var Len, AIndex: Byte; Found: Boolean;
begin
  Len := umlcansishortstrings.Length(FullStr);
  // obtain length of full string
  // obtener longuitud de cadena completa

  AIndex := Len;
  // "AIndex" indicates from which character in full string starts analysis
  // "AIndex" indica de cual caracter eb la cadena completa comienza el analisis

  Found := FALSE;
  while ((not Found) and (AIndex > 0)) do
  begin
    Found :=
      umlcansishortstrparsers.StartsWithSameStr
        (System.Copy(FullStr, AIndex, Len), SubStr);

    Dec(AIndex);
  end;

  if (Found)
    then Result := Succ(AIndex)
    else Result := 0;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function TryStrPosOf
  (const AHaystack:  umlcansishortstring;
   const ANeedleStr: umlcansishortstring;
   out   AIndex:     Word): Boolean;
var Len, EachIndex: Word; Found: Boolean;
begin
  Result := false;
  AIndex := 0;

  Len :=
    umlcansishortstrings.Length(AHaystack);
  // obtain length of full umlcansishortstring
  // obtener longuitud de cadena completa

  EachIndex := 1;

  Found := FALSE;
  while ((not Found) and (EachIndex < Len)) do
  begin
    Found :=
      umlcansishortstrparsers.StartsWithEqualStr
        (ANeedleStr, System.Copy(AHaystack, EachIndex, Len));

    Inc(AIndex);
  end;

  if (Found)
    then AIndex := Pred(EachIndex)
    else AIndex := 0;
  Result := Found;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function TryStrPosOfSame
  (const AHaystack:  umlcansishortstring;
   const ANeedleStr: umlcansishortstring;
   out   AIndex:     Word): Boolean;
var Len, EachIndex: Word; Found: Boolean;
begin
  Result := false;
  AIndex := 0;

  Len :=
    umlcansishortstrings.Length(AHaystack);
  // obtain length of full umlcansishortstring
  // obtener longuitud de cadena completa

  EachIndex := 1;

  Found := FALSE;
  while ((not Found) and (EachIndex < Len)) do
  begin
    Found :=
      umlcansishortstrparsers.StartsWithSameStr
        (ANeedleStr, System.Copy(AHaystack, EachIndex, Len));

    Inc(EachIndex);
  end;

  if (Found)
    then AIndex := Pred(EachIndex)
    else AIndex := 0;
  Result := Found;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function TryStrAtPosOf
  (const AHaystack:    umlcansishortstring;
   const ANeedle:      umlcansishortstring;
   const ASourceIndex: byte;
   out   ADestIndex:   byte): Boolean;
var ANewIndex, Len: byte; Found: Boolean;
begin
  Result := false;
  ADestIndex := 0;

  Len :=
    umlcansishortstrings.Length(AHaystack);
  ANewIndex :=
    ASourceIndex;
  Found  := FALSE;

  while (not Found and (ANewIndex <= Len)) do
  begin
    if (umlcansishortstrings.EqualStrAt(AHaystack, ANeedle, ANewIndex))
      then Found := TRUE
      else Inc(ANewIndex);
  end;

  if (Found) then
  begin
    ADestIndex := ANewIndex;
  end;

  Result := Found;
  // Goal: Searches for a substring inside other umlcansishortstring
  // beginning at the "Index" character and returns the next character.

  // Objetivo: Busca una subcadena adentro de otra cadena
  // comenzando en el caracter numero "indice" y regresa el siguiente caracter.
end;

function StrAtPosOf
 (const AHaystack: umlcansishortstring;
  const ANeedle:   umlcansishortstring;
  const AIndex:    byte): byte;
var ADestIndex: byte;
begin
  Result := 0;
  ADestIndex := 0;
  if (umlcansishortstrparsers.TryStrAtPosOf
    (AHaystack, ANeedle, AIndex, ADestIndex)) then
  begin
    Result := ADestIndex;
  end;
  // Goal: Searches for a substring inside other umlcansishortstring
  // beginning at the "Index" character and returns the next character.

  // Objetivo: Busca una subcadena adentro de otra cadena
  // comenzando en el caracter numero "indice" y regresa el siguiente caracter.
end;

function StrPosOf
  (const AHaystack:  umlcansishortstring;
   const ANeedleStr: umlcansishortstring): Word;
begin
  Result := 0;
  umlcansishortstrparsers.TryStrPosOf(AHaystack, ANeedleStr, Result);
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function StrPosOfSame
  (const AHaystack:  umlcansishortstring;
   const ANeedleStr: umlcansishortstring): Word;
begin
  Result := 0;
  umlcansishortstrparsers.TryStrPosOfSame(AHaystack, ANeedleStr, Result);
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function StartsWithEqualChar
  (const AHaystack: umlcansishortstring; const ANeedle: Char): Boolean;
begin
  Result :=
    (umlcansishortstrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      umlcansichars.Equal(AHaystack[1], ANeedle);
  end;
end;

function StartsWithSameChar
  (const AHaystack: umlcansishortstring; const ANeedle: Char): Boolean;
begin
  Result :=
    (umlcansishortstrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      umlcansichars.SameText(AHaystack[1], ANeedle);
  end;
end;


function StartsWithEqualStr
  (const AHaystack: umlcansishortstring; const ANeedleStr: umlcansishortstring): Boolean;
var Shorter, Len: Word; LeadStr: umlcansishortstring;
begin
  Shorter :=
    umlcansishortstrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcansishortstrings.Length(AHaystack);
  // obtain length of full umlcansishortstring
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full umlcansishortstring
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    LeadStr :=
      System.Copy(AHaystack, 1, Shorter);
    Result  :=
      umlcansishortstrings.Equal(ANeedleStr, LeadStr);
  end;
  // Objetivo: Regresa si una subcadena es igual o esta al inicio de
  // otra cadena.

  // Goal: Returns if a substring is equal o is the start of another umlcansishortstring.
end;

function StartsWithSameStr
  (const AHaystack: umlcansishortstring; const ANeedleStr: umlcansishortstring): Boolean;
var Shorter, Len: Word; LeadStr: umlcansishortstring;
begin
  Shorter :=
    umlcansishortstrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcansishortstrings.Length(AHaystack);
  // obtain length of full umlcansishortstring
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full umlcansishortstring
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    LeadStr := System.Copy(AHaystack, 1, Shorter);
    Result  :=
      umlcansishortstrings.SameText(ANeedleStr, LeadStr);
  end;
  // Objetivo: Regresa si una subcadena es igual o esta al inicio de
  // otra cadena.

  // Goal: Returns if a substring is equal o is the start of another umlcansishortstring.
end;

function StartsWithEqualCharSet
  (const AHaystack: umlcansishortstring; const ANeedleSet: ansicharset): Boolean;
begin
  Result :=
    (umlcansishortstrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      (umlcansicharsets.IsMember(ANeedleSet, AHaystack[1]));
  end;
end;

function StartsWithSameCharSet
  (const AHaystack: umlcansishortstring; const ANeedleSet: ansicharset): Boolean;
begin
  Result :=
    (umlcansishortstrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      (umlcansicharsets.IsSameMember(ANeedleSet, AHaystack[1]));
  end;
end;

function FinishesWithEqualChar
  (const AHaystack: umlcansishortstring; const ANeedleChar: umlcansishortstring): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcansishortstrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (AHaystack[Pred(ACount)] = ANeedleChar);
  end;
end;

function FinishesWithSameChar
  (const AHaystack: umlcansishortstring; const ANeedleChar: umlcansishortstring): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcansishortstrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (AHaystack[Pred(ACount)] = ANeedleChar);
  end;
end;

function FinishesWithEqualCharSet
 (const AHaystack: umlcansishortstring; const ANeedleSet: ansicharset): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcansishortstrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (umlcansicharsets.IsMember(ANeedleSet, AHaystack[Pred(ACount)]));
  end;
end;

function FinishesWithSameCharSet
 (const AHaystack: umlcansishortstring; const ANeedleSet: ansicharset): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcansishortstrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (umlcansicharsets.IsSameMember(ANeedleSet, AHaystack[Pred(ACount)]));
  end;
end;

function FinishesWithEqualStr
  (const AHaystack: umlcansishortstring; const ANeedleStr: umlcansishortstring): Boolean;
var Shorter, Len, AIndex: Word; TrailingStr: umlcansishortstring;
begin
  Shorter :=
    umlcansishortstrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcansishortstrings.Length(AHaystack);
  // obtain length of full umlcansishortstring
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full string
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    AIndex      :=
      (Len - Shorter + 1);
    TrailingStr :=
      System.Copy(AHaystack, AIndex, Shorter);
    Result      :=
      umlcansishortstrings.Equal(ANeedleStr, TrailingStr);
  end;
end;

function FinishesWithSameStr
  (const AHaystack: umlcansishortstring; const ANeedleStr: umlcansishortstring): Boolean;
var Shorter, Len, AIndex: Word; TrailingStr: umlcansishortstring;
begin
  Shorter :=
    umlcansishortstrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcansishortstrings.Length(AHaystack);
  // obtain length of full umlcansishortstring
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full string
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    AIndex      :=
      (Len - Shorter + 1);
    TrailingStr :=
      System.Copy(AHaystack, AIndex, Shorter);
    Result      :=
      umlcansishortstrings.SameText(ANeedleStr, TrailingStr);
  end;
end;


function ReplaceCharByCharCopy
  (const AHaystack: umlcansishortstring; ASource, ADest: ansichar): umlcansishortstring;
var I, L: Integer;
begin
  umlcansishortstrings.Reset(Result);
  L :=
    umlcansishortstrings.Length(AHaystack);
  for i := 1 to L do
  begin
    if (AHaystack[i] = ASource)
      then umlcansishortstrings.ConcatChar(Result, ADest);
  end;
  // Goal: Replace a specific character from a string.
  // Objetivo: Reemplazar un caracter en especifico de una cadena.
end;

function ReplaceCharSetByCharCopy
 (const AHaystack: umlcansishortstring; ASource: umlcansicharset; ADest: ansichar): umlcansishortstring;
var I, L: Integer;
begin
  umlcansishortstrings.Reset(Result);
  L :=
    umlcansishortstrings.Length(AHaystack);
  for i := 1 to L do
  begin
    if (IsMember(ASource, AHaystack[i]))
      then umlcansishortstrings.ConcatChar(Result, ADest)
      else umlcansishortstrings.ConcatChar(Result, ASource[i]);
  end;
  // Goal: Replace a specific character set from a string.
  // Objetivo: Reemplazar un conjunto caracter en especifico de una cadena.
end;

function RemoveSingleCharCopy
  (const ASource: umlcansishortstring; IgnoreChar: ansichar): umlcansishortstring;
var I, L: Integer;
begin
  umlcansishortstrings.Clear(Result);
  L :=
    umlcansishortstrings.Length(ASource);
  for i := 1 to L do
  begin
    if (ASource[i] <> IgnoreChar) then
    begin
      umlcansishortstrings.ConcatChar(Result, ASource[i]);
    end;
  end;
  // Goal: Delete a specific character from a string.
  // Objetivo: Eliminar un caracter en especifico de una cadena.
end;

function RemoveCharSetCopy
  (const ASource: umlcansishortstring; IgnoreChars: umlcansicharset): umlcansishortstring;
var I, L: Integer;
begin
  umlcansishortstrings.Clear(Result);
  L :=
    umlcansishortstrings.Length(ASource);
  for i := 1 to L do
  begin
    if (not IsMember(IgnoreChars, ASource[i])) then
    begin
      umlcansishortstrings.ConcatChar(Result, ASource[i]);
    end;
  end;
  // Goal: Delete a specific character set from a string.
  // Objetivo: Eliminar un conjunto caracter en especifico de una cadena.
end;

function CharCountOf
  (const AHaystack: umlcansishortstring; ANeedleChar: ansichar): Byte;
var I, ALen: Byte;
begin
  Result := 0;
  if (not umlcansishortstrings.IsEmpty(AHaystack) and (not umlcansichars.IsNull(ANeedleChar))) then
  begin
    ALen :=
       umlcansishortstrings.Length(AHaystack);
    for I := 1 to ALen do
    begin
      if (AHaystack[I] = ANeedleChar) then
      begin
        System.Inc(Result);
      end;
    end;
  end;
end;

function CharSetCountOf
  (const AHaystack: umlcansishortstring; ANeedleSet: ansicharset): Byte;
var I, ALen: Byte;
begin
  Result := 0;
  if (not umlcansishortstrings.IsEmpty(AHaystack) and (not umlcansicharsets.IsEmpty(ANeedleSet))) then
  begin
    ALen :=
       umlcansishortstrings.Length(AHaystack);
    for I := 1 to ALen do
    begin
      if (umlcansicharsets.IsMember(ANeedleSet, AHaystack[I])) then
      begin
        System.Inc(Result);
      end;
    end;
  end;
end;

function ReplaceAllStrByStrCopy
  (const AHaystack: umlcansishortstring;
   const ASource:   umlcansishortstring;
   const ADest:     umlcansishortstring): umlcansishortstring;
var Finished: Boolean; Index: Byte;
    AHaystackCopy: umlcansishortstring;
begin
  Index := 0;
  Result :=
    AHaystack;
  repeat
    umlcansishortstrparsers.ReplaceSingleStrbyStrAt
      (Result, Index, ASource, ADest);
    Finished := (Index = 0);
    //if (not Finished)
    //  then Inc(Result);
  until (Finished);
  // Goal: Replaces all the "SubStr" found in "FullStr" with "AValue".
  // Objetivo: Reemplaza todas las "SubStr" encontradas en "FullStr" por "AValue".}
end;

function ReplaceSingleStrByStrCopy
  (const AHaystack: umlcansishortstring;
   const ASource:   umlcansishortstring;
   const ADest:     umlcansishortstring): umlcansishortstring;
var Index: Byte;
begin
  Result := AHaystack;
  Index  := 0;
  umlcansishortstrparsers.ReplaceSingleStrbyStrAt
    (Result, Index, ASource, ADest);
  // Goal: Returns a copy of the umlcansishortstring with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function RemoveSingleStrCopy
  (const AHaystack: umlcansishortstring;
   const ANeedle:   umlcansishortstring): umlcansishortstring;
begin
  Result := '';
    //umlcansishortstrparsers.ReplaceAllStrbyStrCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the umlcansishortstring with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

function RemoveAllStrCopy
  (const AHaystack: umlcansishortstring;
   const ANeedle:   umlcansishortstring): umlcansishortstring;
begin
  Result := '';
    // umlcansishortstrparsers.ReplaceAllStrByStrCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the umlcansishortstring with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;
function ReplaceForwardCopy
  (const AHaystack, Source, Dest: umlcansishortstring): umlcansishortstring;
var AIndex: Byte;
begin
  Result := AHaystack;
  AIndex := 0;
  umlcansishortstrparsers.ReplaceForwardPos
    (Result, AIndex, Source, Dest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceForwardCopySame
  (const AHaystack, Source, Dest: umlcansishortstring): umlcansishortstring;
var AIndex: Byte;
begin
  Result := AHaystack;
  AIndex := 0;
  umlcansishortstrparsers.ReplaceForwardPosSame
    (Result, AIndex, Source, Dest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceBackwardCopy
  (const AHaystack, Source, Dest: umlcansishortstring): umlcansishortstring;
var AIndex: Byte;
begin
  Result := AHaystack;
  AIndex := 0;
  umlcansishortstrparsers.ReplaceBackwardPos
    (Result, AIndex, Source, Dest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceBackwardCopySame
  (const AHaystack, Source, Dest: umlcansishortstring): umlcansishortstring;
var AIndex: Byte;
begin
  Result := AHaystack;
  AIndex := 0;
  umlcansishortstrparsers.ReplaceBackwardPosSame
    (Result, AIndex, Source, Dest);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceForwardALLCopy
  (const AHaystack, ASourceNeedle, ADestNeedle: umlcansishortstring): umlcansishortstring;
var AIndex: Byte;
begin
  Result := AHaystack;
  AIndex := 0;
  repeat
    umlcansishortstrparsers.ReplaceForwardPos
      (Result, AIndex, ASourceNeedle, ADestNeedle);
  until (AIndex = 0);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function ReplaceBackwardALLCopy
  (const AHaystack, ASourceNeedle, ADestNeedle: umlcansishortstring): umlcansishortstring;
var AIndex: Byte;
begin
  Result := AHaystack;
  AIndex := 0;
  repeat
    umlcansishortstrparsers.ReplaceBackwardPos(Result, AIndex, ASourceNeedle, ADestNeedle);
  until (AIndex = 0);
  // Goal: Returns a copy of the string with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

procedure ReplaceForwardPos
  (var AHaystack: umlcansishortstring;
   var AIndex: Byte; const ASourceNeedle, ADestNeedle: umlcansishortstring);
var BeforeACount, AfterACount, S, V: Byte;
    BeforeString, AfterString: umlcansishortstring;
begin
  AIndex :=
    StrPosOf(ASourceNeedle, AHaystack);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcansishortstrings.Length(ASourceNeedle);
    V := umlcansishortstrings.Length(AHaystack);

    AfterACount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeACount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansishortstrings.ReplaceLeft(BeforeString, AHaystack, BeforeACount);
      umlcansishortstrings.ReplaceRight(AfterString, AHaystack, AfterACount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted string

      umlcansishortstrings.Reset(AHaystack);
      umlcansishortstrings.ConcatStr(AHaystack, BeforeString);
      umlcansishortstrings.ConcatStr(AHaystack, ADestNeedle);
      umlcansishortstrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces in "AHaystack", the first "ASourceNeedle" found with "ADestNeedle".
  // Objetivo: Reemplaza en "AHaystack", la primera "ASourceNeedle" encontrada por "ADestNeedle".}
end;

procedure ReplaceForwardPosSame
  (var AHaystack: umlcansishortstring;
   var AIndex: Byte; const ASourceNeedle, ADestNeedle: umlcansishortstring);
var BeforeACount, AfterACount, S, V: Byte;
    BeforeString, AfterString: umlcansishortstring;
begin
  BeforeString := '';
  AfterString  := '';
  AIndex := StrPosOf(ASourceNeedle, AHaystack);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcansishortstrings.Length(ASourceNeedle);
    V := umlcansishortstrings.Length(AHaystack);

    AfterACount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeACount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansishortstrings.ReplaceLeft(BeforeString, AHaystack, BeforeACount);
      umlcansishortstrings.ReplaceRight(AfterString, AHaystack, AfterACount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted umlcansishortstring

      umlcansishortstrings.Reset(AHaystack);
      umlcansishortstrings.ConcatStr(AHaystack, BeforeString);
      umlcansishortstrings.ConcatStr(AHaystack, ADestNeedle);
      umlcansishortstrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces in "AHaystack", the first "ASourceNeedle" found with "ADestNeedle".
  // Objetivo: Reemplaza en "AHaystack", la primera "ASourceNeedle" encontrada por "ADestNeedle".}
end;

procedure ReplaceBackwardPos
  (var AHaystack: umlcansishortstring;
   var AIndex: Byte; const ASourceNeedle, ADestNeedle: umlcansishortstring);
var BeforeACount, AfterACount, S, V: Byte;
    BeforeString, AfterString: umlcansishortstring;
begin
  AIndex := StrPosOfReverse(ASourceNeedle, AHaystack);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcansishortstrings.Length(ASourceNeedle);
    V := umlcansishortstrings.Length(AHaystack);

    AfterACount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeACount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansishortstrings.ReplaceLeft(BeforeString, AHaystack, BeforeACount);
      umlcansishortstrings.ReplaceRight(AfterString, AHaystack, AfterACount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted umlcansishortstring

      umlcansishortstrings.Reset(AHaystack);
      umlcansishortstrings.ConcatStr(AHaystack, BeforeString);
      umlcansishortstrings.ConcatStr(AHaystack, ADestNeedle);
      umlcansishortstrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces in "AHaystack", the first "ASourceNeedle" found with "ADestNeedle".
  // Objetivo: Reemplaza en "AHaystack" , la primera "ASourceNeedle" encontrada por "ADestNeedle".}
end;

procedure ReplaceBackwardPosSame
  (var AHaystack: umlcansishortstring;
   var AIndex: Byte; const ASourceNeedle, ADestNeedle: umlcansishortstring);
var BeforeACount, AfterACount, S, V: Byte;
    BeforeString, AfterString: umlcansishortstring;
begin
  BeforeString := '';
  AfterString  := '';
  AIndex := StrPosOfSameReverse(ASourceNeedle, AHaystack);
  // localizar en que posicion inicia la cadena que se va a reemplazar
  // locate, which location starts the string to be replaced

  if (AIndex > 0) then
  begin
    S := umlcansishortstrings.Length(ADestNeedle);
    V := umlcansishortstrings.Length(AHaystack);

    AfterACount := (V - Pred(AIndex + S));
    // obtener la long. de la cadena despues de la parte que se va a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeACount := Pred(AIndex);
    // obtener la long. de la cadena antes de la parte que se va a reemplazar
    // obtain the length of the string before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansishortstrings.ReplaceLeft(BeforeString, AHaystack, BeforeACount);
      umlcansishortstrings.ReplaceRight(AfterString, AHaystack, AfterACount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted umlcansishortstring

      umlcansishortstrings.Reset(AHaystack);
      umlcansishortstrings.ConcatStr(AHaystack, BeforeString);
      umlcansishortstrings.ConcatStr(AHaystack, ADestNeedle);
      umlcansishortstrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces in "AHaystack", the first "ASourceNeedle" found with "ADestNeedle".
  // Objetivo: Reemplaza en "AHaystack", la primera "ASourceNeedle" encontrada por "ADestNeedle".}
end;

function DeleteForwardCopy
  (const AHaystack, ANeedle: umlcansishortstring): umlcansishortstring;
begin
  Result :=
    ReplaceForwardCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the string with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

function DeleteBackwardCopy
  (const AHaystack, ANeedle: umlcansishortstring): umlcansishortstring;
begin
  Result :=
    ReplaceBackwardCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the string with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

function DeleteALLCopy
  (const AHaystack, ANeedle: umlcansishortstring): umlcansishortstring;
begin
  Result :=
    ReplaceForwardALLCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the string with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

function CopyFrom
  (const ASource: umlcansishortstring;
   const AStart:  byte;
   const AFinish: byte): umlcansishortstring;
var I, ACount: byte;
begin
  umlcansishortstrings.Reset(Result);

  ACount :=
    Succ(AFinish - AStart);

  for I := 1 to ACount do
  begin
    umlcansishortstrings.ConcatChar(Result, ASource[i]);
  end;
  // Goal: Returns a substring from "astart" to "afinish".
  // Objetivo: Regresa una subcadena de "astart" hasta "afinish".
end;

function ParseFrom
  (const ASource: umlcansishortstring;
   var   AStart:  byte;
   const AFinish: byte): umlcansishortstring;
var I, ACount: byte;
begin
  umlcansishortstrings.Reset(Result);

  ACount :=
    (AFinish - AStart);

  for I := 1 to ACount do
  begin
    umlcansishortstrings.ConcatChar(Result, ASource[Pred(AStart + i)]);
  end;

  AStart := AFinish;
  // Goal: Returns a substring from "astart" to "afinish" & updates "astart".
  // Objetivo: Regresa una subcadena desde "astart" hasta "afinish"
  // y actualiza "astart".
end;

function SkipChar
  (const ASource:    umlcansishortstring;
   const AIndex:     byte;
   const AValidChar: ansichar): Byte;
var L: Byte;
begin
  Result := AIndex;
  L := umlcansishortstrings.Length(ASource);
  if (Result <= L) and (ASource[Result] = AValidChar) then
  begin
    Inc(Result);
  end;
  // Goal: Returns the index of a single character, if matches.
  // Objetivo: Regresa el indice de solo caracter, si coincide.
end;

function SkipCharSet
  (const ASource:     umlcansishortstring;
   const AIndex:      byte;
   const AValidChars: ansicharset): byte;
var L: Integer;
begin
  Result := AIndex;
  L := umlcansishortstrings.Length(ASource);
  if (Result <= L) and umlcansicharsets.IsMember(AValidChars, ASource[Result]) then
  begin
    Inc(Result);
  end;
  // Goal: Returns the index of a single character set item, if matches.
  // Objetivo: Regresa el indice de un solo caracter de un conjunto, si coincide.
end;

function SkipCharWhile
  (const ASource:    umlcansishortstring;
   const AIndex:     byte;
   const AValidChar: ansichar): byte;
var L: Word;
begin
  Result := AIndex;
  L := umlcansishortstrings.Length(ASource);
  while ((Result <= L) and (ASource[Result] = AValidChar)) do
  begin
    Inc(Result);
  end;
  // Goal: Returns the index while a sequence of the same character is found.
  // Objetivo: Regresa el indice mientras una secuencia del mismo caracter es encontrada.
end;

function SkipCharUntil
  (const ASource:   umlcansishortstring;
   const AIndex:    byte;
   const BreakChar: ansichar): byte;
var L: Word;
begin
  Result := AIndex;
  L := umlcansishortstrings.Length(ASource);
  while ((Result <= L) and (ASource[Result] <> BreakChar)) do
  begin
    Inc(Result);
  end;
  // Goal: Returns the index while a sequence of the same character is not found.
  // Objetivo: Regresa el indice mientras una secuencia del mismo caracter no es encontrada.
end;

function SkipCharSetWhile
  (const ASource:     umlcansishortstring;
   const AIndex:      byte;
   const AValidChars: ansicharset): byte;
var L: Word;
begin
  Result := AIndex;
  L :=
    umlcansishortstrings.Length(ASource);
  while ((Result <= L) and umlcansicharsets.IsMember(AValidChars, ASource[Result])) do
  begin
    Inc(Result);
  end;
  // Goal: Returns the index while a sequence of the same character set is found.
  // Objetivo: Regresa el indice mientras una secuencia del mismo conjunto caracter es encontrada.
end;

function SkipCharSetUntil
  (const ASource:    umlcansishortstring;
   const AIndex:     byte;
   const BreakChars: ansicharset): byte;
var L: Word;
begin
  Result := AIndex;
  L :=
    umlcansishortstrings.Length(ASource);
  while ((Result <= L) and not umlcansicharsets.IsMember(BreakChars, ASource[Result])) do
  begin
    Inc(Result);
  end;
  // Goal: Returns the index while a sequence of the same character set is not found.
  // Objetivo: Regresa el indice mientras una secuencia del mismo conjunto caracter no es encontrada.
end;

function SkipToken
  (const ASource:      umlcansishortstring;
   var   AStartIndex:  Byte;
   var   AFinishIndex: Byte): umlcansishortstring;
var StartIndex, FinishIndex: Byte;
begin
  StartIndex  := AStartIndex;

  StartIndex  :=
    umlcansishortstrparsers.SkipCharWhile(ASource, StartIndex, #32);
  FinishIndex :=
    umlcansishortstrparsers.SkipCharUntil(ASource, StartIndex, #32);
  Result      :=
    umlcansishortstrparsers.CopyFrom(ASource, StartIndex, FinishIndex);

  AStartIndex  := StartIndex;
  AFinishIndex := FinishIndex;
end;

function SkipDigit
  (const ASource:      umlcansishortstring;
   var   AStartIndex:  Byte;
   var   AFinishIndex: Byte): umlcansishortstring;
begin
  AStartIndex  :=
    umlcansishortstrparsers.SkipCharWhile(ASource, AStartIndex, #32);
  AFinishIndex :=
    umlcansishortstrparsers.SkipCharSetWhile(ASource, AStartIndex, DecDigitSet);
  Result :=
    umlcansishortstrparsers.CopyFrom(ASource, AStartIndex, AFinishIndex);
  AStartIndex := AFinishIndex;
end;

function SkipLetter
  (const ASource:      umlcansishortstring;
   var   AStartIndex:  Byte;
   var   AFinishIndex: Byte): umlcansishortstring;
begin
  AStartIndex  :=
    umlcansishortstrparsers.SkipCharWhile(ASource, AStartIndex, #32);
  AFinishIndex :=
    umlcansishortstrparsers.SkipCharSetWhile(ASource, AStartIndex, AlphaSet);
  Result :=
    umlcansishortstrparsers.CopyFrom(ASource, AStartIndex, AFinishIndex);
  AStartIndex := AFinishIndex;
end;


(* global procedures *)


procedure ReplaceSingleStrByStrAt
  (var   AHaystack: umlcansishortstring;
   var   Index:     Byte;
   const ASource:   umlcansishortstring;
   const ADest:     umlcansishortstring);
var BeforeCount, AfterCount: Integer;
    BeforeString, AfterString: umlcansishortstring;
begin
  BeforeString := '';
  AfterString  := '';
  Index :=
    umlcansishortstrparsers.StrAtPosOf(AHaystack, ASource, 0);
  // localizar en que posicion inicia la cadena que se va BeforeString reemplazar
  // locate in which location starts the umlcansishortstring to be replaced

  if (Index > 0) then
  begin
    AfterCount :=
      Index + umlcansishortstrings.Length(ASource);
    // obtener la longuitud de la cadena despues de la parte antes a reemplazar
    // obtain the length of the string after the section to be replaced

    BeforeCount :=
      umlcansishortstrings.Length(AHaystack) - umlcansishortstrings.Length(ASource);
    // obtener la longuitud de la cadena antes de la parte antes a reemplazar
    // obtain the length of the string before the section to be replaced

    if (Index <> 0) then
    begin
      umlcansishortstrings.AssignLeft(BeforeString, AHaystack, BeforeCount);
      umlcansishortstrings.AssignRight(AfterString, AHaystack, AfterCount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted string

      umlcansishortstrings.Reset(AHaystack);
      umlcansishortstrings.ConcatStr(AHaystack, BeforeString);
      umlcansishortstrings.ConcatStr(AHaystack, ADest);
      umlcansishortstrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces the first "Source" found in "FullStr" with "Dest".
  // Objetivo: Reemplaza la primera "Source" encontrada en "FullStr" por "Dest".}
end;

function CppEscToStr
  (const ASource: umlcansishortstring): umlcansishortstring;
begin
  Result := ReplaceForwardALLCopy(ASource, '\n', #13#10);
  // replace escape characters by line break characters
  // reemplazar caracteres de escape por caracteres de salto de linea

  Result := ReplaceForwardALLCopy(Result, '\'#39, #39);
  // replace escape characters by single quote characters
  // reemplazar caracteres de escape por caracteres de comilla simple

  Result := ReplaceForwardALLCopy(Result, '\'#34, #34);
  // replace escape characters by double quote characters
  // reemplazar caracteres de escape por caracteres de comilla doble

  Result := ReplaceForwardALLCopy(Result, '\\', '\');
  // replace escape characters by slash characters
  // reemplazar caracteres de escape por caracteres de diagonal

  // Objetivo: Convertir una cadena con caracteres de escape que comienzan con
  // el caracter "\" y otro caracter a una cadena sin caracteres de escape.

  // Goal: To convert a string with escape characters & another character into
  // a string without escape characters.
end;

function StrToCppEsc
  (const ASource: umlcansishortstring): umlcansishortstring;
begin
  Result := ReplaceForwardALLCopy(ASource, #13#10, '\n');
  // replace line break characters by escape characters
  // reemplazar caracteres de salto de linea por caracteres de escape

  Result := ReplaceForwardALLCopy(Result, #39, '$$');
  Result := ReplaceForwardALLCopy(Result, '$$', '\'#39);
  // replace single quote characters by escape characters
  // reemplazar caracteres de comilla simple por caracteres de escape

  Result := ReplaceForwardALLCopy(Result, #34, '$$');
  Result := ReplaceForwardALLCopy(Result, '$$', '\'#34);
  // replace double quote characters by escape characters
  // reemplazar caracteres de comilla doble por caracteres de escape

  Result := ReplaceForwardALLCopy(Result, '\', '$$');
  Result := ReplaceForwardALLCopy(Result, '$$', '\\');
  // replace double quote characters by escape characters
  // reemplazar caracteres de comilla doble por caracteres de escape

  // Objetivo: Convertir una cadena sin caracteres de escape a cadena con
  // caracteres de escape que comienzan con el caracter "\" y otro caracter.

  // Goal: To convert a string without escape characters into
  // a string with escape characters & another character.
end;

end.

