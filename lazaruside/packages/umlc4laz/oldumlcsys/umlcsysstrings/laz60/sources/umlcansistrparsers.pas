(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcansistrparsers;

{$mode objfpc}{$H+}

(**
 **************************************************************************
 ** Description:
 ** Quick Text Search and Parsing operations.
 ** The character encoding is A.N.S.I.
 ** The quantity of items is NOT restricted to 255 characters.
 ** The string is internally managed as memory referenced.
 **************************************************************************
 **)

interface

uses
{$IFDEF MSWINDOWS}
  Windows,
  //Messages,
  //Consts,
{$ENDIF}
{$IFDEF LINUX}
  Types, Libc,
{$ENDIF}
  SysConst, SysUtils,
  umlcmodules, umlctypes,
  umlccomparisons,
  umlcstdchartypes,
  umlcstdstrtypes,
  umlcstdnullstrtypes,
  umlcansichars,
  umlcansicharsets,
  umlcansicharsetconsts,
  umlcansistrings,
  dummy;

(* global functions *)

  function MatchesCharSet
    (const AHaystack: ansistring; const ValidChars: ansicharset): ansistring; overload;

  function IsStringInSet
    (const AHaystack: ansistring; const ValidChars: ansicharset): Boolean; overload;

  function IsWildcard
     (const ASource: ansistring): Boolean;

  function IsIdentifier
    (const ASource: ansistring): Boolean;

  function TryStrPosOf
    (const AHaystack:  ansistring;
     const ANeedleStr: ansistring;
     out   AIndex:     Word): Boolean;
  function TryStrPosOfSame
    (const AHaystack:  ansistring;
     const ANeedleStr: ansistring;
     out   AIndex:     Word): Boolean;

  function TryStrAtPosOf
    (const AHaystack:    ansistring;
     const ANeedle:      ansistring;
     const ASourceIndex: Word;
     out   ADestIndex:   Word): Boolean; overload;

  function StrPosOf
    (const AHaystack:  ansistring;
     const ANeedleStr: ansistring): Word;
  function StrPosOfSame
    (const AHaystack:  ansistring;
     const ANeedleStr: ansistring): Word;

  function StrAtPosOf
    (const AHaystack: ansistring;
     const ANeedle:   ansistring;
     const AIndex:    Word): Word; overload;

  function StartsWithEqualChar
    (const AHaystack: ansistring; const ANeedle: char): Boolean;

  function StartsWithSameChar
    (const AHaystack: ansistring; const ANeedle: char): Boolean;

  function StartsWithEqualStr
    (const AHaystack: ansistring; const ANeedleStr: ansistring): Boolean;

  function StartsWithSameStr
    (const AHaystack: ansistring; const ANeedleStr: ansistring): Boolean;

  function StartsWithEqualCharSet
    (const AHaystack: ansistring; const ANeedleSet: charset): Boolean;

  function StartsWithSameCharSet
    (const AHaystack: ansistring; const ANeedleSet: charset): Boolean;

  function FinishesWithEqualChar
    (const AHaystack: ansistring; const ANeedleChar: ansistring): Boolean;

  function FinishesWithSameChar
    (const AHaystack: ansistring; const ANeedleChar: ansistring): Boolean;

  function FinishesWithEqualCharSet
   (const AHaystack: ansistring; const ANeedleSet: charset): Boolean;

  function FinishesWithSameCharSet
   (const AHaystack: ansistring; const ANeedleSet: charset): Boolean;

  function FinishesWithEqualStr
    (const AHaystack: ansistring; const ANeedleStr: ansistring): Boolean;

  function FinishesWithSameStr
    (const AHaystack: ansistring; const ANeedleStr: ansistring): Boolean;

  function ReplaceCharByCharCopy
    (const AHaystack: ansistring; ASource, ADest: ansichar): ansistring; overload;
  function ReplaceCharSetByCharCopy
    (const AHaystack: ansistring; ASource: umlcansicharset; ADest: ansichar): ansistring; overload;

  function RemoveCharCopy
    (const ASource: ansistring; IgnoreChar: ansichar): ansistring;
  function RemoveCharSetCopy
    (const ASource: ansistring; IgnoreChars: umlcansicharset): ansistring;

  function CharCountOf
    (const AHaystack: ansistring; ANeedleChar: ansichar): Cardinal;
  function CharSetCountOf
    (const AHaystack: ansistring; ANeedleSet: ansicharset): Cardinal;

  function ReplaceAllStrByStrCopy
    (const AHaystack: ansistring;
     const ASource:   ansistring;
     const ADest:     ansistring): ansistring;

  function ReplaceSingleStrByStrCopy
    (const AHaystack: ansistring;
     const ASource:   ansistring;
     const ADest:     ansistring): ansistring;

  function RemoveSingleStrCopy
    (const AHaystack: ansistring;
     const ANeedle:   ansistring): ansistring;

  function RemoveAllStrCopy
    (const AHaystack: ansistring;
     const ANeedle:   ansistring): ansistring;

  function CopyFrom
    (const ASource: ansistring;
     const AStart:  Word;
     const AFinish: Word): ansistring;

  function ParseFrom
    (const ASource: ansistring;
     var   AStart:  Word;
     const AFinish: Word): ansistring;


  function SkipChar
    (const ASource:    ansistring;
     const AIndex:     Word;
     const AValidChar: ansichar): Word;

  function SkipCharWhile
    (const ASource:    ansistring;
     const AIndex:     Word;
     const AValidChar: ansichar): Word;

  function SkipCharUntil
    (const ASource:   ansistring;
     const AIndex:    Word;
     const BreakChar: ansichar): Word;

  function SkipCharSetWhile
    (const ASource:     ansistring;
     const AIndex:      Word;
     const AValidChars: ansicharset): Word;

  function SkipCharSetUntil
    (const ASource:    ansistring;
     const AIndex:     Word;
     const BreakChars: ansicharset): Word;

  function SkipToken
    (const ASource:      ansistring;
     var   AStartIndex:  Word;
     var   AFinishIndex: Word): ansistring;

  function SkipDigit
    (const ASource:      ansistring;
     var   AStartIndex:  Word;
     var   AFinishIndex: Word): ansistring;

  function SkipLetter
    (const ASource:      ansistring;
     var   AStartIndex:  Word;
     var   AFinishIndex: Word): ansistring;

(* global procedures *)

  procedure ReplaceSingleStrByStrAt
    (var   AHaystack: ansistring;
     var   AIndex:    Word;
     const ASource:   ansistring;
     const ADest:     ansistring);

implementation

(* global functions *)

function MatchesCharSet
  (const AHaystack: ansistring; const ValidChars: ansicharset): ansistring;
var CanContinue: Boolean;
    AIndex, ALen: Word; Temp: ansistring;
begin
  umlcansistrings.Clear(Result);

  if (not umlcansistrings.IsEmpty(AHaystack)) then
  begin
    umlcansistrings.Clear(Temp);

    AIndex := 1;
    ALen   := umlcansistrings.Length(AHaystack);

    CanContinue := TRUE;
    while ((AIndex < ALen) and CanContinue) do
    begin
      CanContinue :=
        umlcansicharsets.IsMember(ValidChars, AHaystack[AIndex]);

      if (CanContinue) then
      begin
        Temp := Temp + AHaystack[AIndex];
      end;

      Inc(AIndex);
    end;

    if (CanContinue) then
    begin
      Result := Temp;
    end;
  end;
  // Goal: Moves a null terminated ansistring pointer,
  // as long as each character, matches the valid set.

  // Objetivo: Mueve un apuntador cadena terminada en nulo,
  // mientras cada caracter, coincide con el conjunto valido.
end;

function IsStringInSet
  (const AHaystack: ansistring; const ValidChars: ansicharset): Boolean;
begin
  Result :=
    (umlcansistrparsers.MatchesCharSet(AHaystack, ValidChars) <> AHaystack);
  // Goal: Returns if all the characters, a ansistring are valid.
  // Objetivo: Regresa si todos los caracteres en una cadena son validos.
end;

function IsWildcard
   (const ASource: ansistring): Boolean;
var P: ansinullstring;
begin
  Result :=
    umlcansistrparsers.IsStringInSet(ASource, WildcardSet);
  // Goal: Returns if a ansistring is a wildcard.
  // Objetivo: Regresa si una cadena es un comodin.
end;

function IsIdentifier
  (const ASource: ansistring): Boolean;
var I, ACount: Word;
begin
  Result := False;
  ACount :=
    umlcansistrings.Length(ASource);
  if (ACount = 0) or not umlcansicharsets.IsMember(AlphaSet, ASource[1])
    then Exit;
  for I := 2 to ACount do
  begin
    if (not umlcansicharsets.IsMember(IDSet, ASource[I]))
      then Exit;
  end;
  Result := TRUE;
  // Goal: To return if a ansistring is a valid identifier.
  // Objetivo: Regresar si una cadena es un identificador valido.
end;

function TryStrPosOf
  (const AHaystack:  ansistring;
   const ANeedleStr: ansistring;
   out   AIndex:     Word): Boolean;
var Len, EachIndex: Word; Found: Boolean;
begin
  Result := false;
  AIndex := 0;

  Len := umlcansistrings.Length(AHaystack);
  // obtain length of full ansistring
  // obtener longuitud de cadena completa

  EachIndex := 1;

  Found := FALSE;
  while ((not Found) and (EachIndex < Len)) do
  begin
    Found :=
      umlcansistrparsers.StartsWithEqualStr
        (ANeedleStr, System.Copy(AHaystack, EachIndex, Len));

    Inc(AIndex);
  end;

  if (Found)
    then AIndex := Pred(EachIndex)
    else AIndex := 0;
  Result := Found;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function TryStrPosOfSame
  (const AHaystack:  ansistring;
   const ANeedleStr: ansistring;
   out   AIndex:     Word): Boolean;
var Len, EachIndex: Word; Found: Boolean;
begin
  Result := false;
  AIndex := 0;

  Len :=
    umlcansistrings.Length(AHaystack);
  // obtain length of full ansistring
  // obtener longuitud de cadena completa

  EachIndex := 1;

  Found := FALSE;
  while ((not Found) and (EachIndex < Len)) do
  begin
    Found :=
      umlcansistrparsers.StartsWithSameStr
        (ANeedleStr, System.Copy(AHaystack, EachIndex, Len));

    Inc(EachIndex);
  end;

  if (Found)
    then AIndex := Pred(EachIndex)
    else AIndex := 0;
  Result := Found;
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function TryStrAtPosOf
  (const AHaystack:    ansistring;
   const ANeedle:      ansistring;
   const ASourceIndex: Word;
   out   ADestIndex:   Word): Boolean;
var ANewIndex, Len: Word; Found: Boolean;
begin
  Result := false;
  ADestIndex := 0;

  Len :=
    umlcansistrings.Length(AHaystack);
  ANewIndex :=
    ASourceIndex;
  Found  := FALSE;

  while (not Found and (ANewIndex <= Len)) do
  begin
    if (umlcansistrings.EqualStrAt(AHaystack, ANeedle, ANewIndex))
      then Found := TRUE
      else Inc(ANewIndex);
  end;

  if (Found) then
  begin
    ADestIndex := ANewIndex;
  end;

  Result := Found;
  // Goal: Searches for a substring inside other ansistring
  // beginning at the "Index" character and returns the next character.

  // Objetivo: Busca una subcadena adentro de otra cadena
  // comenzando en el caracter numero "indice" y regresa el siguiente caracter.
end;

function StrPosOf
  (const AHaystack:  ansistring;
   const ANeedleStr: ansistring): Word;
begin
  Result := 0;
  umlcansistrparsers.TryStrPosOf(AHaystack, ANeedleStr, Result);
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the index of the first ocurrence of "Substr".
end;

function StrPosOfSame
  (const AHaystack:  ansistring;
   const ANeedleStr: ansistring): Word;
begin
  Result := 0;
  umlcansistrparsers.TryStrPosOfSame(AHaystack, ANeedleStr, Result);
  // Objetivo: Regresa el indice de la primer ocurrencia de "Substr".
  // Goal: Returns the AIndex of the first ocurrence of "Substr".
end;

function StrAtPosOf
 (const AHaystack: ansistring;
  const ANeedle:   ansistring;
  const AIndex:    Word): Word;
var ANewIndex: Word;
begin
  Result := 0;
  ANewIndex := 0;
  if (umlcansistrparsers.TryStrAtPosOf
    (AHaystack, ANeedle, AIndex, ANewIndex)) then
  begin
    Result := ANewIndex;
  end;
  // Goal: Searches for a substring inside other ansistring
  // beginning at the "Index" character and returns the next character.

  // Objetivo: Busca una subcadena adentro de otra cadena
  // comenzando en el caracter numero "indice" y regresa el siguiente caracter.
end;

function StartsWithEqualChar
  (const AHaystack: ansistring; const ANeedle: Char): Boolean;
begin
  Result :=
    (umlcansistrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      umlcansichars.Equal(AHaystack[1], ANeedle);
  end;
end;

function StartsWithSameChar
  (const AHaystack: ansistring; const ANeedle: Char): Boolean;
begin
  Result :=
    (umlcansistrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      umlcansichars.SameText(AHaystack[1], ANeedle);
  end;
end;


function StartsWithEqualStr
  (const AHaystack: ansistring; const ANeedleStr: ansistring): Boolean;
var Shorter, Len: Word; LeadStr: ansistring;
begin
  Shorter :=
    umlcansistrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcansistrings.Length(AHaystack);
  // obtain length of full ansistring
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full ansistring
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    LeadStr :=
      System.Copy(AHaystack, 1, Shorter);
    Result  :=
      umlcansistrings.Equal(ANeedleStr, LeadStr);
  end;
  // Objetivo: Regresa si una subcadena es igual o esta al inicio de
  // otra cadena.

  // Goal: Returns if a substring is equal o is the start of another ansistring.
end;

function StartsWithSameStr
  (const AHaystack: ansistring; const ANeedleStr: ansistring): Boolean;
var Shorter, Len: Word; LeadStr: ansistring;
begin
  Shorter :=
    umlcansistrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcansistrings.Length(AHaystack);
  // obtain length of full ansistring
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full ansistring
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    LeadStr := System.Copy(AHaystack, 1, Shorter);
    Result  :=
      umlcansistrings.SameText(ANeedleStr, LeadStr);
  end;
  // Objetivo: Regresa si una subcadena es igual o esta al inicio de
  // otra cadena.

  // Goal: Returns if a substring is equal o is the start of another ansistring.
end;

function StartsWithEqualCharSet
  (const AHaystack: ansistring; const ANeedleSet: charset): Boolean;
begin
  Result :=
    (umlcansistrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      (umlcansicharsets.IsMember(ANeedleSet, AHaystack[1]));
  end;
end;

function StartsWithSameCharSet
  (const AHaystack: ansistring; const ANeedleSet: charset): Boolean;
begin
  Result :=
    (umlcansistrings.Length(AHaystack) > 0);
  if (Result) then
  begin
    Result :=
      (umlcansicharsets.IsSameMember(ANeedleSet, AHaystack[1]));
  end;
end;

function FinishesWithEqualChar
  (const AHaystack: ansistring; const ANeedleChar: ansistring): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcansistrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (AHaystack[Pred(ACount)] = ANeedleChar);
  end;
end;

function FinishesWithSameChar
  (const AHaystack: ansistring; const ANeedleChar: ansistring): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcansistrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (AHaystack[Pred(ACount)] = ANeedleChar);
  end;
end;

function FinishesWithEqualCharSet
 (const AHaystack: ansistring; const ANeedleSet: charset): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcansistrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (umlcansicharsets.IsMember(ANeedleSet, AHaystack[Pred(ACount)]));
  end;
end;

function FinishesWithSameCharSet
 (const AHaystack: ansistring; const ANeedleSet: charset): Boolean;
var ACount: Word;
begin
  ACount :=
    umlcansistrings.Length(AHaystack);
  Result :=
    (ACount > 0);
  if (Result) then
  begin
    Result :=
      (umlcansicharsets.IsSameMember(ANeedleSet, AHaystack[Pred(ACount)]));
  end;
end;

function FinishesWithEqualStr
  (const AHaystack: ansistring; const ANeedleStr: ansistring): Boolean;
var Shorter, Len, AIndex: Word; TrailingStr: ansistring;
begin
  Shorter :=
    umlcansistrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcansistrings.Length(AHaystack);
  // obtain length of full ansistring
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full ansistring
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    AIndex      :=
      (Len - Shorter + 1);
    TrailingStr :=
      System.Copy(AHaystack, AIndex, Shorter);
    Result      :=
      umlcansistrings.Equal(ANeedleStr, TrailingStr);
  end;
end;

function FinishesWithSameStr
  (const AHaystack: ansistring; const ANeedleStr: ansistring): Boolean;
var Shorter, Len, AIndex: Word; TrailingStr: ansistring;
begin
  Shorter :=
    umlcansistrings.Length(ANeedleStr);
  // obtain length of substring
  // obtener longuitud de subcadena

  Len :=
    umlcansistrings.Length(AHaystack);
  // obtain length of full ansistring
  // obtener longuitud de cadena completa

  Result := not (Shorter > Len);
  // substring must be shorter or equal size than full ansistring
  // la subcadena debe ser mas corta o de igual tamaño que la cadena completa

  if (Result) then
  begin
    AIndex      :=
      (Len - Shorter + 1);
    TrailingStr :=
      System.Copy(AHaystack, AIndex, Shorter);
    Result      :=
      umlcansistrings.SameText(ANeedleStr, TrailingStr);
  end;
end;


function ReplaceCharByCharCopy
  (const AHaystack: ansistring; ASource, ADest: ansichar): ansistring;
var I, L: Word;
begin
  umlcansistrings.Reset(Result);
  L :=
    umlcansistrings.Length(AHaystack);
  for i := 1 to L do
  begin
    if (AHaystack[i] = ASource)
      then umlcansistrings.ConcatChar(Result, ADest);
  end;
  // Goal: Replace a specific character from a ansistring.
  // Objetivo: Reemplazar un caracter en especifico de una cadena.
end;

function ReplaceCharSetByCharCopy
 (const AHaystack: ansistring; ASource: umlcansicharset; ADest: ansichar): ansistring;
var I, L: Word;
begin
  umlcansistrings.Reset(Result);
  L :=
    umlcansistrings.Length(AHaystack);
  for i := 1 to L do
  begin
    if (IsMember(ASource, AHaystack[i]))
      then umlcansistrings.ConcatChar(Result, ADest)
      else umlcansistrings.ConcatChar(Result, ASource[i]);
  end;
  // Goal: Replace a specific character set from a ansistring.
  // Objetivo: Reemplazar un conjunto caracter en especifico de una cadena.
end;

function RemoveCharCopy
  (const ASource: ansistring; IgnoreChar: ansichar): ansistring;
var I, L: Word;
begin
  umlcansistrings.Clear(Result);
  L :=
    umlcansistrings.Length(ASource);
  for i := 1 to L do
    if (ASource[i] <> IgnoreChar) then
    begin
      umlcansistrings.ConcatChar(Result, ASource[i]);
    end;
  // Goal: Delete a specific character from a ansistring.
  // Objetivo: Eliminar un caracter en especifico de una cadena.
end;

function RemoveCharSetCopy
  (const ASource: ansistring; IgnoreChars: umlcansicharset): ansistring;
var I, L: Word;
begin
  umlcansistrings.Clear(Result);
  L :=
    umlcansistrings.Length(ASource);
  for i := 1 to L do
  begin
    if (not IsMember(IgnoreChars, ASource[i])) then
    begin
      umlcansistrings.ConcatChar(Result, ASource[i]);
    end;
  end;
  // Goal: Delete a specific character set from a ansistring.
  // Objetivo: Eliminar un conjunto caracter en especifico de una cadena.
end;

function CharCountOf
  (const AHaystack: ansistring; ANeedleChar: ansichar): Cardinal;
var I, ALen: Cardinal;
begin
  Result := 0;
  if (not umlcansistrings.IsEmpty(AHaystack) and (not umlcansichars.IsNull(ANeedleChar))) then
  begin
    ALen :=
       umlcansistrings.Length(AHaystack);
    for I := 1 to ALen do
    begin
      if (AHaystack[I] = ANeedleChar) then
      begin
        System.Inc(Result);
      end;
    end;
  end;
end;

function CharSetCountOf
  (const AHaystack: ansistring; ANeedleSet: ansicharset): Cardinal;
var I, ALen: Cardinal;
begin
  Result := 0;
  if (not umlcansistrings.IsEmpty(AHaystack) and (not umlcansicharsets.IsEmpty(ANeedleSet))) then
  begin
    ALen :=
       umlcansistrings.Length(AHaystack);
    for I := 1 to ALen do
    begin
      if (umlcansicharsets.IsMember(ANeedleSet, AHaystack[I])) then
      begin
        System.Inc(Result);
      end;
    end;
  end;
end;

function ReplaceAllStrByStrCopy
  (const AHaystack: ansistring;
   const ASource:   ansistring;
   const ADest:     ansistring): ansistring;
var Finished: Boolean; Index: word;
    AHaystackCopy: ansistring;
begin
  Index := 0;
  Result :=
    AHaystack;
  repeat
    umlcansistrparsers.ReplaceSingleStrByStrAt
      (AHaystackCopy, Index, ASource, ADest);
    Finished := (Index = 0);
    //if (not Finished)
    //  then Inc(Result);
  until (Finished);
  // Goal: Replaces all the "SubStr" found in "FullStr" with "AValue".
  // Objetivo: Reemplaza todas las "SubStr" encontradas en "FullStr" por "AValue".}
end;

function ReplaceSingleStrByStrCopy
  (const AHaystack: ansistring;
   const ASource:   ansistring;
   const ADest:     ansistring): ansistring;
var Index: Word;
begin
  Result := AHaystack;
  Index  := 0;
  umlcansistrparsers.ReplaceSingleStrByStrAt
    (Result, Index, ASource, ADest);
  // Goal: Returns a copy of the ansistring with the given substring replaced.
  // Objetivo: Regresa una copia de la cadena con la subcadena reemplazada.
end;

function RemoveSingleStrCopy
  (const AHaystack: ansistring;
   const ANeedle:   ansistring): ansistring;
begin
  Result :=
    umlcansistrparsers.ReplaceSingleStrByStrCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the ansistring with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

function RemoveAllStrCopy
  (const AHaystack: ansistring;
   const ANeedle:   ansistring): ansistring;
begin
  Result :=
    umlcansistrparsers.ReplaceAllStrByStrCopy(AHaystack, ANeedle, '');
  // Goal: Returns a copy of the ansistring with the given substring deleted.
  // Objetivo: Regresa una copia de la cadena con la subcadena eliminada.
end;

function CopyFrom
  (const ASource: ansistring;
   const AStart:  Word;
   const AFinish: Word): ansistring;
var I, ACount: Word;
begin
  umlcansistrings.Reset(Result);

  ACount :=
    Succ(AFinish - AStart);

  for I := 1 to ACount do
  begin
    umlcansistrings.ConcatChar(Result, ASource[i]);
  end;
  // Goal: Returns a substring from "astart" to "afinish".
  // Objetivo: Regresa una subcadena de "astart" hasta "afinish".
end;

function ParseFrom
  (const ASource: ansistring;
   var   AStart:  Word;
   const AFinish: Word): ansistring;
var I, ACount: Word;
begin
  umlcansistrings.Reset(Result);

  ACount :=
    (AFinish - AStart);

  for I := 1 to ACount do
  begin
    umlcansistrings.ConcatChar(Result, ASource[Pred(AStart + i)]);
  end;

  AStart := AFinish;
  // Goal: Returns a substring from "astart" to "afinish" & updates "astart".
  // Objetivo: Regresa una subcadena desde "astart" hasta "afinish"
  // y actualiza "astart".
end;

function SkipChar
  (const ASource: ansistring;
   const AIndex: word;
   const AValidChar: ansichar): Word;
var L: word;
begin
  Result := AIndex;
  L := umlcansistrings.Length(ASource);
  if (Result <= L) and (ASource[Result] = AValidChar) then
  begin
    Inc(Result);
  end;
  // Goal: Returns the index of a single character, if matches.
  // Objetivo: Regresa el indice de solo caracter, si coincide.
end;

function SkipCharSet
  (const ASource: ansistring; AIndex: Word; AValidChars: ansicharset): Word;
var L: Word;
begin
  Result := AIndex;
  L := umlcansistrings.Length(ASource);
  if (Result <= L) and umlcansicharsets.IsMember(AValidChars, ASource[Result]) then
  begin
    Inc(Result);
  end;
  // Goal: Returns the index of a single character set item, if matches.
  // Objetivo: Regresa el indice de un solo caracter de un conjunto, si coincide.
end;

function SkipCharWhile
  (const ASource:    ansistring;
   const AIndex:     Word;
   const AValidChar: ansichar): Word;
var L: Word;
begin
  Result := AIndex;
  L := umlcansistrings.Length(ASource);
  while ((Result <= L) and (ASource[Result] = AValidChar)) do
  begin
    Inc(Result);
  end;
  // Goal: Returns the index while a sequence of the same character is found.
  // Objetivo: Regresa el indice mientras una secuencia del mismo caracter es encontrada.
end;

function SkipCharUntil
  (const ASource:   ansistring;
   const AIndex:    Word;
   const BreakChar: ansichar): Word;
var L: Word;
begin
  Result := AIndex;
  L := umlcansistrings.Length(ASource);
  while ((Result <= L) and (ASource[Result] <> BreakChar)) do
  begin
    Inc(Result);
  end;
  // Goal: Returns the index while a sequence of the same character is not found.
  // Objetivo: Regresa el indice mientras una secuencia del mismo caracter no es encontrada.
end;

function SkipCharSetWhile
  (const ASource:     ansistring;
   const AIndex:      Word;
   const AValidChars: ansicharset): Word;
var L: Word;
begin
  Result := AIndex;
  L :=
    umlcansistrings.Length(ASource);
  while ((Result <= L) and umlcansicharsets.IsMember(AValidChars, ASource[Result])) do
  begin
    Inc(Result);
  end;
  // Goal: Returns the index while a sequence of the same character set is found.
  // Objetivo: Regresa el indice mientras una secuencia del mismo conjunto caracter es encontrada.
end;

function SkipCharSetUntil
  (const ASource:    ansistring;
   const AIndex:     Word;
   const BreakChars: ansicharset): Word;
var L: Word;
begin
  Result := AIndex;
  L :=
    umlcansistrings.Length(ASource);
  while ((Result <= L) and not umlcansicharsets.IsMember(BreakChars, ASource[Result])) do
  begin
    Inc(Result);
  end;
  // Goal: Returns the index while a sequence of the same character set is not found.
  // Objetivo: Regresa el indice mientras una secuencia del mismo conjunto caracter no es encontrada.
end;

function SkipToken
  (const ASource:      ansistring;
   var   AStartIndex:  Word;
   var   AFinishIndex: Word): ansistring;
var StartIndex, FinishIndex: Word;
begin
  StartIndex  := AStartIndex;

  StartIndex  :=
    umlcansistrparsers.SkipCharWhile(ASource, StartIndex, #32);
  FinishIndex :=
    umlcansistrparsers.SkipCharUntil(ASource, StartIndex, #32);
  Result      :=
    umlcansistrparsers.CopyFrom(ASource, StartIndex, FinishIndex);

  AStartIndex  := StartIndex;
  AFinishIndex := FinishIndex;
end;

function SkipDigit
  (const ASource:      ansistring;
   var   AStartIndex:  Word;
   var   AFinishIndex: Word): ansistring;
begin
  AStartIndex  :=
    umlcansistrparsers.SkipCharWhile(ASource, AStartIndex, #32);
  AFinishIndex :=
    umlcansistrparsers.SkipCharSetWhile(ASource, AStartIndex, DecDigitSet);
  Result :=
    umlcansistrparsers.CopyFrom(ASource, AStartIndex, AFinishIndex);
  AStartIndex := AFinishIndex;
end;

function SkipLetter
  (const ASource:      ansistring;
   var   AStartIndex:  Word;
   var   AFinishIndex: Word): ansistring;
begin
  AStartIndex  :=
    umlcansistrparsers.SkipCharWhile(ASource, AStartIndex, #32);
  AFinishIndex :=
    umlcansistrparsers.SkipCharSetWhile(ASource, AStartIndex, AlphaSet);
  Result :=
    umlcansistrparsers.CopyFrom(ASource, AStartIndex, AFinishIndex);
  AStartIndex := AFinishIndex;
end;



(* global procedures *)

procedure ReplaceSingleStrByStrAt
  (var   AHaystack: ansistring;
   var   AIndex:    Word;
   const ASource:   ansistring;
   const ADest:     ansistring);
var BeforeCount, AfterCount: Word;
    BeforeString, AfterString: ansistring;
begin
  BeforeString := '';
  AfterString  := '';
  AIndex :=
    umlcansistrparsers.StrPosOf(AHaystack, ASource);
  // localizar en que posicion inicia la cadena que se va BeforeString reemplazar
  // locate in which location starts the ansistring to be replaced

  if (AIndex > 0) then
  begin
    AfterCount :=
      AIndex + umlcansistrings.Length(ASource);
    // obtener la longuitud de la cadena despues de la parte antes a reemplazar
    // obtain the length of the ansistring after the section to be replaced

    BeforeCount :=
      umlcansistrings.Length(AHaystack) - umlcansistrings.Length(ASource);
    // obtener la longuitud de la cadena antes de la parte antes a reemplazar
    // obtain the length of the ansistring before the section to be replaced

    if (AIndex <> 0) then
    begin
      umlcansistrings.AssignLeft(BeforeString, AHaystack, BeforeCount);
      umlcansistrings.AssignRight(AfterString, AHaystack, AfterCount);
      // extraer secciones antes y despues de cadena nodeseada
      // extract before & after section of unwanted string

      umlcansistrings.Reset(AHaystack);
      umlcansistrings.ConcatStr(AHaystack, BeforeString);
      umlcansistrings.ConcatStr(AHaystack, ADest);
      umlcansistrings.ConcatStr(AHaystack, AfterString);
    end;
  end;
  // Goal: Replaces the first "Source" found in "FullStr" with "Dest".
  // Objetivo: Reemplaza la primera "Source" encontrada en "FullStr" por "Dest".}
end;


end.

