unit ufrmmain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls,
  umlcdebug,
  umlcobjtypes,
  umlcobjstrtypes, umlcobjinttypes, umlcobjfloatbtypes,
  umlcobjdatetimetypes,
  dummy;

type

  { Tfrmmain }

  Tfrmmain = class(TForm)
    btnExit: TButton;
    btnTest: TButton;
    mmConsole: TMemo;
    pnTop: TPanel;
    sbMain: TStatusBar;
    procedure btnExitClick(Sender: TObject);
    procedure btnTestClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    (* private declarations *)
  public
    (* public declarations *)

    DebugStr: string;

    procedure DebugWrite(const AMsg: string);
    procedure DebugWriteEoLn();
    procedure DebugWriteLn(const AMsg: string);
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.lfm}

{ Tfrmmain }

procedure Tfrmmain.DebugWrite(const AMsg: string);
begin
  DebugStr := DebugStr + AMsg;
end;

procedure Tfrmmain.DebugWriteEoLn();
begin
  mmConsole.Lines.Add(DebugStr);
  DebugStr := '';
end;

procedure Tfrmmain.DebugWriteLn(const AMsg: string);
begin
  self.DebugWrite(AMsg);
  self.DebugWriteEoLn();
end;

procedure Tfrmmain.FormCreate(Sender: TObject);
begin
  DebugStr := '';
  umlcdebug.DebugWrite :=
    @self.DebugWrite;
  umlcdebug.DebugWriteEoLn :=
    @self.DebugWriteEoLn;
  umlcdebug.DebugWriteLn :=
    @self.DebugWriteLn;
end;

procedure Tfrmmain.btnExitClick(Sender: TObject);
begin
  Self.Close();
end;

procedure Tfrmmain.btnTestClick(Sender: TObject);
var ABoolean: TUMLCBooleanObject;
    AChar: TUMLCCharObject;
    AStr: TUMLCStringObject;
    AByte: TUMLCByteObject;
    AWord: TUMLCWordObject;
    AShortInt: TUMLCShortIntObject;
    ALongInt: TUMLCLongIntObject;
    AReal: TUMLCRealObject;
    ADouble: TUMLCDoubleObject;
    ADate: TUMLCDateObject;
    ATime: TUMLCTimeObject;
    ADateTime: TUMLCDateTimeObject;
begin
  mmConsole.Clear();
  mmConsole.Lines.Add('Java alike Object Classes');
  mmConsole.Lines.Add('');

  ABoolean := TUMLCBooleanObject.CreateByValue(true);
  AChar := TUMLCCharObject.CreateByValue('@');
  AStr := TUMLCStringObject.CreateByValue('Hello World');
  AByte := TUMLCByteObject.CreateByValue(254);
  AWord := TUMLCWordObject.CreateByValue(256);
  AShortInt := TUMLCShortIntObject.CreateByValue(256);
  ALongInt := TUMLCLongIntObject.CreateByValue(256);
  AReal := TUMLCRealObject.CreateByValue(256);
  ADouble := TUMLCDoubleObject.CreateByValue(256);
  //ADate :=
  //  TUMLCDateObject.CreateByValue(256);
  //ATime :=
  //  TUMLCTimeObject.CreateByValue(256);
  //ADateTime :=
  //  TUMLCDateTimeObject.CreateByValue(256);



  mmConsole.Lines.Add('Boolean: [' + ABoolean.AsText() +']');
  mmConsole.Lines.Add('Character: [' + AChar.AsText() +']');
  mmConsole.Lines.Add('String: [' + AStr.AsText() +']');
  mmConsole.Lines.Add('Byte: [' + AByte.AsText() +']');
  mmConsole.Lines.Add('Word: [' + AWord.AsText() +']');
  mmConsole.Lines.Add('ShortInt: [' + AShortInt.AsText() +']');
  mmConsole.Lines.Add('LongInt: [' + ALongInt.AsText() +']');
  mmConsole.Lines.Add('Real: [' + AReal.AsText() +']');
  mmConsole.Lines.Add('Double: [' + ADouble.AsText() +']');
  mmConsole.Lines.Add('Date: [' + ADate.AsText() +']');
  mmConsole.Lines.Add('Time: [' + ATime.AsText() +']');
  mmConsole.Lines.Add('DateTime: [' + ADateTime.AsText() +']');
  mmConsole.Lines.Add('Word: [' + AWord.AsText() +']');
  mmConsole.Lines.Add('Word: [' + AWord.AsText() +']');

  mmConsole.Lines.Add('');

  //ADateTime.Free();
  //ATime.Free();
  //ADate.Free();
  ADouble.Free();
  AReal.Free();
  ALongInt.Free();
  AShortInt.Free();
  AWord.Free();
  AByte.Free();
  AStr.Free();
  AChar.Free();
  ABoolean.Free();

  mmConsole.Lines.Add('');
  mmConsole.Lines.Add('Done.');
end;





end.

