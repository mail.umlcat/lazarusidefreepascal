(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstrtypeobjs;

  // Encapsulate standard and simple types into objects,
  // like Java / ECMAScript

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcnormobjs,
  umlcstdobjs,
  umlctypeobjs,
  dummy;

// ---

const

  MOD_umlcstrtypeobjs : TUMLCModule =
    ($37,$66,$93,$1A,$15,$03,$00,$42,$84,$B4,$18,$66,$D3,$F6,$14,$6D);

// ---

const

 ID_TUMLCCharObject : TUMLCType =
   ($AF,$D1,$CA,$10,$EF,$FC,$5A,$45,$89,$50,$73,$DE,$34,$14,$26,$8C);

 ID_TUMLCStringObject : TUMLCType =
   ($1B,$1A,$A3,$91,$B2,$4D,$FF,$43,$BC,$DB,$D9,$FA,$A0,$88,$71,$7F);

// ---

type

(* TUMLCCharObject *)

  TUMLCCharObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: Char;

    function getValue: Char;
    procedure setValue(const AValue: Char);
  public
    { public declarations }

    constructor CreateByValue(const AValue: Char);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: Char read getValue write setValue;
  end;

(* TUMLCAnsiCharObject *)

  TUMLCAnsiCharObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: AnsiChar;

    function getValue: AnsiChar;
    procedure setValue
      (const AValue: AnsiChar);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: AnsiChar);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: AnsiChar read getValue write setValue;
  end;

(* TUMLCWideCharObject *)

  TUMLCWideCharObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: WideChar;

    function getValue: WideChar;
    procedure setValue
      (const AValue: WideChar);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: WideChar);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: WideChar read getValue write setValue;
  end;

(* TUMLCStringObject *)

  TUMLCStringObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: String;

    function getValue: String;
    procedure setValue(const AValue: String);
  public
    { public declarations }

    constructor CreateByValue(const AValue: String);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: String read getValue write setValue;
  end;

(* TUMLCAnsiStringObject *)

  TUMLCAnsiStringObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: ansistring;

    function getValue: String;
    procedure setValue
      (const AValue: ansistring);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: ansistring);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: ansistring read getValue write setValue;
  end;

(* TUMLCWideStringObject *)

  TUMLCWideStringObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: WideString;

    function getValue: WideString;
    procedure setValue
      (const AValue: WideString);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: WideString);
  public
    { public declarations }

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    function AsText(): string; override;

    property Value: WideString read getValue write setValue;
  end;

implementation

(* TUMLCCharObject *)

function TUMLCCharObject.getValue: Char;
begin
  Result := FValue;
end;

procedure TUMLCCharObject.setValue(const AValue: Char);
begin
  FValue := AValue;
end;

constructor TUMLCCharObject.CreateByValue(const AValue: Char);
begin
  FValue := AValue;
end;

procedure TUMLCCharObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCCharObject;
begin
  ATypeObject :=
    (ASource as TUMLCCharObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCCharObject.AsText(): string;
begin
  Result := FValue;
end;

(* TUMLCAnsiCharObject *)

function TUMLCAnsiCharObject.getValue: AnsiChar;
begin
  Result := FValue;
end;

procedure TUMLCAnsiCharObject.setValue
  (const AValue: AnsiChar);
begin
  FValue := AValue;
end;

constructor TUMLCAnsiCharObject.CreateByValue
   (const AValue: AnsiChar);
begin
  FValue := AValue;
end;

procedure TUMLCAnsiCharObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCAnsiCharObject;
begin
  ATypeObject :=
    (ASource as TUMLCAnsiCharObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCAnsiCharObject.AsText(): string;
begin
  Result := FValue;
end;

(* TUMLCWideCharObject *)

function TUMLCWideCharObject.getValue: WideChar;
begin
  Result := FValue;
end;

procedure TUMLCWideCharObject.setValue
  (const AValue: WideChar);
begin
  FValue := AValue;
end;

constructor TUMLCWideCharObject.CreateByValue
  (const AValue: WideChar);
begin
  FValue := AValue;
end;

procedure TUMLCWideCharObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCWideCharObject;
begin
  ATypeObject :=
    (ASource as TUMLCWideCharObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCWideCharObject.AsText(): string;
begin
  Result := FValue;
end;

(* TUMLCStringObject *)

function TUMLCStringObject.getValue: String;
begin
  Result := FValue;
end;

procedure TUMLCStringObject.setValue(const AValue: String);
begin
  FValue := AValue;
end;

constructor TUMLCStringObject.CreateByValue(const AValue: String);
begin
  FValue := AValue;
end;

procedure TUMLCStringObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCStringObject;
begin
  ATypeObject :=
    (ASource as TUMLCStringObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCStringObject.AsText(): string;
begin
  Result := FValue;
end;


(* TUMLCStringObject *)

function TUMLCAnsiStringObject.getValue: ansistring;
begin
  Result := FValue;
end;

procedure TUMLCAnsiStringObject.setValue
  (const AValue: ansistring);
begin
  FValue := AValue;
end;

constructor TUMLCAnsiStringObject.CreateByValue
  (const AValue: ansistring);
begin
  FValue := AValue;
end;

procedure TUMLCAnsiStringObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCAnsiStringObject;
begin
  ATypeObject :=
    (ASource as TUMLCAnsiStringObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCAnsiStringObject.AsText(): string;
begin
  Result := FValue;
end;

(* TUMLCStringObject *)

function TUMLCWideStringObject.getValue: widestring;
begin
  Result := FValue;
end;

procedure TUMLCWideStringObject.setValue
  (const AValue: widestring);
begin
  FValue := AValue;
end;

constructor TUMLCWideStringObject.CreateByValue
  (const AValue: widestring);
begin
  FValue := AValue;
end;

procedure TUMLCWideStringObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCWideStringObject;
begin
  ATypeObject :=
    (ASource as TUMLCWideStringObject);
  Self.FValue := ATypeObject.FValue;
end;

function TUMLCWideStringObject.AsText(): string;
begin
  Result := FValue;
end;

end.

