(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcstdobjs;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcnormobjs,
  dummy;

// ---

const

  MOD_umlcstdobjs : TUMLCModule =
    ($52,$01,$6D,$D6,$9E,$EE,$17,$49,$85,$77,$B8,$05,$74,$D3,$C4,$8F);

// ---

const

 // ---

 ID_IUMLCCloneableObject : TUMLCType =
   ($30,$EB,$95,$5D,$A4,$F9,$90,$4F,$9C,$E8,$90,$E0,$20,$A2,$CB,$44);

 ID_IUMLCInternalCloneableObject : TUMLCType =
   ($0D,$AD,$82,$E8,$AD,$5F,$73,$41,$86,$5D,$E1,$8E,$79,$6F,$47,$F7);

 ID_TUMLCCloneableObject : TUMLCType =
   ($B7,$D5,$AE,$04,$EC,$10,$4A,$42,$88,$DA,$45,$BA,$C4,$91,$E2,$C3);

 ID_TUMLCInternalCloneableObject : TUMLCType =
   ($49,$8A,$F4,$05,$EB,$08,$5F,$45,$B3,$17,$DF,$72,$67,$59,$35,$FD);

 // ---


type

(* IUMLCCloneableObject *)

IUMLCCloneableObject =
  interface(IUMLCNormalizedObject)
  ['{30EB955D-A4F9-904F-9CE8-90E020A2CB44}']

  (* interface declarations *)

  procedure AssignFrom
    (const ASource: IUMLCCloneableObject); // virtual;

  //constructor CreateByCopy
    //(const ASource: IUMLCMyClassObject); virtual;
end;

(* IUMLCInternalCloneableObject *)

IUMLCInternalCloneableObject =
  interface(IUMLCNormalizedObject)
  ['{0DAD82E8-AD5F-7341-865D-E18E796F47F7}']

  { interface declarations }

  procedure InternalAssignFrom
    (const ASource: IUMLCInternalCloneableObject); // virtual;

  //procedure AssignFrom
    //(const ASource: IUMLCMyClassObject); // nonvirtual;

  //constructor CreateByCopy
    //(const ASource: IUMLCMyClassObject); virtual;
end;

(* TUMLCCloneableObject *)

  TUMLCCloneableObject =
    class(TUMLCNormalizedObject, IUMLCCloneableObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    constructor CreateByCopy
      (const ASource: IUMLCCloneableObject); virtual;
  public
    (* public declarations *)

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); virtual;
    procedure AssignTo
      (var ADest: IUMLCCloneableObject); // nonvirtual;
  end;

(* TUMLCInternalCloneableObject *)

  TUMLCInternalCloneableObject =
    class(TUMLCNormalizedObject, IUMLCInternalCloneableObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    procedure InternalAssignFrom
      (const ASource: IUMLCInternalCloneableObject); virtual;
    procedure InternalAssignTo
      (var ADest: IUMLCInternalCloneableObject); // nonvirtual;
  public
    (* public declarations *)
//
//      constructor CreateByCopy
//        (const ASource: IUMLCCloneableObject); virtual;
  end;

implementation

{ TUMLCCloneableObject }

constructor TUMLCCloneableObject.CreateByCopy
  (const ASource: IUMLCCloneableObject);
begin
  inherited (* nonvirtual *) Create();
  Self.AssignFrom(ASource);
end;

procedure TUMLCCloneableObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
begin
  Self.DoNothing();
end;

procedure TUMLCCloneableObject.AssignTo
  (var ADest: IUMLCCloneableObject);
begin
  ADest.AssignFrom(Self);
end;

(* TUMLCInternalCloneableObject *)

procedure TUMLCInternalCloneableObject.InternalAssignFrom
  (const ASource: IUMLCInternalCloneableObject);
begin
  Self.DoNothing();
end;

procedure TUMLCInternalCloneableObject.InternalAssignTo
  (var ADest: IUMLCInternalCloneableObject);
begin
  ADest.InternalAssignFrom(Self);
end;

end.

