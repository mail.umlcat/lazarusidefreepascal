(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcnormobjs;

{$mode objfpc}{$H+}

interface
uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  dummy;

(**
 ** Description:
 ** This unit defines an interface for objects,
 ** that supports:
 **
 ** A "normalized" object / class is an object that supports,
 ** partially or fully, the "Normalized Object Software Design Pattern".
 **
 ** . A default virtual constructor without parameters,
 ** . A default virtual destructor without parameters,
 ** . A default virtual function that returns
 **   an string like Java "ToString()" function
 ** . A non virtual method that explicitly does nothing
 **   like z80 CPU instruction "NOP"
 **
 ** The term "Normalized" is "borrowed" from Database Design.
 **
 ** It also provides a base class that supports that interface.
 **)

// ---

const

  MOD_umlcnormobjs : TUMLCModule =
    ($E4,$01,$0C,$62,$01,$CE,$CA,$4A,$BE,$A3,$7F,$4F,$DD,$8B,$4A,$49);

// ---

const

 // ---

 ID_IUMLCNormalizedObject : TUMLCType =
  ($5F,$E0,$03,$B8,$EC,$12,$A8,$47,$93,$1D,$3B,$13,$EA,$58,$70,$65);

 ID_IUMLCExtendedNormalizedObject : TUMLCType =
   ($30,$F6,$F3,$66,$3A,$51,$25,$48,$B5,$66,$AA,$D1,$8C,$09,$F2,$E2);

 ID_TUMLCNormalizedObject : TUMLCType =
   ($E4,$62,$89,$E3,$36,$47,$6E,$41,$81,$75,$BF,$A0,$5C,$C3,$64,$DE);

 ID_TUMLCExtendedNormalizedObject : TUMLCType =
   ($42,$89,$93,$21,$54,$05,$22,$48,$A2,$AB,$36,$DF,$AF,$0A,$25,$11);

// ---

type

(* IUMLCNormalizedObject *)

  IUMLCNormalizedObject =
    interface(IUnknown)
    ['{371704a5-e48d-4969-ad74-ea7f6ac367c6}']

    (* interface declarations *)

    //procedure Create(); // virtual;
    //procedure Destroy(); // virtual;

    // --> constructor cannot be assigned to an interface

    // let's provide a default virtual without parameters constructor
    //destructor Create(); virtual;

    // --> constructor cannot be assigned to an interface

    // let's provide a default virtual without parameters destructor
    //destructor Destroy(); override;

    function AsText(): string; // virtual;

    procedure DoNothing(); // nonvirtual;
  end;

(* IUMLCExtendedNormalizedObject *)

  IUMLCExtendedNormalizedObject =
    interface(IUMLCNormalizedObject)
    ['{207fe9a6-c374-41b2-85eb-0ff9afff40c3}']

    (* interface declarations *)

    // let's provide a default virtual without parameters constructor
    procedure DoCreate(); // virtual;

    // let's provide a default virtual without parameters destructor
    procedure DoDestroy(); // virtual;

    //function AsText(): string; // virtual;

    //procedure DoNothing(); // nonvirtual;
  end;

(* TUMLCNormalizedObject *)

  TUMLCNormalizedObject =
    class(TInterfacedObject, IUMLCNormalizedObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    // TObject.Create() is non-virtual
    // let's provide a default virtual without parameters constructor
    constructor Create(); virtual;
    // let's provide a default virtual without parameters destructor
    destructor Destroy(); override;

    // similar to Java's "ToString()"
    function AsText(): string; virtual;

    procedure DoNothing(); // nonvirtual;
  end;

(* TUMLCExtendedNormalizedObject *)

  TUMLCExtendedNormalizedObject =
    class(TUMLCNormalizedObject, IUMLCExtendedNormalizedObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    // TObject.Create() is non-virtual
    // do not override "TObject.Create()", use "DoCreate", instead

    // do not override this, use "DoDestroy", instead
    destructor Destroy(); reintroduce;

    // let's provide a default virtual without parameters constructor
    procedure DoCreate(); virtual;
    // let's provide a default virtual without parameters destructor
    procedure DoDestroy(); virtual;

    // similar to Java's "ToString()"
    function AsText(): string; override;

    procedure DoNothing(); // nonvirtual;
  end;

implementation

{ TUMLCExtendedNormalizedObject }

destructor TUMLCExtendedNormalizedObject.Destroy;
begin
  DoNothing();
end;

procedure TUMLCExtendedNormalizedObject.DoCreate();
begin
  DoNothing();
end;

procedure TUMLCExtendedNormalizedObject.DoDestroy();
begin
  DoNothing();
end;

function TUMLCExtendedNormalizedObject.AsText(): string;
begin
  Result := ClassName();
end;

procedure TUMLCExtendedNormalizedObject.DoNothing;
begin
  // DoNothing !!!
end;

(* TUMLCNormalizedObject *)

constructor TUMLCNormalizedObject.Create();
begin
  inherited (* nonvirtual *) Create();
end;

destructor TUMLCNormalizedObject.Destroy();
begin
  inherited (* virtual *) Destroy();
end;

function TUMLCNormalizedObject.AsText(): string;
begin
  Result := ClassName();
end;

procedure TUMLCNormalizedObject.DoNothing();
begin
  // Does nothing on purpose !!!
end;

end.

