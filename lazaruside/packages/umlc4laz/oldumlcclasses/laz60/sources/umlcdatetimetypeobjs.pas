(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the umlcat Developer's Component Library.        *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcdatetimetypeobjs;

  // Encapsulate standard and simple types into objects,
  // like Java / ECMAScript

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  umlcmodules, umlctypes,
  umlcstdtypes,
  umlcstddatetimetypes,
  umlcstdtimes,
  umlcstddates,
  umlcstddatetimes,
  umlcstdtimestamps,
  umlcnormobjs,
  umlcstdobjs,
  umlctypeobjs,
  dummy;

// ---

const

  MOD_umlcumlcdatetimetypeobjs: TUMLCModule =
    ($F2,$B7,$A5,$83,$C4,$5E,$99,$48,$94,$36,$72,$58,$09,$C8,$95,$8D);

// ---

const

  ID_TUMLCTimeObject : TUMLCType =
    ($D7,$46,$06,$97,$16,$0F,$B7,$4C,$9C,$F9,$9B,$5A,$51,$61,$ED,$F5);

  ID_TUMLCDateObject : TUMLCType =
    ($08,$F7,$61,$94,$42,$89,$55,$4F,$86,$49,$46,$11,$6C,$AA,$CB,$B2);

  ID_TUMLCDateTimeObject : TUMLCType =
    ($CA,$83,$81,$C3,$90,$BF,$CF,$43,$9F,$0E,$FF,$A9,$CD,$4B,$2B,$AC);

  ID_TUMLCTimeStampObject : TUMLCType =
    ($DB,$DE,$1D,$8F,$C6,$00,$8C,$47,$A6,$44,$5F,$36,$ED,$C0,$F8,$AF);

// ---

type

{ TUMLCTimeObject }

  TUMLCTimeObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: umlcstdtime;

    function getValue: umlcstdtime;
    procedure setValue(const AValue: umlcstdtime);
  public
    { public declarations }

    constructor CreateByValue
      (const AHour: Word;
       const AMin: Word;
       const ASec: Word;
       const AMSec: Word);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: umlcstdtime read getValue write setValue;
  end;

{ TUMLCDateObject }

  TUMLCDateObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: umlcdate;

    function getValue: umlcdate;
    procedure setValue(const AValue: umlcdate);
  public
    { public declarations }

    constructor CreateByValue
      (const AYear:  Word;
       const ADay:  Word;
       const AMonth: Word);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: umlcdate read getValue write setValue;
  end;

{ TUMLCDateTimeObject }

  TUMLCDateTimeObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: umlcdateTime;

    function getValue: umlcdateTime;
    procedure setValue(const AValue: umlcdateTime);
  public
    { public declarations }

    constructor CreateByValue
      (const AHour:  Word;
       const AMin:   Word;
       const ASec:   Word;
       const AMSec:  Word;
       const AYear:  Word;
       const ADay:   Word;
       const AMonth: Word);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: umlcdateTime read getValue write setValue;
  end;

{ TUMLCTimeStampObject }

  TUMLCTimeStampObject =
    class(TUMLCPrimitiveTypeObject)
  private
    { private declarations }
  protected
    { protected declarations }

    FValue: umlcstdtimeStamp;

    function getValue: umlcstdtimeStamp;
    procedure setValue
      (const AValue: umlcstdtimeStamp);
  public
    { public declarations }

    constructor CreateByValue
      (const AValue: umlcstdtimeStamp);
  public
    { public declarations }

    function AsText(): string; override;

    procedure AssignFrom
      (const ASource: IUMLCCloneableObject); override;

    property Value: umlcstdtimeStamp read getValue write setValue;
  end;

implementation


{ TUMLCTimeObject }

function TUMLCTimeObject.getValue: umlcstdtime;
begin
  Result := FValue;
end;

procedure TUMLCTimeObject.setValue
  (const AValue: umlcstdtime);
begin
  FValue := AValue;
end;

constructor TUMLCTimeObject.CreateByValue
  (const AHour: Word;
   const AMin: Word;
   const ASec: Word;
   const AMSec: Word);
begin
  FValue :=
    SysUtils.EncodeTime
      (AHour, AMin, ASec, AMSec);
end;

function TUMLCTimeObject.AsText(): string;
begin
  //Result := umlcstdtimes.TimeToStr(FValue);
  Result := '';
end;

procedure TUMLCTimeObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCTimeObject;
begin
  ATypeObject :=
    (ASource as TUMLCTimeObject);
  Self.FValue := ATypeObject.FValue;
end;

{ TUMLCDateObject }

function TUMLCDateObject.getValue: umlcdate;
begin
  Result := FValue;
end;

procedure TUMLCDateObject.setValue(const AValue: umlcdate);
begin
  FValue := AValue;
end;

constructor TUMLCDateObject.CreateByValue
 (const AYear:  Word;
  const ADay:   Word;
  const AMonth: Word);
begin
  FValue :=
    SysUtils.EncodeDate
      (AYear, ADay, AMonth);
end;

function TUMLCDateObject.AsText(): string;
begin
  //Result := umlcdates.DateToStr(FValue);
  Result := '';
end;

procedure TUMLCDateObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCDateObject;
begin
  ATypeObject :=
    (ASource as TUMLCDateObject);
  Self.FValue := ATypeObject.FValue;
end;

{ TUMLCDateTimeObject }

function TUMLCDateTimeObject.getValue: umlcdateTime;
begin
  Result := FValue;
end;

procedure TUMLCDateTimeObject.setValue(const AValue: umlcdateTime);
begin
  FValue := AValue;
end;

constructor TUMLCDateTimeObject.CreateByValue
  (const AHour:  Word;
   const AMin:   Word;
   const ASec:   Word;
   const AMSec:  Word;
   const AYear:  Word;
   const ADay:   Word;
   const AMonth: Word);
begin
  umlcstddatetimes.EncodeDateTime
    (FValue, AYear, AMonth, ADay, AHour, AMin, ASec, AMSec);
end;

function TUMLCDateTimeObject.AsText(): string;
begin
  //Result := umlcdatetimes.DateTimeToStr(FValue);
  Result := '';
end;

procedure TUMLCDateTimeObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCDateTimeObject;
begin
  ATypeObject :=
    (ASource as TUMLCDateTimeObject);
  Self.FValue := ATypeObject.FValue;
end;

{ TUMLCTimeStampObject }

function TUMLCTimeStampObject.getValue: umlcstdtimeStamp;
begin
  Result := FValue;
end;

procedure TUMLCTimeStampObject.setValue
  (const AValue: umlcstdtimeStamp);
begin
  FValue := AValue;
end;

constructor TUMLCTimeStampObject.CreateByValue
  (const AValue: umlcstdtimeStamp);
begin
  FValue := AValue;
end;

function TUMLCTimeStampObject.AsText(): string;
begin
  //Result := umlcstdtimestamps.TimeStampToStr(FValue);
  Result := '';
end;

procedure TUMLCTimeStampObject.AssignFrom
  (const ASource: IUMLCCloneableObject);
var ATypeObject: TUMLCTimeStampObject;
begin
  ATypeObject :=
    (ASource as TUMLCTimeStampObject);
  Self.FValue := ATypeObject.FValue;
end;

end.

