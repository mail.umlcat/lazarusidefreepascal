{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit umlcclasses60;

{$warn 5023 off : no warning about unused units}
interface

uses
  umlcclasses_rtti, umlckeytypevaluetokens, umlckeyvaluemodes, umlcmodobjs, 
  umlcnormobjs, umlctypeobjs, umlconlystringkeytypevaluelists, 
  umlconlystringkeyvaluelists, umlcstdobjs, umlcstrstrlists, umlcinttypeobjs, 
  umlcdatetimetypeobjs, umlcfloattypeobjs, umlcstrtypeobjs, umlcbooltypeobjs;

implementation

end.
