(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctogglespeedbtns;

interface
uses
  SysUtils,
  Classes,
(*.IFDEF MSWINDOWS*)
  //Windows,
  //Messages,
  Graphics,
  Controls, 
  //StdCtrls,
  Forms,
  Buttons,
(*.ENDIF*)
  umlctogglebuttons,
  umlcspeedbtns,
  dummy;

type

(* TCustomUMLCToggledSpeedButton *)

  TCustomUMLCToggledSpeedButton = class(TCustomUMLCSpeedButton, IUMLCToggledButton)
  private
    (* Private declarations *)
  protected
    (* Protected declarations *)

    FToggled: Boolean;

    FCaptions: array[Boolean] of string;
    FGlyphs:   array[Boolean] of TBitmap;

    function getCaptionToggled: string; virtual;
    function getCaptionUnToggled: string; virtual;

    function getGlyphToggled: TBitmap; virtual;
    function getGlyphUnToggled: TBitmap; virtual;

    function getToggled: Boolean; virtual;

    procedure setCaptionToggled(const Value: string); virtual;
    procedure setCaptionUnToggled(const Value: string); virtual;

    procedure setGlyphToggled(const Value: TBitmap); virtual;
    procedure setGlyphUnToggled(const Value: TBitmap); virtual;

    procedure setToggled(const Value: Boolean); virtual;

    procedure RefreshButton();
  public
    (* Public declarations *)

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Activated;
    procedure Click; override;

    property Toggled: Boolean
      read getToggled write setToggled;

    property GlyphToggled: TBitmap
      read getGlyphToggled write setGlyphToggled;
    property GlyphUnToggled: TBitmap
      read getGlyphUnToggled write setGlyphUnToggled;

    property CaptionToggled: string
      read getCaptionToggled write setCaptionToggled;
    property CaptionUnToggled: string
      read getCaptionUnToggled write setCaptionUnToggled;
  end;

(* TUMLCToggledSpeedButton *)

  TUMLCToggledSpeedButton = class(TCustomUMLCToggledSpeedButton)
  published
    (* Published declarations *)

    (* TCustomUMLCToggledSpeedButton: *)

    property Toggled;

    property GlyphToggled;
    property GlyphUnToggled;

    property CaptionToggled;
    property CaptionUnToggled;
  end;

implementation

(* TCustomUMLCToggledSpeedButton *)

function TCustomUMLCToggledSpeedButton.getToggled: Boolean;
begin
  Result := FToggled;
end;

function TCustomUMLCToggledSpeedButton.getGlyphToggled: TBitmap;
begin
  Result := FGlyphs[true];
end;

function TCustomUMLCToggledSpeedButton.getGlyphUnToggled: TBitmap;
begin
  Result := FGlyphs[false];
end;

function TCustomUMLCToggledSpeedButton.getCaptionToggled: string;
begin
  Result := FCaptions[true];
end;

function TCustomUMLCToggledSpeedButton.getCaptionUnToggled: string;
begin
  Result := FCaptions[false];
end;

procedure TCustomUMLCToggledSpeedButton.setToggled(const Value: Boolean);
begin
  if (FToggled <> Value) then
  begin
    FToggled := Value;
    RefreshButton();
  end;
end;

procedure TCustomUMLCToggledSpeedButton.RefreshButton();
begin
  Glyph.Assign(FGlyphs[FToggled]);
  Caption := FCaptions[FToggled];
end;

procedure TCustomUMLCToggledSpeedButton.setCaptionToggled(const Value: string);
begin
  if (FCaptions[true] <> Value) then
  begin
    FCaptions[true] := Value;
    RefreshButton();
  end;
end;

procedure TCustomUMLCToggledSpeedButton.setCaptionUnToggled(const Value: string);
begin
  if (FCaptions[false] <> Value) then
  begin
    FCaptions[false] := Value;
    RefreshButton();
  end;
end;

procedure TCustomUMLCToggledSpeedButton.setGlyphToggled(const Value: TBitmap);
begin
  if (FGlyphs[true] <> Value) then
  begin
    FGlyphs[true].Assign(Value);
    RefreshButton();
  end;
end;

procedure TCustomUMLCToggledSpeedButton.setGlyphUnToggled(const Value: TBitmap);
begin
  if (FGlyphs[false] <> Value) then
  begin
    FGlyphs[false].Assign(Value);
    RefreshButton();
  end;
end;

procedure TCustomUMLCToggledSpeedButton.Click;
begin
  Toggled := not Toggled;
  inherited Click;
end;

constructor TCustomUMLCToggledSpeedButton.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FToggled := false;

  FGlyphs[false] := TBitmap.Create;
  FGlyphs[true]  := TBitmap.Create;

  FCaptions[false] := 'UnToggled';
  FCaptions[true]  := 'Toggled';
end;

destructor TCustomUMLCToggledSpeedButton.Destroy;
begin
  FCaptions[true]  := '';
  FCaptions[false] := '';

  inherited Destroy;
end;

procedure TCustomUMLCToggledSpeedButton.Activated;
begin
  RefreshButton();
end;

end.
