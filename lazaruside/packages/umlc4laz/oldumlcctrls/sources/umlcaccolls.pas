unit umlcaccolls;

interface
uses
  Classes, 
  ActnList,
  dummy;

type

{ TUMLCActionItem }

  TUMLCActionItem =
    class(TCollectionItem)
  private
    { Private declarations }
  protected
    { Protected declarations }

    FAction: TBasicAction;
  public
    { Public declarations }
  published
    { Published declarations }

    property Action: TBasicAction
      read FAction write FAction;
  end;

{ TUMLCActionCollection }

  TUMLCActionCollection =
    class(TOwnedCollection)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Friend declarations }
  end;

implementation

end.
