readme.txt
==========

The "umlcfctrls" folder contains the UMLCat set of libraries for the FreePascal &
Lazarus Programming Framework.

This package contains several visual controls, with the visual feature of changing color,
while focused and switching back while not focused.

