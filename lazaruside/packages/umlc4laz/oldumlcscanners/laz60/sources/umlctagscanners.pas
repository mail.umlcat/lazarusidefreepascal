(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlcTagScanners;

interface
uses
  SysUtils, Classes,
  umlcStreams,
  umlcScannerStates, umlcScanners,
  dummy;

type

(* TCustomUMLCTagScanner *)

  TCustomUMLCTagScanner = class(TCustomUMLCScanner)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    constructor Create(AOwner: TComponent); override;
  end;

implementation

(* TCustomUMLCTagScanner *)

constructor TCustomUMLCTagScanner.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
//  FInternalOptions := tagopDefaultOptions;
end;

end.
