(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctagclasses;

interface
uses
  Messages, Windows, SysUtils, Classes, Math, Graphics,
  umlcTagProps,
  dummy;

const
  EoLn = #13#10;

type

(* TUMLCTagMode *)

  TUMLCTagMode = (tgSingle, tgBlock);

(* TUMLCTagPropertiesEditorProc *)

  TUMLCTagObject = class; // forward;

  TUMLCTagPropertiesEditorFunc =
    (*@*)function (const Value: TUMLCTagObject): Boolean;

(* TUMLCTagObject *)

  TUMLCTagObject = class(TObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FKeyword: string;
    FEditor: TUMLCTagPropertiesEditorFunc;

    FProperties: TUMLCTagProperties;
  public
    (* public declarations *)

    constructor Create; virtual;
    destructor Destroy; override;

    function HasEditor: Boolean;

    function IsSystem: Boolean; dynamic; abstract;
    function TagMode: TUMLCTagMode; dynamic; abstract;
    function TagCaption: string; dynamic; abstract;

    property Editor: TUMLCTagPropertiesEditorFunc
      read FEditor write FEditor;
    property Keyword: string
      read FKeyword write FKeyword;
    property Properties: TUMLCTagProperties
      read FProperties write FProperties;
  end;

(* TUMLCSingleTagObject *)

  TUMLCSingleTagObject = class(TUMLCTagObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function TagMode: TUMLCTagMode; override;

    function ObjectToHTML: string; dynamic; abstract;
    function ObjectToEHTML: string; dynamic; abstract;
  end;

(* TUMLCBlockTagObject *)

  TUMLCBlockTagObject = class(TUMLCTagObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function TagMode: TUMLCTagMode; override;

    function ObjectToHTMLStart: string; dynamic; abstract;
    function ObjectToHTMLFinish: string; dynamic; abstract;

    function ObjectToEHTMLStart: string; dynamic; abstract;
    function ObjectToEHTMLFinish: string; dynamic; abstract;
  end;

(* TUMLCEntityTagObject *)

  TUMLCEntityTagObject = class(TUMLCSingleTagObject)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    function IsSystem: Boolean; override;
    function TagMode: TUMLCTagMode; override;
  end;

implementation

(* TUMLCTagObject *)

constructor TUMLCTagObject.Create;
begin
  inherited;
  FEditor := nil;
  FProperties := TUMLCTagProperties.Create(TUMLCTagProperty);
end;

destructor TUMLCTagObject.Destroy;
begin
  FProperties.Free;
  FEditor := nil;
  inherited;
end;

function TUMLCTagObject.HasEditor: Boolean;
begin
  Result := Assigned(FEditor);
end;

(* TUMLCSingleTagObject *)

function TUMLCSingleTagObject.TagMode: TUMLCTagMode;
begin
  Result := tgSingle;
end;

(* TUMLCBlockTagObject *)

function TUMLCBlockTagObject.TagMode: TUMLCTagMode;
begin
  Result := tgBlock;
end;

(* TUMLCEntityTagObject *)

function TUMLCEntityTagObject.IsSystem: Boolean;
begin
  Result := FALSE;
end;

function TUMLCEntityTagObject.TagMode: TUMLCTagMode;
begin
  Result := tgSingle;
end;

end.
