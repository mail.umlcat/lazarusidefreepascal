(**
 **************************************************************************
 *                                                                        *
 *  This file is part of the UMLCat's Component Library.                  *
 *                                                                        *
 *  See the file COPYING.modifiedLGPL.txt, included in this distribution, *
 *  for details about the copyright.                                      *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  *
 *                                                                        *
 **************************************************************************
**)

unit umlctagdictionaries;

interface
uses
  SysUtils, Classes,
  dummy;

type

(* TUMLCTagExecuteEvent *)

  TUMLCTagDictionaryItem = class;

  TUMLCTagExecuteEvent =
     procedure
  (*^*)(TagDictionaryItem: TUMLCTagDictionaryItem; const Param: pointer) of object;

(* TUMLCTagDictionaryItem *)

  TCustomUMLCTagDictionary = class;

  TUMLCTagDictionaryItem = class(TCollectionItem)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FKeyword: string;
//    FStyle: TUMLCTagStyle;
    FIsSystem: Boolean;

    FOnSingle: TUMLCTagExecuteEvent;
    FOnStart:  TUMLCTagExecuteEvent;
    FOnFinish: TUMLCTagExecuteEvent;

    procedure DelegateOnSingle(const Param: pointer);
    procedure DelegateOnStart(const Param: pointer);
    procedure DelegateOnFinish(const Param: pointer);
  public
    (* public declarations *)

    constructor Create(ACollection: TCollection); override;

    procedure ExecuteSingle(const Param: pointer); dynamic;
    procedure ExecuteStart(const Param: pointer); dynamic;
    procedure ExecuteFinish(const Param: pointer); dynamic;
  published
    (* published declarations *)

    property IsSystem: Boolean
      read FIsSystem write FIsSystem;
    property Keyword: string
      read FKeyword write FKeyword;

    property OnSingle: TUMLCTagExecuteEvent
      read FOnSingle write FOnSingle;
    property OnStart: TUMLCTagExecuteEvent
      read FOnStart write FOnStart;
    property OnFinish: TUMLCTagExecuteEvent
      read FOnFinish write FOnFinish;
  end;

(* TUMLCTagDictionaryCollection *)

  TUMLCTagDictionaryCollection = class(TCollection)
  private
    (* private declarations *)
  protected
    (* protected declarations *)
  public
    (* public declarations *)

    constructor Create(AOwner: TPersistent; AItemClass: TCollectionItemClass);
    destructor Destroy(); override;
  published
    (* published declarations *)
  end;

  TItemNotFoundEvent =
    procedure (Sender: TObject; const Keyword: string) of object;

(* TCustomUMLCTagDictionary *)

  TCustomUMLCTagDictionary = class(TComponent)
  private
    (* private declarations *)
  protected
    (* protected declarations *)

    FItems: TUMLCTagDictionaryCollection;

    FOnItemNotFound:   TItemNotFoundEvent;

    procedure DelegateOnItemNotFound(const Keyword: string);

    procedure ItemNotFound(const Keyword: string); dynamic;
  public
    (* public declarations *)

    function ItemByKeyword(const Keyword: string): TUMLCTagDictionaryItem;

    function RegisterKeyword
      (const Keyword: string; IsSystem: Boolean): TUMLCTagDictionaryItem;

    procedure ExecuteSingle(const Keyword: string; const Param: pointer);
    procedure ExecuteStart(const Keyword: string; const Param: pointer);
    procedure ExecuteFinish(const Keyword: string; const Param: pointer);

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property Items: TUMLCTagDictionaryCollection
      read FItems write FItems;

    property OnItemNotFound: TItemNotFoundEvent
      read FOnItemNotFound write FOnItemNotFound;
  end;

(* TUMLCTagDictionary *)

  TUMLCTagDictionary = class(TCustomUMLCTagDictionary)
  published
    (* published declarations *)

    (* TCustomUMLCTagDictionary : *)

    property Items;

    property OnItemNotFound;
  end;

implementation

(* TUMLCTagDictionaryItem *)

procedure TUMLCTagDictionaryItem.DelegateOnSingle(const Param: pointer);
begin
  if Assigned(FOnSingle) then FOnSingle(Self, Param);
end;

procedure TUMLCTagDictionaryItem.DelegateOnStart(const Param: pointer);
begin
  if Assigned(FOnStart) then FOnStart(Self, Param);
end;

procedure TUMLCTagDictionaryItem.DelegateOnFinish(const Param: pointer);
begin
  if Assigned(FOnFinish) then FOnFinish(Self, Param);
end;

constructor TUMLCTagDictionaryItem.Create(ACollection: TCollection);
begin
  inherited Create(ACollection);
  FKeyword := '';

  FOnSingle := nil;
  FOnStart  := nil;
  FOnFinish := nil;
end;

procedure TUMLCTagDictionaryItem.ExecuteSingle(const Param: pointer);
begin
  DelegateOnSingle(Param);
end;

procedure TUMLCTagDictionaryItem.ExecuteStart(const Param: pointer);
begin
  DelegateOnStart(Param);
end;

procedure TUMLCTagDictionaryItem.ExecuteFinish(const Param: pointer);
begin
  DelegateOnFinish(Param);
end;

(* TUMLCTagDictionaryCollection *)

constructor TUMLCTagDictionaryCollection.Create(AOwner: TPersistent; AItemClass: TCollectionItemClass);
begin
  inherited Create(AItemClass);
  (*Your Code...*)
end;

destructor TUMLCTagDictionaryCollection.Destroy();
begin
  (*Your Code...*)
  inherited Destroy();
end;

(* TCustomUMLCTagDictionary *)

procedure TCustomUMLCTagDictionary.DelegateOnItemNotFound
  (const Keyword: string);
begin
  if Assigned(FOnItemNotFound) then FOnItemNotFound(Self, Keyword);
end;

procedure TCustomUMLCTagDictionary.ItemNotFound(const Keyword: string);
begin
  DelegateOnItemNotFound(Keyword);
end;

function TCustomUMLCTagDictionary.
  ItemByKeyword(const Keyword: string): TUMLCTagDictionaryItem;
var Found: Boolean; Index: Integer;
begin
  Index := 0; Found := FALSE; Result := nil;
  while ((Index < Items.Count) and (not Found)) do
  begin
    Result := (Items.Items[Index] as TUMLCTagDictionaryItem);
    Found  := ANSISameText(Result.Keyword, Keyword);
    System.Inc(Index);
  end;
  if not Found
    then Result := nil;
end;

function TCustomUMLCTagDictionary.
  RegisterKeyword(const Keyword: string; IsSystem: Boolean): TUMLCTagDictionaryItem;
begin
  Result := (Items.Add as TUMLCTagDictionaryItem);
  Result.Keyword := Keyword;
  Result.IsSystem := IsSystem;
end;

procedure TCustomUMLCTagDictionary.ExecuteSingle
  (const Keyword: string; const Param: pointer);
var Item: TUMLCTagDictionaryItem;
begin
  Item := ItemByKeyword(Keyword);
  if (Assigned(Item))
    then Item.ExecuteSingle(Param)
    else ItemNotFound(Keyword);
end;

procedure TCustomUMLCTagDictionary.ExecuteStart
 (const Keyword: string; const Param: pointer);
var Item: TUMLCTagDictionaryItem;
begin
  Item := ItemByKeyword(Keyword);
  if (Assigned(Item))
    then Item.ExecuteStart(Param)
    else ItemNotFound(Keyword);
end;

procedure TCustomUMLCTagDictionary.ExecuteFinish
 (const Keyword: string; const Param: pointer);
var Item: TUMLCTagDictionaryItem;
begin
  Item := ItemByKeyword(Keyword);
  if (Assigned(Item))
    then Item.ExecuteFinish(Param)
    else ItemNotFound(Keyword);
end;

constructor TCustomUMLCTagDictionary.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FItems := TUMLCTagDictionaryCollection.Create(Self, TUMLCTagDictionaryItem);
  (*Your Code...*)
end;

destructor TCustomUMLCTagDictionary.Destroy;
begin
  (*Your Code...*)
  FItems.Free;
  inherited Destroy;
end;

end.
 
